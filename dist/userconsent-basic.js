(function () {
    'use strict';

    var CmpStatus;
    (function (CmpStatus) {
        CmpStatus["STUB"] = "stub";
        CmpStatus["LOADING"] = "loading";
        CmpStatus["LOADED"] = "loaded";
        CmpStatus["ERROR"] = "error";
    })(CmpStatus || (CmpStatus = {}));

    var CmpDisplayStatus;
    (function (CmpDisplayStatus) {
        CmpDisplayStatus["VISIBLE"] = "visible";
        CmpDisplayStatus["HIDDEN"] = "hidden";
        CmpDisplayStatus["DISABLED"] = "disabled";
    })(CmpDisplayStatus || (CmpDisplayStatus = {}));

    class EventData {
        constructor(eventName, listenerId, data, pingData) {
            this.eventName = eventName;
            this.listenerId = listenerId;
            this.data = data;
            this.pingData = pingData;
        }
    }

    class PingData {
        constructor(cmpApiContext) {
            this.gppVersion = cmpApiContext.gppVersion;
            this.cmpStatus = cmpApiContext.cmpStatus;
            this.cmpDisplayStatus = cmpApiContext.cmpDisplayStatus;
            this.signalStatus = cmpApiContext.signalStatus;
            this.supportedAPIs = cmpApiContext.supportedAPIs;
            this.cmpId = cmpApiContext.cmpId;
            this.sectionList = cmpApiContext.gppModel.getSectionIds();
            this.applicableSections = cmpApiContext.applicableSections;
            this.gppString = cmpApiContext.gppModel.encode();
            this.parsedSections = cmpApiContext.gppModel.toObject();
        }
    }

    class EventListenerQueue {
        constructor(cmpApiContext) {
            this.eventQueue = new Map();
            this.queueNumber = 1000;
            this.cmpApiContext = cmpApiContext;
            try {
                // get queued commands
                let events = window["__gpp"]("events") || [];
                for (var i = 0; i < events.length; i++) {
                    let eventItem = events[i];
                    this.eventQueue.set(eventItem.id, {
                        callback: eventItem.callback,
                        parameter: eventItem.parameter,
                    });
                }
            }
            catch (err) {
                console.log(err);
            }
        }
        add(eventItem) {
            this.eventQueue.set(this.queueNumber, eventItem);
            return this.queueNumber++;
        }
        get(listenerId) {
            return this.eventQueue.get(listenerId);
        }
        remove(listenerId) {
            return this.eventQueue.delete(listenerId);
        }
        exec(eventName, data) {
            this.eventQueue.forEach((eventItem, listenerId) => {
                let eventData = new EventData(eventName, listenerId, data, new PingData(this.cmpApiContext));
                let success = true;
                eventItem.callback(eventData, success);
            });
        }
        clear() {
            this.queueNumber = 1000;
            this.eventQueue.clear();
        }
        get size() {
            return this.eventQueue.size;
        }
        events() {
            let resp = [];
            this.eventQueue.forEach((e, listenerId) => {
                resp.push({ id: listenerId, callback: e.callback, parameter: e.parameter });
            });
            return resp;
        }
    }

    /**
     * class for decoding errors
     *
     * @extends {Error}
     */
    class InvalidFieldError extends Error {
        /**
         * constructor - constructs an DecodingError
         *
         * @param {string} msg - Decoding Error Message
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "InvalidFieldError";
        }
    }

    class AbstractLazilyEncodableSection {
        constructor() {
            this.encodedString = null;
            this.dirty = false;
            this.decoded = true;
            this.segments = this.initializeSegments();
        }
        hasField(fieldName) {
            if (!this.decoded) {
                this.segments = this.decodeSection(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            for (let i = 0; i < this.segments.length; i++) {
                let segment = this.segments[i];
                if (segment.getFieldNames().includes(fieldName)) {
                    return segment.hasField(fieldName);
                }
            }
            return false;
        }
        getFieldValue(fieldName) {
            if (!this.decoded) {
                this.segments = this.decodeSection(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            for (let i = 0; i < this.segments.length; i++) {
                let segment = this.segments[i];
                if (segment.hasField(fieldName)) {
                    return segment.getFieldValue(fieldName);
                }
            }
            throw new InvalidFieldError("Invalid field: '" + fieldName + "'");
        }
        setFieldValue(fieldName, value) {
            if (!this.decoded) {
                this.segments = this.decodeSection(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            for (let i = 0; i < this.segments.length; i++) {
                let segment = this.segments[i];
                if (segment.hasField(fieldName)) {
                    segment.setFieldValue(fieldName, value);
                    return;
                }
            }
            throw new InvalidFieldError("Invalid field: '" + fieldName + "'");
        }
        //Overriden
        toObj() {
            let obj = {};
            for (let i = 0; i < this.segments.length; i++) {
                let segmentObject = this.segments[i].toObj();
                for (const [fieldName, value] of Object.entries(segmentObject)) {
                    obj[fieldName] = value;
                }
            }
            return obj;
        }
        encode() {
            if (this.encodedString == null || this.encodedString.length === 0 || this.dirty) {
                this.encodedString = this.encodeSection(this.segments);
                this.dirty = false;
                this.decoded = true;
            }
            return this.encodedString;
        }
        decode(encodedString) {
            this.encodedString = encodedString;
            this.dirty = false;
            this.decoded = false;
        }
    }

    /**
     * class for decoding errors
     *
     * @extends {Error}
     */
    class DecodingError extends Error {
        /**
         * constructor - constructs an DecodingError
         *
         * @param {string} msg - Decoding Error Message
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "DecodingError";
        }
    }

    /**
     * class for encoding errors
     *
     * @extends {Error}
     */
    class EncodingError extends Error {
        /**
         * constructor - constructs an EncodingError
         *
         * @param {string} msg - Encoding Error Message
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "EncodingError";
        }
    }

    class FixedIntegerEncoder {
        static encode(value, bitStringLength) {
            //let bitString = value.toString(2);
            let bin = [];
            if (value >= 1) {
                bin.push(1);
                while (value >= bin[0] * 2) {
                    bin.unshift(bin[0] * 2);
                }
            }
            let bitString = "";
            for (let i = 0; i < bin.length; i++) {
                let b = bin[i];
                if (value >= b) {
                    bitString += "1";
                    value -= b;
                }
                else {
                    bitString += "0";
                }
            }
            if (bitString.length > bitStringLength) {
                throw new EncodingError("Numeric value '" + value + "' is too large for a bit string length of '" + bitStringLength + "'");
            }
            while (bitString.length < bitStringLength) {
                bitString = "0" + bitString;
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString)) {
                throw new DecodingError("Undecodable FixedInteger '" + bitString + "'");
            }
            //return parseInt(bitString, 2);
            let value = 0;
            let bin = [];
            for (let i = 0; i < bitString.length; i++) {
                if (i === 0) {
                    bin[bitString.length - (i + 1)] = 1;
                }
                else {
                    bin[bitString.length - (i + 1)] = bin[bitString.length - i] * 2;
                }
            }
            for (let i = 0; i < bitString.length; i++) {
                if (bitString.charAt(i) === "1") {
                    value += bin[i];
                }
            }
            return value;
        }
    }

    class AbstractBase64UrlEncoder {
        encode(bitString) {
            // should only be 0 or 1
            if (!/^[0-1]*$/.test(bitString)) {
                throw new EncodingError("Unencodable Base64Url '" + bitString + "'");
            }
            bitString = this.pad(bitString);
            let str = "";
            let index = 0;
            while (index <= bitString.length - 6) {
                let s = bitString.substring(index, index + 6);
                try {
                    let n = FixedIntegerEncoder.decode(s);
                    let c = AbstractBase64UrlEncoder.DICT.charAt(n);
                    str += c;
                    index += 6;
                }
                catch (e) {
                    throw new EncodingError("Unencodable Base64Url '" + bitString + "'");
                }
            }
            return str;
        }
        decode(str) {
            // should contain only characters from the base64url set
            if (!/^[A-Za-z0-9\-_]*$/.test(str)) {
                throw new DecodingError("Undecodable Base64URL string '" + str + "'");
            }
            let bitString = "";
            for (let i = 0; i < str.length; i++) {
                let c = str.charAt(i);
                let n = AbstractBase64UrlEncoder.REVERSE_DICT.get(c);
                let s = FixedIntegerEncoder.encode(n, 6);
                bitString += s;
            }
            return bitString;
        }
    }
    /**
     * Base 64 URL character set.  Different from standard Base64 char set
     * in that '+' and '/' are replaced with '-' and '_'.
     */
    AbstractBase64UrlEncoder.DICT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
    // prettier-ignore
    AbstractBase64UrlEncoder.REVERSE_DICT = new Map([
        ['A', 0], ['B', 1], ['C', 2], ['D', 3], ['E', 4], ['F', 5], ['G', 6], ['H', 7],
        ['I', 8], ['J', 9], ['K', 10], ['L', 11], ['M', 12], ['N', 13], ['O', 14], ['P', 15],
        ['Q', 16], ['R', 17], ['S', 18], ['T', 19], ['U', 20], ['V', 21], ['W', 22], ['X', 23],
        ['Y', 24], ['Z', 25], ['a', 26], ['b', 27], ['c', 28], ['d', 29], ['e', 30], ['f', 31],
        ['g', 32], ['h', 33], ['i', 34], ['j', 35], ['k', 36], ['l', 37], ['m', 38], ['n', 39],
        ['o', 40], ['p', 41], ['q', 42], ['r', 43], ['s', 44], ['t', 45], ['u', 46], ['v', 47],
        ['w', 48], ['x', 49], ['y', 50], ['z', 51], ['0', 52], ['1', 53], ['2', 54], ['3', 55],
        ['4', 56], ['5', 57], ['6', 58], ['7', 59], ['8', 60], ['9', 61], ['-', 62], ['_', 63]
    ]);

    class CompressedBase64UrlEncoder extends AbstractBase64UrlEncoder {
        constructor() {
            super();
        }
        static getInstance() {
            return this.instance;
        }
        // Overriden
        pad(bitString) {
            while (bitString.length % 8 > 0) {
                bitString += "0";
            }
            while (bitString.length % 6 > 0) {
                bitString += "0";
            }
            return bitString;
        }
    }
    CompressedBase64UrlEncoder.instance = new CompressedBase64UrlEncoder();

    class BitStringEncoder {
        constructor() { }
        static getInstance() {
            return this.instance;
        }
        encode(fields, fieldNames) {
            let bitString = "";
            for (let i = 0; i < fieldNames.length; i++) {
                let fieldName = fieldNames[i];
                if (fields.containsKey(fieldName)) {
                    let field = fields.get(fieldName);
                    bitString += field.encode();
                }
                else {
                    throw new Error("Field not found: '" + fieldName + "'");
                }
            }
            return bitString;
        }
        decode(bitString, fieldNames, fields) {
            let index = 0;
            for (let i = 0; i < fieldNames.length; i++) {
                let fieldName = fieldNames[i];
                if (fields.containsKey(fieldName)) {
                    let field = fields.get(fieldName);
                    try {
                        let substring = field.substring(bitString, index);
                        field.decode(substring);
                        index += substring.length;
                    }
                    catch (e) {
                        if (e.name === "SubstringError" && !field.getHardFailIfMissing()) {
                            return;
                        }
                        else {
                            throw new DecodingError("Unable to decode field '" + fieldName + "'");
                        }
                    }
                }
                else {
                    throw new Error("Field not found: '" + fieldName + "'");
                }
            }
        }
    }
    BitStringEncoder.instance = new BitStringEncoder();

    class FibonacciIntegerEncoder {
        static encode(value) {
            let fib = [];
            if (value >= 1) {
                fib.push(1);
                if (value >= 2) {
                    fib.push(2);
                    let i = 2;
                    while (value >= fib[i - 1] + fib[i - 2]) {
                        fib.push(fib[i - 1] + fib[i - 2]);
                        i++;
                    }
                }
            }
            let bitString = "1";
            for (let i = fib.length - 1; i >= 0; i--) {
                let f = fib[i];
                if (value >= f) {
                    bitString = "1" + bitString;
                    value -= f;
                }
                else {
                    bitString = "0" + bitString;
                }
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString) || bitString.length < 2 || bitString.indexOf("11") !== bitString.length - 2) {
                throw new DecodingError("Undecodable FibonacciInteger '" + bitString + "'");
            }
            let value = 0;
            let fib = [];
            for (let i = 0; i < bitString.length - 1; i++) {
                if (i === 0) {
                    fib.push(1);
                }
                else if (i === 1) {
                    fib.push(2);
                }
                else {
                    fib.push(fib[i - 1] + fib[i - 2]);
                }
            }
            for (let i = 0; i < bitString.length - 1; i++) {
                if (bitString.charAt(i) === "1") {
                    value += fib[i];
                }
            }
            return value;
        }
    }

    class BooleanEncoder {
        static encode(value) {
            if (value === true) {
                return "1";
            }
            else if (value === false) {
                return "0";
            }
            else {
                throw new EncodingError("Unencodable Boolean '" + value + "'");
            }
        }
        static decode(bitString) {
            if (bitString === "1") {
                return true;
            }
            else if (bitString === "0") {
                return false;
            }
            else {
                throw new DecodingError("Undecodable Boolean '" + bitString + "'");
            }
        }
    }

    class FibonacciIntegerRangeEncoder {
        static encode(value) {
            value = value.sort((n1, n2) => n1 - n2);
            let groups = [];
            let offset = 0;
            let groupStartIndex = 0;
            while (groupStartIndex < value.length) {
                let groupEndIndex = groupStartIndex;
                while (groupEndIndex < value.length - 1 && value[groupEndIndex] + 1 === value[groupEndIndex + 1]) {
                    groupEndIndex++;
                }
                groups.push(value.slice(groupStartIndex, groupEndIndex + 1));
                groupStartIndex = groupEndIndex + 1;
            }
            let bitString = FixedIntegerEncoder.encode(groups.length, 12);
            for (let i = 0; i < groups.length; i++) {
                if (groups[i].length == 1) {
                    let v = groups[i][0] - offset;
                    offset = groups[i][0];
                    bitString += "0" + FibonacciIntegerEncoder.encode(v);
                }
                else {
                    let startVal = groups[i][0] - offset;
                    offset = groups[i][0];
                    let endVal = groups[i][groups[i].length - 1] - offset;
                    offset = groups[i][groups[i].length - 1];
                    bitString += "1" + FibonacciIntegerEncoder.encode(startVal) + FibonacciIntegerEncoder.encode(endVal);
                }
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString) || bitString.length < 12) {
                throw new DecodingError("Undecodable FibonacciIntegerRange '" + bitString + "'");
            }
            let value = [];
            let count = FixedIntegerEncoder.decode(bitString.substring(0, 12));
            let offset = 0;
            let startIndex = 12;
            for (let i = 0; i < count; i++) {
                let group = BooleanEncoder.decode(bitString.substring(startIndex, startIndex + 1));
                startIndex++;
                if (group === true) {
                    let index = bitString.indexOf("11", startIndex);
                    let start = FibonacciIntegerEncoder.decode(bitString.substring(startIndex, index + 2)) + offset;
                    offset = start;
                    startIndex = index + 2;
                    index = bitString.indexOf("11", startIndex);
                    let end = FibonacciIntegerEncoder.decode(bitString.substring(startIndex, index + 2)) + offset;
                    offset = end;
                    startIndex = index + 2;
                    for (let j = start; j <= end; j++) {
                        value.push(j);
                    }
                }
                else {
                    let index = bitString.indexOf("11", startIndex);
                    let val = FibonacciIntegerEncoder.decode(bitString.substring(startIndex, index + 2)) + offset;
                    offset = val;
                    value.push(val);
                    startIndex = index + 2;
                }
            }
            return value;
        }
    }

    /**
     * class for decoding errors
     *
     * @extends {Error}
     */
    class ValidationError extends Error {
        /**
         * constructor - constructs an DecodingError
         *
         * @param {string} msg - Decoding Error Message
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "ValidationError";
        }
    }

    class AbstractEncodableBitStringDataType {
        constructor(hardFailIfMising = true) {
            this.hardFailIfMissing = hardFailIfMising;
        }
        withValidator(validator) {
            this.validator = validator;
            return this;
        }
        hasValue() {
            return this.value !== undefined && this.value !== null;
        }
        getValue() {
            return this.value;
        }
        setValue(value) {
            if (!this.validator || this.validator.test(value)) {
                this.value = value;
            }
            else {
                throw new ValidationError("Invalid value '" + value + "'");
            }
        }
        getHardFailIfMissing() {
            return this.hardFailIfMissing;
        }
    }

    /**
     * class for decoding errors
     *
     * @extends {DecodingError}
     */
    class SubstringError extends DecodingError {
        /**
         * constructor - constructs an DecodingError
         *
         * @param {string} msg - Decoding Error Message
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "SubstringError";
        }
    }

    class StringUtil {
        /**
         * Throws a SubstringError if the indexes aren't within the length of the string
         */
        static substring(s, start, end) {
            if (end > s.length || start < 0 || start > end) {
                throw new SubstringError("Invalid substring indexes " + start + ":" + end + " for string of length " + s.length);
            }
            return s.substring(start, end);
        }
    }

    class EncodableFibonacciIntegerRange extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.setValue(value);
        }
        encode() {
            try {
                return FibonacciIntegerRangeEncoder.encode(this.value);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FibonacciIntegerRangeEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                //TODO: add some validation
                let count = FixedIntegerEncoder.decode(StringUtil.substring(bitString, fromIndex, fromIndex + 12));
                let index = fromIndex + 12;
                for (let i = 0; i < count; i++) {
                    if (bitString.charAt(index) === "1") {
                        index = bitString.indexOf("11", bitString.indexOf("11", index + 1) + 2) + 2;
                    }
                    else {
                        index = bitString.indexOf("11", index + 1) + 2;
                    }
                }
                return StringUtil.substring(bitString, fromIndex, index);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            super.setValue(Array.from(new Set(value)).sort((n1, n2) => n1 - n2));
        }
    }

    class EncodableFixedInteger extends AbstractEncodableBitStringDataType {
        constructor(bitStringLength, value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.bitStringLength = bitStringLength;
            this.setValue(value);
        }
        encode() {
            try {
                return FixedIntegerEncoder.encode(this.value, this.bitStringLength);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedIntegerEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + this.bitStringLength);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
    }

    class EncodableBitStringFields {
        constructor() {
            this.fields = new Map();
        }
        containsKey(key) {
            return this.fields.has(key);
        }
        put(key, value) {
            this.fields.set(key, value);
        }
        get(key) {
            return this.fields.get(key);
        }
        getAll() {
            return new Map(this.fields);
        }
        reset(fields) {
            this.fields.clear();
            fields.getAll().forEach((value, key) => {
                this.fields.set(key, value);
            });
        }
    }

    var HeaderV1Field;
    (function (HeaderV1Field) {
        HeaderV1Field["ID"] = "Id";
        HeaderV1Field["VERSION"] = "Version";
        HeaderV1Field["SECTION_IDS"] = "SectionIds";
    })(HeaderV1Field || (HeaderV1Field = {}));
    const HEADER_CORE_SEGMENT_FIELD_NAMES = [HeaderV1Field.ID, HeaderV1Field.VERSION, HeaderV1Field.SECTION_IDS];

    class AbstractLazilyEncodableSegment {
        constructor() {
            this.encodedString = null;
            this.dirty = false;
            this.decoded = true;
            this.fields = this.initializeFields();
        }
        //Overriden
        validate() { }
        hasField(fieldName) {
            return this.fields.containsKey(fieldName);
        }
        getFieldValue(fieldName) {
            if (!this.decoded) {
                this.decodeSegment(this.encodedString, this.fields);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.fields.containsKey(fieldName)) {
                return this.fields.get(fieldName).getValue();
            }
            else {
                throw new InvalidFieldError("Invalid field: '" + fieldName + "'");
            }
        }
        setFieldValue(fieldName, value) {
            if (!this.decoded) {
                this.decodeSegment(this.encodedString, this.fields);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.fields.containsKey(fieldName)) {
                this.fields.get(fieldName).setValue(value);
                this.dirty = true;
            }
            else {
                throw new InvalidFieldError(fieldName + " not found");
            }
        }
        //Overriden
        toObj() {
            let obj = {};
            let fieldNames = this.getFieldNames();
            for (let i = 0; i < fieldNames.length; i++) {
                let fieldName = fieldNames[i];
                let value = this.getFieldValue(fieldName);
                obj[fieldName] = value;
            }
            return obj;
        }
        encode() {
            if (this.encodedString == null || this.encodedString.length === 0 || this.dirty) {
                this.validate();
                this.encodedString = this.encodeSegment(this.fields);
                this.dirty = false;
                this.decoded = true;
            }
            return this.encodedString;
        }
        decode(encodedString) {
            this.encodedString = encodedString;
            this.dirty = false;
            this.decoded = false;
        }
    }

    class HeaderV1CoreSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = CompressedBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return HEADER_CORE_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(HeaderV1Field.ID.toString(), new EncodableFixedInteger(6, HeaderV1.ID));
            fields.put(HeaderV1Field.VERSION.toString(), new EncodableFixedInteger(6, HeaderV1.VERSION));
            fields.put(HeaderV1Field.SECTION_IDS.toString(), new EncodableFibonacciIntegerRange([]));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode HeaderV1CoreSegment '" + encodedString + "'");
            }
        }
    }

    class HeaderV1 extends AbstractLazilyEncodableSection {
        constructor(encodedString) {
            super();
            if (encodedString && encodedString.length > 0) {
                this.decode(encodedString);
            }
        }
        //Overriden
        getId() {
            return HeaderV1.ID;
        }
        //Overriden
        getName() {
            return HeaderV1.NAME;
        }
        //Override
        getVersion() {
            return HeaderV1.VERSION;
        }
        //Overriden
        initializeSegments() {
            let segments = [];
            segments.push(new HeaderV1CoreSegment());
            return segments;
        }
        //Overriden
        decodeSection(encodedString) {
            let segments = this.initializeSegments();
            if (encodedString != null && encodedString.length !== 0) {
                let encodedSegments = encodedString.split(".");
                for (let i = 0; i < segments.length; i++) {
                    if (encodedSegments.length > i) {
                        segments[i].decode(encodedSegments[i]);
                    }
                }
            }
            return segments;
        }
        // Overriden
        encodeSection(segments) {
            let encodedSegments = [];
            for (let i = 0; i < segments.length; i++) {
                let segment = segments[i];
                encodedSegments.push(segment.encode());
            }
            return encodedSegments.join(".");
        }
    }
    HeaderV1.ID = 3;
    HeaderV1.VERSION = 1;
    HeaderV1.NAME = "header";

    var TcfEuV2Field;
    (function (TcfEuV2Field) {
        TcfEuV2Field["VERSION"] = "Version";
        TcfEuV2Field["CREATED"] = "Created";
        TcfEuV2Field["LAST_UPDATED"] = "LastUpdated";
        TcfEuV2Field["CMP_ID"] = "CmpId";
        TcfEuV2Field["CMP_VERSION"] = "CmpVersion";
        TcfEuV2Field["CONSENT_SCREEN"] = "ConsentScreen";
        TcfEuV2Field["CONSENT_LANGUAGE"] = "ConsentLanguage";
        TcfEuV2Field["VENDOR_LIST_VERSION"] = "VendorListVersion";
        TcfEuV2Field["POLICY_VERSION"] = "PolicyVersion";
        TcfEuV2Field["IS_SERVICE_SPECIFIC"] = "IsServiceSpecific";
        TcfEuV2Field["USE_NON_STANDARD_STACKS"] = "UseNonStandardStacks";
        TcfEuV2Field["SPECIAL_FEATURE_OPTINS"] = "SpecialFeatureOptins";
        TcfEuV2Field["PURPOSE_CONSENTS"] = "PurposeConsents";
        TcfEuV2Field["PURPOSE_LEGITIMATE_INTERESTS"] = "PurposeLegitimateInterests";
        TcfEuV2Field["PURPOSE_ONE_TREATMENT"] = "PurposeOneTreatment";
        TcfEuV2Field["PUBLISHER_COUNTRY_CODE"] = "PublisherCountryCode";
        TcfEuV2Field["VENDOR_CONSENTS"] = "VendorConsents";
        TcfEuV2Field["VENDOR_LEGITIMATE_INTERESTS"] = "VendorLegitimateInterests";
        TcfEuV2Field["PUBLISHER_RESTRICTIONS"] = "PublisherRestrictions";
        TcfEuV2Field["PUBLISHER_PURPOSES_SEGMENT_TYPE"] = "PublisherPurposesSegmentType";
        TcfEuV2Field["PUBLISHER_CONSENTS"] = "PublisherConsents";
        TcfEuV2Field["PUBLISHER_LEGITIMATE_INTERESTS"] = "PublisherLegitimateInterests";
        TcfEuV2Field["NUM_CUSTOM_PURPOSES"] = "NumCustomPurposes";
        TcfEuV2Field["PUBLISHER_CUSTOM_CONSENTS"] = "PublisherCustomConsents";
        TcfEuV2Field["PUBLISHER_CUSTOM_LEGITIMATE_INTERESTS"] = "PublisherCustomLegitimateInterests";
        TcfEuV2Field["VENDORS_ALLOWED_SEGMENT_TYPE"] = "VendorsAllowedSegmentType";
        TcfEuV2Field["VENDORS_ALLOWED"] = "VendorsAllowed";
        TcfEuV2Field["VENDORS_DISCLOSED_SEGMENT_TYPE"] = "VendorsDisclosedSegmentType";
        TcfEuV2Field["VENDORS_DISCLOSED"] = "VendorsDisclosed";
    })(TcfEuV2Field || (TcfEuV2Field = {}));
    const TCFEUV2_CORE_SEGMENT_FIELD_NAMES = [
        TcfEuV2Field.VERSION,
        TcfEuV2Field.CREATED,
        TcfEuV2Field.LAST_UPDATED,
        TcfEuV2Field.CMP_ID,
        TcfEuV2Field.CMP_VERSION,
        TcfEuV2Field.CONSENT_SCREEN,
        TcfEuV2Field.CONSENT_LANGUAGE,
        TcfEuV2Field.VENDOR_LIST_VERSION,
        TcfEuV2Field.POLICY_VERSION,
        TcfEuV2Field.IS_SERVICE_SPECIFIC,
        TcfEuV2Field.USE_NON_STANDARD_STACKS,
        TcfEuV2Field.SPECIAL_FEATURE_OPTINS,
        TcfEuV2Field.PURPOSE_CONSENTS,
        TcfEuV2Field.PURPOSE_LEGITIMATE_INTERESTS,
        TcfEuV2Field.PURPOSE_ONE_TREATMENT,
        TcfEuV2Field.PUBLISHER_COUNTRY_CODE,
        TcfEuV2Field.VENDOR_CONSENTS,
        TcfEuV2Field.VENDOR_LEGITIMATE_INTERESTS,
        TcfEuV2Field.PUBLISHER_RESTRICTIONS,
    ];
    const TCFEUV2_PUBLISHER_PURPOSES_SEGMENT_FIELD_NAMES = [
        TcfEuV2Field.PUBLISHER_PURPOSES_SEGMENT_TYPE,
        TcfEuV2Field.PUBLISHER_CONSENTS,
        TcfEuV2Field.PUBLISHER_LEGITIMATE_INTERESTS,
        TcfEuV2Field.NUM_CUSTOM_PURPOSES,
        TcfEuV2Field.PUBLISHER_CUSTOM_CONSENTS,
        TcfEuV2Field.PUBLISHER_CUSTOM_LEGITIMATE_INTERESTS,
    ];
    const TCFEUV2_VENDORS_ALLOWED_SEGMENT_FIELD_NAMES = [
        TcfEuV2Field.VENDORS_ALLOWED_SEGMENT_TYPE,
        TcfEuV2Field.VENDORS_ALLOWED,
    ];
    const TCFEUV2_VENDORS_DISCLOSED_SEGMENT_FIELD_NAMES = [
        TcfEuV2Field.VENDORS_DISCLOSED_SEGMENT_TYPE,
        TcfEuV2Field.VENDORS_DISCLOSED,
    ];

    class TraditionalBase64UrlEncoder extends AbstractBase64UrlEncoder {
        constructor() {
            super();
        }
        static getInstance() {
            return this.instance;
        }
        // Overriden
        pad(bitString) {
            while (bitString.length % 24 > 0) {
                bitString += "0";
            }
            return bitString;
        }
    }
    TraditionalBase64UrlEncoder.instance = new TraditionalBase64UrlEncoder();

    class FixedIntegerRangeEncoder {
        static encode(value) {
            value.sort((n1, n2) => n1 - n2);
            let groups = [];
            let groupStartIndex = 0;
            while (groupStartIndex < value.length) {
                let groupEndIndex = groupStartIndex;
                while (groupEndIndex < value.length - 1 && value[groupEndIndex] + 1 === value[groupEndIndex + 1]) {
                    groupEndIndex++;
                }
                groups.push(value.slice(groupStartIndex, groupEndIndex + 1));
                groupStartIndex = groupEndIndex + 1;
            }
            let bitString = FixedIntegerEncoder.encode(groups.length, 12);
            for (let i = 0; i < groups.length; i++) {
                if (groups[i].length === 1) {
                    bitString += "0" + FixedIntegerEncoder.encode(groups[i][0], 16);
                }
                else {
                    bitString +=
                        "1" +
                            FixedIntegerEncoder.encode(groups[i][0], 16) +
                            FixedIntegerEncoder.encode(groups[i][groups[i].length - 1], 16);
                }
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString) || bitString.length < 12) {
                throw new DecodingError("Undecodable FixedIntegerRange '" + bitString + "'");
            }
            let value = [];
            let count = FixedIntegerEncoder.decode(bitString.substring(0, 12));
            let startIndex = 12;
            for (let i = 0; i < count; i++) {
                let group = BooleanEncoder.decode(bitString.substring(startIndex, startIndex + 1));
                startIndex++;
                if (group === true) {
                    let start = FixedIntegerEncoder.decode(bitString.substring(startIndex, startIndex + 16));
                    startIndex += 16;
                    let end = FixedIntegerEncoder.decode(bitString.substring(startIndex, startIndex + 16));
                    startIndex += 16;
                    for (let j = start; j <= end; j++) {
                        value.push(j);
                    }
                }
                else {
                    let val = FixedIntegerEncoder.decode(bitString.substring(startIndex, startIndex + 16));
                    value.push(val);
                    startIndex += 16;
                }
            }
            return value;
        }
    }

    class EncodableFixedIntegerRange extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.setValue(value);
        }
        encode() {
            try {
                return FixedIntegerRangeEncoder.encode(this.value);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedIntegerRangeEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                let count = FixedIntegerEncoder.decode(StringUtil.substring(bitString, fromIndex, fromIndex + 12));
                let index = fromIndex + 12;
                for (let i = 0; i < count; i++) {
                    if (bitString.charAt(index) === "1") {
                        index += 33;
                    }
                    else {
                        index += 17;
                    }
                }
                return StringUtil.substring(bitString, fromIndex, index);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            super.setValue(Array.from(new Set(value)).sort((n1, n2) => n1 - n2));
        }
    }

    class RangeEntry {
        constructor(key, type, ids) {
            this.key = key;
            this.type = type;
            this.ids = ids;
        }
        getKey() {
            return this.key;
        }
        setKey(key) {
            this.key = key;
        }
        getType() {
            return this.type;
        }
        setType(type) {
            this.type = type;
        }
        getIds() {
            return this.ids;
        }
        setIds(ids) {
            this.ids = ids;
        }
    }

    class EncodableArrayOfFixedIntegerRanges extends AbstractEncodableBitStringDataType {
        constructor(keyBitStringLength, typeBitStringLength, value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.keyBitStringLength = keyBitStringLength;
            this.typeBitStringLength = typeBitStringLength;
            this.setValue(value);
        }
        encode() {
            try {
                let entries = this.value;
                let sb = "";
                sb += FixedIntegerEncoder.encode(entries.length, 12);
                for (let i = 0; i < entries.length; i++) {
                    let entry = entries[i];
                    sb += FixedIntegerEncoder.encode(entry.getKey(), this.keyBitStringLength);
                    sb += FixedIntegerEncoder.encode(entry.getType(), this.typeBitStringLength);
                    sb += FixedIntegerRangeEncoder.encode(entry.getIds());
                }
                return sb;
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                let entries = [];
                let size = FixedIntegerEncoder.decode(StringUtil.substring(bitString, 0, 12));
                let index = 12;
                for (let i = 0; i < size; i++) {
                    let key = FixedIntegerEncoder.decode(StringUtil.substring(bitString, index, index + this.keyBitStringLength));
                    index += this.keyBitStringLength;
                    let type = FixedIntegerEncoder.decode(StringUtil.substring(bitString, index, index + this.typeBitStringLength));
                    index += this.typeBitStringLength;
                    let substring = new EncodableFixedIntegerRange([]).substring(bitString, index);
                    let ids = FixedIntegerRangeEncoder.decode(substring);
                    index += substring.length;
                    entries.push(new RangeEntry(key, type, ids));
                }
                this.value = entries;
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                let sb = "";
                sb += StringUtil.substring(bitString, fromIndex, fromIndex + 12);
                let size = FixedIntegerEncoder.decode(sb.toString());
                let index = fromIndex + sb.length;
                for (let i = 0; i < size; i++) {
                    let keySubstring = StringUtil.substring(bitString, index, index + this.keyBitStringLength);
                    index += keySubstring.length;
                    sb += keySubstring;
                    let typeSubstring = StringUtil.substring(bitString, index, index + this.typeBitStringLength);
                    index += typeSubstring.length;
                    sb += typeSubstring;
                    let rangeSubstring = new EncodableFixedIntegerRange([]).substring(bitString, index);
                    index += rangeSubstring.length;
                    sb += rangeSubstring;
                }
                return sb;
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
    }

    class EncodableBoolean extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.setValue(value);
        }
        encode() {
            try {
                return BooleanEncoder.encode(this.value);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = BooleanEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + 1);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
    }

    class DatetimeEncoder {
        static encode(value) {
            if (value) {
                return FixedIntegerEncoder.encode(Math.round(value.getTime() / 100), 36);
            }
            else {
                return FixedIntegerEncoder.encode(0, 36);
            }
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString) || bitString.length !== 36) {
                throw new DecodingError("Undecodable Datetime '" + bitString + "'");
            }
            return new Date(FixedIntegerEncoder.decode(bitString) * 100);
        }
    }

    class EncodableDatetime extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.setValue(value);
        }
        encode() {
            try {
                return DatetimeEncoder.encode(this.value);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = DatetimeEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + 36);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
    }

    class FixedBitfieldEncoder {
        static encode(value, bitStringLength) {
            if (value.length > bitStringLength) {
                throw new EncodingError("Too many values '" + value.length + "'");
            }
            let bitString = "";
            for (let i = 0; i < value.length; i++) {
                bitString += BooleanEncoder.encode(value[i]);
            }
            while (bitString.length < bitStringLength) {
                bitString += "0";
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString)) {
                throw new DecodingError("Undecodable FixedBitfield '" + bitString + "'");
            }
            let value = [];
            for (let i = 0; i < bitString.length; i++) {
                value.push(BooleanEncoder.decode(bitString.substring(i, i + 1)));
            }
            return value;
        }
    }

    class EncodableFixedBitfield extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.numElements = value.length;
            this.setValue(value);
        }
        encode() {
            try {
                return FixedBitfieldEncoder.encode(this.value, this.numElements);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedBitfieldEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + this.numElements);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            let v = [...value];
            for (let i = v.length; i < this.numElements; i++) {
                v.push(false);
            }
            if (v.length > this.numElements) {
                v = v.slice(0, this.numElements);
            }
            super.setValue(v);
        }
    }

    class FixedStringEncoder {
        static encode(value, stringLength) {
            while (value.length < stringLength) {
                value += " ";
            }
            let bitString = "";
            for (let i = 0; i < value.length; i++) {
                let code = value.charCodeAt(i);
                if (code === 32) {
                    // space
                    bitString += FixedIntegerEncoder.encode(63, 6);
                }
                else if (code >= 65) {
                    bitString += FixedIntegerEncoder.encode(value.charCodeAt(i) - 65, 6);
                }
                else {
                    throw new EncodingError("Unencodable FixedString '" + value + "'");
                }
            }
            return bitString;
        }
        static decode(bitString) {
            if (!/^[0-1]*$/.test(bitString) || bitString.length % 6 !== 0) {
                throw new DecodingError("Undecodable FixedString '" + bitString + "'");
            }
            let value = "";
            for (let i = 0; i < bitString.length; i += 6) {
                let code = FixedIntegerEncoder.decode(bitString.substring(i, i + 6));
                if (code === 63) {
                    value += " ";
                }
                else {
                    value += String.fromCharCode(code + 65);
                }
            }
            return value.trim();
        }
    }

    class EncodableFixedString extends AbstractEncodableBitStringDataType {
        constructor(stringLength, value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.stringLength = stringLength;
            this.setValue(value);
        }
        encode() {
            try {
                return FixedStringEncoder.encode(this.value, this.stringLength);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedStringEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + this.stringLength * 6);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
    }

    class EncodableOptimizedFixedRange extends AbstractEncodableBitStringDataType {
        constructor(value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.setValue(value);
        }
        encode() {
            try {
                //TODO: encoding the range before choosing the shortest is inefficient. There is probably a way
                //to identify in advance which will be shorter based on the array length and values
                let max = this.value.length > 0 ? this.value[this.value.length - 1] : 0;
                let rangeBitString = FixedIntegerRangeEncoder.encode(this.value);
                let rangeLength = rangeBitString.length;
                let bitFieldLength = max;
                if (rangeLength <= bitFieldLength) {
                    return FixedIntegerEncoder.encode(max, 16) + "1" + rangeBitString;
                }
                else {
                    let bits = [];
                    let index = 0;
                    for (let i = 0; i < max; i++) {
                        if (i === this.value[index] - 1) {
                            bits[i] = true;
                            index++;
                        }
                        else {
                            bits[i] = false;
                        }
                    }
                    return FixedIntegerEncoder.encode(max, 16) + "0" + FixedBitfieldEncoder.encode(bits, bitFieldLength);
                }
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                if (bitString.charAt(16) === "1") {
                    this.value = FixedIntegerRangeEncoder.decode(bitString.substring(17));
                }
                else {
                    let value = [];
                    let bits = FixedBitfieldEncoder.decode(bitString.substring(17));
                    for (let i = 0; i < bits.length; i++) {
                        if (bits[i] === true) {
                            value.push(i + 1);
                        }
                    }
                    this.value = value;
                }
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                let max = FixedIntegerEncoder.decode(StringUtil.substring(bitString, fromIndex, fromIndex + 16));
                if (bitString.charAt(fromIndex + 16) === "1") {
                    return (StringUtil.substring(bitString, fromIndex, fromIndex + 17) +
                        new EncodableFixedIntegerRange([]).substring(bitString, fromIndex + 17));
                }
                else {
                    return StringUtil.substring(bitString, fromIndex, fromIndex + 17 + max);
                }
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            super.setValue(Array.from(new Set(value)).sort((n1, n2) => n1 - n2));
        }
    }

    class TcfEuV2CoreSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = TraditionalBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFEUV2_CORE_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let date = new Date();
            let fields = new EncodableBitStringFields();
            fields.put(TcfEuV2Field.VERSION.toString(), new EncodableFixedInteger(6, TcfEuV2.VERSION));
            fields.put(TcfEuV2Field.CREATED.toString(), new EncodableDatetime(date));
            fields.put(TcfEuV2Field.LAST_UPDATED.toString(), new EncodableDatetime(date));
            fields.put(TcfEuV2Field.CMP_ID.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfEuV2Field.CMP_VERSION.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfEuV2Field.CONSENT_SCREEN.toString(), new EncodableFixedInteger(6, 0));
            fields.put(TcfEuV2Field.CONSENT_LANGUAGE.toString(), new EncodableFixedString(2, "EN"));
            fields.put(TcfEuV2Field.VENDOR_LIST_VERSION.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfEuV2Field.POLICY_VERSION.toString(), new EncodableFixedInteger(6, 2));
            fields.put(TcfEuV2Field.IS_SERVICE_SPECIFIC.toString(), new EncodableBoolean(false));
            fields.put(TcfEuV2Field.USE_NON_STANDARD_STACKS.toString(), new EncodableBoolean(false));
            fields.put(TcfEuV2Field.SPECIAL_FEATURE_OPTINS.toString(), new EncodableFixedBitfield([false, false, false, false, false, false, false, false, false, false, false, false]));
            fields.put(TcfEuV2Field.PURPOSE_CONSENTS.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfEuV2Field.PURPOSE_LEGITIMATE_INTERESTS.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfEuV2Field.PURPOSE_ONE_TREATMENT.toString(), new EncodableBoolean(false));
            fields.put(TcfEuV2Field.PUBLISHER_COUNTRY_CODE.toString(), new EncodableFixedString(2, "AA"));
            fields.put(TcfEuV2Field.VENDOR_CONSENTS.toString(), new EncodableOptimizedFixedRange([]));
            fields.put(TcfEuV2Field.VENDOR_LEGITIMATE_INTERESTS.toString(), new EncodableOptimizedFixedRange([]));
            fields.put(TcfEuV2Field.PUBLISHER_RESTRICTIONS.toString(), new EncodableArrayOfFixedIntegerRanges(6, 2, [], false));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfEuV2CoreSegment '" + encodedString + "'");
            }
        }
    }

    class EncodableFlexibleBitfield extends AbstractEncodableBitStringDataType {
        constructor(getLength, value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.getLength = getLength;
            this.setValue(value);
        }
        encode() {
            try {
                return FixedBitfieldEncoder.encode(this.value, this.getLength());
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedBitfieldEncoder.decode(bitString);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + this.getLength());
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            let numElements = this.getLength();
            let v = [...value];
            for (let i = v.length; i < numElements; i++) {
                v.push(false);
            }
            if (v.length > numElements) {
                v = v.slice(0, numElements);
            }
            super.setValue([...v]);
        }
    }

    class TcfEuV2PublisherPurposesSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = TraditionalBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFEUV2_PUBLISHER_PURPOSES_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(TcfEuV2Field.PUBLISHER_PURPOSES_SEGMENT_TYPE.toString(), new EncodableFixedInteger(3, 3));
            fields.put(TcfEuV2Field.PUBLISHER_CONSENTS.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfEuV2Field.PUBLISHER_LEGITIMATE_INTERESTS.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            let numCustomPurposes = new EncodableFixedInteger(6, 0);
            fields.put(TcfEuV2Field.NUM_CUSTOM_PURPOSES.toString(), numCustomPurposes);
            fields.put(TcfEuV2Field.PUBLISHER_CUSTOM_CONSENTS.toString(), new EncodableFlexibleBitfield(() => {
                return numCustomPurposes.getValue();
            }, []));
            fields.put(TcfEuV2Field.PUBLISHER_CUSTOM_LEGITIMATE_INTERESTS.toString(), new EncodableFlexibleBitfield(() => {
                return numCustomPurposes.getValue();
            }, []));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfEuV2PublisherPurposesSegment '" + encodedString + "'");
            }
        }
    }

    class TcfEuV2VendorsAllowedSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = TraditionalBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFEUV2_VENDORS_ALLOWED_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(TcfEuV2Field.VENDORS_ALLOWED_SEGMENT_TYPE.toString(), new EncodableFixedInteger(3, 2));
            fields.put(TcfEuV2Field.VENDORS_ALLOWED.toString(), new EncodableOptimizedFixedRange([]));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfEuV2VendorsAllowedSegment '" + encodedString + "'");
            }
        }
    }

    class TcfEuV2VendorsDisclosedSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = TraditionalBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFEUV2_VENDORS_DISCLOSED_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(TcfEuV2Field.VENDORS_DISCLOSED_SEGMENT_TYPE.toString(), new EncodableFixedInteger(3, 1));
            fields.put(TcfEuV2Field.VENDORS_DISCLOSED.toString(), new EncodableOptimizedFixedRange([]));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfEuV2VendorsDisclosedSegment '" + encodedString + "'");
            }
        }
    }

    class TcfEuV2 extends AbstractLazilyEncodableSection {
        constructor(encodedString) {
            super();
            if (encodedString && encodedString.length > 0) {
                this.decode(encodedString);
            }
        }
        //Overriden
        getId() {
            return TcfEuV2.ID;
        }
        //Overriden
        getName() {
            return TcfEuV2.NAME;
        }
        //Override
        getVersion() {
            return TcfEuV2.VERSION;
        }
        //Overriden
        initializeSegments() {
            let segments = [];
            segments.push(new TcfEuV2CoreSegment());
            segments.push(new TcfEuV2PublisherPurposesSegment());
            segments.push(new TcfEuV2VendorsAllowedSegment());
            segments.push(new TcfEuV2VendorsDisclosedSegment());
            return segments;
        }
        //Overriden
        decodeSection(encodedString) {
            let segments = this.initializeSegments();
            if (encodedString != null && encodedString.length !== 0) {
                let encodedSegments = encodedString.split(".");
                for (let i = 0; i < encodedSegments.length; i++) {
                    /**
                     * The first 3 bits contain the segment id. Rather than decode the entire string, just check the first character.
                     *
                     * A-H     = '000' = 0
                     * I-P     = '001' = 1
                     * Q-X     = '010' = 2
                     * Y-Z,a-f = '011' = 3
                     *
                     * Note that there is no segment id field for the core segment. Instead the first 6 bits are reserved
                     * for the encoding version which only coincidentally works here because the version value is less than 8.
                     */
                    let encodedSegment = encodedSegments[i];
                    if (encodedSegment.length !== 0) {
                        let firstChar = encodedSegment.charAt(0);
                        // unfortunately, the segment ordering doesn't match the segment ids
                        if (firstChar >= "A" && firstChar <= "H") {
                            segments[0].decode(encodedSegments[i]);
                        }
                        else if (firstChar >= "I" && firstChar <= "P") {
                            segments[3].decode(encodedSegments[i]);
                        }
                        else if (firstChar >= "Q" && firstChar <= "X") {
                            segments[2].decode(encodedSegments[i]);
                        }
                        else if ((firstChar >= "Y" && firstChar <= "Z") || (firstChar >= "a" && firstChar <= "f")) {
                            segments[1].decode(encodedSegments[i]);
                        }
                        else {
                            throw new DecodingError("Unable to decode TcfEuV2 segment '" + encodedSegment + "'");
                        }
                    }
                }
            }
            return segments;
        }
        // Overriden
        encodeSection(segments) {
            let encodedSegments = [];
            if (segments.length >= 1) {
                encodedSegments.push(segments[0].encode());
                let isServiceSpecific = this.getFieldValue(TcfEuV2Field.IS_SERVICE_SPECIFIC);
                if (isServiceSpecific) {
                    if (segments.length >= 2) {
                        encodedSegments.push(segments[1].encode());
                    }
                }
                else {
                    if (segments.length >= 2) {
                        encodedSegments.push(segments[2].encode());
                        if (segments.length >= 3) {
                            encodedSegments.push(segments[3].encode());
                        }
                    }
                }
            }
            return encodedSegments.join(".");
        }
        //Overriden
        setFieldValue(fieldName, value) {
            //
            // special handling for exceptions
            //
            // purpose 1, 3 - 6 legitimate interest is always false
            if (fieldName == TcfEuV2Field.PURPOSE_LEGITIMATE_INTERESTS) {
                value[0] = value[2] = value[3] = value[4] = value[5] = false;
            }
            // create and last update will need to stay in sync
            if (fieldName == TcfEuV2Field.CREATED) {
                super.setFieldValue(TcfEuV2Field.LAST_UPDATED, value);
            }
            else if (fieldName == TcfEuV2Field.LAST_UPDATED) {
                super.setFieldValue(TcfEuV2Field.CREATED, value);
            }
            else {
                const date = new Date();
                super.setFieldValue(TcfEuV2Field.CREATED, date);
                super.setFieldValue(TcfEuV2Field.LAST_UPDATED, date);
            }
            // update the actual given fields
            super.setFieldValue(fieldName, value);
        }
    }
    TcfEuV2.ID = 2;
    TcfEuV2.VERSION = 2;
    TcfEuV2.NAME = "tcfeuv2";

    var TcfCaV1Field;
    (function (TcfCaV1Field) {
        TcfCaV1Field["VERSION"] = "Version";
        TcfCaV1Field["CREATED"] = "Created";
        TcfCaV1Field["LAST_UPDATED"] = "LastUpdated";
        TcfCaV1Field["CMP_ID"] = "CmpId";
        TcfCaV1Field["CMP_VERSION"] = "CmpVersion";
        TcfCaV1Field["CONSENT_SCREEN"] = "ConsentScreen";
        TcfCaV1Field["CONSENT_LANGUAGE"] = "ConsentLanguage";
        TcfCaV1Field["VENDOR_LIST_VERSION"] = "VendorListVersion";
        TcfCaV1Field["TCF_POLICY_VERSION"] = "TcfPolicyVersion";
        TcfCaV1Field["USE_NON_STANDARD_STACKS"] = "UseNonStandardStacks";
        TcfCaV1Field["SPECIAL_FEATURE_EXPRESS_CONSENT"] = "SpecialFeatureExpressConsent";
        TcfCaV1Field["PUB_PURPOSES_SEGMENT_TYPE"] = "PubPurposesSegmentType";
        TcfCaV1Field["PURPOSES_EXPRESS_CONSENT"] = "PurposesExpressConsent";
        TcfCaV1Field["PURPOSES_IMPLIED_CONSENT"] = "PurposesImpliedConsent";
        TcfCaV1Field["VENDOR_EXPRESS_CONSENT"] = "VendorExpressConsent";
        TcfCaV1Field["VENDOR_IMPLIED_CONSENT"] = "VendorImpliedConsent";
        TcfCaV1Field["PUB_RESTRICTIONS"] = "PubRestrictions";
        TcfCaV1Field["PUB_PURPOSES_EXPRESS_CONSENT"] = "PubPurposesExpressConsent";
        TcfCaV1Field["PUB_PURPOSES_IMPLIED_CONSENT"] = "PubPurposesImpliedConsent";
        TcfCaV1Field["NUM_CUSTOM_PURPOSES"] = "NumCustomPurposes";
        TcfCaV1Field["CUSTOM_PURPOSES_EXPRESS_CONSENT"] = "CustomPurposesExpressConsent";
        TcfCaV1Field["CUSTOM_PURPOSES_IMPLIED_CONSENT"] = "CustomPurposesImpliedConsent";
        TcfCaV1Field["DISCLOSED_VENDORS_SEGMENT_TYPE"] = "DisclosedVendorsSegmentType";
        TcfCaV1Field["DISCLOSED_VENDORS"] = "DisclosedVendors";
    })(TcfCaV1Field || (TcfCaV1Field = {}));
    const TCFCAV1_CORE_SEGMENT_FIELD_NAMES = [
        TcfCaV1Field.VERSION,
        TcfCaV1Field.CREATED,
        TcfCaV1Field.LAST_UPDATED,
        TcfCaV1Field.CMP_ID,
        TcfCaV1Field.CMP_VERSION,
        TcfCaV1Field.CONSENT_SCREEN,
        TcfCaV1Field.CONSENT_LANGUAGE,
        TcfCaV1Field.VENDOR_LIST_VERSION,
        TcfCaV1Field.TCF_POLICY_VERSION,
        TcfCaV1Field.USE_NON_STANDARD_STACKS,
        TcfCaV1Field.SPECIAL_FEATURE_EXPRESS_CONSENT,
        TcfCaV1Field.PURPOSES_EXPRESS_CONSENT,
        TcfCaV1Field.PURPOSES_IMPLIED_CONSENT,
        TcfCaV1Field.VENDOR_EXPRESS_CONSENT,
        TcfCaV1Field.VENDOR_IMPLIED_CONSENT,
        TcfCaV1Field.PUB_RESTRICTIONS,
    ];
    const TCFCAV1_PUBLISHER_PURPOSES_SEGMENT_FIELD_NAMES = [
        TcfCaV1Field.PUB_PURPOSES_SEGMENT_TYPE,
        TcfCaV1Field.PUB_PURPOSES_EXPRESS_CONSENT,
        TcfCaV1Field.PUB_PURPOSES_IMPLIED_CONSENT,
        TcfCaV1Field.NUM_CUSTOM_PURPOSES,
        TcfCaV1Field.CUSTOM_PURPOSES_EXPRESS_CONSENT,
        TcfCaV1Field.CUSTOM_PURPOSES_IMPLIED_CONSENT,
    ];
    const TCFCAV1_DISCLOSED_VENDORS_SEGMENT_FIELD_NAMES = [
        TcfCaV1Field.DISCLOSED_VENDORS_SEGMENT_TYPE,
        TcfCaV1Field.DISCLOSED_VENDORS,
    ];

    class TcfCaV1CoreSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = CompressedBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFCAV1_CORE_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let date = new Date();
            let fields = new EncodableBitStringFields();
            fields.put(TcfCaV1Field.VERSION.toString(), new EncodableFixedInteger(6, TcfCaV1.VERSION));
            fields.put(TcfCaV1Field.CREATED.toString(), new EncodableDatetime(date));
            fields.put(TcfCaV1Field.LAST_UPDATED.toString(), new EncodableDatetime(date));
            fields.put(TcfCaV1Field.CMP_ID.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfCaV1Field.CMP_VERSION.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfCaV1Field.CONSENT_SCREEN.toString(), new EncodableFixedInteger(6, 0));
            fields.put(TcfCaV1Field.CONSENT_LANGUAGE.toString(), new EncodableFixedString(2, "EN"));
            fields.put(TcfCaV1Field.VENDOR_LIST_VERSION.toString(), new EncodableFixedInteger(12, 0));
            fields.put(TcfCaV1Field.TCF_POLICY_VERSION.toString(), new EncodableFixedInteger(6, 2));
            fields.put(TcfCaV1Field.USE_NON_STANDARD_STACKS.toString(), new EncodableBoolean(false));
            fields.put(TcfCaV1Field.SPECIAL_FEATURE_EXPRESS_CONSENT.toString(), new EncodableFixedBitfield([false, false, false, false, false, false, false, false, false, false, false, false]));
            fields.put(TcfCaV1Field.PURPOSES_EXPRESS_CONSENT.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfCaV1Field.PURPOSES_IMPLIED_CONSENT.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfCaV1Field.VENDOR_EXPRESS_CONSENT.toString(), new EncodableOptimizedFixedRange([]));
            fields.put(TcfCaV1Field.VENDOR_IMPLIED_CONSENT.toString(), new EncodableOptimizedFixedRange([]));
            fields.put(TcfCaV1Field.PUB_RESTRICTIONS.toString(), new EncodableArrayOfFixedIntegerRanges(6, 2, [], false));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfCaV1CoreSegment '" + encodedString + "'");
            }
        }
    }

    class TcfCaV1PublisherPurposesSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = CompressedBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFCAV1_PUBLISHER_PURPOSES_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(TcfCaV1Field.PUB_PURPOSES_SEGMENT_TYPE.toString(), new EncodableFixedInteger(3, 3));
            fields.put(TcfCaV1Field.PUB_PURPOSES_EXPRESS_CONSENT.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            fields.put(TcfCaV1Field.PUB_PURPOSES_IMPLIED_CONSENT.toString(), new EncodableFixedBitfield([
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            ]));
            let numCustomPurposes = new EncodableFixedInteger(6, 0);
            fields.put(TcfCaV1Field.NUM_CUSTOM_PURPOSES.toString(), numCustomPurposes);
            fields.put(TcfCaV1Field.CUSTOM_PURPOSES_EXPRESS_CONSENT.toString(), new EncodableFlexibleBitfield(() => {
                return numCustomPurposes.getValue();
            }, []));
            fields.put(TcfCaV1Field.CUSTOM_PURPOSES_IMPLIED_CONSENT.toString(), new EncodableFlexibleBitfield(() => {
                return numCustomPurposes.getValue();
            }, []));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode TcfCaV1PublisherPurposesSegment '" + encodedString + "'");
            }
        }
    }

    class TcfCaV1DisclosedVendorsSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = TraditionalBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return TCFCAV1_DISCLOSED_VENDORS_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(TcfCaV1Field.DISCLOSED_VENDORS_SEGMENT_TYPE.toString(), new EncodableFixedInteger(3, 1));
            fields.put(TcfCaV1Field.DISCLOSED_VENDORS.toString(), new EncodableOptimizedFixedRange([]));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode HeaderV1CoreSegment '" + encodedString + "'");
            }
        }
    }

    class TcfCaV1 extends AbstractLazilyEncodableSection {
        constructor(encodedString) {
            super();
            if (encodedString && encodedString.length > 0) {
                this.decode(encodedString);
            }
        }
        //Overriden
        getId() {
            return TcfCaV1.ID;
        }
        //Overriden
        getName() {
            return TcfCaV1.NAME;
        }
        //Override
        getVersion() {
            return TcfCaV1.VERSION;
        }
        //Overriden
        initializeSegments() {
            let segments = [];
            segments.push(new TcfCaV1CoreSegment());
            segments.push(new TcfCaV1PublisherPurposesSegment());
            segments.push(new TcfCaV1DisclosedVendorsSegment());
            return segments;
        }
        //Overriden
        decodeSection(encodedString) {
            let segments = this.initializeSegments();
            if (encodedString != null && encodedString.length !== 0) {
                let encodedSegments = encodedString.split(".");
                for (let i = 0; i < encodedSegments.length; i++) {
                    /**
                     * The first 3 bits contain the segment id. Rather than decode the entire string, just check the first character.
                     *
                     * A-H     = '000' = 0
                     * I-P     = '001' = 1
                     * Y-Z,a-f = '011' = 3
                     *
                     * Note that there is no segment id field for the core segment. Instead the first 6 bits are reserved
                     * for the encoding version which only coincidentally works here because the version value is less than 8.
                     */
                    let encodedSegment = encodedSegments[i];
                    if (encodedSegment.length !== 0) {
                        let firstChar = encodedSegment.charAt(0);
                        if (firstChar >= "A" && firstChar <= "H") {
                            segments[0].decode(encodedSegments[i]);
                        }
                        else if (firstChar >= "I" && firstChar <= "P") {
                            segments[2].decode(encodedSegments[i]);
                        }
                        else if ((firstChar >= "Y" && firstChar <= "Z") || (firstChar >= "a" && firstChar <= "f")) {
                            segments[1].decode(encodedSegments[i]);
                        }
                        else {
                            throw new DecodingError("Unable to decode TcfCaV1 segment '" + encodedSegment + "'");
                        }
                    }
                }
            }
            return segments;
        }
        // Overriden
        encodeSection(segments) {
            let encodedSegments = [];
            encodedSegments.push(segments[0].encode());
            encodedSegments.push(segments[1].encode());
            if (this.getFieldValue(TcfCaV1Field.DISCLOSED_VENDORS).length > 0) {
                encodedSegments.push(segments[2].encode());
            }
            return encodedSegments.join(".");
        }
        //Overriden
        setFieldValue(fieldName, value) {
            super.setFieldValue(fieldName, value);
            if (fieldName !== TcfCaV1Field.CREATED && fieldName !== TcfCaV1Field.LAST_UPDATED) {
                let date = new Date();
                super.setFieldValue(TcfCaV1Field.CREATED, date);
                super.setFieldValue(TcfCaV1Field.LAST_UPDATED, date);
            }
        }
    }
    TcfCaV1.ID = 5;
    TcfCaV1.VERSION = 1;
    TcfCaV1.NAME = "tcfcav1";

    class UnencodableCharacter {
        constructor(value, validator) {
            this.value = null;
            if (validator) {
                this.validator = validator;
            }
            else {
                this.validator = new (class {
                    test(v) {
                        return true;
                    }
                })();
            }
            this.setValue(value);
        }
        hasValue() {
            return this.value != null;
        }
        getValue() {
            return this.value;
        }
        setValue(value) {
            if (value) {
                this.value = value.charAt(0);
            }
            else {
                value = null;
            }
        }
    }

    class UnencodableInteger {
        constructor(value, validator) {
            this.value = null;
            if (validator) {
                this.validator = validator;
            }
            else {
                this.validator = new (class {
                    test(v) {
                        return true;
                    }
                })();
            }
            this.setValue(value);
        }
        hasValue() {
            return this.value != null;
        }
        getValue() {
            return this.value;
        }
        setValue(value) {
            this.value = value;
        }
    }

    class GenericFields {
        constructor() {
            this.fields = new Map();
        }
        containsKey(key) {
            return this.fields.has(key);
        }
        put(key, value) {
            this.fields.set(key, value);
        }
        get(key) {
            return this.fields.get(key);
        }
        getAll() {
            return new Map(this.fields);
        }
        reset(fields) {
            this.fields.clear();
            fields.getAll().forEach((value, key) => {
                this.fields.set(key, value);
            });
        }
    }

    var UspV1Field;
    (function (UspV1Field) {
        UspV1Field["VERSION"] = "Version";
        UspV1Field["NOTICE"] = "Notice";
        UspV1Field["OPT_OUT_SALE"] = "OptOutSale";
        UspV1Field["LSPA_COVERED"] = "LspaCovered";
    })(UspV1Field || (UspV1Field = {}));
    const USPV1_CORE_SEGMENT_FIELD_NAMES = [
        UspV1Field.VERSION,
        UspV1Field.NOTICE,
        UspV1Field.OPT_OUT_SALE,
        UspV1Field.LSPA_COVERED,
    ];

    class UspV1CoreSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return USPV1_CORE_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            const validator = new (class {
                test(s) {
                    return s === "-" || s === "Y" || s === "N";
                }
            })();
            let fields = new GenericFields();
            fields.put(UspV1Field.VERSION, new UnencodableInteger(UspV1.VERSION));
            fields.put(UspV1Field.NOTICE, new UnencodableCharacter("-", validator));
            fields.put(UspV1Field.OPT_OUT_SALE, new UnencodableCharacter("-", validator));
            fields.put(UspV1Field.LSPA_COVERED, new UnencodableCharacter("-", validator));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let str = "";
            str += fields.get(UspV1Field.VERSION).getValue();
            str += fields.get(UspV1Field.NOTICE).getValue();
            str += fields.get(UspV1Field.OPT_OUT_SALE).getValue();
            str += fields.get(UspV1Field.LSPA_COVERED).getValue();
            return str;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length != 4) {
                throw new DecodingError("Unable to decode UspV1CoreSegment '" + encodedString + "'");
            }
            try {
                fields.get(UspV1Field.VERSION).setValue(parseInt(encodedString.substring(0, 1)));
                fields.get(UspV1Field.NOTICE).setValue(encodedString.charAt(1));
                fields.get(UspV1Field.OPT_OUT_SALE).setValue(encodedString.charAt(2));
                fields.get(UspV1Field.LSPA_COVERED).setValue(encodedString.charAt(3));
            }
            catch (e) {
                throw new DecodingError("Unable to decode UspV1CoreSegment '" + encodedString + "'");
            }
        }
    }

    // Deprecated
    class UspV1 extends AbstractLazilyEncodableSection {
        constructor(encodedString) {
            super();
            if (encodedString && encodedString.length > 0) {
                this.decode(encodedString);
            }
        }
        //Overriden
        getId() {
            return UspV1.ID;
        }
        //Overriden
        getName() {
            return UspV1.NAME;
        }
        //Override
        getVersion() {
            return UspV1.VERSION;
        }
        //Overriden
        initializeSegments() {
            let segments = [];
            segments.push(new UspV1CoreSegment());
            return segments;
        }
        //Overriden
        decodeSection(encodedString) {
            let segments = this.initializeSegments();
            if (encodedString != null && encodedString.length !== 0) {
                let encodedSegments = encodedString.split(".");
                for (let i = 0; i < segments.length; i++) {
                    if (encodedSegments.length > i) {
                        segments[i].decode(encodedSegments[i]);
                    }
                }
            }
            return segments;
        }
        // Overriden
        encodeSection(segments) {
            let encodedSegments = [];
            for (let i = 0; i < segments.length; i++) {
                let segment = segments[i];
                encodedSegments.push(segment.encode());
            }
            return encodedSegments.join(".");
        }
    }
    UspV1.ID = 6;
    UspV1.VERSION = 1;
    UspV1.NAME = "uspv1";

    var UsNatField;
    (function (UsNatField) {
        UsNatField["VERSION"] = "Version";
        UsNatField["SHARING_NOTICE"] = "SharingNotice";
        UsNatField["SALE_OPT_OUT_NOTICE"] = "SaleOptOutNotice";
        UsNatField["SHARING_OPT_OUT_NOTICE"] = "SharingOptOutNotice";
        UsNatField["TARGETED_ADVERTISING_OPT_OUT_NOTICE"] = "TargetedAdvertisingOptOutNotice";
        UsNatField["SENSITIVE_DATA_PROCESSING_OPT_OUT_NOTICE"] = "SensitiveDataProcessingOptOutNotice";
        UsNatField["SENSITIVE_DATA_LIMIT_USE_NOTICE"] = "SensitiveDataLimitUseNotice";
        UsNatField["SALE_OPT_OUT"] = "SaleOptOut";
        UsNatField["SHARING_OPT_OUT"] = "SharingOptOut";
        UsNatField["TARGETED_ADVERTISING_OPT_OUT"] = "TargetedAdvertisingOptOut";
        UsNatField["SENSITIVE_DATA_PROCESSING"] = "SensitiveDataProcessing";
        UsNatField["KNOWN_CHILD_SENSITIVE_DATA_CONSENTS"] = "KnownChildSensitiveDataConsents";
        UsNatField["PERSONAL_DATA_CONSENTS"] = "PersonalDataConsents";
        UsNatField["MSPA_COVERED_TRANSACTION"] = "MspaCoveredTransaction";
        UsNatField["MSPA_OPT_OUT_OPTION_MODE"] = "MspaOptOutOptionMode";
        UsNatField["MSPA_SERVICE_PROVIDER_MODE"] = "MspaServiceProviderMode";
        UsNatField["GPC_SEGMENT_TYPE"] = "GpcSegmentType";
        UsNatField["GPC_SEGMENT_INCLUDED"] = "GpcSegmentIncluded";
        UsNatField["GPC"] = "Gpc";
    })(UsNatField || (UsNatField = {}));
    const USNAT_CORE_SEGMENT_FIELD_NAMES = [
        UsNatField.VERSION,
        UsNatField.SHARING_NOTICE,
        UsNatField.SALE_OPT_OUT_NOTICE,
        UsNatField.SHARING_OPT_OUT_NOTICE,
        UsNatField.TARGETED_ADVERTISING_OPT_OUT_NOTICE,
        UsNatField.SENSITIVE_DATA_PROCESSING_OPT_OUT_NOTICE,
        UsNatField.SENSITIVE_DATA_LIMIT_USE_NOTICE,
        UsNatField.SALE_OPT_OUT,
        UsNatField.SHARING_OPT_OUT,
        UsNatField.TARGETED_ADVERTISING_OPT_OUT,
        UsNatField.SENSITIVE_DATA_PROCESSING,
        UsNatField.KNOWN_CHILD_SENSITIVE_DATA_CONSENTS,
        UsNatField.PERSONAL_DATA_CONSENTS,
        UsNatField.MSPA_COVERED_TRANSACTION,
        UsNatField.MSPA_OPT_OUT_OPTION_MODE,
        UsNatField.MSPA_SERVICE_PROVIDER_MODE,
    ];
    const USNAT_GPC_SEGMENT_FIELD_NAMES = [UsNatField.GPC_SEGMENT_TYPE, UsNatField.GPC];

    class FixedIntegerListEncoder {
        static encode(value, elementBitStringLength, numElements) {
            if (value.length > numElements) {
                throw new EncodingError("Too many values '" + value.length + "'");
            }
            let bitString = "";
            for (let i = 0; i < value.length; i++) {
                bitString += FixedIntegerEncoder.encode(value[i], elementBitStringLength);
            }
            while (bitString.length < elementBitStringLength * numElements) {
                bitString += "0";
            }
            return bitString;
        }
        static decode(bitString, elementBitStringLength, numElements) {
            if (!/^[0-1]*$/.test(bitString)) {
                throw new DecodingError("Undecodable FixedInteger '" + bitString + "'");
            }
            if (bitString.length > elementBitStringLength * numElements) {
                throw new DecodingError("Undecodable FixedIntegerList '" + bitString + "'");
            }
            if (bitString.length % elementBitStringLength != 0) {
                throw new DecodingError("Undecodable FixedIntegerList '" + bitString + "'");
            }
            while (bitString.length < elementBitStringLength * numElements) {
                bitString += "0";
            }
            if (bitString.length > elementBitStringLength * numElements) {
                bitString = bitString.substring(0, elementBitStringLength * numElements);
            }
            let value = [];
            for (let i = 0; i < bitString.length; i += elementBitStringLength) {
                value.push(FixedIntegerEncoder.decode(bitString.substring(i, i + elementBitStringLength)));
            }
            while (value.length < numElements) {
                value.push(0);
            }
            return value;
        }
    }

    class EncodableFixedIntegerList extends AbstractEncodableBitStringDataType {
        constructor(elementBitStringLength, value, hardFailIfMissing = true) {
            super(hardFailIfMissing);
            this.elementBitStringLength = elementBitStringLength;
            this.numElements = value.length;
            this.setValue(value);
        }
        encode() {
            try {
                return FixedIntegerListEncoder.encode(this.value, this.elementBitStringLength, this.numElements);
            }
            catch (e) {
                throw new EncodingError(e);
            }
        }
        decode(bitString) {
            try {
                this.value = FixedIntegerListEncoder.decode(bitString, this.elementBitStringLength, this.numElements);
            }
            catch (e) {
                throw new DecodingError(e);
            }
        }
        substring(bitString, fromIndex) {
            try {
                return StringUtil.substring(bitString, fromIndex, fromIndex + this.elementBitStringLength * this.numElements);
            }
            catch (e) {
                throw new SubstringError(e);
            }
        }
        // Overriden
        getValue() {
            return [...super.getValue()];
        }
        // Overriden
        setValue(value) {
            let v = [...value];
            for (let i = v.length; i < this.numElements; i++) {
                v.push(0);
            }
            if (v.length > this.numElements) {
                v = v.slice(0, this.numElements);
            }
            super.setValue(v);
        }
    }

    class UsNatCoreSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = CompressedBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return USNAT_CORE_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            const nullableBooleanAsTwoBitIntegerValidator = new (class {
                test(n) {
                    return n >= 0 && n <= 2;
                }
            })();
            const nonNullableBooleanAsTwoBitIntegerValidator = new (class {
                test(n) {
                    return n >= 1 && n <= 2;
                }
            })();
            const nullableBooleanAsTwoBitIntegerListValidator = new (class {
                test(l) {
                    for (let i = 0; i < l.length; i++) {
                        let n = l[i];
                        if (n < 0 || n > 2) {
                            return false;
                        }
                    }
                    return true;
                }
            })();
            let fields = new EncodableBitStringFields();
            fields.put(UsNatField.VERSION.toString(), new EncodableFixedInteger(6, UsNat.VERSION));
            fields.put(UsNatField.SHARING_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SALE_OPT_OUT_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SHARING_OPT_OUT_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.TARGETED_ADVERTISING_OPT_OUT_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SENSITIVE_DATA_PROCESSING_OPT_OUT_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SENSITIVE_DATA_LIMIT_USE_NOTICE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SALE_OPT_OUT.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SHARING_OPT_OUT.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.TARGETED_ADVERTISING_OPT_OUT.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.SENSITIVE_DATA_PROCESSING.toString(), new EncodableFixedIntegerList(2, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]).withValidator(nullableBooleanAsTwoBitIntegerListValidator));
            fields.put(UsNatField.KNOWN_CHILD_SENSITIVE_DATA_CONSENTS.toString(), new EncodableFixedIntegerList(2, [0, 0]).withValidator(nullableBooleanAsTwoBitIntegerListValidator));
            fields.put(UsNatField.PERSONAL_DATA_CONSENTS.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.MSPA_COVERED_TRANSACTION.toString(), new EncodableFixedInteger(2, 1).withValidator(nonNullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.MSPA_OPT_OUT_OPTION_MODE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            fields.put(UsNatField.MSPA_SERVICE_PROVIDER_MODE.toString(), new EncodableFixedInteger(2, 0).withValidator(nullableBooleanAsTwoBitIntegerValidator));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode UsNatCoreSegment '" + encodedString + "'");
            }
        }
        // overriden
        validate() {
            let sharingNotice = this.fields.get(UsNatField.SHARING_NOTICE).getValue();
            let sharingOptOutNotice = this.fields.get(UsNatField.SHARING_OPT_OUT_NOTICE).getValue();
            let sharingOptOut = this.fields.get(UsNatField.SHARING_OPT_OUT).getValue();
            let saleOptOutNotice = this.fields.get(UsNatField.SALE_OPT_OUT_NOTICE).getValue();
            let saleOptOut = this.fields.get(UsNatField.SALE_OPT_OUT).getValue();
            let targetedAdvertisingOptOutNotice = this.fields
                .get(UsNatField.TARGETED_ADVERTISING_OPT_OUT_NOTICE)
                .getValue();
            let targetedAdvertisingOptOut = this.fields.get(UsNatField.TARGETED_ADVERTISING_OPT_OUT).getValue();
            let mspaServiceProviderMode = this.fields.get(UsNatField.MSPA_SERVICE_PROVIDER_MODE).getValue();
            let mspaOptOutOptionMode = this.fields.get(UsNatField.MSPA_OPT_OUT_OPTION_MODE).getValue();
            let sensitiveDataLimtUserNotice = this.fields.get(UsNatField.SENSITIVE_DATA_LIMIT_USE_NOTICE).getValue();
            if (sharingNotice == 0) {
                if (sharingOptOut != 0) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingNotice + " / " + sharingOptOut + "}");
                }
            }
            else if (sharingNotice == 1) {
                if (sharingOptOut != 1 && sharingOptOut != 2) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingNotice + " / " + sharingOptOut + "}");
                }
            }
            else if (sharingNotice == 2) {
                if (sharingOptOut != 1) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingNotice + " / " + sharingOptOut + "}");
                }
            }
            if (sharingOptOutNotice == 0) {
                if (sharingOptOut != 0) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingOptOutNotice + " / " + sharingOptOut + "}");
                }
            }
            else if (sharingOptOutNotice == 1) {
                if (sharingOptOut != 1 && sharingOptOut != 2) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingOptOutNotice + " / " + sharingOptOut + "}");
                }
            }
            else if (sharingOptOutNotice == 2) {
                if (sharingOptOut != 1) {
                    throw new ValidationError("Invalid usnat sharing notice / opt out combination: {" + sharingOptOutNotice + " / " + sharingOptOut + "}");
                }
            }
            if (saleOptOutNotice == 0) {
                if (saleOptOut != 0) {
                    throw new ValidationError("Invalid usnat sale notice / opt out combination: {" + saleOptOutNotice + " / " + saleOptOut + "}");
                }
            }
            else if (saleOptOutNotice == 1) {
                if (saleOptOut != 1 && saleOptOut != 2) {
                    throw new ValidationError("Invalid usnat sale notice / opt out combination: {" + saleOptOutNotice + " / " + saleOptOut + "}");
                }
            }
            else if (saleOptOutNotice == 2) {
                if (saleOptOut != 1) {
                    throw new ValidationError("Invalid usnat sale notice / opt out combination: {" + saleOptOutNotice + " / " + saleOptOut + "}");
                }
            }
            if (targetedAdvertisingOptOutNotice == 0) {
                if (targetedAdvertisingOptOut != 0) {
                    throw new ValidationError("Invalid usnat targeted advertising notice / opt out combination: {" +
                        targetedAdvertisingOptOutNotice +
                        " / " +
                        targetedAdvertisingOptOut +
                        "}");
                }
            }
            else if (targetedAdvertisingOptOutNotice == 1) {
                if (saleOptOut != 1 && saleOptOut != 2) {
                    throw new ValidationError("Invalid usnat targeted advertising notice / opt out combination: {" +
                        targetedAdvertisingOptOutNotice +
                        " / " +
                        targetedAdvertisingOptOut +
                        "}");
                }
            }
            else if (targetedAdvertisingOptOutNotice == 2) {
                if (saleOptOut != 1) {
                    throw new ValidationError("Invalid usnat targeted advertising notice / opt out combination: {" +
                        targetedAdvertisingOptOutNotice +
                        " / " +
                        targetedAdvertisingOptOut +
                        "}");
                }
            }
            if (mspaServiceProviderMode == 0) {
                if (saleOptOutNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sale opt out notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        saleOptOutNotice +
                        "}");
                }
                if (sharingOptOutNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sharing opt out notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        sharingOptOutNotice +
                        "}");
                }
                if (sensitiveDataLimtUserNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sensitive data limit use notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        sensitiveDataLimtUserNotice +
                        "}");
                }
            }
            else if (mspaServiceProviderMode == 1) {
                if (mspaOptOutOptionMode != 2) {
                    throw new ValidationError("Invalid usnat mspa service provider / opt out option modes combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        mspaServiceProviderMode +
                        "}");
                }
                if (saleOptOutNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sale opt out notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        saleOptOutNotice +
                        "}");
                }
                if (sharingOptOutNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sharing opt out notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        sharingOptOutNotice +
                        "}");
                }
                if (sensitiveDataLimtUserNotice != 0) {
                    throw new ValidationError("Invalid usnat mspa service provider mode / sensitive data limit use notice combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        sensitiveDataLimtUserNotice +
                        "}");
                }
            }
            else if (mspaServiceProviderMode == 2) {
                if (mspaOptOutOptionMode != 1) {
                    throw new ValidationError("Invalid usnat mspa service provider / opt out option modes combination: {" +
                        mspaServiceProviderMode +
                        " / " +
                        mspaOptOutOptionMode +
                        "}");
                }
            }
        }
    }

    class UsNatGpcSegment extends AbstractLazilyEncodableSegment {
        constructor(encodedString) {
            super();
            this.base64UrlEncoder = CompressedBase64UrlEncoder.getInstance();
            this.bitStringEncoder = BitStringEncoder.getInstance();
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        // overriden
        getFieldNames() {
            return USNAT_GPC_SEGMENT_FIELD_NAMES;
        }
        // overriden
        initializeFields() {
            let fields = new EncodableBitStringFields();
            fields.put(UsNatField.GPC_SEGMENT_TYPE.toString(), new EncodableFixedInteger(2, 1));
            fields.put(UsNatField.GPC_SEGMENT_INCLUDED.toString(), new EncodableBoolean(true));
            fields.put(UsNatField.GPC.toString(), new EncodableBoolean(false));
            return fields;
        }
        // overriden
        encodeSegment(fields) {
            let bitString = this.bitStringEncoder.encode(fields, this.getFieldNames());
            let encodedString = this.base64UrlEncoder.encode(bitString);
            return encodedString;
        }
        // overriden
        decodeSegment(encodedString, fields) {
            if (encodedString == null || encodedString.length === 0) {
                this.fields.reset(fields);
            }
            try {
                let bitString = this.base64UrlEncoder.decode(encodedString);
                this.bitStringEncoder.decode(bitString, this.getFieldNames(), fields);
            }
            catch (e) {
                throw new DecodingError("Unable to decode UsNatGpcSegment '" + encodedString + "'");
            }
        }
    }

    class UsNat extends AbstractLazilyEncodableSection {
        constructor(encodedString) {
            super();
            if (encodedString && encodedString.length > 0) {
                this.decode(encodedString);
            }
        }
        //Overriden
        getId() {
            return UsNat.ID;
        }
        //Overriden
        getName() {
            return UsNat.NAME;
        }
        //Override
        getVersion() {
            return UsNat.VERSION;
        }
        //Overriden
        initializeSegments() {
            let segments = [];
            segments.push(new UsNatCoreSegment());
            segments.push(new UsNatGpcSegment());
            return segments;
        }
        //Overriden
        decodeSection(encodedString) {
            let segments = this.initializeSegments();
            if (encodedString != null && encodedString.length !== 0) {
                let encodedSegments = encodedString.split(".");
                if (encodedSegments.length > 0) {
                    segments[0].decode(encodedSegments[0]);
                }
                if (encodedSegments.length > 1) {
                    segments[1].setFieldValue(UsNatField.GPC_SEGMENT_INCLUDED, true);
                    segments[1].decode(encodedSegments[1]);
                }
                else {
                    segments[1].setFieldValue(UsNatField.GPC_SEGMENT_INCLUDED, false);
                }
            }
            return segments;
        }
        // Overriden
        encodeSection(segments) {
            let encodedSegments = [];
            if (segments.length >= 1) {
                encodedSegments.push(segments[0].encode());
                if (segments.length >= 2 && segments[1].getFieldValue(UsNatField.GPC_SEGMENT_INCLUDED) === true) {
                    encodedSegments.push(segments[1].encode());
                }
            }
            return encodedSegments.join(".");
        }
    }
    UsNat.ID = 7;
    UsNat.VERSION = 1;
    UsNat.NAME = "usnat";

    /*
    import { UsCa } from "./UsCa.js";
    import { UsVa } from "./UsVa.js";
    import { UsCo } from "./UsCo.js";
    import { UsUt } from "./UsUt.js";
    import { UsCt } from "./UsCt.js";
    import { UsFl } from "./UsFl.js";
    import { UsMt } from "./UsMt.js";
    import { UsOr } from "./UsOr.js";
    import { UsTx } from "./UsTx.js";
    */
    class Sections {
    }
    Sections.SECTION_ID_NAME_MAP = new Map([
        [TcfEuV2.ID, TcfEuV2.NAME],
        [TcfCaV1.ID, TcfCaV1.NAME],
        [UspV1.ID, UspV1.NAME],
        [UsNat.ID, UsNat.NAME],
        /*
            [UsCa.ID, UsCa.NAME],
            [UsVa.ID, UsVa.NAME],
            [UsCo.ID, UsCo.NAME],
            [UsUt.ID, UsUt.NAME],
            [UsCt.ID, UsCt.NAME],
            [UsFl.ID, UsFl.NAME],
            [UsMt.ID, UsMt.NAME],
            [UsOr.ID, UsOr.NAME],
            [UsTx.ID, UsTx.NAME],
        */
    ]);
    Sections.SECTION_ORDER = [
        TcfEuV2.NAME,
        TcfCaV1.NAME,
        UspV1.NAME,
        UsNat.NAME,
        /*
            UsCa.NAME,
            UsVa.NAME,
            UsCo.NAME,
            UsUt.NAME,
            UsCt.NAME,
            UsFl.NAME,
            UsMt.NAME,
            UsOr.NAME,
            UsTx.NAME,
        */
    ];

    class GppModel {
        constructor(encodedString) {
            this.sections = new Map();
            this.encodedString = null;
            this.decoded = true;
            this.dirty = false;
            if (encodedString) {
                this.decode(encodedString);
            }
        }
        setFieldValue(sectionName, fieldName, value) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            let section = null;
            if (!this.sections.has(sectionName)) {
                if (sectionName === TcfCaV1.NAME) {
                    section = new TcfCaV1();
                    this.sections.set(TcfCaV1.NAME, section);
                }
                else if (sectionName === TcfEuV2.NAME) {
                    section = new TcfEuV2();
                    this.sections.set(TcfEuV2.NAME, section);
                }
                else if (sectionName === UspV1.NAME) {
                    section = new UspV1();
                    this.sections.set(UspV1.NAME, section);
                }
                else if (sectionName === UsNat.NAME) {
                    section = new UsNat();
                    this.sections.set(UsNat.NAME, section);
                    /*
                          } else if (sectionName === UsCa.NAME) {
                            section = new UsCa();
                            this.sections.set(UsCa.NAME, section);
                          } else if (sectionName === UsVa.NAME) {
                            section = new UsVa();
                            this.sections.set(UsVa.NAME, section);
                          } else if (sectionName === UsCo.NAME) {
                            section = new UsCo();
                            this.sections.set(UsCo.NAME, section);
                          } else if (sectionName === UsUt.NAME) {
                            section = new UsUt();
                            this.sections.set(UsUt.NAME, section);
                          } else if (sectionName === UsCt.NAME) {
                            section = new UsCt();
                            this.sections.set(UsCt.NAME, section);
                          } else if (sectionName === UsFl.NAME) {
                            section = new UsFl();
                            this.sections.set(UsFl.NAME, section);
                          } else if (sectionName === UsMt.NAME) {
                            section = new UsMt();
                            this.sections.set(UsMt.NAME, section);
                          } else if (sectionName === UsOr.NAME) {
                            section = new UsOr();
                            this.sections.set(UsOr.NAME, section);
                          } else if (sectionName === UsTx.NAME) {
                            section = new UsTx();
                            this.sections.set(UsTx.NAME, section);
                    */
                }
            }
            else {
                section = this.sections.get(sectionName);
            }
            if (section) {
                section.setFieldValue(fieldName, value);
                this.dirty = true;
            }
            else {
                throw new InvalidFieldError(sectionName + "." + fieldName + " not found");
            }
        }
        setFieldValueBySectionId(sectionId, fieldName, value) {
            this.setFieldValue(Sections.SECTION_ID_NAME_MAP.get(sectionId), fieldName, value);
        }
        getFieldValue(sectionName, fieldName) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.sections.has(sectionName)) {
                return this.sections.get(sectionName).getFieldValue(fieldName);
            }
            else {
                return null;
            }
        }
        getFieldValueBySectionId(sectionId, fieldName) {
            return this.getFieldValue(Sections.SECTION_ID_NAME_MAP.get(sectionId), fieldName);
        }
        hasField(sectionName, fieldName) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.sections.has(sectionName)) {
                return this.sections.get(sectionName).hasField(fieldName);
            }
            else {
                return false;
            }
        }
        hasFieldBySectionId(sectionId, fieldName) {
            return this.hasField(Sections.SECTION_ID_NAME_MAP.get(sectionId), fieldName);
        }
        hasSection(sectionName) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            return this.sections.has(sectionName);
        }
        hasSectionId(sectionId) {
            return this.hasSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        deleteSection(sectionName) {
            // lazily decode
            if (!this.decoded && this.encodedString != null && this.encodedString.length > 0) {
                this.decode(this.encodedString);
            }
            this.sections.delete(sectionName);
            this.dirty = true;
        }
        deleteSectionById(sectionId) {
            this.deleteSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        clear() {
            this.sections.clear();
            this.encodedString = "DBAA";
            this.decoded = false;
            this.dirty = false;
        }
        getHeader() {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            let header = new HeaderV1();
            header.setFieldValue("SectionIds", this.getSectionIds());
            return header.toObj();
        }
        getSection(sectionName) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.sections.has(sectionName)) {
                return this.sections.get(sectionName).toObj();
            }
            else {
                return null;
            }
        }
        getSectionIds() {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            let sectionIds = [];
            for (let i = 0; i < Sections.SECTION_ORDER.length; i++) {
                let sectionName = Sections.SECTION_ORDER[i];
                if (this.sections.has(sectionName)) {
                    let section = this.sections.get(sectionName);
                    sectionIds.push(section.getId());
                }
            }
            return sectionIds;
        }
        encodeModel(sections) {
            let encodedSections = [];
            let sectionIds = [];
            for (let i = 0; i < Sections.SECTION_ORDER.length; i++) {
                let sectionName = Sections.SECTION_ORDER[i];
                if (sections.has(sectionName)) {
                    let section = sections.get(sectionName);
                    encodedSections.push(section.encode());
                    sectionIds.push(section.getId());
                }
            }
            let header = new HeaderV1();
            header.setFieldValue("SectionIds", sectionIds);
            encodedSections.unshift(header.encode());
            return encodedSections.join("~");
        }
        decodeModel(str) {
            if (!str || str.length == 0 || str.startsWith("DB")) {
                let encodedSections = str.split("~");
                let sections = new Map();
                if (encodedSections[0].startsWith("D")) {
                    //GPP String
                    let header = new HeaderV1(encodedSections[0]);
                    let sectionIds = header.getFieldValue("SectionIds");
                    if (sectionIds.length !== encodedSections.length - 1) {
                        throw new DecodingError("Unable to decode '" +
                            str +
                            "'. The number of sections does not match the number of sections defined in the header.");
                    }
                    for (let i = 0; i < sectionIds.length; i++) {
                        let encodedSection = encodedSections[i + 1];
                        if (encodedSection.trim() === "") {
                            throw new DecodingError("Unable to decode '" + str + "'. Section " + (i + 1) + " is blank.");
                        }
                        if (sectionIds[i] === TcfCaV1.ID) {
                            let section = new TcfCaV1(encodedSections[i + 1]);
                            sections.set(TcfCaV1.NAME, section);
                        }
                        else if (sectionIds[i] === TcfEuV2.ID) {
                            let section = new TcfEuV2(encodedSections[i + 1]);
                            sections.set(TcfEuV2.NAME, section);
                        }
                        else if (sectionIds[i] === UspV1.ID) {
                            let section = new UspV1(encodedSections[i + 1]);
                            sections.set(UspV1.NAME, section);
                        }
                        else if (sectionIds[i] === UsNat.ID) {
                            let section = new UsNat(encodedSections[i + 1]);
                            sections.set(UsNat.NAME, section);
                            /*
                                      } else if (sectionIds[i] === UsCa.ID) {
                                        let section = new UsCa(encodedSections[i + 1]);
                                        sections.set(UsCa.NAME, section);
                                      } else if (sectionIds[i] === UsVa.ID) {
                                        let section = new UsVa(encodedSections[i + 1]);
                                        sections.set(UsVa.NAME, section);
                                      } else if (sectionIds[i] === UsCo.ID) {
                                        let section = new UsCo(encodedSections[i + 1]);
                                        sections.set(UsCo.NAME, section);
                                      } else if (sectionIds[i] === UsUt.ID) {
                                        let section = new UsUt(encodedSections[i + 1]);
                                        sections.set(UsUt.NAME, section);
                                      } else if (sectionIds[i] === UsCt.ID) {
                                        let section = new UsCt(encodedSections[i + 1]);
                                        sections.set(UsCt.NAME, section);
                                      } else if (sectionIds[i] === UsFl.ID) {
                                        let section = new UsFl(encodedSections[i + 1]);
                                        sections.set(UsFl.NAME, section);
                                      } else if (sectionIds[i] === UsMt.ID) {
                                        let section = new UsMt(encodedSections[i + 1]);
                                        sections.set(UsMt.NAME, section);
                                      } else if (sectionIds[i] === UsOr.ID) {
                                        let section = new UsOr(encodedSections[i + 1]);
                                        sections.set(UsOr.NAME, section);
                                      } else if (sectionIds[i] === UsTx.ID) {
                                        let section = new UsTx(encodedSections[i + 1]);
                                        sections.set(UsTx.NAME, section);
                            */
                        }
                    }
                }
                return sections;
            }
            else if (str.startsWith("C")) {
                // old tcfeu only string
                let sections = new Map();
                let section = new TcfEuV2(str);
                sections.set(TcfEuV2.NAME, section);
                let header = new HeaderV1();
                header.setFieldValue(HeaderV1Field.SECTION_IDS, [2]);
                sections.set(HeaderV1.NAME, section);
                return sections;
            }
            else {
                throw new DecodingError("Unable to decode '" + str + "'");
            }
        }
        encodeSection(sectionName) {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            if (this.sections.has(sectionName)) {
                return this.sections.get(sectionName).encode();
            }
            else {
                return null;
            }
        }
        encodeSectionById(sectionId) {
            return this.encodeSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        decodeSection(sectionName, encodedString) {
            let section = null;
            if (!this.sections.has(sectionName)) {
                if (sectionName === TcfCaV1.NAME) {
                    section = new TcfCaV1();
                    this.sections.set(TcfCaV1.NAME, section);
                }
                else if (sectionName === TcfEuV2.NAME) {
                    section = new TcfEuV2();
                    this.sections.set(TcfEuV2.NAME, section);
                }
                else if (sectionName === UspV1.NAME) {
                    section = new UspV1();
                    this.sections.set(UspV1.NAME, section);
                }
                else if (sectionName === UsNat.NAME) {
                    section = new UsNat();
                    this.sections.set(UsNat.NAME, section);
                    /*
                          } else if (sectionName === UsCa.NAME) {
                            section = new UsCa();
                            this.sections.set(UsCa.NAME, section);
                          } else if (sectionName === UsVa.NAME) {
                            section = new UsVa();
                            this.sections.set(UsVa.NAME, section);
                          } else if (sectionName === UsCo.NAME) {
                            section = new UsCo();
                            this.sections.set(UsCo.NAME, section);
                          } else if (sectionName === UsUt.NAME) {
                            section = new UsUt();
                            this.sections.set(UsUt.NAME, section);
                          } else if (sectionName === UsCt.NAME) {
                            section = new UsCt();
                            this.sections.set(UsCt.NAME, section);
                          } else if (sectionName === UsFl.NAME) {
                            section = new UsFl();
                            this.sections.set(UsFl.NAME, section);
                          } else if (sectionName === UsMt.NAME) {
                            section = new UsMt();
                            this.sections.set(UsMt.NAME, section);
                          } else if (sectionName === UsOr.NAME) {
                            section = new UsOr();
                            this.sections.set(UsOr.NAME, section);
                          } else if (sectionName === UsTx.NAME) {
                            section = new UsTx();
                            this.sections.set(UsTx.NAME, section);
                    */
                }
            }
            else {
                section = this.sections.get(sectionName);
            }
            if (section) {
                section.decode(encodedString);
            }
        }
        decodeSectionById(sectionId, encodedString) {
            this.decodeSection(Sections.SECTION_ID_NAME_MAP.get(sectionId), encodedString);
        }
        toObject() {
            if (!this.decoded) {
                this.sections = this.decodeModel(this.encodedString);
                this.dirty = false;
                this.decoded = true;
            }
            let obj = {};
            for (let i = 0; i < Sections.SECTION_ORDER.length; i++) {
                let sectionName = Sections.SECTION_ORDER[i];
                if (this.sections.has(sectionName)) {
                    obj[sectionName] = this.sections.get(sectionName).toObj();
                }
            }
            return obj;
        }
        encode() {
            if (this.encodedString == null || this.encodedString.length === 0 || this.dirty) {
                this.encodedString = this.encodeModel(this.sections);
                this.dirty = false;
                this.decoded = true;
            }
            return this.encodedString;
        }
        decode(encodedString) {
            this.encodedString = encodedString;
            this.dirty = false;
            this.decoded = false;
        }
    }

    var SignalStatus;
    (function (SignalStatus) {
        SignalStatus["NOT_READY"] = "not ready";
        SignalStatus["READY"] = "ready";
    })(SignalStatus || (SignalStatus = {}));

    /**
     * Class holds shareable data across cmp api and provides change event listener for GppModel.
     * Within the context of the CmpApi, this class acts much like a global state or database,
     * where CmpApi sets data and Commands read the data.
     */
    class CmpApiContext {
        constructor() {
            this.gppVersion = "1.1";
            this.supportedAPIs = [];
            this.eventQueue = new EventListenerQueue(this);
            this.cmpStatus = CmpStatus.LOADING;
            this.cmpDisplayStatus = CmpDisplayStatus.HIDDEN;
            this.signalStatus = SignalStatus.NOT_READY;
            this.applicableSections = [];
            this.gppModel = new GppModel();
        }
        reset() {
            this.eventQueue.clear();
            this.cmpStatus = CmpStatus.LOADING;
            this.cmpDisplayStatus = CmpDisplayStatus.HIDDEN;
            this.signalStatus = SignalStatus.NOT_READY;
            this.applicableSections = [];
            this.supportedAPIs = [];
            this.gppModel = new GppModel();
            delete this.cmpId;
            delete this.cmpVersion;
            delete this.eventStatus;
        }
    }

    var GppCommand;
    (function (GppCommand) {
        GppCommand["ADD_EVENT_LISTENER"] = "addEventListener";
        GppCommand["GET_FIELD"] = "getField";
        GppCommand["GET_SECTION"] = "getSection";
        GppCommand["HAS_SECTION"] = "hasSection";
        GppCommand["PING"] = "ping";
        GppCommand["REMOVE_EVENT_LISTENER"] = "removeEventListener";
    })(GppCommand || (GppCommand = {}));

    class Command {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        constructor(cmpApiContext, callback, parameter) {
            this.success = true;
            this.cmpApiContext = cmpApiContext;
            Object.assign(this, {
                callback,
                parameter,
            });
        }
        execute() {
            try {
                return this.respond();
            }
            catch (error) {
                this.invokeCallback(null);
                return null;
            }
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        invokeCallback(response) {
            const success = response !== null;
            if (this.callback) {
                this.callback(response, success);
            }
        }
    }

    class PingCommand extends Command {
        respond() {
            let pingReturn = new PingData(this.cmpApiContext);
            this.invokeCallback(pingReturn);
        }
    }

    class GetFieldCommand extends Command {
        respond() {
            if (!this.parameter || this.parameter.length === 0) {
                throw new Error("<section>.<field> parameter required");
            }
            let parts = this.parameter.split(".");
            if (parts.length != 2) {
                throw new Error("Field name must be in the format <section>.<fieldName>");
            }
            let sectionName = parts[0];
            let fieldName = parts[1];
            let fieldValue = null;
            if (this.parameter != "tcfeuv2") {
                fieldValue = this.cmpApiContext.gppModel.getFieldValue(sectionName, fieldName);
            }
            this.invokeCallback(fieldValue);
        }
    }

    class GetSectionCommand extends Command {
        respond() {
            if (!this.parameter || this.parameter.length === 0) {
                throw new Error("<section> parameter required");
            }
            let section = null;
            if (this.parameter != "tcfeuv2") {
                if (this.cmpApiContext.gppModel.hasSection(this.parameter)) {
                    section = this.cmpApiContext.gppModel.getSection(this.parameter);
                }
            }
            this.invokeCallback(section);
        }
    }

    class HasSectionCommand extends Command {
        respond() {
            if (!this.parameter || this.parameter.length === 0) {
                throw new Error("<section>[.version] parameter required");
            }
            let hasSection = this.cmpApiContext.gppModel.hasSection(this.parameter);
            this.invokeCallback(hasSection);
        }
    }

    class AddEventListenerCommand extends Command {
        respond() {
            let listenerId = this.cmpApiContext.eventQueue.add({
                callback: this.callback,
                parameter: this.parameter,
            });
            let eventData = new EventData("listenerRegistered", listenerId, true, new PingData(this.cmpApiContext));
            this.invokeCallback(eventData);
        }
    }

    class RemoveEventListenerCommand extends Command {
        respond() {
            let listenerId = this.parameter;
            let removed = this.cmpApiContext.eventQueue.remove(listenerId);
            let eventData = new EventData("listenerRemoved", listenerId, removed, new PingData(this.cmpApiContext));
            this.invokeCallback(eventData);
        }
    }

    var _a, _b, _c, _d, _e, _f;
    class CommandMap {
    }
    _a = GppCommand.ADD_EVENT_LISTENER, _b = GppCommand.GET_FIELD, _c = GppCommand.GET_SECTION, _d = GppCommand.HAS_SECTION, _e = GppCommand.PING, _f = GppCommand.REMOVE_EVENT_LISTENER;
    CommandMap[_a] = AddEventListenerCommand;
    CommandMap[_b] = GetFieldCommand;
    CommandMap[_c] = GetSectionCommand;
    CommandMap[_d] = HasSectionCommand;
    CommandMap[_e] = PingCommand;
    CommandMap[_f] = RemoveEventListenerCommand;

    class CallResponder {
        constructor(cmpApiContext, customCommands) {
            this.cmpApiContext = cmpApiContext;
            if (customCommands) {
                /**
                 * The addEventListener command and removeEventListener are the only ones
                 * that shouldn't be overwritten. The addEventListener command utilizes
                 * getTCData command, so overridding the TCData response should happen
                 * there.
                 */
                let command = GppCommand.ADD_EVENT_LISTENER;
                if (customCommands === null || customCommands === void 0 ? void 0 : customCommands[command]) {
                    throw new Error(`Built-In Custom Commmand for ${command} not allowed`);
                }
                command = GppCommand.REMOVE_EVENT_LISTENER;
                if (customCommands === null || customCommands === void 0 ? void 0 : customCommands[command]) {
                    throw new Error(`Built-In Custom Commmand for ${command} not allowed`);
                }
                this.customCommands = customCommands;
            }
            try {
                // get queued commands
                this.callQueue = window["__gpp"]() || [];
            }
            catch (err) {
                this.callQueue = [];
            }
            finally {
                window["__gpp"] = this.apiCall.bind(this);
                this.purgeQueuedCalls();
            }
        }
        /**
         * Handler for all page call commands
         * @param {string} command
         * @param {CommandCallback} callback
         * @param {any} param
         * @param {number} version
         */
        apiCall(command, callback, parameter, version) {
            if (typeof command !== "string") {
                callback(null, false);
            }
            else if (command === "events") {
                return this.cmpApiContext.eventQueue.events();
            }
            else if (callback && typeof callback !== "function") {
                throw new Error("invalid callback function");
            }
            else if (this.isCustomCommand(command)) {
                this.customCommands[command](callback, parameter);
            }
            else if (this.isBuiltInCommand(command)) {
                new CommandMap[command](this.cmpApiContext, callback, parameter).execute();
            }
            else {
                if (callback) {
                    callback(null, false);
                }
            }
        }
        purgeQueuedCalls() {
            const queueCopy = this.callQueue;
            this.callQueue = [];
            queueCopy.forEach((args) => {
                window["__gpp"](...args);
            });
        }
        isCustomCommand(command) {
            return this.customCommands && typeof this.customCommands[command] === "function";
        }
        isBuiltInCommand(command) {
            return CommandMap[command] !== undefined;
        }
    }

    class JsonHttpClient {
        static absCall(url, body, sendCookies, timeout) {
            return new Promise((resolve, reject) => {
                const req = new XMLHttpRequest();
                const onLoad = () => {
                    // is the response done
                    if (req.readyState == XMLHttpRequest.DONE) {
                        /**
                         * For our purposes if it's not a 200 range response, then it's a
                         * failure.
                         */
                        if (req.status >= 200 && req.status < 300) {
                            let response = req.response;
                            if (typeof response === "string") {
                                try {
                                    response = JSON.parse(response);
                                }
                                catch (e) { }
                            }
                            resolve(response);
                        }
                        else {
                            reject(new Error(`HTTP Status: ${req.status} response type: ${req.responseType}`));
                        }
                    }
                };
                const onError = () => {
                    reject(new Error("error"));
                };
                const onAbort = () => {
                    reject(new Error("aborted"));
                };
                const onTimeout = () => {
                    reject(new Error("Timeout " + timeout + "ms " + url));
                };
                req.withCredentials = sendCookies;
                req.addEventListener("load", onLoad);
                req.addEventListener("error", onError);
                req.addEventListener("abort", onAbort);
                if (body === null) {
                    req.open("GET", url, true);
                }
                else {
                    req.open("POST", url, true);
                }
                req.responseType = "json";
                // IE has a problem if this is before the open
                req.timeout = timeout;
                req.ontimeout = onTimeout;
                req.send(body);
            });
        }
        /**
         * @static
         * @param {string} url - full path to POST to
         * @param {object} body - JSON object to post
         * @param {boolean} sendCookies - Whether or not to send the XMLHttpRequest with credentials or not
         * @param {number} [timeout] - optional timeout in milliseconds
         * @return {Promise<object>} - if the server responds the response will be returned here
         */
        static post(url, body, sendCookies = false, timeout = 0) {
            return this.absCall(url, JSON.stringify(body), sendCookies, timeout);
        }
        /**
         * @static
         * @param {string} url - full path to the json
         * @param {boolean} sendCookies - Whether or not to send the XMLHttpRequest with credentials or not
         * @param {number} [timeout] - optional timeout in milliseconds
         * @return {Promise<object>} - resolves with parsed JSON
         */
        static fetch(url, sendCookies = false, timeout = 0) {
            return this.absCall(url, null, sendCookies, timeout);
        }
    }

    /**
     * class for General GVL Errors
     *
     * @extends {Error}
     */
    class GVLError extends Error {
        /**
         * constructor - constructs a GVLError
         *
         * @param {string} msg - Error message to display
         * @return {undefined}
         */
        constructor(msg) {
            super(msg);
            this.name = "GVLError";
        }
    }

    class ConsentLanguages {
        has(key) {
            return ConsentLanguages.langSet.has(key);
        }
        forEach(callback) {
            ConsentLanguages.langSet.forEach(callback);
        }
        get size() {
            return ConsentLanguages.langSet.size;
        }
    }
    ConsentLanguages.langSet = new Set([
        "BG",
        "CA",
        "CS",
        "DA",
        "DE",
        "EL",
        "EN",
        "ES",
        "ET",
        "FI",
        "FR",
        "HR",
        "HU",
        "IT",
        "JA",
        "LT",
        "LV",
        "MT",
        "NL",
        "NO",
        "PL",
        "PT",
        "RO",
        "RU",
        "SK",
        "SL",
        "SV",
        "TR",
        "ZH",
    ]);

    var __awaiter$1 = (window && window.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    /**
     * class with utilities for managing the global vendor list.  Will use JSON to
     * fetch the vendor list from specified url and will serialize it into this
     * object and provide accessors.  Provides ways to group vendors on the list by
     * purpose and feature.
     */
    class GVL {
        constructor() {
            this.consentLanguages = new ConsentLanguages();
            this.language = GVL.DEFAULT_LANGUAGE;
            this.ready = false;
            this.languageFilename = "purposes-[LANG].json";
        }
        static fromVendorList(vendorList) {
            let gvl = new GVL();
            gvl.populate(vendorList);
            return gvl;
        }
        /**
         * **
         * @param {GVLConfig} - Configuration containing url configuration
         *
         * @throws {GVLError} - If the url is http[s]://vendorlist.consensu.org/...
         * this will throw an error.  IAB Europe requires that that CMPs and Vendors
         * cache their own copies of the GVL to minimize load on their
         * infrastructure.  For more information regarding caching of the
         * vendor-list.json, please see [the TCF documentation on 'Caching the Global
         * Vendor List'
         */
        static fromUrl(config) {
            return __awaiter$1(this, void 0, void 0, function* () {
                let baseUrl = config.baseUrl;
                if (!baseUrl || baseUrl.length === 0) {
                    throw new GVLError("Invalid baseUrl: '" + baseUrl + "'");
                }
                if (/^https?:\/\/vendorlist\.consensu\.org\//.test(baseUrl)) {
                    throw new GVLError("Invalid baseUrl!  You may not pull directly from vendorlist.consensu.org and must provide your own cache");
                }
                // if a trailing slash was forgotten
                if (baseUrl.length > 0 && baseUrl[baseUrl.length - 1] !== "/") {
                    baseUrl += "/";
                }
                let gvl = new GVL();
                gvl.baseUrl = baseUrl;
                if (config.languageFilename) {
                    gvl.languageFilename = config.languageFilename;
                }
                else {
                    gvl.languageFilename = "purposes-[LANG].json";
                }
                if (config.version > 0) {
                    let versionedFilename = config.versionedFilename;
                    if (!versionedFilename) {
                        versionedFilename = "archives/vendor-list-v[VERSION].json";
                    }
                    let url = baseUrl + versionedFilename.replace("[VERSION]", String(config.version));
                    gvl.populate((yield JsonHttpClient.fetch(url)));
                }
                else {
                    /**
                     * whatever it is (or isn't)... it doesn't matter we'll just get the latest.
                     */
                    let latestFilename = config.latestFilename;
                    if (!latestFilename) {
                        latestFilename = "vendor-list.json";
                    }
                    let url = baseUrl + latestFilename;
                    gvl.populate((yield JsonHttpClient.fetch(url)));
                }
                return gvl;
            });
        }
        /**
         * changeLanguage - retrieves the purpose language translation and sets the
         * internal language variable
         *
         * @param {string} lang - ISO 639-1 langauge code to change language to
         * @return {Promise<void | GVLError>} - returns the `readyPromise` and
         * resolves when this GVL is populated with the data from the language file.
         */
        changeLanguage(lang) {
            return __awaiter$1(this, void 0, void 0, function* () {
                const langUpper = lang.toUpperCase();
                if (this.consentLanguages.has(langUpper)) {
                    if (langUpper !== this.language) {
                        this.language = langUpper;
                        const url = this.baseUrl + this.languageFilename.replace("[LANG]", lang);
                        try {
                            this.populate((yield JsonHttpClient.fetch(url)));
                        }
                        catch (err) {
                            throw new GVLError("unable to load language: " + err.message);
                        }
                    }
                }
                else {
                    throw new GVLError(`unsupported language ${lang}`);
                }
            });
        }
        /**
         * getJson - Method for getting the JSON that was downloaded to created this
         * `GVL` object
         *
         * @return {VendorList} - The basic JSON structure without the extra
         * functionality and methods of this class.
         */
        getJson() {
            return JSON.parse(JSON.stringify({
                gvlSpecificationVersion: this.gvlSpecificationVersion,
                vendorListVersion: this.vendorListVersion,
                tcfPolicyVersion: this.tcfPolicyVersion,
                lastUpdated: this.lastUpdated,
                purposes: this.purposes,
                specialPurposes: this.specialPurposes,
                features: this.features,
                specialFeatures: this.specialFeatures,
                stacks: this.stacks,
                dataCategories: this.dataCategories,
                vendors: this.fullVendorList,
            }));
        }
        isVendorList(gvlObject) {
            return gvlObject !== undefined && gvlObject.vendors !== undefined;
        }
        populate(gvlObject) {
            /**
             * these are populated regardless of whether it's a Declarations file or
             * a VendorList
             */
            this.purposes = gvlObject.purposes;
            this.specialPurposes = gvlObject.specialPurposes;
            this.features = gvlObject.features;
            this.specialFeatures = gvlObject.specialFeatures;
            this.stacks = gvlObject.stacks;
            this.dataCategories = gvlObject.dataCategories;
            if (this.isVendorList(gvlObject)) {
                this.gvlSpecificationVersion = gvlObject.gvlSpecificationVersion;
                this.tcfPolicyVersion = gvlObject.tcfPolicyVersion;
                this.vendorListVersion = gvlObject.vendorListVersion;
                this.lastUpdated = gvlObject.lastUpdated;
                if (typeof this.lastUpdated === "string") {
                    this.lastUpdated = new Date(this.lastUpdated);
                }
                this.vendors = gvlObject.vendors;
                this.fullVendorList = gvlObject.vendors;
                this.mapVendors();
                this.ready = true;
            }
        }
        mapVendors(vendorIds) {
            // create new instances of the maps
            this.byPurposeVendorMap = {};
            this.bySpecialPurposeVendorMap = {};
            this.byFeatureVendorMap = {};
            this.bySpecialFeatureVendorMap = {};
            // initializes data structure for purpose map
            Object.keys(this.purposes).forEach((purposeId) => {
                this.byPurposeVendorMap[purposeId] = {
                    legInt: new Set(),
                    impCons: new Set(),
                    consent: new Set(),
                    flexible: new Set(),
                };
            });
            // initializes data structure for special purpose map
            Object.keys(this.specialPurposes).forEach((purposeId) => {
                this.bySpecialPurposeVendorMap[purposeId] = new Set();
            });
            // initializes data structure for feature map
            Object.keys(this.features).forEach((featureId) => {
                this.byFeatureVendorMap[featureId] = new Set();
            });
            // initializes data structure for feature map
            Object.keys(this.specialFeatures).forEach((featureId) => {
                this.bySpecialFeatureVendorMap[featureId] = new Set();
            });
            if (!Array.isArray(vendorIds)) {
                vendorIds = Object.keys(this.fullVendorList).map((vId) => +vId);
            }
            this.vendorIds = new Set(vendorIds);
            // assigns vendor ids to their respective maps
            this.vendors = vendorIds.reduce((vendors, vendorId) => {
                const vendor = this.vendors[String(vendorId)];
                if (vendor && vendor.deletedDate === undefined) {
                    vendor.purposes.forEach((purposeId) => {
                        const purpGroup = this.byPurposeVendorMap[String(purposeId)];
                        purpGroup.consent.add(vendorId);
                    });
                    vendor.specialPurposes.forEach((purposeId) => {
                        this.bySpecialPurposeVendorMap[String(purposeId)].add(vendorId);
                    });
                    if (vendor.legIntPurposes) {
                        vendor.legIntPurposes.forEach((purposeId) => {
                            this.byPurposeVendorMap[String(purposeId)].legInt.add(vendorId);
                        });
                    }
                    // canada has added impConsPurposes in lieu of europe's legIntPurposes
                    if (vendor.impConsPurposes) {
                        vendor.impConsPurposes.forEach((purposeId) => {
                            this.byPurposeVendorMap[String(purposeId)].impCons.add(vendorId);
                        });
                    }
                    // could not be there
                    if (vendor.flexiblePurposes) {
                        vendor.flexiblePurposes.forEach((purposeId) => {
                            this.byPurposeVendorMap[String(purposeId)].flexible.add(vendorId);
                        });
                    }
                    vendor.features.forEach((featureId) => {
                        this.byFeatureVendorMap[String(featureId)].add(vendorId);
                    });
                    vendor.specialFeatures.forEach((featureId) => {
                        this.bySpecialFeatureVendorMap[String(featureId)].add(vendorId);
                    });
                    vendors[vendorId] = vendor;
                }
                return vendors;
            }, {});
        }
        getFilteredVendors(purposeOrFeature, id, subType, special) {
            const properPurposeOrFeature = purposeOrFeature.charAt(0).toUpperCase() + purposeOrFeature.slice(1);
            let vendorSet;
            const retr = {};
            if (purposeOrFeature === "purpose" && subType) {
                vendorSet = this["by" + properPurposeOrFeature + "VendorMap"][String(id)][subType];
            }
            else {
                vendorSet = this["by" + (special ? "Special" : "") + properPurposeOrFeature + "VendorMap"][String(id)];
            }
            vendorSet.forEach((vendorId) => {
                retr[String(vendorId)] = this.vendors[String(vendorId)];
            });
            return retr;
        }
        /**
         * getVendorsWithConsentPurpose
         *
         * @param {number} purposeId
         * @return {IntMap<Vendor>} - list of vendors that have declared the consent purpose id
         */
        getVendorsWithConsentPurpose(purposeId) {
            return this.getFilteredVendors("purpose", purposeId, "consent");
        }
        /**
         * getVendorsWithLegIntPurpose
         *
         * @param {number} purposeId
         * @return {IntMap<Vendor>} - list of vendors that have declared the legInt (Legitimate Interest) purpose id
         */
        getVendorsWithLegIntPurpose(purposeId) {
            return this.getFilteredVendors("purpose", purposeId, "legInt");
        }
        /**
         * getVendorsWithFlexiblePurpose
         *
         * @param {number} purposeId
         * @return {IntMap<Vendor>} - list of vendors that have declared the flexible purpose id
         */
        getVendorsWithFlexiblePurpose(purposeId) {
            return this.getFilteredVendors("purpose", purposeId, "flexible");
        }
        /**
         * getVendorsWithSpecialPurpose
         *
         * @param {number} specialPurposeId
         * @return {IntMap<Vendor>} - list of vendors that have declared the special purpose id
         */
        getVendorsWithSpecialPurpose(specialPurposeId) {
            return this.getFilteredVendors("purpose", specialPurposeId, undefined, true);
        }
        /**
         * getVendorsWithFeature
         *
         * @param {number} featureId
         * @return {IntMap<Vendor>} - list of vendors that have declared the feature id
         */
        getVendorsWithFeature(featureId) {
            return this.getFilteredVendors("feature", featureId);
        }
        /**
         * getVendorsWithSpecialFeature
         *
         * @param {number} specialFeatureId
         * @return {IntMap<Vendor>} - list of vendors that have declared the special feature id
         */
        getVendorsWithSpecialFeature(specialFeatureId) {
            return this.getFilteredVendors("feature", specialFeatureId, undefined, true);
        }
        /**
         * narrowVendorsTo - narrows vendors represented in this GVL to the list of ids passed in
         *
         * @param {number[]} vendorIds - list of ids to narrow this GVL to
         * @return {void}
         */
        narrowVendorsTo(vendorIds) {
            this.mapVendors(vendorIds);
        }
        /**
         * isReady - Whether or not this instance is ready to be used.  This will be
         * immediately and synchronously true if a vendorlist object is passed into
         * the constructor or once the JSON vendorllist is retrieved.
         *
         * @return {boolean} whether or not the instance is ready to be interacted
         * with and all the data is populated
         */
        get isReady() {
            return this.ready;
        }
        static isInstanceOf(questionableInstance) {
            const isSo = typeof questionableInstance === "object";
            return isSo && typeof questionableInstance.narrowVendorsTo === "function";
        }
    }
    GVL.DEFAULT_LANGUAGE = "EN";

    var __awaiter = (window && window.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    class CmpApi {
        /**
         * @param {number} cmpId - IAB assigned CMP ID
         * @param {number} cmpVersion - integer version of the CMP
         * @param {CustomCommands} [customCommands] - custom commands from the cmp
         */
        constructor(cmpId, cmpVersion, customCommands) {
            this.cmpApiContext = new CmpApiContext();
            this.cmpApiContext.cmpId = cmpId;
            this.cmpApiContext.cmpVersion = cmpVersion;
            this.callResponder = new CallResponder(this.cmpApiContext, customCommands);
        }
        fireEvent(eventName, value) {
            this.cmpApiContext.eventQueue.exec(eventName, value);
        }
        fireErrorEvent(value) {
            this.cmpApiContext.eventQueue.exec("error", value);
        }
        fireSectionChange(value) {
            this.cmpApiContext.eventQueue.exec("sectionChange", value);
        }
        getEventStatus() {
            return this.cmpApiContext.eventStatus;
        }
        setEventStatus(eventStatus) {
            this.cmpApiContext.eventStatus = eventStatus;
        }
        getCmpStatus() {
            return this.cmpApiContext.cmpStatus;
        }
        setCmpStatus(cmpStatus) {
            this.cmpApiContext.cmpStatus = cmpStatus;
            this.cmpApiContext.eventQueue.exec("cmpStatus", cmpStatus);
        }
        getCmpDisplayStatus() {
            return this.cmpApiContext.cmpDisplayStatus;
        }
        setCmpDisplayStatus(cmpDisplayStatus) {
            this.cmpApiContext.cmpDisplayStatus = cmpDisplayStatus;
            this.cmpApiContext.eventQueue.exec("cmpDisplayStatus", cmpDisplayStatus);
        }
        getSignalStatus() {
            return this.cmpApiContext.signalStatus;
        }
        setSignalStatus(signalStatus) {
            this.cmpApiContext.signalStatus = signalStatus;
            this.cmpApiContext.eventQueue.exec("signalStatus", signalStatus);
        }
        getApplicableSections() {
            return this.cmpApiContext.applicableSections;
        }
        setApplicableSections(applicableSections) {
            this.cmpApiContext.applicableSections = applicableSections;
        }
        getSupportedAPIs() {
            return this.cmpApiContext.supportedAPIs;
        }
        setSupportedAPIs(supportedAPIs) {
            this.cmpApiContext.supportedAPIs = supportedAPIs;
        }
        setGppString(encodedGppString) {
            this.cmpApiContext.gppModel.decode(encodedGppString);
        }
        getGppString() {
            return this.cmpApiContext.gppModel.encode();
        }
        setSectionString(sectionName, encodedSectionString) {
            this.cmpApiContext.gppModel.decodeSection(sectionName, encodedSectionString);
        }
        setSectionStringById(sectionId, encodedSectionString) {
            this.setSectionString(Sections.SECTION_ID_NAME_MAP.get(sectionId), encodedSectionString);
        }
        getSectionString(sectionName) {
            return this.cmpApiContext.gppModel.encodeSection(sectionName);
        }
        getSectionStringById(sectionId) {
            return this.getSectionString(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        setFieldValue(sectionName, fieldName, value) {
            this.cmpApiContext.gppModel.setFieldValue(sectionName, fieldName, value);
        }
        setFieldValueBySectionId(sectionId, fieldName, value) {
            this.setFieldValue(Sections.SECTION_ID_NAME_MAP.get(sectionId), fieldName, value);
        }
        getFieldValue(sectionName, fieldName) {
            return this.cmpApiContext.gppModel.getFieldValue(sectionName, fieldName);
        }
        getFieldValueBySectionId(sectionId, fieldName) {
            return this.getFieldValue(Sections.SECTION_ID_NAME_MAP.get(sectionId), fieldName);
        }
        getSection(sectionName) {
            return this.cmpApiContext.gppModel.getSection(sectionName);
        }
        getSectionById(sectionId) {
            return this.getSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        hasSection(sectionName) {
            return this.cmpApiContext.gppModel.hasSection(sectionName);
        }
        hasSectionId(sectionId) {
            return this.hasSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        deleteSection(sectionName) {
            this.cmpApiContext.gppModel.deleteSection(sectionName);
        }
        deleteSectionById(sectionId) {
            this.deleteSection(Sections.SECTION_ID_NAME_MAP.get(sectionId));
        }
        clear() {
            this.cmpApiContext.gppModel.clear();
        }
        getObject() {
            return this.cmpApiContext.gppModel.toObject();
        }
        getGvlFromVendorList(vendorList) {
            return GVL.fromVendorList(vendorList);
        }
        getGvlFromUrl(gvlUrlConfig) {
            return __awaiter(this, void 0, void 0, function* () {
                return GVL.fromUrl(gvlUrlConfig);
            });
        }
    }

    window.WBD = window.WBD || {};
    window.WM = window.WM || {};

    /**
     * Polyfill for CustomEvent constructor for old browsers
     */
    (function (win, doc) {

        if (typeof win.CustomEvent !== 'function') {
            var CustomEvent = function _CustomEvent(event, params) {
                    var evt;

                    params = params || { bubbles: false, cancelable: false, detail: undefined };
                    evt = doc.createEvent('CustomEvent');
                    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                    return evt;
                };

            CustomEvent.prototype = win.Event.prototype;
            win.CustomEvent = CustomEvent;
            if (win.Event !== 'function') win.Event = CustomEvent;
        }
    })(window, document);


    /**
     * User Consent checking and setup of global object
     *
     * @name WBD.UserConsent
     * @namespace
     * @memberOf WBD
     */
    window.WBD.UserConsent = window.WBD.UserConsent || (function UserConsent(win, doc) {

        var acString = '',
            frameCallId = 0,
            categories,
            cmpCallbacks = {},
            config = {},
            consentConfirmed = false,
            consentHistory = [],
            consentId = '',
            consentInteractions = 0,
            consentSource = '',
            consentState = null,
            consentTime = null,
            consentVersion = 'unknown',
            controls = {},
            dbg = false,
            eventCallbacks = {},
            frame = '',
            geoCountry = '',
            geoState = '',
            geoCS = '',
            gppApisAvail = {
                tcfeuv2: 2,
                tcfcav1: 5,
                uspv1: 6,
                usnat: 7
            },
            gppCmpApi = null,
            gppCmpId = 1,
            gppString = '',
            gppStringVer = '',
            gppStub = null,
            oneTrustLocationSet = false,
            optanonLoaded = false,
            otBlocked,
            otFailed,
            pageLang = 'en',
            parsableTypes = {
                'binary': true,
                'boolean': true,
                'trinary': true,
                'integer': true
            },
            tcString = '',
            tcStringVer = '',
            ucChild = false,
            userConsentVersion = '4.1.23-nba',
            usingCmpFrame = null,
            usingCompatMode = false,
            usingGpc = false,
            usingGppFrame = null,
            usingIabCcpa = false,
            usingIabGdpr = false,
            usingIabGpp = false,
            usingOTGpp = false,
            usingUspFrame = null,
            uspString = '',
            defaults = 
    /* Default UserConsent configuration for basic WBD configs. */
    /* See https://github.com/turnercode/user-consent#readme for details. */
    {
        addtlConsentCookie: 'OTAdditionalConsentString',
        adChoicesLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
        adChoicesLinkTitle: {
            ar: 'اختيارات الإعلان',
            en: 'Ad Choices',
            de: 'Anzeigenauswahl',
            es: 'Elecciones de anuncios',
            fr: 'Choix d’annonces'
        },
        affiliatesLinkAction: 'https://www.wbdprivacy.com/policycenter/affiliates/',
        affiliatesLinkTitle: {
            ar: 'الشركات التابعة',
            en: 'Affiliates',
            de: 'Mitgliedsorganisationen',
            es: 'Afiliadas',
            fr: 'Affiliées'
        },
        categories: {
            sc: 'required',            /* Built-in */
            tpv: 'vendor',
            fc: 'functional',
            mc: 'media',
            pc: 'performance',
            tc: 'targeting',
            ftc: '1p-targeting'
        },
        ccCookie: 'countryCode',
        ccpaGeos: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
        confirmCookie: 'OptanonAlertBoxClosed',
        consentChangeAction: null,
        consentChangeActionDelay: 500,
        consentCookie: 'OptanonConsent',
        consentDefaults: {
            'required': true,
            'vendor': true,
            'functional': true,
            'media': true,
            'performance': true,
            'targeting': true,
            '1p-targeting': true
        },
        consentExpireIn: 1,
        consentLinkTitle: {
            ar: 'ملفات تعريف الارتباط',
            de: 'Cookie-Einstellungen',
            en: 'Cookie Settings',
            es: 'Configuración de Cookies',
            fr: 'Paramètres des Cookies'
        },
        consentNotApplicable: [],
        controlCookie: 'OptanonControl',
        cookieSameSite: 'Lax',
        cookieSecure: false,
        enableDebug: false,
        enableGPC: true,
        enableTransitionCheck: true,
        enableWebViewCheck: true,
        defaultCountry: 'US',
        defaultLanguage: 'en',
        defaultState: '',
        gdprIabCookie: 'eupubconsent-v2',
        geoCheckFunction: null,
        geoPassedToOneTrust: true,
        gpcFixCookie: '',
        gppCategories: {
            usnat: [
                {
                    field: 'SharingNotice',
                    type: 'trinary',
                    default: 1
                },
                {
                    field: 'SaleOptOutNotice',
                    type: 'trinary',
                    default: 1
                },
                {
                    field: 'SharingOptOutNotice',
                    type: 'trinary',
                    default: 1
                },
                {
                    field: 'TargetedAdvertisingOptOutNotice',
                    type: 'trinary',
                    default: 1
                },
                {
                    field: 'SharingOptOut',
                    type: 'trinary',
                    val: 'vendor'
                },
                {
                    field: 'SaleOptOut',
                    type: 'trinary',
                    val: 'vendor'
                },
                {
                    field: 'TargetedAdvertisingOptOut',
                    type: 'trinary',
                    val: 'targeting'
                }
            ],
            uspv1: [
                {
                    field: 'OptOutSale',
                    type: 'binary',
                    val: ['vendor', 'targeting']
                }
            ]
        },
        gppIabCookie: 'OTGPPConsent',
        gppSection: '',
        iabRegion: '',
        languageFromBrowser: true,
        oneTrustLoadTimeout: 10000,
        privacyCenterLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
        privacyCenterLinkTitle: {
            ar: 'سياسة خصوصية المستهلك',
            de: 'Datenschutzhinweise',
            en: 'Privacy Policy',
            es: 'Política de Privacidad',
            fr: 'Politique de Confidentialité'
        },
        regionChangeAction: null,
        regions: [
            {
                id: 'us',
                compatCodes: {
                    req: ['sc'],
                    ven: ['tpv']
                },
                consentExpireIn: 3,
                consentGpcDefaults: {
                    'vendor': false,
                    'targeting': false
                },
                consentImpliedDefaults: {
                    'required': true,
                    'functional': true,
                    'media': true,
                    'performance': true,
                    '1p-targeting': true
                },
                consentLinkTitle: {
                    ar: 'لا تبيع أو تشارك معلوماتي الشخصية',
                    de: 'Nicht Verkauf oder Nicht Weitergabe Ihrer personenbezogenen Daten zu stellen',
                    en: 'Do Not Sell Or Share My Personal Information',
                    es: 'No Venda Vi Comparta Mi Información Personal',
                    fr: 'Ne pas vendre ni partager mes informations personnelles'
                },
                rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/usstatesupplement/en-us/',
                geoMatch: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
                gppSection: 'usnat',  /* usnat */
                iabRegion: 'ccpa'
            },
            {
                id: 'gdpr',
                consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                    'functional': false,
                    'media': false,
                    'performance': false,
                    'targeting': false
                },
                consentImpliedDefaults: {
                    'vendor': true
                },
                consentLinkTitle: {
                    ar: 'إدارة ملفات تعريف الارتباط+',
                    de: 'Cookies Verwalten+',
                    en: 'Manage Cookies+',
                    es: 'Administrar cookies+',
                    fr: 'Gérer les Cookies+'
                },
                geoMatch: ['GB', 'DE', 'FR', 'IT', 'ES', 'PL', 'RO', 'NL', 'BE', 'GR', 'CZ', 'PT', 'SE', 'HU', 'AT', 'BG', 'DK', 'FI', 'SK', 'IE', 'HR', 'LT', 'SI', 'LV', 'EE', 'CY', 'LU', 'MT', 'NO', 'IS', 'LI', /* GDPR */
                    'CH']  /* Other Europe */
            },
            {
                id: 'other-optin',
                consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                    'functional': false,
                    'media': false,
                    'performance': false,
                    'targeting': false
                },
                consentImpliedDefaults: {
                    'vendor': true,
                    '1p-targeting': true
                },
                geoMatch: ['CO', 'UY', 'PE', 'AR', 'CR', 'CL']
            },
            {
                id: 'other-optout',
                consentImpliedDefaults: {
                    'vendor': true,
                    '1p-targeting': true
                },
                geoMatch: ['MX', 'PY', 'BR', 'VE', 'NI']
            },
            {
                id: 'global',
                geoMatch: ['*'],  /* Matches everything not yet matched */
                useFixedConsent: true
            }
        ],
        reloadOnConsentChange: true,
        reloadOnConsentReduction: false,
        rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
        rightsRequestLinkTitle: {
            ar: 'بوابة طلبات الحقوق الفردية',
            de: 'Anfrageportal für Individualrechte',
            en: 'Individual Rights Request Portal',
            es: 'Portal de solicitud de derechos individuales',
            fr: 'Portail de demande de droits des individus'
        },
        scCookie: 'stateCode',
        setPageClass: false,
        src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
        strictIabCompliance: false,  /* False unless using all the IAB categories */
        ucFlavor: 'basic',
        useFixedConsent: false,
        useGPP: true,
        useIAB: true,
        useIabString: true,
        uspApiCookieName: 'usprivacy',
        uspApiExplicitNotice: true,
        uspApiIsLspa: false
    }

    ;


        /**
         * Log to the console for debugging and errors
         *
         * @param {string} level - The log level ('error', 'info', 'debug')
         * @param {...mixed} args - Arguments to output
         */
        function uclog(level /*, args */) {
            const args = Array.prototype.slice.call(arguments);
            args[0] = ('[WMUC]' + (frame.length === 0 ? '' : ' (' + frame + ')') + ':');
            if (level === 'error') {
                console.error.apply(console, args);
            } else {
                console.log.apply(console, args);
            }
        }

        /**
         * Get Consent History (debug only)
         *
         * @returns {array} Array of consent history objects
         */
        function getConsentHistory() {
            return consentHistory;
        }

        /**
         * Returns the value of the given cookie name
         *
         * @param {string} name - the cookie name
         * @returns {mixed} value of the cookie
         */
        function getCookie(name) {
            const matches = doc.cookie.match(new RegExp('(^|;) *' + name + ' *= *([^;]+)'));
            return matches ? matches.pop() : null;
        }

        /**
         * Sets a new cookie with the given values.
         *
         * @param {string} name - the cookie name
         * @param {string} value - the cookie value
         * @param {object} [opts] - the cookie option strings (expires, maxage, domain, path, secure, samesite)
         */
        function setCookie(name, value, opts) {
            if (!name) return;  /* Nothing to do */
            opts = opts || {};
            doc.cookie = name + '=' + (typeof value === 'string' ? value : '') +
                '; Domain=' + (opts.domain || config.cookieDomain) +
                '; Path=' + (opts.path || '/') +
                (opts.maxage ? '; Max-Age=' + opts.maxage : (opts.expires ? '; Expires=' + opts.expires : '')) +
                (opts.secure ? '; Secure' : '') +
                (opts.samesite ? '; SameSite=' + opts.samesite : '');
        }

        
    /* When TCF not in use, no need for all that code. */

    function getTcfConsent(_tcfCookie, _addtlCookie) {
        return null;
    }



        /**
         * Returns geoCountry passed in config
         * - if this is unavailable, check the controls cookie for a geolocation parameter
         * - if that is not available, use OneTrust Geolocation service to set the country code cookie.
         *
         * @returns {string} - The current value of geoCountry or an empty string if not yet initialized
         */
        function getGeoCountry() {
            var countryRegEx = new RegExp('^[A-z]{2}($|:[A-z]{2}$)');
            if (geoCountry && countryRegEx.test(geoCountry)) {
                return geoCountry.toUpperCase();
            } else if (processControls().countryCode && countryRegEx.test(processControls().countryCode)) {
                return processControls().countryCode.toUpperCase();
            } else {
                uclog(
                    'error',
                    'User-Consent unable to determine country, missing or invalid cookies. Dispatching to OneTrust.'
                );
                win.otGeoCheck = function (obj) {
                    setCookie('countryCode', obj.country, {
                        domain: config.cookieDomain,
                        maxage: '31536000',
                        path: '/',
                        samesite: config.cookieSameSite,
                        secure: config.cookieSecure
                    });
                    if (getCookie('countryCode') !== null) {
                        setTimeout(doReload, 100);
                    }
                };
                var ns = doc.createElement('script');
                ns.src = 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/otGeoCheck';
                doc.head.appendChild(ns);
                return geoCountry;
            }
        }

        /**
         * Returns geoState
         *
         * @returns {string} - The current value of geoState or an empty string if not yet initialized or not set
         */
        function getGeoState() {
            return geoState;
        }

        /**
         * Checks if the geoCountry is in the specified region
         *
         * @param {Array} codes - An array of uppercase country codes, country code:state codes that
         *                        represent the region, and/or lowercase region names ("ccpa").
         *                        Prefix an entry with "!" to exclude that value.
         * @returns {boolean} - true if in the specified region
         */
        function checkInRegion(codes) {
            let r = false;

            for (let i = 0; i < codes.length; i++) {
                if (codes[i]) {
                    if (geoCountry === codes[i] || geoCS === codes[i] || codes[i] === '*' || codes[i] === config.regId) {
                        r = true;
                    } else if(codes[i].charAt(0) === '!') {
                        let n = codes[i].substring(1);
                        if (geoCountry === n || geoCS === n || config.regId === n) {
                            r = false;
                            break;
                        }
                    }
                }
            }
            return r;
        }

        /**
         * Returns the expiration date/time given the number of years and an optional start time
         *
         * @param {number} years - Number of years before expiration
         * @param {Date} [start] - Start date/time (optional)
         * @returns {Date} - Expiration date/time
         */
        function getExpiration(years, start) {
            let e = start ? new Date(start) : new Date();

            e.setUTCFullYear(e.getUTCFullYear() + years);
            return e;
        }

        /**
         * Returns a localized string given a string/object
         *
         * @param {Object|string} obj - Either a source string or object of localizaed strings
         * @returns {string} - Localized string
         */
        function getLocalizedString(obj) {
            let str;

            if (typeof obj === 'object' && obj !== null) {
                str = obj[pageLang] || obj[config.defaultLanguage] || '';
            } else {
                str = (typeof obj === 'string') ? obj : '';
            }
            return str;
        }

        /**
         * Returns a shallow copy of the passed-in consent object
         *
         * @param {Object} cObj - A consent object
         * @returns {Object} - A shallow copy of the consent object
         */
        function copyConsent(cObj) {
            let nObj = {};

            if (cObj) {
                for (let c = 0; c < categories.length; c++) {
                    nObj[categories[c]] = cObj[categories[c]];
                }
            } else {
                uclog('error', 'Critical Error: Attempt to read or copy consent before UserConsent is initialized!');
            }
            return nObj;
        }

        /**
         * Returns a shallow merge of the passed-in consent object to the target consent obj
         *
         * @param {Object} tObj - The target consent object
         * @param {Object} cObj - The consent object to merge into the target
         * @returns {Object} - The resulting target consent object
         */
        function mergeConsent(tObj, cObj) {
            if (cObj) {
                tObj = tObj || {};
                for (let c = 0; c < categories.length; c++) {
                    if (typeof cObj[categories[c]] === 'boolean') {
                        tObj[categories[c]] = cObj[categories[c]];
                    }
                }
            }
            return tObj;
        }

        /**
         * Push the region information to OneTrust
         */
        function pushGeoLocation() {
            if (!oneTrustLocationSet) {
                if (win.OneTrust && typeof win.OneTrust.getGeolocationData === 'function') {
                    let otloc = win.OneTrust.getGeolocationData();
                    if (otloc && (otloc.country !== geoCountry || otloc.state !== geoState)) {
                        if (typeof win.OneTrust.setGeoLocation === 'function') {
                            win.OneTrust.setGeoLocation(geoCountry, geoState);
                            oneTrustLocationSet = true;
                        } else if (win.OneTrustStub && typeof win.OneTrustStub.setGeoLocation === 'function') {
                            win.OneTrustStub.setGeoLocation(geoCountry, geoState);
                            oneTrustLocationSet = true;
                        }
                    } else {
                        oneTrustLocationSet = true;
                    }
                } else if (win.OneTrustStub && typeof win.OneTrustStub.setGeoLocation === 'function') {
                    win.OneTrustStub.setGeoLocation(geoCountry, geoState);
                    oneTrustLocationSet = true;
                }
                if (dbg) {
                    if (oneTrustLocationSet) {
                        uclog('debug', 'Set OneTrust geo-location.');
                    } else {
                        uclog('debug', 'Not yet able to set OneTrust geo-location.');
                    }
                }
            }
        }

        /**
         * Read the GPP String from the cookie(s)
         *
         * @param {string} gcookie - The base cookie name to read.
         * @returns {string} - The GPP String
         */
        function readGppCookie(gcookie) {
            let gstr;

            if ((gstr = getCookie(gcookie)) === null) {
                /* Large GPP strings are chunked by OT into multiple cookies... */
                gstr = '';
                for (let c = 1, cstr = null; cstr !== '' && c < 10; c++, gstr += cstr) {
                    cstr = getCookie(gcookie + c.toString(10)) || '';
                }
            }
            return gstr;
        }

        /**
         * Reads a GPP field and assigns it (dobj.type must be binary, trinary, boolean, or an array of such!)
         *
         * @param {string} section - IAB section name
         * @param {Object} dobj - Field descriptor object
         * @param {Object} state - State to update with field value(s)
         */
        function parseGppField(section, dobj, state) {
            let gval = gppCmpApi.getFieldValue(section, dobj.field),
                rt = dobj.type.toLowerCase(),
                assignToCats = (val, cats, type) => {
                    const carr = (typeof cats === 'string') ? [cats] : cats,
                        v = (type === 'boolean' ? val : (type === 'trinary' ? (val === 2) : (val !== 0)));

                    for (let c of carr) {
                        if (config.consentNotApplicable.length === 0 || config.consentNotApplicable.indexOf(c) < 0) {
                            if (categories.indexOf(c) >= 0 && config.consentNotApplicable.indexOf(c) < 0) {
                                state[c] = v;
                            } else {
                                uclog('error', 'Invalid consent "' + c + '" specified in GPP Categories!');
                            }
                        }
                    }
                };

            if (rt.startsWith('array')) {
                rt = rt.substring(6);
                if (parsableTypes[rt] && dobj.maxCount && Array.isArray(gval)) {
                    for (let i = 0; i < dobj.maxCount; i++) {
                        if (dobj[i]) {
                            assignToCats(gval[i], dobj[i], rt);
                        }
                    }
                } else {
                    uclog('error', 'Error: Unparsable data type "' + rt + '" or missing maxCount in GPP Categories "' + dobj.field + '" value!');
                }
            } else if (dobj.val) {
                if (parsableTypes[rt]) {
                    assignToCats(gval, (typeof dobj.val !== 'undefined' ? dobj.val : dobj.default), rt);
                } else {
                    uclog('error', 'Error: Unparsable data type "' + rt + '" in GPP Categories!');
                }
            }
        }

        /**
         * Sets a GPP field and from the category data (dobj.type must be binary, trinary, boolean, or an array of such!)
         *
         * @param {string} section - IAB section name
         * @param {Object} dobj - Field descriptor object
         * @param {Object} state - State to update field value(s)
         */
        function writeGppField(section, dobj, state) {
            let gval = gppCmpApi.getFieldValue(section, dobj.field),
                rt = dobj.type.toLowerCase(),
                assignFromCats = (cats, type) => {
                    const carr = (typeof cats === 'string') ? [cats] : cats;
                    let cnt = 0,
                        v = true;

                    for (let c of carr) {
                        if (categories.indexOf(c) >= 0) {
                            if (typeof state[c] !== 'undefined') {
                                v = v && state[c];
                                cnt++;
                            }
                        } else {
                            uclog('error', 'Invalid consent "' + c + '" specified in GPP Categories!');
                        }
                    }
                    if (cnt > 0) {
                        return (type === 'boolean' ? v : (type === 'trinary' ? (v ? 2 : 1) : (v ? 1 : 0)));
                    }
                    return (type === 'boolean' ? false : 0);
                };

            try {
                if (rt.startsWith('array')) {
                    rt = rt.substring(6);
                    if (!parsableTypes[rt]) {
                        throw 'unparse';
                    }
                    if (!dobj.maxCount || !Array.isArray(gval)) {
                        throw 'badarray';
                    }
                    for (let i = 0; i < dobj.maxCount; i++) {
                        if (dobj[i]) {
                            gval[i] = assignFromCats(dobj[i], rt);
                        } else if (dobj.default && typeof dobj.default[i] !== 'undefined') {
                            gval[i] = dobj.default[i];
                        }
                    }
                } else {
                    if (!parsableTypes[rt]) {
                        throw 'unparse';
                    }
                    if (dobj.val) {
                        gval = assignFromCats(dobj.val, rt);
                    } else if (typeof dobj.default !== 'undefined') {
                        gval = dobj.default;
                    }
                }
                gppCmpApi.setFieldValue(section, dobj.field, gval);
            } catch (e) {
                if (e === 'unparse') {
                    uclog('error', 'Error: Unparsable data type "' + rt + '" in GPP Categories!');
                } else if (e === 'badarray') {
                    uclog('error', 'Error: Missing maxCount or bad array in GPP Categories "' + dobj.field + '" value!');
                } else {
                    uclog('error', 'Failed to set GPP field "' + dobj.field + '" value!');
                }
            }
        }

        /**
         * Returns a consent object derived from a GPP string
         *
         * @param {string} gstring - The GPP string passed-in with the change, or an empty string if not known.
         * @returns {Object} - Consent state object
         */
        function getGppConsent(gstring) {
            const sect = config.gppSection;
            let gstr = (typeof gstring === 'string' ? gstring : ''),
                src = 'CMP',
                state = null;

            if (gstr.length === 0) {
                /* Check for external consent in a webview... */
                if (config.useExternalConsent) {
                    /* Get the consent string(s) from the external consent settings */
                    gstr = win.OTExternalConsent.gppString || '';
                    src = 'external consent';
                } else {
                    /* Get the consent string(s) from the cookie */
                    gstr = readGppCookie(config.gppIabCookie) || '';
                    src = 'cookie';
                }
            }

            if (gstr === '') {
                /* We have no GPP string */
                if (dbg) {
                    uclog('debug', 'No GPP string present.');
                }
                return null;
            }

            /* We have a GPP string, so process it for the consent state */
            try {
                gppCmpApi.setGppString(gstr);
            } catch (err) {
                uclog('error', 'GPP string invalid, ignoring.');
                return null;
            }
            if (!gppCmpApi.hasSection(sect) || !Array.isArray(config.gppCategories[sect])) {
                /* We have a GPP string, but it doesn't have the current section/region */
                if (dbg) {
                    uclog('debug', 'GPP string present, but for different region/section.');
                }
                return null;
            } else {
                /* We have a GPP string in the current section, set the API to use it */
                gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
            }

            /* Parse or set the GPP string and associate fields to/from categories */
            const gsect = config.gppCategories[sect];
            state = copyConsent(config.consentDefaults);
            for (let c of gsect) {
                if (typeof c === 'object' && c !== null && c.field) {
                    /* Field descriptor, use it */
                    parseGppField(sect, c, state);
                }
            }

            if (!config.useExternalConsent) {
                /* Check against GPC */
                if (gppCmpApi.getFieldValue(sect, 'GpcSegmentIncluded')) {
                    if (!gppCmpApi.getFieldValue(sect, 'Gpc') === usingGpc) {
                        /* Fix GPP string */
                        gppCmpApi.setFieldValue(sect, 'Gpc', usingGpc);
                        gstr = gppCmpApi.getGppString();
                    }
                }
                if (usingGpc) {
                    state = mergeConsent(state, config.consentGpcDefaults);
                }
            }
            if (gstr !== gppString) {
                gppString = gstr;
            }

            if (dbg) {
                uclog('debug', 'Processed GPP string from ' + src + ': ' + gppString);
            }

            return state;
        }

        /**
         * Sets the internal GPP string from a new GPP consent string or a consent object
         *
         * @param {string} gstring - The GPP string passed-in with the change, or an empty string if not known.
         * @param {Object} state - The new consent state to set to the GPP string.
         * @returns {boolean} - true on success, false on failure
         */
        function setGppConsent(gstring, state) {
            const oldGpp = gppString,
                sect = config.gppSection;

            if (typeof gstring === 'string' && gstring.length > 0) {
                /* Set the new GPP string from a string we probably got from OT */
                try {
                    gppCmpApi.setGppString(gstring);
                    gppString = gstring;
                    if (gppString && gppCmpApi.hasSection(sect)) {
                        /* We have a GPP string in the current section, set the API to use it */
                        gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                    }
                    if (dbg) {
                        uclog('debug', 'GPP string set from CMP: ', gstring);
                    }
                } catch (e) {
                    uclog('error', 'Failed to set GPP string: ', e);
                    return false;
                }
            } else if (typeof state === 'object' && state !== null && Array.isArray(config.gppCategories[config.gppSection])) {
                try {
                    /* Set the new GPP string from a consent state */
                    if (!gppString) {
                        /* prepping new string from a blank slate */
                        gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                    }
                    if (Array.isArray(config.gppCategories[sect])) {
                        /* Parse or set the GPP string and associate fields to/from categories */
                        const gsect = config.gppCategories[sect];
                        for (let c of gsect) {
                            if (typeof c === 'object' && c !== null && c.field) {
                                /* Field descriptor, use it */
                                writeGppField(sect, c, state);
                            }
                        }
                    }
                    if (usingGpc) {
                        /* Fix GPP string */
                        gppCmpApi.setFieldValue(sect, 'Gpc', usingGpc);
                    }
                    gppString = gppCmpApi.getGppString();
                    if (gppString && gppCmpApi.hasSection(sect)) {
                        /* We have a GPP string in the current section, set the API to use it */
                        gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                        /* If this is an update of the internal GPP, send the section change event */
                        if (usingOTGpp === false && oldGpp.length !== 0 && oldGpp !== gppString) {
                            gppCmpApi.fireSectionChange(sect);
                        }
                    }
                    if (dbg) {
                        uclog('debug', 'GPP string set from consent state: ', gppString);
                    }
                } catch (e) {
                    uclog('error', 'Failed to set GPP string: ', e);
                    return false;
                }
            } else {
                uclog('error', 'Failed to set GPP string, invalid parameters.');
                return false;
            }

            return true;
        }

        /**
         * Compare consent states to determine if consent has been reduced in any way
         *
         * @param {string} oldConsent - Old consent object
         * @param {string} newConsent - New consent object
         * @returns {object} - A consent object
         */
        function hasConsentReduced(oldConsent, newConsent) {
            if (oldConsent && newConsent) {
                for (let c = 0; c < categories.length; c++) {
                    if (oldConsent[categories[c]] && !newConsent[categories[c]]) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Processes the OneTrust consent cookie for non-consent data.
         */
        function processOneTrustCookie() {
            let cookieValue = getCookie(config.consentCookie);

            /* If no cookie is set and has a valid groups param then we have something to do */
            if (cookieValue && cookieValue.indexOf('&groups=') >= 0) {
                const fields = cookieValue.split('&');

                for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
                    let encodedParameterGroup = fields[fieldIndex];
                    if (encodedParameterGroup) {
                        /* fields are encoded paramName=paramVal */
                        let values = encodedParameterGroup.split('='),
                            paramName = values[0],
                            paramValue = values[1];

                        if (paramName === 'version' && paramValue) {
                            consentVersion = paramValue;
                        } else if (paramName === 'consentId' && paramValue) {
                            consentId = paramValue;
                        }
                    }
                }
                return true;
            }
            return false;
        }

        /**
         * Processes the consent state from the OneTrust cookie only.
         *
         * @returns {Object} - An object representing the current consent state of the OT cookie, or null if none.
         */
        function processConsentCookie() {
            let cookieValue = getCookie(config.consentCookie),
                state = null;

            /* If using external consent, override cookie value... */
            if (config.useExternalConsent) {
                cookieValue = 'groups=' + encodeURIComponent(win.OTExternalConsent.groups);
                usingGpc = false;
            }

            /* If no cookie is set and has a valid groups param then we have something to do */
            if (cookieValue && cookieValue.indexOf('&groups=') >= 0) {
                const fields = cookieValue.split('&');

                for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
                    let encodedParameterGroup = fields[fieldIndex];
                    if (encodedParameterGroup) {
                        /* fields are encoded paramName=paramVal */
                        let values = encodedParameterGroup.split('='),
                            paramName = values[0],
                            paramValue = values[1];

                        /* the 'groups' parameter contains consent data */
                        if (paramName === 'groups' && paramValue) {
                            /* group value is uri encoded */
                            let groups = decodeURIComponent(paramValue).split(','),
                                comCodes = [],
                                comValues = [],
                                count = 0,
                                useTransition = false;

                            state = mergeConsent(copyConsent(config.consentDefaults), config.consentImpliedDefaults);
                            for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
                                let group = groups[groupIndex].split(':'),
                                    consentCode = group[0].toLowerCase(),
                                    consentValue = (group[1] === '1');

                                if (consentCode) {
                                    if (config.categories[consentCode]) { /* && config.consentNotApplicable.indexOf(config.categories[consentCode]) === -1) { */
                                        /* We have a consent value, record it */
                                        state[config.categories[consentCode]] = consentValue;
                                        if (config.categories[consentCode] !== 'required') {
                                            count++;
                                        }
                                    } else {
                                        if (config.compatTransition && config.compatTransition.old === consentCode && config.compatTransition.cond === consentValue) {
                                            /* We are in a transitional mixed state */
                                            useTransition = true;
                                        }
                                        if (config.compatCodes[consentCode]) {
                                            /* We have a compatiblity value, store it for maybe later */
                                            comCodes.push(consentCode);
                                            comValues.push(consentValue);
                                        }
                                    }
                                }
                            }
                            if (count === 0 && comCodes.length > 0) {
                                /* No current codes in cookie, we are in compatibility mode.
                                 * Process the compatibility code lists.
                                 */
                                usingCompatMode = true;
                                for (let ccIndex = 0; ccIndex < comCodes.length; ccIndex++) {
                                    let comVals = config.compatCodes[comCodes[ccIndex]];
                                    if (comVals && !Array.isArray(comVals)) {
                                        comVals = [comVals];
                                    }
                                    for (let comVal of comVals) {
                                        let comCode = config.categories[comVal];
                                        if (comCode) {
                                            /* Compatibility matches valid consentState so set it */
                                            state[comCode] = comValues[ccIndex];
                                            count++;
                                        }
                                    }
                                }
                            } else if (useTransition && config.compatTransition.new) {
                                /* Make sure the new state corresponds to the compatTransition state's condition */
                                if (Array.isArray(config.compatTransition.new)) {
                                    for (let comIndex = 0; comIndex < config.compatTransition.new.length; comIndex++) {
                                        state[config.categories[config.compatTransition.new[comIndex]]] = config.compatTransition.cond;
                                        count++;
                                    }
                                } else {
                                    state[config.categories[config.compatTransition.new]] = config.compatTransition.cond;
                                    count++;
                                }
                            }
                            if (count === 0) {
                                state = null;
                            }
                        }
                    }
                }
            }

            return state;
        }

        /**
         * Processes the current consent state from the cookie(s), GPP string, and/or the TC string
         *
         * @param {string} [gstr] - An optional new GPP string received from an event notification.
         * @returns {Object} - An object representing the current consent state of the cookie
         */
        function processConsentState(gstr) {
            let state;

            /* If not using external consent and no cookie is set or cookie is invalid then we have no consent */
            if (!config.useExternalConsent && !processOneTrustCookie()) {
                consentConfirmed = false;
                consentTime = null;
            }

            /* Only process if consent is confirmed and not fixed */
            if (!consentConfirmed || config.useFixedConsent) {
                state = copyConsent(config.consentDefaults);
                consentSource = 'defaults';
            } else if (config.useExternalConsent !== true && usingIabGpp && config.gppIabCookie && (state = getGppConsent(gstr))) {
                consentSource = (gstr ? 'gpp string' : config.gppIabCookie + ' cookie');
            } else if (config.useExternalConsent !== true && usingIabGdpr && config.gdprIabCookie && (state = getTcfConsent(config.gdprIabCookie, config.addtlConsentCookie))) {
                consentSource = config.gdprIabCookie + ' cookie';
            } else if (state = processConsentCookie()) {
                consentSource = config.useExternalConsent ? 'external consent' : config.consentCookie + ' cookie';
            } else {
                /* No state set by anything, so use the default. */
                state = copyConsent(config.consentDefaults);
                consentSource = 'defaults';
            }

            if (!config.useExternalConsent) {
                /* Handle special consent cases */
                if (usingGpc) {
                    /* If we're using GPC, force relevant states */
                    state = mergeConsent(state, config.consentGpcDefaults);
                }
            }

            return state;
        }

        /**
         * Correct badly implemented GPC signal extensions (PrivacyBadger, DuckDuckGo, etc.)
         *
         * We really shouldn't have to do this, but people expect these things to "just work"
         * and when they don't they blame the web site.  Guess what, some of those extensions
         * really do a terrible job at setting a single global boolean value.  So here we are.
         * As a side note, this would be WAY easier if Javascript had access to the request
         * headers that loaded the page.
         *
         * @param {string} cookie - The cookie to use for GPC detection
         */
        function fixGpcWithCookie(cookie) {
            if (typeof win.navigator.globalPrivacyControl === "undefined" && cookie) {
                const val = getCookie(cookie);
                if (val && (val === '1' || val.startsWith('t'))) {
                    /* We have GPC cookie, so set GPC in Javascript */
                    try {
                        Object.defineProperty(win.Navigator.prototype, 'globalPrivacyControl', {
                            get: function globalPrivacyControl() {
                                return true;
                            },
                            configurable: true,
                            enumerable: true
                        });
                    } catch(e) {
                        uclog('error', 'GPC signal error in browser.');
                    }
                }
            }
        }

        /**
         * Returns true if user has confirmed consent state, false if using a default state
         *
         * @returns {boolean} - Current user consent confirmation state
         */
        function getConsentConfirmed() {
            return consentConfirmed;
        }

        /**
         * Returns a copy of the current consent state object.
         *
         * @returns {object} - Copy of the current consent object
         */
        function getConsentState() {
            return copyConsent(consentState);
        }

        /**
         * Returns a copy of the current consent state object with specific consents only, no undefined
         *
         * @returns {object} - Copy of the current consent object with undefined values removed
         */
        function getSimpleConsentState() {
            let ncs = {};
            for (let k in consentState) {
                if (typeof consentState[k] === 'boolean') {
                    ncs[k] = consentState[k];
                }
            }
            return ncs;
        }

        /**
         * Returns the current consent time as read from the consent cookie.
         *
         * @returns {string} - The current consent time string
         */
        function getConsentTime() {
            return consentTime;
        }

        /**
         * Returns the current consent version as read from the consent cookie.
         *
         * @returns {string} - The current consent version string
         */
        function getConsentVersion() {
            return consentVersion;
        }

        /**
         * Returns the GPP API region being used, if any
         *
         * @returns {string} - Region name or 'none'
         */
        function getGppSection() {
            return (usingIabGpp && config.gppSection !== '') ? config.gppSection : 'none';
        }

        /**
         * Returns the current IAB CMP interface name.
         *
         * @returns {string} - The current IAB interface name or "none".
         */
        function getIABInterface() {
            return usingIabGpp ? '__gpp' : (usingIabCcpa ? '__uspapi' : (usingIabGdpr ? '__tcfapi' : 'none'));
        }

        /**
         * Returns the IAB region being used, if any
         *
         * @returns {string} - Region name or 'none'
         */
        function getIABRegion() {
            return (config.iabRegion !== '' ? config.iabRegion : 'none');
        }

        /**
         * Returns the current IAB CMP interface version.
         *
         * @returns {string} - The current IAB interface version string or "none".
         */
        function getIABVersion() {
            return usingIabGpp ? '1.1' : (usingIabCcpa ? '1.0' : (usingIabGdpr ? '2.2' : 'none'));
        }

        /**
         * Returns the current link action function.
         *
         * @returns {function} - A function to call on "cookie settings" or "do not sell" link clicks
         */
        function getLinkAction() {
            return config.consentLinkAction || (win.OneTrust && win.OneTrust.ToggleInfoDisplay) || function _linkAct() {
                if (win.OneTrust && win.OneTrust.ToggleInfoDisplay) {
                    win.Optanon.ToggleInfoDisplay();
                }
            };
        }

        /**
         * Returns the current link action title.
         *
         * @returns {string} - The current link text to use, such as "Manage Cookies+".  Empty if not used in region.
         */
        function getLinkTitle() {
            return config.consentLinkTitle;
        }

        /**
         * Returns the current link action function for the AdChoices, if any is configured.
         *
         * @returns {function} - A function to call on "AdChoices" link clicks or null
         */
        function getAdChoicesLinkAction() {
            if (typeof config.adChoicesLinkAction === 'function') {
                return config.adChoicesLinkAction;
            } else if (typeof config.adChoicesLinkAction === 'string' && config.adChoicesLinkAction.search(/^http/) !== -1) {
                /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
                return function _redirToPC() {
                    win.open(config.adChoicesLinkAction, '_blank');
                };
            }
            return null;
        }

        /**
         * Returns the current link action title for the AdChoices, if it exists.
         *
         * @returns {string} - The current link text to use, such as "AdChoices".  Empty if not configured.
         */
        function getAdChoicesLinkTitle() {
            return config.adChoicesLinkTitle;
        }

        /**
         * Returns the current link action function for the Affiliates, if any is configured.
         *
         * @returns {function} - A function to call on "Affiliates" link clicks or null
         */
        function getAffiliatesLinkAction() {
            if (typeof config.affiliatesLinkAction === 'function') {
                return config.affiliatesLinkAction;
            } else if (typeof config.affiliatesLinkAction === 'string' && config.affiliatesLinkAction.search(/^http/) !== -1) {
                /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
                return function _redirToPC() {
                    win.open(config.affiliatesLinkAction, '_blank');
                };
            }
            return null;
        }

        /**
         * Returns the current link action title for the Affiliates, if it exists.
         *
         * @returns {string} - The current link text to use, such as "Affiliates".  Empty if not configured.
         */
        function getAffiliatesLinkTitle() {
            return config.affiliatesLinkTitle;
        }

        /**
         * Returns the current link action function for the Privacy Center, if any is configured.
         *
         * @returns {function} - A function to call on "privacy center" link clicks or null
         */
        function getPrivacyCenterLinkAction() {
            if (typeof config.privacyCenterLinkAction === 'function') {
                return config.privacyCenterLinkAction;
            } else if (typeof config.privacyCenterLinkAction === 'string' && config.privacyCenterLinkAction.search(/^http/) !== -1) {
                /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
                return function _redirToPC() {
                    win.open(config.privacyCenterLinkAction, '_blank');
                };
            }
            return null;
        }

        /**
         * Returns the current link action title for the Privacy Center, if it exists.
         *
         * @returns {string} - The current link text to use, such as "Privacy Center".  Empty if not configured.
         */
        function getPrivacyCenterLinkTitle() {
            return config.privacyCenterLinkTitle;
        }

        /**
         * Returns the current link action function for the Individual Rights Request Portal, if any is configured.
         *
         * @returns {function} - A function to call on "rights request" link clicks or null
         */
        function getRightsRequestLinkAction() {
            if (typeof config.rightsRequestLinkAction === 'function') {
                return config.rightsRequestLinkAction;
            } else if (typeof config.rightsRequestLinkAction === 'string' && config.rightsRequestLinkAction.search(/^http/) !== -1) {
                /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
                return function _redirToPC() {
                    win.open(config.rightsRequestLinkAction, '_blank');
                };
            }
            return null;
        }

        /**
         * Returns the current link action title for the Individual Rights Request Portal, if it exists.
         *
         * @returns {string} - The current link text to use, such as "Rights Request".  Empty if not configured.
         */
        function getRightsRequestLinkTitle() {
            return config.rightsRequestLinkTitle;
        }

        /**
         * Returns the current region string
         *
         * @returns {string} - The current region string: ccpa, gdpr, or global.
         */
        function getRegion() {
            return config.regId;
        }

        /**
         * Returns whether or not the page will reload on consent changes
         *
         * @returns {boolean} - true if the page will reload, false if it will not
         */
        function getReloadOnChange() {
            return config.reloadOnConsentChange;
        }

        /**
         * Returns whether or not the page will reload on consent changes
         * resulting in a reduction of consent
         *
         * @returns {boolean} - true if the page will reload, false if it will not
         */
        function getReloadOnConsentReduction() {
            return config.reloadOnConsentReduction;
        }

        /**
         * Returns the current UserConsent version string
         *
         * @returns {string} - The current version of UserConsent
         */
        function getVersion() {
            return userConsentVersion;
        }

        /**
         * Returns a boolean with the child instance state.
         *
         * @returns {boolean} - true if child instance, false otherwise
         */
        function isChild() {
            return ucChild;
        }

        /**
         * Returns a boolean with the top instance state.
         *
         * @returns {boolean} - true if top instance, false otherwise
         */
        function isTop() {
            return !ucChild;
        }

        /**
         * Returns a boolean with the enabled state.  Always true.  DEPRECATED
         *
         * @returns {boolean} - true
         */
        function isEnabled() {
            return true;
        }

        /**
         * Returns a boolean true if GPC is being used for current privacy settings
         *
         * @returns {boolean} - true if GPC is being used, false otherwise
         */
        function isGpcInUse() {
            return usingGpc;
        }

        /**
         * Returns a boolean true if the user has the GPC header set
         *
         * @returns {boolean} - true if GPC is set, false otherwise
         */
        function isGpcSet() {
            return !!navigator.globalPrivacyControl;
        }

        /**
         * Returns a boolean true if currently in the "ccpa" region
         *
         * @returns {boolean} - true if in "ccpa", false otherwise
         */
        function isInCcpaRegion() {
            return checkInRegion(config.ccpaGeos);
        }

        /**
         * Returns a boolean true if currently in the "gdpr" region
         *
         * @returns {boolean} - true if in "gdpr", false otherwise
         */
        function isInGdprRegion() {
            return usingIabGdpr;
        }

        /**
         * Returns a boolean true if currently in a region using GPP
         *
         * @returns {boolean} - true if in a GPP region, false otherwise
         */
        function isInGppRegion() {
            return usingIabGpp;
        }

        /**
         * Returns a boolean true if currently in the specified region
         *
         * @param {string} region - The region to check ("ccpa", "gdpr", or "global").
         * @returns {boolean} - true if in region, false otherwise
         */
        function isInRegion(region) {
            return config.regId === region;
        }

        /**
         * Returns the IAB region being used, if any
         *
         * @param {string} region - The IAB region to check ("gpp", "ccpa", or "gdpr").
         * @returns {boolean} - true if in region, false otherwise
         */
        function isInIabRegion(region) {
            region = (typeof region === 'string') ? region : '';
            return config.iabRegion === region;
        }

        /**
         * Returns a boolean true if OneTrust is being blocked by something like the EasyList - Cookie Notices filter.
         *
         * @returns {mixed} - boolean true if OneTrust is being blocked, false if not, undefined if still testing
         */
        function isOneTrustBlocked() {
            return otBlocked;
        }

        /**
         * Returns a boolean true if OneTrust failed to fully load, which may have happened because
         * things are down or because a browser extension is blocking it.
         *
         * @returns {mixed} - boolean true if OneTrust has not completely loaded, false if things are fine, undefined if still waiting on success/failure
         */
        function isOneTrustFailing() {
            return otFailed;
        }

        /**
         * Returns a boolean true if OneTrust has been loaded.
         *
         * @returns {boolean} - true if OneTrust has been loaded, false if not
         */
        function isOneTrustLoaded() {
            return optanonLoaded;
        }

        /**
         * Returns a boolean true if UserConsent is initialized and ready.
         *
         * @returns {boolean} - true if UserConsent is ready, false if not
         */
        function isReady() {
            return consentState !== null;
        }

        /**
         * Returns a boolean true if UserConsent configuration provides full IAB compliance
         *
         * @returns {boolean} - true if IAB compliant, false if not
         */
        function isSiteIABCompliant() {
            return config.strictIabCompliance;
        }

        /**
         * Returns a boolean true if the requested consent states currently have consent.
         *
         * @param {array} states - An array of strings, where each is a consent state to check.
         * @param {object} [options] - Options, such as "id" which is the idenitifer of the service
         *                             being checked.  (Optional)
         * @returns {boolean} - true if consent is granted, false otherwise
         */
        function inUserConsentState(states, options) {
            const iabReg = 'iab-' + (config.iabRegion || 'N/A'),
                gppSect = 'gpp-' + (config.gppSection || 'N/A');
            let endState = true,
                state = 'not ready';

            options = options || {};
            if (isReady() && states) {
                states = Array.isArray(states) ? states : [states];
                for (let i = 0; i < states.length && endState; i++) {
                    state = states[i];
                    if (state && state !== 'required') {
                        if (state === 'gpp' || state === 'iab-gpp' || state === gppSect) {
                            if (usingIabGpp && !(options && options.ignoreIAB)) {
                                break;  /* If the "gpp" consent type is added, and usingIabGpp is true, consent is true */
                            }
                        } else if (state === 'iab' || state === iabReg) {
                            if (config.useIAB && config.iabRegion !== '' && !(options && options.ignoreIAB)) {
                                break;  /* If the "iab" consent type is added, and useIAB is true, consent is true */
                            }
                        } else if (typeof consentState[state] === 'boolean' && consentState[state] === false) {
                            endState = false;
                        } else if (config.compatCategories[state]) {
                            /* Check every matching compatCategory value */
                            let ccats = config.compatCategories[state];
                            for (let c = 0; c < ccats.length; c++) {
                                let ccat = ccats[c];
                                if (typeof consentState[ccat] === 'boolean' && consentState[ccat] === false) {
                                    endState = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (dbg && !options.internal) {
                options.name = (options.name || options.id || 'unnamed');
                /* Track consent check in history */
                consentHistory.push({
                    ts: new Date(),
                    act: options.cact || 'CHK',
                    desc: options.name,
                    res: endState,
                    note: (!endState && state) || ''
                });
                if (endState) {
                    uclog('debug', 'Check for consent [' + ((states && states.join(',')) || 'empty') + '] ALLOWS "' + options.name + '"' + (options.cact === 'ADD' ? ', script added' : ''));
                } else {
                    uclog('debug', 'Check for consent [' + ((states && states.join(',')) || 'empty') + '] REJECTS "' + options.name + '"' + (options.cact === 'ADD' ? ', script NOT added' : ''));
                }
            }
            return endState;
        }

        /**
         * Returns the current IAB GPP string.
         *
         * @returns {string} - Current GPP consent string or "" if not set or not in a GPP region
         */
        function getGppAPIstring() {
            return gppString;
        }

        /**
         * Returns the current GDPR AC string.
         *
         * @returns {string} - Current TCFAPI additional consent string or "" if not set or not in GDPR region
         */
        function getTcfAPIaddtlString() {
            return acString;
        }

        /**
         * Returns the current GDPR TC string.
         *
         * @returns {string} - Current TCFAPI consent string or "" if not in GDPR region
         */
        function getTcfAPIstring() {
            return tcString;
        }

        /**
         * Returns the current CCPA USPAPI consent string.
         *
         * @returns {string} - Current USPAPI consent string or "1---" if not in CCPA region
         */
        function getUspAPIstring() {
            return uspString;
        }

        /**
         * Internal function to set the USP string based on current consent.
         *
         * @returns {string} - Current USPAPI consent string or "1---" if not in CCPA region
         */
        function setUspAPIstring() {
            let uspStr;

            if (usingIabCcpa) {
                uspStr = '1' + (config.uspApiExplicitNotice ? 'Y' : 'N') + (inUserConsentState(['vendor'], {internal: true}) ? 'N' : 'Y') + (config.uspApiIsLspa ? 'Y' : 'N');
            } else {
                uspStr = '1---';
            }
            if (uspStr !== uspString) {
                uspString = uspStr;
                if (isTop()) {
                    if (usingUspFrame === null) {
                        /* Set the UspApi session cookie */
                        setCookie(config.uspApiCookieName, uspStr, {
                            domain: config.cookieDomain,
                            path: '/',
                            samesite: config.cookieSameSite,
                            secure: config.cookieSecure
                        });
                    }
                    if (dbg) {
                        uclog('debug', 'USP string updated: ', uspStr);
                    }
                }
            }
            return uspString;
        }

        /**
         * Returns whether or not the cookie put UserConsent into compatibility mode.
         *
         * @returns {boolean} - true if in compatibility mode, false otherwise
         */
        function usingCompatConsent() {
            return usingCompatMode;
        }

        /**
         * Returns whether or not we are using external consent, such as in a WebView
         *
         * @returns {boolean} - true if using external consent, false if not
         */
        function usingExternalConsent() {
            return config.useExternalConsent;
        }

        /**
         * Returns whether or not IAB GPP is available in this region
         *
         * @returns {boolean} - true if GPP is available, false if not
         */
        function usingGPP() {
            return usingIabGpp;
        }

        /**
         * Returns whether or not IAB CMP is available in this region
         *
         * @returns {boolean} - true if IAB is available, false if not
         */
        function usingIAB() {
            return (config.useIAB && (usingIabGpp || usingIabCcpa || usingIabGdpr));
        }

        /**
         * Returns whether or not PSM was detected
         * DEPRECATED - Always returns false
         *
         * @returns {boolean} - true if PSM was detected, false if not
         */
        function usingPSM() {
            return false;
        }

        /* Not long for this world... VERY DEPRECATED */
        function getUserConsentAdvertisingState() {
            return (typeof config.ucFlavor !== 'iab') ? inUserConsentState(['vendor', 'targeting']) : inUserConsentState(['data-share', 'data-sell', 'ads-contextual', 'ads-person-prof', 'ads-person']);
        }

        /**
         * Internal function to detect if something is blocking OneTrust.
         */
        function testOTBlocking() {
            let testElem = function (i, c, e) {
                let chkDiv = doc.createElement(e || 'div'),
                    chkStl,
                    res;

                if (i) {
                    chkDiv.id = i;
                }
                if (c) {
                    chkDiv.className = c;
                }
                chkDiv.style.width = '1px';
                chkDiv.style.display = 'block';
                chkDiv = doc.body.appendChild(chkDiv);
                chkStl = win.getComputedStyle(chkDiv);
                res = (chkStl.display === 'none');
                chkDiv.remove();
                return res;
            };

            if (doc.body) {
                otBlocked = (testElem('onetrust-consent-sdk', 'ot-cookie-consent') ||
                    testElem('ot-lst-cnt', 'ot-sdk-show-settings') ||
                    testElem('onetrust-pc-sdk', 'otPcCenter ot-fade-in') ||
                    testElem('ot-pc-header', 'onetrust-pc-dark-filter') ||
                    testElem('ot-pc-content', 'ot-pc-scrollbar') ||
                    testElem('ot-sdk-btn', 'ot_cookie_settings_btn') || false);

                if (otBlocked) {
                    // Browser blocked
                    if (dbg) {
                        uclog('debug', 'OneTrust being blocked by filter.');
                    }
                    doc.dispatchEvent(new CustomEvent('oneTrustBlocked', {
                        bubbles: false,
                        cancelable: false,
                        detail: {
                            region: config.regId,
                            time: new Date(),
                            consentConfirmed: consentConfirmed,
                            otId: consentId,
                            otVers: consentVersion
                        }
                    }));
                }
            } else {
                // No body yet, defer a bit
                setTimeout(testOTBlocking.bind(win), 5);
            }
        }

        /**
         * Adds a script element to the DOM if we have consent for it.
         *
         * @param {object} elem - A DOM ScriptElement to be conditionally added.
         * @param {array} consent - An array of strings, where each is a consent state to check.
         * @param {object} [parent] - A DOM ScriptElement to use as the script parent.
         * @param {array} [regions] - An array of strings, where each is either the name of a region
         *                            ("gdpr", "ccpa"), a 2-letter country code ("GB", "DE"), a
         *                            country:state code ("US:CA"), or an "*" for everywhere.  If
         *                            prefixed with "!", that entry will be excluded.  Entries are
         *                            evaluaed in order.  Default is ["*"].
         * @returns {boolean} - true if script was added, false if not.
         */
        function addScriptElement(elem, consent, parent, regions) {
            if (elem) {
                const opts = {
                        cact: 'ADD',
                        name: (elem.name || elem.src || elem.id || 'unnamed inline')
                    },
                    p = parent || doc.head,
                    r = regions || ['*'];

                if (!checkInRegion(r)) {
                    if (dbg) {
                        /* Track region check in history */
                        consentHistory.push({
                            ts: new Date(),
                            act: 'ADD',
                            desc: opts.name,
                            res: false,
                            note: 'Not in script region'
                        });
                        uclog('debug', 'Check for region [' + (r.join(',') || 'empty') + '] REJECTS "' + opts.name + '"' + ', script NOT added');
                    }
                    return false;
                }
                if (inUserConsentState(consent, opts)) {
                    p.appendChild(elem);
                    return true;
                }
            } else {
                uclog('error', 'Invalid or missing options to addScriptElement.');
            }
            return false;
        }

        /**
         * Adds a script to the DOM if we have consent for it.
         *
         * @param {object} options - The script options, same as if passed to createElement("script").
         * @param {array} consent - An array of strings, where each is a consent state to check.
         * @param {object} [parent] - A DOM ScriptElement to use as the script parent.
         * @param {array} [regions] - An array of strings, where each is either the name of a region
         *                            ("gdpr", "ccpa"), a 2-letter country code ("GB", "DE"), a
         *                            country:state code ("US:CA"), or an "*" for everywhere.  If
         *                            prefixed with "!", that entry will be excluded.  Entries are
         *                            evaluaed in order.  Default is ["*"].
         * @returns {boolean} - true if script was added, false if not.
         */
        function addScript(options, consent, parent, regions) {
            if (options && (options.src || options.text)) {
                const opts = {
                        cact: 'ADD',
                        name: (options.name || options.src || options.id || 'unnamed inline')
                    },
                    p = parent || doc.head,
                    r = regions || ['*'];

                if (!checkInRegion(r)) {
                    if (dbg) {
                        /* Track region check in history */
                        consentHistory.push({
                            ts: new Date(),
                            act: 'ADD',
                            desc: opts.name,
                            res: false,
                            note: 'Not in script region'
                        });
                        uclog('debug', 'Check for region [' + (r.join(',') || 'empty') + '] REJECTS "' + opts.name + '"' + ', script NOT added');
                    }
                    return false;
                }
                if (inUserConsentState(consent, opts)) {
                    const ns = doc.createElement('script'),
                        ops = Object.keys(options);

                    for (let o = 0; o < ops.length; o++) {
                        ns[ops[o]] = options[ops[o]];
                    }
                    p.appendChild(ns);
                    return true;
                }
            } else {
                uclog('error', 'Invalid or missing options to addScript.');
            }
            return false;
        }

        /**
         * Internal function to reload the current page.
         */
        function doReload() {
            win.location.reload();
        }

        /**
         * Internal function to add a frame for messaging
         *
         * @params {string} locator - locator string to use to name new frame
         * @returns {boolean} - true if successful, false if not
         */
        function addFrame(locator) {
            if (!win.frames[locator]) {
                if (doc.body) {
                    const iframe = doc.createElement('iframe');
                    iframe.style.cssText = 'display:none';
                    iframe.name = locator;
                    doc.body.appendChild(iframe);
                } else {
                    /* In the case where this stub is located in the head,
                     * this allows us to inject the iframe more quickly than
                     * relying on DOMContentLoaded or other events.
                     */
                    setTimeout(addFrame.bind(win, locator), 5);
                }
                return true;
            }
            return false;
        }

        /**
         * Internal function to find a frame, if it exists
         *
         * @param {string} locator - Locator string to use to find frame
         * @returns {object} - Child frame object if found, else null
         */
        function findFrame(locator) {
            let cf = null;

            for (let w = win; w; w = w.parent) {
                try {
                    if (w.frames && w.frames[locator]) {
                        cf = w;
                        break;
                    }
                } catch (e) {}
                if (w === win.top) {
                    break;
                }
            }
            return cf;
        }

        /**
         * Internal function to set a messaging handler
         *
         * @param {function} handler - Function to use as messaging handler
         */
        function setHandler(handler) {
            if (win.addEventListener) {
                win.addEventListener('message', handler, false);
            } else {
                win.attachEvent('onmessage', handler);
            }
        }

        /**
         * Handle detected changes in region (such as CCPA to GDPR).
         *
         * @param {string} prevRegion - The previous region
         * @param {string} newRegion - The new (current) region
         */
        function onRegionChange(prevRegion, newRegion) {
            if (dbg) {
                uclog('debug', 'User-Consent detected region change from "' + prevRegion + '" to "' + newRegion + '".');
            }
            if (typeof config.regionChangeAction === 'function') {
                config.regionChangeAction(prevRegion, newRegion, config.consentLinkAction);
            }
        }

        /**
         * Sets the control cookie
         *
         * @param {Object} [ctrls] - Optional controls object to use to write to cookie (writes current values by default)
         */
        function setControlsCookie(ctrl) {
            /* Update the controls values */
            controls = ctrl || {
                ccpaTCS: uspString,
                consentInteractions: consentInteractions,
                consentTime: consentTime,
                consentVersion: consentVersion,
                countryCode: geoCountry,
                region: config.regId,
                stateCode: geoState,
                userConsentVersion: userConsentVersion
            };
            if (isTop()) {
                /* Set the cookie */
                setCookie(config.controlCookie, 'ccc=' + controls.countryCode + '&csc=' + controls.stateCode +
                    '&cic=' + controls.consentInteractions + '&otvers=' + controls.consentVersion +
                    '&pctm=' + ((controls.consentTime && encodeURIComponent(controls.consentTime.toISOString())) || '0') +
                    '&reg=' + controls.region + '&ustcs=' + encodeURIComponent(controls.ccpaTCS) +
                    '&vers=' + controls.userConsentVersion, {
                        domain: config.cookieDomain,
                        expires: getExpiration(config.consentExpireIn).toUTCString(),
                        path: '/',
                        samesite: config.cookieSameSite,
                        secure: config.cookieSecure
                    }
                );
            }
        }

        /**
         * Internal function to force reconsent by removing the consent cookies and reloading the page.
         */
        function forceReconsent() {
            if (isTop()) {
                doc.cookie = config.consentCookie + '=; Domain=' + config.cookieDomain + '; Path=/; Expires=Thu, 01 Jan 2000 00:00:01 GMT;';
                doc.cookie = config.confirmCookie + '=; Domain=' + config.cookieDomain + '; Path=/; Expires=Thu, 01 Jan 2000 00:00:01 GMT;';
                setControlsCookie({
                    ccpaTCS: '',
                    consentInteractions: consentInteractions,
                    consentTime: null,
                    consentVersion: consentVersion,
                    countryCode: geoCountry,
                    region: config.regId,
                    stateCode: geoState,
                    userConsentVersion: userConsentVersion
                });
                setTimeout(doReload, 100);
            }
        }

        /**
         * Process the control cookie
         *
         * @returns {object} - Object containing the values from the current control cookie, if any
         */
        function processControls() {
            const value = getCookie(config.controlCookie),
                result = {
                    consentInteractions: consentInteractions,
                    consentTime: null,
                    consentVersion: '',
                    countryCode: '',
                    region: '',
                    stateCode: '',
                    userConsentVersion: ''
                };

            /* Grab control values from control cookie, if set */
            if (typeof value === 'string' && value.length !== 0) {
                /* We have a control cookie */
                const fields = value.split('&');
                for (let f = 0; f < fields.length; f++) {
                    let vals = fields[f].split('=');
                    if (typeof vals[0] === 'string' && vals[0].length !== 0 && typeof vals[1] === 'string') {
                        switch (vals[0]) {
                        case 'ccc':
                            result.countryCode = vals[1].toLowerCase();
                            break;
                        case 'csc':
                            result.stateCode = vals[1].toLowerCase();
                            break;
                        case 'cic':
                            result.consentInteractions = parseInt(vals[1], 10);
                            if (isNaN(result.consentInteractions) || result.consentInteractions < consentInteractions) {
                                result.consentInteractions = consentInteractions;
                            }
                            break;
                        case 'otvers':
                            result.consentVersion = vals[1].toLowerCase();
                            break;
                        case 'pctm':
                            let ct;
                            try {
                                ct = (vals[1] === '0') ? null : new Date(decodeURIComponent(vals[1]));
                            } catch (e) {
                                ct = null;
                            }
                            result.consentTime = (ct === null || isNaN(ct.valueOf())) ? null : ct;
                            break;
                        case 'reg':
                            result.region = vals[1].toLowerCase();
                            break;
                        case 'ustcs':
                            try {
                                result.ccpaTCS = decodeURIComponent(vals[1]).toUpperCase();
                            } catch (e) {
                                result.ccpaTCS = '';
                            }
                            break;
                        case 'vers':
                            result.userConsentVersion = vals[1].toLowerCase();
                            break;
                        }
                    }
                }
            }

            return result;
        }

        /**
         * Process the consent time of the user from the Optanon confirmation cookie (aka OptanonAlertBoxClosed).
         *
         * @returns {object} - A Date object containing the value of the cookie, if any
         */
        function processConsentTime() {
            const val = getCookie(config.confirmCookie);

            /* Grab consent time data from cookie, if set */
            if (typeof val === 'string' && val.length !== 0) {
                /* We have a confirmation cookie */
                let newTime = new Date(val);
                if (!win.isNaN(newTime.valueOf())) {
                    return newTime;
                }
            }

            return null;
        }

        /**
         * Process the IAB, OptanonConsent, and Control cookies to determine any changes in the consent value.
         */
        function processConsentChanges(gstr) {
            const abcDate = processConsentTime(),
                oldConfirmed = consentConfirmed,
                oldDate = consentTime,
                otDomData = (win.OneTrust && typeof win.OneTrust.GetDomainData === 'function') ? win.OneTrust.GetDomainData() : null;
            let changed = false,
                reduced = false,
                interact = '',
                newState;

            /* Pull a bit of OT context data */
            if (otDomData) {
                try {
                    interact = otDomData.ConsentIntegrationData.consentPayload.dsDataElements.InteractionType;
                } catch (e) {
                    interact = '';
                }
            }

            /* Prep the consentTime from the OptanonAlertBoxClosed cookie, if it exists.
             * As this is a potential consent change, this must exist for the change to be valid. */
            if (abcDate && (consentTime === null || abcDate > consentTime)) {
                consentConfirmed = true;
                consentTime = abcDate;
            }

            /* If GPP, try pulling directly */
            if (gstr && !usingIabGpp) {
                gstr = '';
            }

            /* Compare the new state to the previous state */
            newState = processConsentState(gstr);
            for (let c of categories) {
                if (newState[c] !== consentState[c]) {
                    changed = true;
                    if (newState[c] !== true) {
                        reduced = true;
                        break;
                    }
                }
            }

            if (changed || (!oldConfirmed && consentConfirmed)) {
                /* The consent states are different so set the control cookie and do the consent change logic... */
                const newDate = win.WBD.UserConsent_wrapproc > 0 ? new Date(win.WBD.UserConsent_wrapproc) : null;
                let oldState;

                /* Increment the consent change counter */
                consentInteractions++;
                /* Tweak the consent time, if we should */
                if (newDate && (consentTime === null || newDate.getTime() > (consentTime.getTime() + config.consentChangeActionDelay + 1000))) {
                    consentTime = newDate;
                }
                /* Update local state so new values are available */
                oldState = consentState;
                consentState = newState;
                /* Update the UspAPI string */
                setUspAPIstring();
                if (usingIabGpp) {
                    /* Update the GPP string */
                    setGppConsent(gstr, newState);
                }
                /* Update the controls cookie */
                setControlsCookie();
                /* If this is an actual change of consent, not just confirming the defaults... */
                if (changed) {
                    if (dbg) {
                        try {
                            /* Track consent change in history */
                            consentHistory.push({
                                ts: new Date(),
                                act: 'CHG',
                                desc: JSON.stringify(newState),
                                res: (config.reloadOnConsentChange || (config.reloadOnConsentReduction && reduced)),
                                note: (typeof config.consentChangeAction === 'function' ? 'change function' : '')
                            });
                        } catch (e) {
                            uclog('error', 'Failed to track consent change: ', e);
                        }
                    }
                    /* If we have a consentChangeAction, do that now... */
                    if (typeof config.consentChangeAction === 'function') {
                        config.consentChangeAction(getConsentState(), config.regId, consentVersion, oldState);
                    }
                    /* Fire off the consent changed event */
                    doc.dispatchEvent(new CustomEvent('userConsentChanged', {
                        bubbles: false,
                        cancelable: false,
                        detail: {
                            region: config.regId,
                            time: consentTime,
                            otId: consentId,
                            otVers: consentVersion,
                            otIact: interact,
                            old: oldState,
                            new: getConsentState(),
                            gpcActive: usingGpc,
                            gpp: gppString,
                            gppCmpId: gppCmpId,
                            gppVers: gppStringVer,
                            usp: uspString,
                            tcf: tcString,
                            tcfVers: tcStringVer,
                            acf: acString
                        }
                    }));
                    if (config.reloadOnConsentChange || (reduced && config.reloadOnConsentReduction)) {
                        /* Reload the page - new values will be available on re-initialization */
                        setTimeout(doReload, 500);
                    } else {
                        if (isTop()) {
                            try {
                                win.sessionStorage.setItem('_ucWBDCons', JSON.stringify({
                                    consentState: consentState,
                                    consentTime: consentTime,
                                    consentVersion: consentVersion,
                                    consentConfirmed: consentConfirmed,
                                    gppString: gppString,
                                    tcString: tcString,
                                    acString: acString
                                }));
                                win.postMessage('_ucWBDConsReset', '*');
                            } catch (e) {
                                uclog('error', 'Failed to update session storage and notify children of consent change: ', e);
                            }
                        }
                    }
                }
            }
            if (!changed && win.WBD.UserConsent_optLoaded) {
                /* Consent didn't change. */
                try {
                    /* Track consent no-change in history */
                    consentHistory.push({
                        ts: new Date(),
                        act: 'NCC',
                        desc: JSON.stringify(consentState),
                        res: false,
                        note: config.regId
                    });
                } catch (e) {
                    uclog('error', 'Failed to track consent no-change: ', e);
                }

                consentTime = oldDate;
                if (!controls.region || (!controls.consentVersion && consentVersion) ||
                    !controls.userConsentVersion || controls.userConsentVersion < '3.1.1') {

                    /* No controlCookie set, so do that now */
                    setControlsCookie();
                }

                /* Fire off the consent changed event */
                doc.dispatchEvent(new CustomEvent('userConsentNotChanged', {
                    bubbles: false,
                    cancelable: false,
                    detail: {
                        region: config.regId,
                        time: consentTime,
                        otId: consentId,
                        otVers: consentVersion,
                        otIact: interact,
                        new: getConsentState(),
                        gpcActive: usingGpc,
                        gpp: gppString,
                        gppCmpId: gppCmpId,
                        gppVers: gppStringVer,
                        usp: uspString,
                        tcf: tcString,
                        tcfVers: tcStringVer,
                        acf: acString
                    }
                }));
            }
            win.WBD.UserConsent_optLoaded = true;
            win.WBD.UserConsent_wrapproc = 0;
        }

        /**
         * This creates the GPP, USPAPI, and TCFAPI stub functions used for IAB, as required.
         * The GPP and TCFAPI stub code was based off an example from the IAB Tech Lab.
         */
        function createIabHandlers() {
            let cmpName,
                cmpVer,
                cmpPostHandler = function _cmpPostHandler(cmpName, event) {
                    const msgIsString = typeof event.data === 'string',
                        cmpCall = cmpName + 'Return';
                    let json,
                        data;

                    try {
                        json = msgIsString ? JSON.parse(event.data) : event.data;
                    } catch (err) {
                        json = {};
                    }
                    data = json[cmpCall];
                    if (data && typeof data.callId !== 'undefined' && typeof cmpCallbacks[data.callId] === 'function') {
                        const cid = data.callId,
                            rval = data.returnValue;
                        try {
                            if (rval && typeof rval.listenerId === 'number' && data.success === true) {
                                if (dbg) {
                                    uclog('debug', 'Calling post message callback ' + cid + ' (listenerId: ' + rval.listenerId + ')');
                                }
                                eventCallbacks[rval.listenerId] = cid;
                                cmpCallbacks[cid](rval, data.success);
                            } else {
                                if (dbg) {
                                    uclog('debug', 'Calling post message callback ', cid);
                                }
                                cmpCallbacks[cid](rval, data.success);
                                delete cmpCallbacks[cid];
                            }
                        } catch (err) {
                            uclog('error', 'Post message callback error (callId ' + cid + '): ', err);
                        }
                    } else if (data) {
                        uclog('error', 'Post message bad or missing callback (callId ' + data.callId + ').');
                    }
                },
                cmpMsgHandler = function _cmpMsgHandler(cmpName, event) {
                    const msgIsString = typeof event.data === 'string',
                        cmpCall = cmpName + 'Call';
                    let i = {},
                        json,
                        handler = function (retValue, success) {
                            let returnMsg = {};

                            returnMsg[cmpName + 'Return'] = {
                                returnValue: retValue,
                                success: success,
                                callId: i.callId
                            };
                            try {
                                event.source.postMessage(msgIsString ? JSON.stringify(returnMsg) : returnMsg, '*');
                            } catch (e) {
                                uclog('error', 'Failed to post reply: ', e);
                            }
                        };

                    try {
                        json = msgIsString ? JSON.parse(event.data) : event.data;
                    } catch (err) {
                        json = {};
                    }

                    if (typeof json === 'object' && json !== null && json[cmpCall]) {
                        i = json[cmpCall];
                        if (cmpName === '__gpp') {
                            win.__gpp(i.command, handler, i.parameter, i.version);
                        } else {
                            win[cmpName](i.command, i.version, handler, i.parameter);
                        }
                    }
                };

            /* CCPA IAB handler must always be defined, even in GDPR regions... */
            usingUspFrame = findFrame('__uspapiLocator');
            if (usingUspFrame === null) {
                addFrame('__uspapiLocator');
                win.__uspapi = function _uspapi(cmd, vers, cb) {
                    if (typeof cb === 'function') {
                        vers = (vers === 0 ? 1 : vers);  /* TCF convention is 0 = latest */
                        if (cmd === 'getUSPData' && vers === 1) {
                            cb({
                                version: 1,
                                uspString: win.WBD.UserConsent.getUspAPIstring()
                            }, true);
                            return true;
                        } else if (cmd === 'ping') {
                            cb({
                                version: 1,
                                uspapiLoaded: true
                            }, true);
                            return true;
                        } else {
                            cb(null, false);
                        }
                    }
                    return false;
                };
                win.__uspapi.msgHandler = cmpMsgHandler.bind(win, '__uspapi');
                setHandler(win.__uspapi.msgHandler);
                if (isChild()) {
                    uclog('error', 'Unable to locate USP messaging frame from iframe!  Consent logic may not work correctly!');
                } else if (dbg) {
                    uclog('debug', 'IAB for CCPA ready.');
                }
            } else {
                /* USP Messaging to an existing iframe */
                win.__uspapi = function (cmd, vers, cb, param) {
                    const callId = frameCallId++,
                        msg = {
                            __uspapiCall: {
                                command: cmd,
                                parameter: param,
                                version: vers || 1,
                                callId: callId
                            }
                        };
                    cmpCallbacks[callId] = cb;
                    usingUspFrame.postMessage(msg, '*');
                };
                win.__uspapi.postHandler = cmpPostHandler.bind(win, '__uspapi');
                setHandler(win.__uspapi.postHandler);
                if (dbg) {
                    uclog('debug', 'IAB for CCPA ready (via frame).');
                }
            }

            /* GDPR IAB handler stub is only needed in GDPR regions... */
            if (usingIabGdpr) {
                cmpName = '__tcfapi';
                cmpVer = '2.0';
                usingCmpFrame = findFrame(cmpName + 'Locator');
                if (usingCmpFrame === null && typeof win[cmpName] !== 'function') {
                    addFrame(cmpName + 'Locator');

                    win[cmpName] = function stubCMP() {
                        let b = arguments;

                        win[cmpName].a = win[cmpName].a || [];
                        if (b.length > 0) {
                            if (b[0] === 'ping') {
                                b[2]({
                                    apiVersion: cmpVer,
                                    gdprApplies: true,
                                    gdprAppliesGlobally: false,
                                    cmpLoaded: false,
                                    cmpStatus: 'stub',
                                    displayStatus: 'hidden'
                                }, true);
                            } else if (b[0] === 'setGdprApplies' && b.length > 3 && typeof b[3] === 'boolean') {
                                usingIabGdpr = b[3];
                                if (usingIabGdpr && usingIabCcpa) {
                                    usingIabCcpa = false;
                                }
                            } else {
                                win[cmpName].a.push([].slice.apply(b));
                            }
                        }
                        return win[cmpName].a;
                    };
                    win[cmpName].msgHandler = cmpMsgHandler.bind(win, cmpName);
                    setHandler(win[cmpName].msgHandler);
                    if (isChild()) {
                        uclog('error', 'Unable to locate TCF messaging frame from iframe!  Consent logic may not work correctly!');
                    } else if (dbg) {
                        uclog('debug', 'IAB (v' + cmpVer + ') for GDPR ready.');
                    }

                    /* Add CMP message with callback to catch OT not calling OptanonWrapper in some cases */
                    win[cmpName]('getTCData', 0, win.OptanonWrapper);
                } else if (usingCmpFrame) {
                    /* CMP Messaging to an existing iframe */
                    win.__tcfapi = function (cmd, vers, cb, param) {
                        const callId = frameCallId++,
                            msg = {
                                __tcfapiCall: {
                                    command: cmd,
                                    parameter: param,
                                    version: vers || 2,
                                    callId: callId
                                }
                            };
                        cmpCallbacks[callId] = cb;
                        usingCmpFrame.postMessage(msg, '*');
                        if (cmd === 'removeEventListener' && typeof param === 'number' && typeof eventCallbacks[param] !== 'undefined' && cmpCallbacks[eventCallbacks[param]]) {
                            /* Remove the event listener handler */
                            delete cmpCallbacks[eventCallbacks[param]];
                            delete eventCallbacks[param];
                        }
                    };
                    win.__tcfapi.postHandler = cmpPostHandler.bind(win, '__tcfapi');
                    setHandler(win.__tcfapi.postHandler);
                    if (dbg) {
                        uclog('debug', 'IAB (v' + cmpVer + ') for GDPR ready (via frame).');
                    }
                }
            }

            /* GPP IAB handler stub is only needed when enabled... */
            if (usingIabGpp) {
                cmpName = '__gpp';
                cmpVer = '1.1';
                usingGppFrame = findFrame('__gppLocator');
                if (usingGppFrame === null && !win.__gpp) {
                    let apis = [],
                        sects;

                    addFrame('__gppLocator');
                    /* Init the GPP CMP.  We get __gpp as part of the deal. */
                    win.__gpp = function () { return null };
                    gppCmpApi = gppCmpApi || new CmpApi(1, 1);
                    gppCmpApi.setCmpStatus('loading');
                    sects = Object.keys(gppApisAvail);
                    for (let s = 0; s < sects.length; s++) {
                        apis.push(gppApisAvail[sects[s]].toString(10) + ':' + sects[s]);
                    }
                    gppCmpApi.setSupportedAPIs(apis);
                    gppStub = win.__gpp;  /* Keep this for later */
                    win.__gpp.msgHandler = cmpMsgHandler.bind(win, '__gpp');
                    setHandler(win.__gpp.msgHandler);
                    if (isChild()) {
                        uclog('error', 'Unable to locate GPP messaging frame from iframe!  Consent logic may not work correctly!');
                    } else if (dbg) {
                        uclog('debug', 'IAB for GPP ready.');
                    }
                } else if (usingGppFrame !== null) {
                    /* GPP Messaging to an existing iframe */
                    win.__gpp = function (cmd, cb, param, vers) {
                        const callId = frameCallId++,
                            msg = {
                                __gppCall: {
                                    command: cmd,
                                    parameter: param,
                                    version: vers || cmpVer,
                                    callId: callId
                                }
                            };
                        cmpCallbacks[callId] = cb;
                        usingGppFrame.postMessage(msg, '*');
                        if (cmd === 'removeEventListener' && typeof param === 'number' && typeof eventCallbacks[param] !== 'undefined' && cmpCallbacks[eventCallbacks[param]]) {
                            /* Remove the event listener handler */
                            delete cmpCallbacks[eventCallbacks[param]];
                            delete eventCallbacks[param];
                        }
                    };
                    win.__gpp.postHandler = cmpPostHandler.bind(win, '__gpp');
                    setHandler(win.__gpp.postHandler);
                    if (dbg) {
                        uclog('debug', 'IAB for GPP ready (via frame).');
                    }
                }
            }
        }

        /**
         * Function to be clever and get the correct IAB consent string (and addtl consent string) from the CMP
         *
         * @param {function} callback - The callback to call with the consent string (may be immediate),
         *                              expected to take five parameters, a string, a number, a string, and an Error:
         *                                  function (region, iabVersion, cmpString, addtlString, error)
         *                              If there is an error, the cmpString and addtlString will both be empty.
         * @param {string} [region] - The region to force getting the consent from (optional).
         */
        function getCmpString(callback, region) {
            let cmp,
                reqIab = '',
                reqCmd,
                reqVer;

            if (typeof callback !== 'function') {
                uclog('error', 'getCmpString called without callback');
                return;  // Nothing to do
            }
            region = region || this.getRegion();
            if (!config.useIAB) {
                if (dbg) {
                    uclog('debug', 'getCmpString called with IAB disabled');
                }
                callback(region, 0, '', '', new Error('IAB disabled'));
                return;
            }
            if (usingIabGpp) {
                if (typeof gppString === 'string' && gppString.length !== 0) {
                    // We have the string, so return it now.
                    if (dbg) {
                        uclog('debug', 'getCmpString returning GPP CMP string');
                    }
                    callback(region, 1, gppString, '', null);
                    return;
                }
                cmp = win.__gpp;
                reqCmd = 'ping';
                reqVer = 1;
                reqIab = 'GPP';
            } else if (usingIabGdpr) {
                if (typeof tcString === 'string' && tcString.length !== 0) {
                    // We have the string, so return it now.
                    if (dbg) {
                        uclog('debug', 'getCmpString returning GDPR v2 CMP string');
                    }
                    callback(region, 2, tcString, acString, null);
                    return;
                }
                cmp = win.__tcfapi;
                reqCmd = 'getTCData';
                reqVer = 2;
                reqIab = 'TCF';
            }
            if (reqCmd) {
                let cmpCb = function _getCmpStringCB(cb, reg, iab, ver, data, success) {
                    if (success) {
                        if (dbg) {
                            uclog('debug', 'getCmpString returning ' + iab + ' v' + ver + ' CMP string');
                        }
                        cb(reg, ver, (iab === 'GPP' ? data.pingData.gppString : data.tcString), (data.addtlConsent ? data.addtlConsent : ''), null);
                    } else {
                        if (dbg) {
                            uclog('debug', 'getCmpString returning ' + iab + ' v' + ver + ' error');
                        }
                        cb(reg, ver, '', '', new Error(iab + ' CMP request failure'));
                    }
                }.bind(this, callback, region, reqIab, reqVer);
                if (reqIab === 'GPP') {
                    cmp(reqCmd, cmpCb);
                } else {
                    cmp(reqCmd, reqVer, cmpCb);
                }
            } else {
                // We have the USPAPI string, so return it now.
                if (dbg) {
                    uclog('debug', 'getCmpString returning CCPA v1 CMP string');
                }
                callback(region, 1, uspString, '', (uspString.length !== 0 ? null : new Error('CMP request failure')));
            }
        }

        /**
         * Function to initialize the UserConsent code
         *
         * @param {object} conf - the configuration object
         */
        function init(conf) {
            let cats,
                forceLang = false,
                prop,
                reg = null,
                ucFrame;

            if (consentState !== null) {
                return;  /* init already called */
            }

            /* Process config */
            if (!conf || !conf.domId || !conf.cookieDomain) {
                throw new Error('Invalid config passed to user-consent!');
            }

            /* Are we running in an iframe with a parent UC? */
            conf.regId = '';
            ucFrame = findFrame('_usrConWBD');
            if (ucFrame !== null) {
                if (win === win.top) {
                    /* We are NOT an iframe, but another instance is acting like it. */
                    uclog('error', 'Detected an instance of UserConsent in an iframe acting as the primary instance.  This was likely caused by a delay in this instance initializing, which must be corrected.  Consent is not working correctly!');
                } else {
                    /* We are an iframe in a window running UserConsent */
                    let sconf,
                        scons;

                    frame = win.name || 'child';
                    try {
                        sconf = JSON.parse(win.sessionStorage.getItem('_ucWBDConf'));
                    } catch (err) {
                        sconf = null;
                        uclog('error', 'Failed to parse parent frame consent settings.');
                    }
                    if (typeof sconf === 'object' && sconf !== null) {
                        /* We have a remote config object from a parent UserConsent, use it */
                        conf.countryCode = sconf.countryCode;
                        conf.cookieDomain = sconf.cookieDomain;
                        conf.cookieSameSite = sconf.cookieSameSite;
                        conf.cookieSecure = sconf.cookieSecure;
                        conf.domId = sconf.domId;
                        conf.languageFromBrowser = !!sconf.langFromBrowser;
                        conf.enableDebug = !!sconf.enableDebug;
                        conf.enableGPC = !!sconf.enableGPC;
                        conf.regId = sconf.regId;
                        conf.stateCode = sconf.stateCode;
                        conf.src = sconf.src;
                        try {
                            scons = JSON.parse(win.sessionStorage.getItem('_ucWBDCons'));
                        } catch (err) {
                            scons = null;
                            uclog('error', 'Failed to parse parent frame consent state.');
                        }
                        if (typeof scons === 'object' && scons !== null) {
                            consentState = scons.consentState;
                            consentTime = scons.consentTime;
                            consentVersion = scons.consentVersion;
                            consentConfirmed = scons.consentConfirmed;
                            gppString = scons.gppString;
                            tcString = scons.tcString;
                            acString = scons.acString;
                            ucChild = true;
                        }
                        if (!sconf.parentReload) {
                            /* If the parent window won't reload on consent changes... */
                            setHandler(function _handleChangesInFrame(e) {
                                var reduced,
                                    val;

                                if (e.data === '_ucWBDConsReset') {
                                    try {
                                        val = JSON.parse(win.sessionStorage.getItem('_ucWBDCons'));
                                    } catch (err) {
                                        val = null;
                                    }
                                    if (typeof val === 'object' && val !== null) {
                                        reduced = hasConsentReduced(consentState, val.consentState);
                                        /* If the iframe is set to reload on changes, do that... */
                                        if (config.reloadOnConsentChange || (reduced && config.reloadOnConsentReduction)) {
                                            /* Reload the frame - new values will be available on re-initialization */
                                            setTimeout(doReload, 600 + config.consentChangeActionDelay);
                                        } else {
                                            /* Update local state so new values are available */
                                            consentState = val.consentState;
                                            consentTime = val.consentTime;
                                            consentVersion = val.consentVersion;
                                            consentConfirmed = val.consentConfirmed;
                                            gppString = val.gppString || '';
                                            tcString = val.tcString || '';
                                            acString = val.acString || '';
                                            setUspAPIstring();
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }

            /* Override with any explicit settings */
            if (typeof conf.gppCategories === 'undefined') {
                conf.gppCategories = defaults.gppCategories;
            } else {
                for (prop in conf.gppCategories) {
                    if (prop === 'usnatv1') {
                        conf.gppCategories.usnat = conf.gppCategories.usnatv1;
                        delete conf.gppCategories.usnatv1;
                    } else if (prop === 'uspnatv1') {
                        conf.gppCategories.uspv1 = conf.gppCategories.uspnatv1;
                        delete conf.gppCategories.uspnatv1;
                    } else if (!gppApisAvail[prop]) {
                        uclog('error', 'Error: Unsupported GPP section "' + prop + '" ignored.');
                        delete conf.gppCategories[prop];
                    }
                }
                for (prop in defaults.gppCategories) {
                    conf.gppCategories[prop] = conf.gppCategories[prop] || defaults.gppCategories[prop];
                }
            }
            for (prop in defaults) {
                config[prop] = (typeof conf[prop] !== 'undefined') ? conf[prop] : defaults[prop];
            }
            defaults = null;  /* Done with defaults */

            /* Top-level configs with no defaults */
            config.cookieDomain = conf.cookieDomain;
            config.domId = conf.domId;
            config.changeRegions = conf.changeRegions;
            dbg = !!(console && (conf.enableDebug || (win.location.search.search(/[?&]wmuc_debug=[1t]/) !== -1)));
            if (dbg) {
                uclog('debug', 'Initializing UserConsent v' + userConsentVersion);
            }
            config.strictIabCompliance = !!config.strictIabCompliance;
            if (typeof conf.countryCode === 'string' && conf.countryCode.length === 2) {
                config.countryCode = conf.countryCode;
            }
            if (typeof conf.stateCode === 'string' && conf.stateCode.length === 2) {
                config.stateCode = conf.stateCode;
            }

            if (config.gpcFixCookie !== '') {
                /* Some GPC extensions are... not great.  Let's try to fix them up a bit with a cookie-based thing in Fastly. */
                fixGpcWithCookie(config.gpcFixCookie);
            }

            /* Are we using external consent data in a webview? */
            if (typeof win.GetExternalConsent === 'object' && win.GetExternalConsent !== null && typeof win.GetExternalConsent.oneTrustCookie === 'function') {
                /* Initialize external consent values */
                let extcon;
                try {
                    extcon = JSON.parse(win.GetExternalConsent.oneTrustCookie());
                } catch (e) {
                    extcon = null;
                }
                if (typeof extcon === 'object' && extcon !== null && extcon.consentedDate && (extcon.gppString || extcon.tcString || extcon.groups)) {
                    /* Valid ExternalConsent, use it */
                    extcon.gppString = extcon.gppString || '';
                    win.OTExternalConsent = extcon;
                }

                if (typeof win.GetExternalConsent.countryCode === 'function' && win.GetExternalConsent.countryCode()) {
                    win.ExternalConsentGeo = {
                        countryCode: win.GetExternalConsent.countryCode(),
                        stateCode: win.GetExternalConsent.stateCode() || ''
                    };
                }
            }
            if (config.enableWebViewCheck && typeof win.OTExternalConsent === 'object' && win.OTExternalConsent !== null && win.OTExternalConsent.consentedDate) {
                config.useExternalConsent = true;
                if (dbg) {
                    uclog('debug', 'Reading consent from external consent data: ', win.OTExternalConsent);
                }
            } else {
                config.useExternalConsent = false;
            }

            /* Process country/state */
            const geoRet = (typeof config.geoCheckFunction === 'function') ? config.geoCheckFunction() : null;
            if (isTop() && dbg && win.location.search.search(/[?&]wmuc_cc=[A-Za-z]{2}/) !== -1) {
                /* Pulled from the query string */
                geoCountry = win.location.search.match(/[?&]wmuc_cc=([A-Za-z]{2})/)[1].toUpperCase();
                uclog('debug', 'Set debug CC to: ', geoCountry);
            } else if (config.useExternalConsent && typeof win.ExternalConsentGeo === 'object' && typeof win.ExternalConsentGeo.countryCode === 'string' && win.ExternalConsentGeo.countryCode.length === 2) {
                /* Pulled from the external consent object */
                geoCountry = win.ExternalConsentGeo.countryCode.toUpperCase();
            } else if (typeof config.countryCode === 'string' && config.countryCode.length === 2) {
                /* Pulled from the config options */
                geoCountry = config.countryCode.toUpperCase();
            } else if (geoRet && geoRet.countryCode && geoRet.countryCode.length === 2) {
                /* Pulled from the configured geoCheckFunction */
                geoCountry = geoRet.countryCode.toUpperCase();
            } else {
                /* Pulled from the cookie */
                const ccValue = getCookie(config.ccCookie || 'countryCode');
                if (ccValue && ccValue.length === 2) {
                    geoCountry = ccValue.toUpperCase();
                } else {
                    /* No country value found.  Go get the country code */
                    uclog('error', 'User-Consent unable to determine country, missing or invalid cookies!');
                    getGeoCountry();
                }
            }
            if (!geoCountry || geoCountry.length !== 2) {
                /* No country value found.  Default to default setting or "US", since that is what Prism does. */
                geoCountry = (config.defaultCountry && config.defaultCountry.length == 2) ? config.defaultCountry.toUpperCase() : 'US';
                uclog('error', 'User-Consent unable to determine country, missing or invalid cookies!  Using default (' + geoCountry + ').');
            }
            if (dbg && win.location.search.search(/[?&]wmuc_sc=[A-Za-z]{2}/) !== -1) {
                geoState = win.location.search.match(/[?&]wmuc_sc=([A-Za-z]{2})/)[1].toUpperCase();
                uclog('debug', 'Set debug SC to: ', geoState);
            } else if (config.useExternalConsent && typeof win.ExternalConsentGeo === 'object' && typeof win.ExternalConsentGeo.stateCode === 'string' && win.ExternalConsentGeo.stateCode.length === 2) {
                geoState = win.ExternalConsentGeo.stateCode.toUpperCase();
            } else if (typeof config.stateCode === 'string' && config.stateCode.length === 2) {
                geoState = config.stateCode.toUpperCase();
            } else if (geoRet && geoRet.countryCode && geoRet.countryCode.length === 2) {
                geoState = (typeof geoRet.stateCode === 'string') ? geoRet.stateCode.toUpperCase() : '';
            } else {
                const scValue = getCookie(config.scCookie || 'stateCode');
                if (scValue && scValue.length === 2) {
                    geoState = scValue.toUpperCase();
                }
            }
            if (!geoState || geoState.length === 0) {
                geoState = (config.defaultState && config.defaultState.length > 0) ? config.defaultState.toUpperCase() : '';
                if (dbg) {
                    uclog('debug', 'User-Consent unable to determine state.  Using default (' + geoState + ').');
                }
            }
            geoCS = geoCountry + ':' + geoState;

            if (isTop()) {
                if (!oneTrustLocationSet && config.geoPassedToOneTrust) {
                    /* Prep OneTrust to use our geoIP data (needed for older versions) */
                    win.OneTrust = win.OneTrust || {};
                    win.OneTrust.geolocationResponse = {
                        countryCode: geoCountry,
                        stateCode: geoState
                    };
                }
                /* Set-up handler to catch OneTrust consent change (and other) events */
                let handler = function _OTchanges(e) {
                    /* Make sure we've correctly set the OT region */
                    if (config.geoPassedToOneTrust) {
                        pushGeoLocation();
                    }
                    /* Handle change events after loading */
                    if (optanonLoaded && !config.useExternalConsent && consentState !== null && win.WBD.UserConsent_wrapproc === 0) {
                        /* Handle consent changes */
                        win.WBD.UserConsent_wrapproc = new Date().getTime();
                        if (dbg) {
                            uclog('debug', 'Consent changed event handler determining consent changes.');
                        }
                        /* If we're using GPP, get the new consent data asynchronously */
                        if (usingIabGpp && usingOTGpp) {
                            win.__gpp('ping', function (pd) {
                                if (pd && pd.gppString) {
                                    processConsentChanges(pd.gppString);
                                }
                            });
                        } else {
                            /* We need to wait on cookie updates */
                            setTimeout(processConsentChanges, config.consentChangeActionDelay);
                        }
                    }
                };
                if (win.addEventListener) {
                    win.addEventListener('consent.onetrust', handler, false);
                } else {
                    win.attachEvent('consent.onetrust', handler);
                }
            }

            cats = Object.keys(config.categories);
            categories = [];
            for (let c = 0; c < cats.length; c++) {
                categories.push(config.categories[cats[c]]);
            }
            if (config.changeRegions) {
                for (let act of ['remove', 'replace', 'insert']) {
                    if (config.changeRegions[act] && Array.isArray(config.changeRegions[act]) && config.changeRegions[act].length !== 0) {
                        prop = config.changeRegions[act];
                        for (let i = 0; i < prop.length; i++) {
                            if (typeof prop[i] === 'object' && prop[i] !== null && prop[i].id) {
                                let crl = config.regions.length,
                                    cReg = prop[i],
                                    rid = (act === 'insert' && cReg.insertAfter) ? cReg.insertAfter : cReg.id,
                                    r = 0;

                                RLOOP: for (; r < crl; r++) {
                                    if (config.regions[r] && config.regions[r].id && config.regions[r].id === rid) {
                                        break RLOOP;
                                    }
                                }
                                if (act === 'remove') {
                                    if (r < crl) {
                                        delete config.regions[r];
                                    }
                                } else if (act === 'replace') {
                                    if (r < crl) {
                                        config.regions[r] = cReg;
                                    }
                                } else if (act === 'insert') {
                                    if (r < crl) {
                                        if (cReg.insertAfter) {
                                            delete cReg.insertAfter;
                                            config.regions.splice(r + 1, 0, cReg);
                                        } else {
                                            /* Replace the matching region */
                                            config.regions[r] = cReg;
                                        }
                                    } else {
                                        /* Insert at top of list */
                                        delete cReg.insertAfter;
                                        config.regions.splice(r + 1, 0, cReg);
                                    }
                                }
                            }
                        }
                    }
                }
                delete config.changeRegions;
            }
            for (let r = 0; r < config.regions.length; r++) {
                if (!config.regions[r] || !config.regions[r].id || !config.regions[r].geoMatch) {
                    uclog('error', 'Invalid region, missing id or geoMatch!');
                } else {
                    if ((isTop() && checkInRegion(config.regions[r].geoMatch)) ||
                        (isChild() && config.regions[r].id === conf.regId)) {

                        reg = config.regions[r];
                        break;
                    }
                }
            }
            if (!reg) {
                if (isChild()) {
                    throw new Error('No matching user-consent region, parent and iframe configs do not match!');
                }
                throw new Error('No matching user-consent region!');
            }
            config.regId = reg.id;
            config.defaultLanguage = (reg.defaultLanguage || config.defaultLanguage).toLowerCase();
            /* Get the page language we are using */
            try {
                let lang = '';
                if (!config.languageFromBrowser) {
                    /* We want the language from the page, so try that first... */
                    /* Apparently xml:lang gets precedence over lang.  Thanks W3! */
                    lang = doc.getElementsByTagName('html')[0].getAttribute('xml:lang') || doc.documentElement.lang || config.defaultLanguage;
                }
                if (!lang) {
                    /* Pull the language from the browser */
                    lang = win.navigator.language || config.defaultLanguage;
                }
                pageLang = lang ? lang.substr(0, 2).toLowerCase() : 'en';
            } catch(err) {
                pageLang = 'en';
            }
            if (dbg && win.location.search.search(/[?&]wmuc_lang=[A-Za-z]{2}/) !== -1) {
                /* Try to force the page language to the debug value passed-in */
                let fl = win.location.search.match(/[?&]wmuc_lang=([A-Za-z]{2})/)[1].toLowerCase();
                forceLang = (pageLang !== fl);
                pageLang = fl;
                uclog('debug', 'Set debug Language to: ', pageLang);
            }
            config.adChoicesLinkAction = reg.adChoicesLinkAction || config.adChoicesLinkAction || null;
            config.adChoicesLinkTitle = getLocalizedString(reg.adChoicesLinkTitle || config.adChoicesLinkTitle);
            config.affiliatesLinkAction = reg.affiliatesLinkAction || config.affiliatesLinkAction || null;
            config.affiliatesLinkTitle = getLocalizedString(reg.affiliatesLinkTitle || config.affiliatesLinkTitle);
            config.compatTransition = config.enableTransitionCheck && reg.compatTransition ? reg.compatTransition : null;
            config.compatCategories = reg.compatCategories || config.compatCategories || {};
            config.compatCodes = reg.compatCodes || config.compatCodes || {};
            config.consentExpireIn = reg.consentExpireIn || config.consentExpireIn || 1;
            config.consentLinkAction = reg.consentLinkAction || config.consentLinkAction || null;
            config.consentLinkTitle = getLocalizedString(reg.consentLinkTitle || config.consentLinkTitle);
            config.confirmCookie = reg.confirmCookie || config.confirmCookie;
            config.consentCookie = reg.consentCookie || config.consentCookie;
            config.addtlConsentCookie = reg.addtlConsentCookie || config.addtlConsentCookie;
            if (reg.consentDefaults) {
                config.consentDefaults = mergeConsent(config.consentDefaults, reg.consentDefaults);
            }
            config.consentGpcDefaults = reg.consentGpcDefaults || config.consentGpcDefaults || null;
            config.consentImpliedDefaults = reg.consentImpliedDefaults || config.consentImpliedDefaults || {};
            if (!config.consentImpliedDefaults.required) {
                config.consentImpliedDefaults.required = true;
            }
            config.consentNotApplicable = reg.consentNotApplicable || config.consentNotApplicable || [];
            if (config.consentNotApplicable && Array.isArray(config.consentNotApplicable) && config.consentNotApplicable.length !== 0) {
                /* Remove N/A consents from defaults and implied defaults */
                for (let na of config.consentNotApplicable) {
                    if (typeof config.consentDefaults[na] !== 'undefined') {
                        delete config.consentDefaults[na];
                    }
                    if (typeof config.consentImpliedDefaults[na] !== 'undefined') {
                        delete config.consentImpliedDefaults[na];
                    }
                }
            }
            config.useFixedConsent = (typeof reg.useFixedConsent === 'boolean' ? reg.useFixedConsent : config.useFixedConsent);
            config.domId = reg.domId || config.domId;
            config.src = reg.src || config.src;
            config.gdprIabCookie = reg.gdprIabCookie || config.gdprIabCookie;
            config.tcfOpts = reg.tcfOpts || config.tcfOpts || null;
            config.privacyCenterLinkAction = reg.privacyCenterLinkAction || config.privacyCenterLinkAction || null;
            config.privacyCenterLinkTitle = getLocalizedString(reg.privacyCenterLinkTitle || config.privacyCenterLinkTitle);
            config.rightsRequestLinkAction = reg.rightsRequestLinkAction || config.rightsRequestLinkAction || null;
            config.rightsRequestLinkTitle = getLocalizedString(reg.rightsRequestLinkTitle || config.rightsRequestLinkTitle);
            /* Prep IAB options for region */
            if (config.useIAB) {
                config.iabRegion = ((typeof reg.iabRegion === 'string' && reg.iabRegion) || config.iabRegion).toLowerCase();
                if (config.iabRegion === 'ccpa') {
                    usingIabCcpa = true;
                } else if (config.iabRegion === 'gdpr' && config.tcfOpts) {
                    usingIabGdpr = true;
                } else if (config.iabRegion && config.iabRegion !== 'gpp') {
                    uclog('error', 'Error: Invalid IAB region "' + config.iabRegion + '" specified for region "' + config.regId + '", IAB not enabled for region!');
                }
            }
            /* Prep GPP options for region */
            if (config.useGPP) {
                config.gppSection = ((typeof reg.gppSection === 'string' && reg.gppSection) || config.gppSection).toLowerCase();
                config.gppSection = (config.gppSection === 'usnat' || config.gppSection === 'usnatv1') ? 'usnat' : ((config.gppSection === 'uspv1' || config.gppSection === 'uspnatv1') ? 'uspv1' : config.gppSection);
                if (config.useGPP && config.gppSection) {
                    if (gppApisAvail[config.gppSection] && config.gppCategories[config.gppSection]) {
                        usingIabGpp = true;
                        if (usingIabCcpa && config.ccpaGeos && !checkInRegion(config.ccpaGeos)) {
                            usingIabCcpa = false;
                        }
                    } else {
                        uclog('error', 'Error: Invalid GPP section "' + config.gppSection + '" specified for region "' + config.regId + '", IAB/GPP not enabled for region!');
                    }
                }
            }
            if (!usingIabGpp && !usingIabCcpa && !usingIabGdpr) {
                /* Ignore IAB/GPP region if not supported */
                config.iabRegion = '';
                config.gppSection = '';
                config.useIAB = false;
                config.useGPP = false;
            }

            /* Detect GPC existance and use */
            if (config.enableGPC && config.consentGpcDefaults && navigator.globalPrivacyControl) {
                usingGpc = true;
            }

            /* Expose the countryCode, stateCode, region, and GPC use by adding "userconsent-cntry-<cc> userconsent-state-<sc> userconsent-reg-<reg> userconsent-gpc" classes to the HTML tag... */
            if (config.setPageClass && doc.documentElement) {
                doc.documentElement.className = ((doc.documentElement.className && doc.documentElement.className !== ' ') ? doc.documentElement.className + ' userconsent-cntry-' : 'userconsent-cntry-') + geoCountry.toLowerCase() + ' userconsent-state-' + geoState.toLowerCase() + ' userconsent-reg-' + config.regId.toLowerCase() + (usingGpc ? ' userconsent-gpc' : '');
            }

            if (dbg) {
                uclog('debug', 'GeoIP Country Code: ' + geoCountry + ', using consent region: ' + config.regId);
                uclog('debug', 'IAB ' + (config.useIAB ? 'enabled' : 'disabled'));
            }

            if (usingIabGpp || usingIabCcpa || usingIabGdpr || config.ccpaGeos) {
                /* We are using IAB, so prep the __uspapi function and the __gpp/__tcfapi stubs (later replaced by OneTrust) */
                createIabHandlers();
            }

            /* If we are the top-level instance... */
            if (isTop()) {
                let gpclog;

                /* Grab control values from control cookie, if set */
                controls = processControls();
                consentInteractions = controls.consentInteractions;

                if (config.useExternalConsent) {
                    /* Grab consent time data from external consent object */
                    try {
                        consentTime = new Date(win.OTExternalConsent.consentedDate);
                        usingGpc = false;
                        if (dbg) {
                            uclog('debug', 'Consent time read from external consent data: ', consentTime);
                        }
                    } catch (e) {
                        uclog('error', 'Consent Date from external consent data is invalid.');
                        consentTime = null;  /* Force using defaults */
                    }
                } else {
                    /* Grab consent time data from cookie, if set */
                    consentTime = processConsentTime();

                    /* Use the most relevant consent time */
                    if (controls.consentTime !== null && (consentTime === null || controls.consentTime > consentTime)) {
                        consentTime = controls.consentTime;
                        if (dbg) {
                            uclog('debug', 'Consent time read from "' + config.controlCookie + '": ', consentTime);
                        }
                    } else if (consentTime !== null && dbg) {
                        uclog('debug', 'Consent time read from "' + config.confirmCookie + '": ', consentTime);
                    }
                }

                gpclog = (dbg && usingGpc) ? ' [GPC override]' : '';
                if (consentTime !== null) {
                    /* We have a confirmation cookie */
                    consentConfirmed = true;
                    consentState = processConsentState();
                    if (consentConfirmed) {
                        if (controls.consentTime !== null && controls.consentTime < consentTime) {
                            /* Need to reset controls cookie */
                            controls.region = '';
                        }
                        if (dbg) {
                            uclog('debug', 'Consent state read from ' + consentSource + ' (' + consentVersion + ')' + gpclog + ': ', consentState);
                            if (usingCompatMode) {
                                uclog('debug', 'Consent state using compatibility config.');
                            }
                        }
                    } else {
                        /* This means the OptanonConsent cookie is expired or deleted, so we are using defaults again */
                        consentTime = null;
                        controls.region = '';  /* This will force the cookie to be reset */
                        if (dbg) {
                            uclog('debug', 'Consent state expired or removed, reset from defaults' + gpclog + ': ', consentState);
                        }
                    }
                } else {
                    /* We are using the defaults */
                    consentState = copyConsent(config.consentDefaults);
                    /* Check against GPC */
                    if (usingGpc) {
                        consentState = mergeConsent(consentState, config.consentGpcDefaults);
                    }
                    if (dbg) {
                        uclog('debug', 'Consent state from defaults' + gpclog + ': ', consentState);
                    }
                }

                /* If we're using GPP, but don't have a GPP string, set it */
                if (usingIabGpp && !gppString) {
                    setGppConsent('', consentState);
                }

                if (dbg) {
                    try {
                        /* Track consent change in history */
                        consentHistory.push({
                            ts: new Date(),
                            act: 'SET',
                            desc: JSON.stringify(consentState),
                            res: (consentTime !== null),
                            note: config.regId
                        });
                    } catch (e) {
                        uclog('error', 'Failed to track setting initial consent: ', e);
                    }
                }
            }

            /* Make sure this is not a second instance */
            if (win.WBD.UserConsent_initted) {
                uclog('error', 'ERROR:  Second instance of UserConsent initialized!');
                return;
            }
            win.WBD.UserConsent_initted = true;

            setUspAPIstring();

            /* If we are the top-level instance... */
            if (isTop()) {
                if (config.useExternalConsent) {
                    /* If using external consent, write the controls cookie */
                    setControlsCookie();
                } else if (controls.region && controls.region !== config.regId) {
                    /* If we have switched regions, maybe do something about that... */
                    onRegionChange(controls.region, config.regId);
                }

                /* Tag __gpp as "ready" if we have a GPP string */
                /* if (usingIabGpp && gppString.length > 0) {
                    gppCmpApi.setEventStatus('gpploaded');
                } */

                /* Setup the frame data */
                if (ucFrame === null) {
                    /* Prep the session data for frames */
                    if (addFrame('_usrConWBD')) {
                        if (dbg) {
                            uclog('debug', 'Setup UserConsent IPC frame.');
                        }

                        try {
                            win.sessionStorage.setItem('_ucWBDConf', JSON.stringify({
                                cookieDomain: config.cookieDomain,
                                cookieSameSite: config.cookieSameSite,
                                cookieSecure: config.cookieSecure,
                                countryCode: geoCountry,
                                domId: config.domId,
                                enableDebug: dbg,
                                langFromBrowser: config.languageFromBrowser,
                                parentReload: config.reloadOnConsentChange,
                                regId: config.regId,
                                src: config.src,
                                stateCode: geoState
                            }));
                            win.sessionStorage.setItem('_ucWBDCons', JSON.stringify({
                                consentState: consentState,
                                consentTime: consentTime,
                                consentVersion: consentVersion,
                                consentConfirmed: consentConfirmed,
                                gppString: gppString,
                                tcString: tcString,
                                acString: acString
                            }));
                        } catch (err) {
                            funcs.uclog('error', 'Failed to set UserConsent frame data!');
                        }
                    } else {
                        uclog('error', 'Failed to setup UserConsent IPC frame!');
                    }
                }
            }

            const funcs = {
                    isTop,
                    uclog
                },
                vals = {
                    acString,
                    config,
                    consentState,
                    consentTime,
                    consentVersion,
                    consentConfirmed,
                    dbg,
                    forceLang,
                    geoCountry,
                    geoState,
                    gppString,
                    pageLang,
                    tcString,
                    ucFrame,
                    usingGpc
                };

            /* Create a function to modify the DOM once it's ready. */
            const moddom = function _moddom(win, doc, funcs, vals, evt) {
                    if (!evt || (evt && doc.readyState === 'interactive')) {
                        /* OK to modify the DOM, so get to it... */
                        /* Expose the countryCode, stateCode, region, and GPC use by adding "userconsent-cntry-<cc> userconsent-state-<sc> userconsent-reg-<reg> userconsent-gpc" classes to the HTML tag... */
                        if (vals.config.setPageClass && !doc.documentElement.className.toString().includes('userconsent-cntry-')) {
                            doc.documentElement.className = ((doc.documentElement.className && doc.documentElement.className !== ' ') ? doc.documentElement.className + ' userconsent-cntry-' : 'userconsent-cntry-') + vals.geoCountry.toLowerCase() + ' userconsent-state-' + vals.geoState.toLowerCase() + ' userconsent-reg-' + vals.config.regId.toLowerCase() + (vals.usingGpc ? ' userconsent-gpc' : '');
                        }

                        if (funcs.isTop()) {
                            /* Load the OneTrust script */
                            const ots = doc.createElement('script');

                            win.WBD.UserConsent_wait = setTimeout(win.OptanonWrapper.bind(win, true), vals.config.oneTrustLoadTimeout);
                            if (vals.consentConfirmed) {
                                ots.async = true;
                            }
                            ots.charset = 'utf-8';
                            if (!vals.config.languageFromBrowser || vals.forceLang) {
                                ots.dataset.documentLanguage = 'true';
                                if (vals.forceLang) {
                                    ots.dataset.language = vals.pageLang;
                                }
                            }
                            ots.dataset.domainScript = vals.config.domId;
                            ots.type = 'text/javascript';
                            ots.src = vals.config.src;
                            if (doc.head) {
                                doc.head.appendChild(ots);
                            } else {
                                doc.body.appendChild(ots);
                            }
                            if (vals.dbg) {
                                funcs.uclog('debug', 'Loading OneTrust.');
                            }
                        }
                    }
                };

            if (doc.readyState === 'loading') {
                /* Wait on the DOM to finish being read */
                doc.addEventListener('readystatechange', moddom.bind(this, win, doc, funcs, vals));
            } else {
                /* Ready to go now, so just do it */
                moddom(win, doc, funcs, vals);
            }

            /* Debug GPP events */
            if (dbg && usingIabGpp && win.__gpp) {
                win.__gpp('addEventListener', function _handleGppChanges(obj, _suc) {
                    uclog('debug', 'GPP event: ', obj);
                });
            }

            /* Detect EasyList Cookie blocking of OneTrust */
            testOTBlocking();

            /***
                We are ready to handle consent at this point!
            ***/

            if (dbg) {
                uclog('debug', 'Dispatching UserConsentReady event.');
            }
            doc.dispatchEvent(new CustomEvent('userConsentReady', {
                bubbles: false,
                cancelable: false,
                detail: {
                    region: config.regId,
                    time: new Date(),
                    consentConfirmed: consentConfirmed
                }
            }));
        }

        /* The wrapper called on Optanon load and cookie changes */
        if (win.WBD.UserConsent_loaded) {
            uclog('error', 'ERROR:  Second instance of UserConsent loaded!');
        } else {
            win.WBD.UserConsent_loaded = true;
            win.WBD.UserConsent_optLoaded = false;
            win.WBD.UserConsent_wrapproc = 0;
            win.WBD.UserConsent_wait = -1;
            win.OptanonWrapper = function OptanonWrapper(tos) {
                if (!optanonLoaded && !otFailed) {
                    /* Set this up for later... */
                    let doLoaded = function () {
                        if (optanonLoaded) {
                            consentId = (typeof win.OneTrust.getDataSubjectId === 'function' && win.OneTrust.getDataSubjectId()) || consentId;
                            /* Send OT loaded events */
                            if (dbg) {
                                uclog('debug', 'Dispatching oneTrustLoaded event.');
                            }
                            doc.dispatchEvent(new CustomEvent('oneTrustLoaded', {
                                bubbles: false,
                                cancelable: false,
                                detail: {
                                    region: config.regId,
                                    time: new Date(),
                                    consentConfirmed: consentConfirmed,
                                    otId: consentId,
                                    otVers: consentVersion
                                }
                            }));
                            doc.dispatchEvent(new CustomEvent('optanonLoaded', {
                                bubbles: false,
                                cancelable: false,
                                detail: {
                                    region: config.regId,
                                    time: new Date(),
                                    consentConfirmed: consentConfirmed,
                                    otId: consentId,
                                    otVers: consentVersion
                                }
                            }));

                            /* If WebView with external consent, drop any banner pop-ups. */
                            if (config.useExternalConsent && !win.OneTrust.IsAlertBoxClosed()) {
                                win.Optanon.Close();
                            }
                        }

                        /* No need to keep calling this */
                        win.OptanonWrapper = function () {};
                    };

                    /* Remove the timeout */
                    if (win.WBD.UserConsent_wait >= 0) {
                        clearTimeout(win.WBD.UserConsent_wait);
                        win.WBD.UserConsent_wait = -1;
                    }

                    /* Did OneTrust actually load, or did we timeout/incompletely load? */
                    if (!win.OneTrust || typeof win.OneTrust.GetDomainData !== 'function') {
                        /* 1 = Stub load failure, 2 = Initialization failure, 3 = SDK load timeout/fail, 4 = other fail */
                        const code = (!win.OneTrustStub ? 1 : (!win.OneTrustStub.otSdkStub ? 2 : (tos ? 3 : 4))),
                            msg = (code === 1 ? 'Stub load failure' : (code === 2 ? 'Initialization failure' : (code === 3 ? 'SDK load timeout' : 'Unknown error')));
                        /* OneTrust failed to fully load, so reflect that. */
                        optanonLoaded = false;
                        otFailed = true;
                        uclog('error', 'OneTrust Error (', code, '): ', msg);
                        if (dbg) {
                            uclog('debug', 'Dispatching oneTrustFailed event.');
                        }
                        doc.dispatchEvent(new CustomEvent('oneTrustFailed', {
                            bubbles: false,
                            cancelable: false,
                            detail: {
                                region: config.regId,
                                time: new Date(),
                                consentConfirmed: consentConfirmed,
                                otId: consentId,
                                otVers: consentVersion,
                                code: code,
                                msg: msg
                            }
                        }));
                        /* No need to keep calling this */
                        win.OptanonWrapper = function () {};
                        return;
                    }

                    /* This is the first call, on page load once OneTrust has been loaded */
                    optanonLoaded = true;

                    /* Make sure we've correctly set the geo-location with OT */
                    if (config.geoPassedToOneTrust) {
                        pushGeoLocation();
                    }
                    /* If using GPP, we need to verify GPP was set, otherwise keep usingOTGpp set to false */
                    if (usingIabGpp) {
                        if (typeof win.__gpp !== 'function') {
                            /* OneTrust loaded, but reset __gpp incorrectly and is broken.  Reset it to our own. */
                            win.__gpp = gppStub;
                        }
                        win.__gpp('ping', function (pd) {
                            if (!pd || pd.cmpId <= 0) {
                                /* OneTrust loaded, but __gpp is not working, so GPP is broken in this OT template */
                                usingIabGpp = false;
                                if (gppCmpApi) {
                                    gppCmpApi.setCmpStatus('error');
                                    gppCmpApi.fireErrorEvent('CMP did not initialize GPP for this region.');
                                }
                                if (dbg) {
                                    uclog('debug', 'OneTrust GPP for this region is broken.  Disabling use of GPP.');
                                }
                            } else if (pd.cmpId === 1) {
                                /* OneTrust loaded, but didn't try to reset __gpp, so GPP is not supported by this OT template. */
                                usingOTGpp = false;
                                if (gppCmpApi) {
                                    if (dbg) {
                                        uclog('debug', 'OneTrust did NOT initialize GPP for this region.  Using GPP from UserConsent.');
                                    }
                                    gppCmpApi.setCmpStatus('loaded');
                                    gppCmpApi.fireEvent('cmpStatus', 'loaded');
                                    gppCmpApi.setSignalStatus('ready');
                                    gppCmpApi.fireEvent('signalStatus', 'ready');
                                } else {
                                    usingIabGpp = false;
                                    if (dbg) {
                                        uclog('debug', 'OneTrust did NOT initialize GPP for this region.  UserConsent GPP failed to initialize.  Disabling use of GPP.');
                                    }
                                }
                            } else {
                                /* OneTrust has loaded, we need to grab the new GPP string, if any, and use that */
                                const cmpStat = pd.cmpStatus;
                                gppCmpId = pd.cmpId;
                                usingOTGpp = true;
                                if (pd.gppVersion && pd.gppVersion === '1.0') {
                                    pd = win.__gpp('getGPPData');
                                }
                                if (pd && pd.gppString) {
                                    gppStringVer = pd.gppVersion;
                                    setGppConsent(pd.gppString, null);
                                }
                                if (dbg) {
                                    uclog('debug', 'OneTrust GPP initialized (status "' + cmpStat + '").');
                                }
                            }
                            doLoaded();
                        });
                    } else {
                        doLoaded();
                    }
                }
            };

            /* Auto-init if we can */
            if (typeof win.WBD.UserConsentConfig === 'object' && win.WBD.UserConsentConfig !== null) {
                init(win.WBD.UserConsentConfig);
            } else if (typeof win.WM.UserConsentConfig === 'object' && win.WM.UserConsentConfig !== null) {
                init(win.WM.UserConsentConfig);
            }
        }

        return {
            addScript: addScript,
            addScriptElement: addScriptElement,
            forceReconsent: forceReconsent,
            getAdChoicesLinkAction: getAdChoicesLinkAction,
            getAdChoicesLinkTitle: getAdChoicesLinkTitle,
            getAffiliatesLinkAction: getAffiliatesLinkAction,
            getAffiliatesLinkTitle: getAffiliatesLinkTitle,
            getCmpString: getCmpString,
            getConsentConfirmed: getConsentConfirmed,
            getConsentHistory: getConsentHistory,
            getConsentState: getConsentState,
            getConsentTime: getConsentTime,
            getConsentVersion: getConsentVersion,
            getGeoCountry: getGeoCountry,
            getGeoState: getGeoState,
            getGppAPIstring: getGppAPIstring,
            getGppSection: getGppSection,
            getIABInterface: getIABInterface,
            getIABRegion: getIABRegion,
            getIABVersion: getIABVersion,
            getLinkAction: getLinkAction,
            getLinkTitle: getLinkTitle,
            getPrivacyCenterLinkAction: getPrivacyCenterLinkAction,
            getPrivacyCenterLinkTitle: getPrivacyCenterLinkTitle,
            getRegion: getRegion,
            getReloadOnChange: getReloadOnChange,
            getReloadOnConsentReduction: getReloadOnConsentReduction,
            getRightsRequestLinkAction: getRightsRequestLinkAction,
            getRightsRequestLinkTitle: getRightsRequestLinkTitle,
            getSimpleConsentState: getSimpleConsentState,
            getTcfAPIaddtlString: getTcfAPIaddtlString,
            getTcfAPIstring: getTcfAPIstring,
            getUserConsentAdvertisingState: getUserConsentAdvertisingState,
            getUspAPIstring: getUspAPIstring,
            getVersion: getVersion,
            init: init,
            inUserConsentState: inUserConsentState,
            isChild: isChild,
            isTop: isTop,
            isEnabled: isEnabled,
            isGpcInUse: isGpcInUse,
            isGpcSet: isGpcSet,
            isInCcpaRegion: isInCcpaRegion,
            isInGdprRegion: isInGdprRegion,
            isInGppRegion: isInGppRegion,
            isInIabRegion: isInIabRegion,
            isInRegion: isInRegion,
            isOneTrustBlocked: isOneTrustBlocked,
            isOneTrustFailing: isOneTrustFailing,
            isOneTrustLoaded: isOneTrustLoaded,
            isOptanonLoaded: isOneTrustLoaded,
            isReady: isReady,
            isSiteIABCompliant: isSiteIABCompliant,
            usingCompatConsent: usingCompatConsent,
            usingExternalConsent: usingExternalConsent,
            usingGPP: usingGPP,
            usingIAB: usingIAB,
            usingPSM: usingPSM
        };
    })(window, document);

    /* For compatibility... */
    window.WM = window.WBD;

})();
