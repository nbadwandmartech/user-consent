import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import terser from '@rollup/plugin-terser';
/* import eslint from '@rollup/plugin-eslint'; */
import pkg from './package.json' assert { type: 'json' };
import fs from 'fs';

export default [
    { /*** Basic config ***/
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-basic.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-basic.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-basic-cjs.min.js',
                format: 'cjs',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-basic-es.min.js',
                format: 'es',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-basic.js'),
                __ucTcf__: fs.readFileSync('src/tcf-stub.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            /* eslint({ exclude: ['node_modules/**', 'src/gpp/**'] }), */
            commonjs()
        ]
    },
    { /*** Bleacher-Report config ***/
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-br.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-br.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-br-cjs.min.js',
                format: 'cjs',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-br-es.min.js',
                format: 'es',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-br.js'),
                __ucTcf__: fs.readFileSync('src/tcf.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            /* eslint({ exclude: ['node_modules/**', 'src/gpp/**'] }), */
            commonjs()
        ]
    },
    { /* Legacy Discovery config */
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-disco.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-disco.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-disco-cjs.min.js',
                format: 'cjs',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-disco-es.min.js',
                format: 'es',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-disco.js'),
                __ucTcf__: fs.readFileSync('src/tcf.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            /* eslint({ exclude: ['node_modules/**', 'src/gpp/**'] }), */
            commonjs()
        ]
    },
    { /*** Max config ***/
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-max.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-max.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-max-cjs.min.js',
                format: 'cjs',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-max-es.min.js',
                format: 'es',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-max.js'),
                __ucTcf__: fs.readFileSync('src/tcf.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            /* eslint({ exclude: ['node_modules/**', 'src/gpp/**'] }), */
            commonjs()
        ]
    },
    { /*** NBA config ***/
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-nba.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/index.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-nba.js'),
                __ucTcf__: fs.readFileSync('src/tcf.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            /* eslint({ exclude: ['node_modules/**', 'src/gpp/**'] }), */
            commonjs()
        ]
    },
    { /*** IAB config ***/
        context: 'window',
        input: 'src/uc.js',
        output: [
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-iab.js',
                format: 'iife',
                generatedCode: 'es2015'
            },
            {
                externalLiveBindings: false,
                file: 'dist/userconsent-iab.min.js',
                format: 'iife',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-iab-cjs.min.js',
                format: 'cjs',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            },
            {
                file: 'dist/userconsent-iab-es.min.js',
                format: 'es',
                generatedCode: 'es2015',
                plugins: [terser({ecma: 2016})]
            }
        ],
        plugins: [
            replace({
                __ucConfig__: fs.readFileSync('src/defaults-iab.js'),
                __ucTcf__: fs.readFileSync('src/tcf.js'),
                __ucVersion__: pkg.version,
                preventAssignment: true
            }),
            commonjs()
        ]
    }
];

