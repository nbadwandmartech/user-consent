# User-Consent

A brand agnostic approach to privacy and user consent compliance using OneTrust.

User-Consent provides a Javascript interface to OneTrust functionality, providing
an automatic mechanism for controlling what consents are needed by users based on
their location and what consents have been granted.  This allows the programmer
to focus on what their code does and needs without having to worry about "where"
it is being run.

----

## Usage

Include code in your application (as the first script in the `<head>` block).
It is very important that User-Consent be included and initialized PRIOR to
including ANY third-party code or any code that requires consent.

### Adding the Library

#### The Good Way

```html
<head>
    <!-- place this at or near the top -->
    <script src="path/to/this-script.min.js" type="text/javascript"></script>
    <script>
        (function initalizeUserConsent(win) {
            win.WBD.UserConsent.init({
                cookieDomain: ".your-brand.com",
                domId: "ONETRUST-GUID-HERE"
            });
        })(window);
    </script>
    <!-- other head tags -->
</head>
```

Or by pre-defining the config, which will allow the code to self-initialize.

```html
<head>
    <!-- place this at or near the top -->
    <script>
        window.WBD = window.WBD || {};
        window.WBD.UserConsentConfig = {
            cookieDomain: ".your-brand.com",
            domId: "ONETRUST-GUID-HERE"
        };
    </script>
    <script src="path/to/this-script.min.js" type="text/javascript"></script>
    <!-- other head tags -->
</head>
```

Both methods are functionally equavalent, so just use whichever works best for your situation.

**VERY IMPORTANT:  DO NOT USE `async` OR `defer` IN THE SCRIPT TAG THAT LOADS USER CONSENT!**
_Using either of these properties will prevent proper setup and initialization._  If you do not
want to use a synchronous script tag in the `<head>` block of your page, use "The Best Way"
described below...


#### The Best Way

Essentially the same options and logic as above, but instead of pulling User Consent on each page
with a synchronous script tag, embed the minified User Consent script itself onto the page.

In short, replace:
```html
    <script src="path/to/this-script.min.js" type="text/javascript"></script>
```

With:
```html
    <script>
        <!-- Add the contents of the User Consent index.min.js file here -->
    </script>
```

With either the `WBD.UserConsentConfig` defined before it or `WBD.UserConsent.init()` called
after it, just as you would do with "The Good Way".

This guarantees the code is on your page and eliminates any concerns about
[network failures](https://developers.google.com/web/fundamentals/performance/poor-connectivity),
[impact on performance](https://web.dev/render-blocking-resources/), or other issues with
synchronous script tags in your `<head>` block (which do come with their own problems).

It is worth noting that since User Consent is generally required on every page (as is Prism, if
it is also in use), it is not generally beneficial for submitting it to "tree-shaking" type build
optimizations you might otherwise apply to client-side code packaged with something like WebPack
or Rollup.  For this reason, we are sensitive to the overall size of User Consent and recommend
that you are as well.  The current minified script is about 25k in size.  We actively strive to
maintain this as small as is feasible.


#### With Next.JS

When including User Consent while using Next.JS, generally you want to include User Consent with
a "Script" tag that uses the "beforeInteractive" strategy, and you want to include it before any
scripts that might require User Consent, and before any third-party scripts also using that
strategy.  **Do not use `async` or `defer` properties within the Script tag!**
You should immediately follow this Script tag with a second Script tag using an identical
strategy that calls `WBD.UserConsent.init()` with the appropriate configuration options.

It should also be noted that when using the App Router, you want to include this tag
in the root layout (`app/layout.tsx`), and when using the Pages Router, you want to include this
tag in the Document (`pages/_document.js`).  For example, let's say your server is `www.myhost.com`
and you are using the basic User Consent script exposed via path `/js/user-consent-basic.min.js`.
You will want to create a script to initialize User Consent (let's call it `/js/init-user-consent.js`):

```
window.WBD.UserConsent.init({
    cookieDomain: ".myhost.com",
    domId: "ONETRUST-GUID-HERE"
};
```

Then we would add User Consent (using the App Router) via code like the following in
`app/layout.tsx`:
```
import Script from 'next/script'
 
export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>{children}</body>
      <Script
        src="https://www.myhost.com/js/user-consent-basic.min.js"
        strategy="beforeInteractive"
      />
      <Script
        src="https://www.myhost.com/js/init-user-consent.js"
        strategy="beforeInteractive"
      />
    </html>
  )
}
```

Or we would add User Consent (using the Pages Router) via code like the following in
`pages/_document.js`:
```
import { Html, Head, Main, NextScript } from 'next/document'
import Script from 'next/script'
 
export default function Document() {
  return (
    <Html>
      <Head />
      <body>
        <Main />
        <NextScript />
        <Script
          src="https://www.myhost.com/js/user-consent-basic.min.js"
          strategy="beforeInteractive"
        />
        <Script
          src="https://www.myhost.com/js/init-user-consent.js"
          strategy="beforeInteractive"
        />
      </body>
    </Html>
  )
}
```


#### Which Script to Use

The "wm-user-consent" package delivers three primary variants, with a number of script options:

- **Basic** - Suitable for most WBD brands and sites.  This version provides a default configuration
suitable for using the conventional (or similar) WBD OneTrust templates.  Basic variants define the
following core categories:  required, vendor, functional, media, performance, targeting, and
1p-targeting.

- **IAB** - Suitable for most Ad-supported brands and sites.  This version provides configuration
defaults for using the IAB-compliant OneTrust templates that adhere to the IAB consent category
definitions.  The IAB variants define the following core category descriptions:  required, vendor,
data-store, ads-contextual, ads-person-prof, ads-person, content-person-prof, content-person,
measure-ads, measure-content, measure-market, product-develop, product-security, deliver-content,
privacy-choices, combine-data, link-devices, id-devices, geolocate, scan-devices, 1p-targeting,
data-share, data-sell, personal-data, known-child-12, known-child-16, sensitive-racial,
sensitive-belief, sensitive-health, sensitive-sexual, sensitive-citizen, sensitive-gene,
sensitive-biometric, sensitive-spi, sensitive-ssi, sensitive-org, and sensitive-comm.

- **Legacy Discovery (Disco)** - Suitable for per-merger Discovery brands and sites.  This version
provides a default configuration suitable for using the conventional Discover OneTrust templates.
Basic variants define the following core categories:  required, analytics, personalization, ads, social.
Note that this configuration type will only be supported until legacy Discovery sites are migrated to
either the WBD OneTrust templates or IAB-compliant OneTrust templates.

The follow code variants are available:
- Scripts suitable for direct inclusion on a web page or via a script tag:
    + `dist/userconsent-basic.min.js`
    + `dist/userconsent-iab.min.js`
    + `dist/userconsent-disco.min.js`
    + `dist/userconsent-max.min.js`
- CommonJS modules (client-side only):
    + `userconsent-basic-cjs.min.js`
    + `userconsent-iab-cjs.min.js`
    + `userconsent-disco-cjs.min.js`
    + `userconsent-max-cjs.min.js`
- ES modules (client-side only):
    + `userconsent-basic-es.min.js`
    + `userconsent-iab-es.min.js`
    + `userconsent-disco-es.min.js`
    + `userconsent-max-es.min.js`

**User-Consent must be used on the client-side ONLY!**  While the User-Consent script can be "rendered"
on the server-side, it must only be initialized and invoked on the client-side.


#### Configuration

The **required** configuration values are:

- _cookieDomain_: The domain of the Optanon cookies you are using.  For example: `".cnn.com"` (note the leading `.`).
- _domId_: The GUID provided by OneTrust for your domain.

Some other **important** configuration values are:
- _ccCookie_: The name of the session cookie that will contain the country-code set via Geo-IP from the CDN.  As several sites--including CNN--use `countryCode`, this is the default.  Note that _either_ passing in the GeoIP-derived current country code directly (via the `countryCode` option below) or getting it from a session cookie on the response is required for User-Consent to work properly.  The session cookie method is easily added to both Akamai and Fastly CDN configurations.
- _ccpaGeos_: An array of 2-letter country codes or country:state codes to use as the CCPA geoMatch.  A convenience for identifying the default "ccpa" region within the larger US National GPP region.  Defaults to ["US:CA", "US:CO", "US:CT", "US:DE", "US:IA", "US:MT", "US:NE", "US:NH", "US:NJ", "US:OR", "US:TX", "US:UT", "US:VA"] for migration purposes.  This will be deprecated once CCPA vendors have fully migrated to GPP (late 2023).
- _countryCode_: The 2-letter GeoIP-derived country code to use.  Only use this if you are acquiring this value directly through your own means and don't wish to use the "session cookie" method via the `ccCookie` option (above).
- _confirmCookie_: The name of the "confirmation" cookie used by OneTrust.  This defaults to "OptanonAlertBoxClosed".
- _consentCookie_: The name of the "consent" cookie used by OneTrust.  This defaults to "OptanonConsent".
- _controlCookie_: The name of the "control" cookie used by User-Consent to help manage OneTrust cookies.  This defaults to "OptanonControl".
- _cookieSameSite_: The "SameSite" value to use for cookies.  Can be "None", "Lax", or "Strict".  Defaults to "Lax".
- _cookieSecure_: Boolean to determine if cookies should have the "Secure" flag set.  Default is false (not set).
- _defaultCountry_: The default country code to use if the normal geolocation detection mechanisms have failed or returned invalid data.  The default is "US".
- _defaultState_: The default state code to use if the normal geolocation detection mechanisms have failed or returned invalid data.  The default is "" (an empty string).
- _gdprIabCookie_: The name of the GDPR IAB/TCF v2.x cookie to use.  This defaults to "eupubprivacy-v2".
- _gppIabCookie_: The base name of the IAB GPP cookie to use.  This defaults to "OTGPPConsent".  Note that this may be used as a "base name" if the GPP string needs to be spread across multiple cookies.
- _geoCheckFunction_: An optional function that returns an object of the format `{countryCode: 'XX', stateCode: 'XX'}` to provide the user's current geographical country and state.  If set, the provided function **MUST be synchronous**!  The default is null (not set).
- _reloadOnConsentChange_: Boolean that controls whether or not any change to consent will result in the page being reloaded.  The default is true.
- _reloadOnConsentReduction_: Boolean that controls whether or not any change that results in *reduced* consent will result in the page being reloaded, and changes that increase consent will not trigger a reload.  Note that this will only work if "reloadOnConsentChange" is false.  The default is false.
- _scCookie_: The name of the session cookie that will contain the state-code or region set via Geo-IP from the CDN.  As several sites--including CNN--use `stateCode`, this is the default.  Note that _either_ passing in the GeoIP-derived current state code directly (via the `stateCode` option below) or getting it from a session cookie on the response is required for User-Consent to work properly.  The session cookie method is easily added to both Akamai and Fastly CDN configurations.
- _src_: The URL for the OneTrust script to use.  This defaults to `"https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"`.
- _uspApiExplicitConsent_: Set to boolean `true` if explicit notice been provided as required by 1798.115(d) of the CCPA and the opportunity to opt-out of the sale of their data pursuant to 1798.120 and 1798.135 of the CCPA.  This defaults to `true`, since this is generally the case on Warner Bros. Discovery sites.
- _uspApiIsLspa_: Set to boolean `true` if the publisher is a signatory to the IAB Limited Service Provider Agreement (LSPA) and the publisher declares that the transaction is covered as a “Covered Opt Out Transaction” or a “Non Opt Out Transaction” as those terms are defined in the Agreement.  This defaults to `false`, since this is not common for most Warner Bros. Discovery sites.


### Wrapping Scripts for Consent Control

#### The `addScript` Method

Replace scripts implemented like this:
```html
<script src="//domain.com/path/to/script.js" onload="myLoadEventHandler()"></script>
```
 
With something like this:
```html
<script>
    window.WBD.UserConsent.addScript({
        async: true,
        onload: function () {
            myLoadEventHandler();
        },
        src: "//domain.com/path/to/script.js"
    }, ["measure-content"]);
</script>
```

Any script tag properties can be passed in the object for the first parameter,
just as you would use with a script tag created via.
[`document.createElement('script')`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLScriptElement).

The second parameter should be an array of strings specifying the list of
the consent categories required for this script to be invoked.  If more than
one are included, all must have consent.

There is an optional third parameter, which (if specified) is the DOM node on
the page to append the script to.  This is not normally needed.

There is also an optional fourth parameter, which (if specified) is an array of
lower-case region codes (like "gdpr" or "ccpa"), or upper-case country codes
(like "GB", "DE, "US"), or upper-case country:state codes (like "US:CA") which
can be used to specify areas where the script should be added.  This is not
normally needed, and the default is to add the script everywhere.

Note that the function returns boolean `true` if consent exists and the script
tag was added or boolean `false` if consent was not granted.


#### The `addScriptElement` Method

Very similar to `addScript` above, the `addScriptElement` call will conditionally
add a script tag based on consent.  The difference is instead of the first parameter
being an object consisting of script properties, it is a script element created by
your code but not yet added to the DOM.

For example, if you have something like this in your code:
```javascript
    var scpt = doc.getElementsByTagName("script")[0],
        elem = doc.createElement("script");

    elem.id = "quantScript";
    elem.async = true;
    elem.type = "text/javascript";
    elem.src = doc.location.protocol === "https:" ? config.runtime.quantcast.sslSrc : config.runtime.quantcast.src;
    scpt.parentNode.appendChild(elem);
```

You would replace that with this:
```javascript
    var scpt = doc.getElementsByTagName("script")[0],
        elem = doc.createElement("script");

    elem.id = "quantScript";
    elem.async = true;
    elem.type = "text/javascript";
    elem.src = doc.location.protocol === "https:" ? config.runtime.quantcast.sslSrc : config.runtime.quantcast.src;
    window.WBD.UserConsent.addScriptElement(elem, config.runtime.quantcast.ucStates, scpt.parentNode);
```

Here we see that the first parameter is simply the script element created within
the code, but the instead of adding it directly to the DOM, we use `addScriptElement`.
Note that the second parameter and optional third and fourth parameters are identical
to `addScript`.

Also note that the function returns `true` or `false`, similar to `addScript`.


#### Wrapping Inline Code with `inUserConsentState`

If you simple need to make code conditional on a consent state, you can also do
that by using `inUserConsentState` with a conditional.  For example, let's say you have
a block of code like the following:

```javascript
    doThingA();
    doContentMeasurementThing();
    doThingB();
    if (doThirdPartyCookieIdentityThing() === 'success') {
        doThingC();
    } else {
        doThingD();
    }
```

You could re-write that to use consent with logic like the following:

```javascript
    doThingA();
    if (window.WBD.UserConsent.inUserConsentState(['measure-content'], {id: 'content-measurement-thing'})) {
        doContentMeasurementThing();
    }
    doThingB();
    if (window.WBD.UserConsent.inUserConsentState(['vendor', 'data-store'], {id: 'third-party-cookie-id-thing'})) {
        if (doThirdPartyCookieIdentityThing() === 'success') {
            doThingC();
        } else {
            doThingD();
        }
    } else {
        doThingD();
    }
```

Note that the second parameter is an optional options object.  If you pass an `id`
property with a string value in that object, that will be used as the "name" of
what is being tested in any debug logging (see "Debugging" below).


#### A Note About Consent

In all the examples above, consent is specified as an array of strings.
Consent must exist for ALL the consent types specified for consent to be
successful.  If the consent array is empty (`[]`), it will always be true.
Note that a special consent string exists called `"iab"`.  If this string
appears in the consent array, and IAB is used in the current region, then
consent will be granted, as it is assumed that the code being invoked will
correctly be using CMP/USPAPI to manage consent internally on its own in
an IAB-compliant way.  There is also a similar consent string for each IAB
region by using `"iab-<iabRegion>"`.  For example, `"iab-ccpa"` will only
grant consent for IAB in the CCPA region, where `"iab-gdpr"` will only
grant consent for IAB in the GDPR region.  Similarly, there is a "gpp"
consent string which acts accordingly for code that will be invoked
correctly using the GPP CMP logic.  As with "iab", there are "gpp"
variants for each supported GPP section:  "gpp-uspnatv1", "gpp-uspv1",
"gpp-tcfcav1", and "gpp-tcfeuv2".  In most cases, using "gpp"
or "gpp-uspnatv1" will be appropriate, depending on the vendor.

----

## Managing Categorization and Scripts

Ideally, scripts should be managed via an upstream configuration file - similar to:

```json
// global config for application
{
    "someThirdParty": {
        "src": "//domain.com/path/to/script.js",
        "ucStates": ["vendor", "measure-ads"]
    }
}
```

```html
<script>
    window.WBD.UserConsent.addScript({ src: Config.someThirdParty.src }, Config.someThirdParty.ucStates);
</script>
```

----

## ES6


If using ES6-style `import`, replace something like this:
```html
<script>
    import 'usabilla';
    // Do stuff with usabilla
</script>
```
 
With something like:
```html
<script>
    if (window.WBD.UserConsent.inUserConsentState(['measure-market'], {id: 'usabilla'})) {
        import 'usabilla';
        // Do stuff with usabilla
    }
</script>
```

----

## Legacy Approaches

The legacy "optanon-category-<xyz>" apprach is *no longer supported*.  As of IAB v1.1
it caused problems with IAB, and now with IAB 2.x it flat doesn't work.  **To repeat,
do not use this method:**

```html
<script type="text/plain" class="optanon-category-pad" src="//domain.com/path/to/script.js"></script>
```

----

## Handling the Footer Link

A requirement for CCPA and GDPR is to have a link on every page that opens the consent control panel.
In CCPA, it is required that this link be titled something along the lines of "Do Not Sell or Share My
Personal Information", while in GDPR the requirement is that it just be available.

While you must create the link in a way that is appropriate for your application, you can call
the `WBD.UserConsent.getLinkTitle()` funciton to get two things:  First it will return the title
appropriate for the client-side user's consent region (CCPA or GDPR), and second it will only
return a title if you need to add the link.  If you do NOT need to add the link, it will return
an empty string.

If you have called the function and it DOES return a non-empty string, you should use that for your
link title.  You should then call `WBD.UserConsent.getLinkAction()` to get a function suitable for
use as an `onClick` handler for your link to open the OneTrust consent control panel.

----

## Migration from User-Consent 1.x

_User-Consent 1.x and GDPR IAB 1.1 are now fully deprecated.  This migration information is
now a bit out of date.  I'll leave it here for historical interest._

User-Consent 3.0 represents a shift in internal categorization of consent to the new
[GDPR IAB TCF 2.x framework](https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/tree/master/TCFv2).
See a summary of the new categories in the section on consent, below.

Compatibility with the previous GDPR IAB 1.1 is no longer support and you MUST migrate to the
appropriate categories for your use case.  If you are configuration UserConsent for an
ad-supported site, you should migrate to the IAB-compliant version of UserConsent, otherwise
the basic version should be fine.

----

## Migration from User-Consent 3.x

User-Consent 4.x is functionally identical to User-Consent 3.x, with the following
major changes:

- Global Privacy Policy (GPP) support - Full GPP support for US National v1,
TCF Canada v1, and TCF EU v2.
- Pre-built libraries supporting both basic WBD and full IAB-compliant configurations - Allows
vastly simpler configuration for most WBD brands and sites.  Note that this changes the code
to include from the package; no longer just `<package_dir>/index.js`.  See the
"[Which Script to Use](#which-script-to-use)" section above for the new details.
- Now in the `WBD` namespace - While User-Consent has formally moved from the `WM`
namespace to the `WBD` namespace, it can still be accessed via the old `WM` namespace for
compatibility.  Similarly, the auto-initialization logic will fire if either
`window.WBD.UserConsentConfig` or `window.WM.UserConsentConfig` is present.
- Full deprecation of the legacy TCF 1.x categories - IAB officially ceased support for
this in 2020, and dropped it outright in 2021.  Time to let it go.
- Full deprecation of Prism/PSM v1.x integration - The CDX team ended support for this in
2021, and if you are still using it the time to migrate was some time ago.  The CCPA code
it delivers is NOT compliant with CPRA requirements.  _Note that this does NOT impact
Prism/PSM v2 or later._

Because of the new "basic" and "iab" default configurations, many sites will no longer need
to use a complex configuration defining every region.  The new default "basic" config
pre-defines the following regions:

```javascript
    regions: [
        {
            id: "us",
            compatCodes: {
                req: ["sc"],
                ven: ["tpv"]
            },
            consentExpireIn: 3,
            consentGpcDefaults: {
                "vendor": false,
                "targeting": false
            },
            consentImpliedDefaults: {
                "required": true,
                "functional": true,
                "media": true,
                "performance": true
            },
            consentLinkTitle: {
                ar: "لا تبيع أو تشارك معلوماتي الشخصية",
                en: "Do Not Sell Or Share My Personal Information",
                es: "No Venda Vi Comparta Mi Información Personal"
            },
            geoMatch: ["US:CA", "US:CO", "US:CT", "US:DE", "US:IA", "US:MT", "US:NE", "US:NH", "US:NJ", "US:OR", "US:TX", "US:UT", "US:VA"],
            gppSection: "uspnat",  /* usnat */
            iabRegion: "ccpa"
        },
        {
            id: "gdpr",
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                "functional": false,
                "media": false,
                "performance": false,
                "targeting": false
            },
            consentImpliedDefaults: {
                "vendor": true
            },
            consentLinkTitle: {
                ar: "إدارة ملفات تعريف الارتباط+",
                en: "Manage Cookies+",
                es: "Administrar cookies+"
            },
            geoMatch: ["GB", "DE", "FR", "IT", "ES", "PL", "RO", "NL", "BE", "GR", "CZ", "PT", "SE", "HU", "AT", "BG", "DK", "FI", "SK", "IE", "HR", "LT", "SI", "LV", "EE", "CY", "LU", "MT", "NO", "IS", "LI", "CH"]
        },
        {
            id: "other-optin",
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                "functional": false,
                "media": false,
                "performance": false,
                "targeting": false,
                "1p-targeting": false
            },
            consentImpliedDefaults: {
                "vendor": true
            },
            geoMatch: ["CO", "UY", "PE", "AR", "CR", "CL"]
        },
        {
            id: "other-optout",
            consentImpliedDefaults: {
                "vendor": true
            },
            geoMatch: ["MX", "PY", "BR", "VE", "NI"]
        },
        {
            id: "global",
            geoMatch: ["*"],  /* Matches everything not yet matched */
            useFixedConsent: true
        }
    ]
```

Note that some configs used a separate region for France and Germany (FR and DE).  In general, this
should not be necessary, even when they are using a separate template within OneTrust.  This is
because the categories and region behavior between the "general GDPR" and "FR/DE GDPR" templates
are idental, with the only difference being a change to the UI for France and Germany (adding the
"Reject All" button).

Should you find that you DO need to add a new region, you no longer need to replace the entire
regions setting.  You can now insert, remove, or update a default value with a new one using the new
config `changeRegions` option.  For example, if you wanted to remove the "other-optin" region and
move those countries to the "other-optout" region, and you wanted to add a new "partial-optout"
region for Qatar, you could now do:

```javascript
UserConsentOptions.changeRegions = {
    remove: [
        {
            id: "other-optin"
        }
    ],
    replace: [
        {
            id: "other-optout",
            consentImpliedDefaults: {
                "vendor": true
            },
            geoMatch: ["MX", "PY", "BR", "VE", "NI", "CO", "UY", "PE", "AR", "CR", "CL"]
        }
    ],
    insert: {
        {
            id: "partial-optin",
            insertAfter: "other-optout",
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                "functional": true,
                "media": false,
                "performance": true,
                "targeting": false
            },
            consentImpliedDefaults: {
                "vendor": true
            },
            geoMatch: ["QA"]
        }
    }
};
```

Some code used the `getRegion()` function to determine if a user was in GDPR or CCPA regions.  This
was somewhat inappropriate, and region names are a configuration artifact and not really useful in
a general way.  When you need to identify the region for privacy reasons, those uses should be
replaced with calls to `getIABRegion()`, or (probably better) one of the `isInGppRegion()`,
`isInCcpaRegion()`, or `isInGdprRegion()` calls.

Note that this is for cases of identifying regionality for the purposes of privacy ONLY.  If you
want to pull the country code and/or state code, it is FAR better to just use the `getGeoCountry()`
and `getGeoState()` functions.

----

## Notes on Current and Future IAB Support

Currently, User-Consent provides IAB-compliant code stub for GDPR TCF v2.0/2.1 and GPP v1.0
(with compatibility for GPP v1.1), as determined by the current OneTrust consent cookie and
the OneTrust templates in use.  The stubs are then replaced with the IAB-certified functions
provided by OneTrust once it has loaded.

User-Consent also includes the full IAB-compliant CPRA USP v1 logic, providing the
[U.S. Privacy User Signal Mechanism](https://github.com/InteractiveAdvertisingBureau/USPrivacy/blob/master/CCPA/Version%201.0/USP%20API.md)
and returns a correct
[U.S. Privacy String](https://github.com/InteractiveAdvertisingBureau/USPrivacy/blob/master/CCPA/Version%201.0/US%20Privacy%20String.md).
This logic is fully integrated with support for the Global Privacy Control (GPC) signal, per
CPRA requirements.

User-Consent is ready for IAB TCFv4.  Those changes will be enabled in the default IAB
config once OneTrust supports them.

----

## Using with Prism (PSM) v2 and Higher

When using User-Consent on a page that will also use WarnerMedia Prism/PSM v2 or later, it is
important that User-Consent be present and initialized PRIOR to loading Prism/PSM.

----

## Performing an Action on Consent Changes

User-Consent provides two mechanisms to take an action on consent changes:

1. Handle the `userConsentChanged` custom event _(recommended)_.
2. Set the `consentChangeAction` to a function.

### Handling the `userConsentChanged` Custom Event

You need only define a handler for the event on the `document`.  Something like this example
will work fine:

```
/* Define a handler for consent changes */
function handleChanges(event) {
    /* Consent has changed */
    /* For this example, just log the time and the new consent values */
    console.log("Consent Changed at ", event.detail.time);
    console.log("New Consent Values: ", event.detail.new);
}

/* Add handler to userConsentChanged event */
document.addEventListener("userConsentChanged", handleChanges, false);
```

The data passed to the event handler as properties of the `detail` object include:
* _region_: A string containing the region config. name, generally `"ccpa"`, `"gdpr"`, or `"global"`.
* _time_: A Javascript Date object containing the time of the consent change.
* _old_: The old (pre-change) consent state object.
* _new_: The new (post-change) consent state object.
* _gpcActive_: Boolean `true` if GPP is in use.
* _gpp_: The GPP string (if any).
* _gppCmpId_: The CMP ID of the GPP instance.
* _gppVers_: The GPP string version.
* _tcf_: The GDPR IAB TC string (if any).
* _tcfVers_: The TCF string version.
* _acf_: The TCF Additional Consent string (if any).
* _usp_: The CCPA USP string.
* _otId_: The OneTrust consent ID.
* _otVers_: The OneTrust API version.
* _otIact_: The OneTrust interaction/description string.

Note that multiple listeners may be present on the handler, so any action you take should
respect that.  If you need to trigger a page reload or something similar that would prevent
the other handlers from being invoked, it would be best to do so within a `setTimeout`
function to allow the other handlers to be called prior to your action.

### Set the `consentChangeAction` to a function.

You can also set the `consentChangeAction` to a function that will be called when a change
in consent occurs.  This function is passed 4 parameters:

1. _new_: The "new" (post-change) consent state object.
2. _region_: A string containing the region config. name, generally `"ccpa"`, `"gdpr"`, or `"global"`.
3. _consentVersion_: A string containing the OneTrust version string set in the consent update, if any.
4. _old_: The "old" (pre-change) consent state object.

A simple example of using this method would be to modify your options before initialization
to set the option, as follows:

```
    window.WBD.UserConsentConfig = {
        cookieDomain: ".your-brand.com",
        domId: "ONETRUST-GUID-HERE",
        src: "//domain.com/path/to/one-trust-script.js",
        consentChangeAction: function (newConsent, region, vers, oldConsent) {
            /* Consent has changed */
            /* For this example, just log the new consent values */
            console.log("New Consent Values: ", newConsent);
        }
    };
```

Note that this method is no longer recommended in favor of using the `userConsentChanged`
event (see above).  This method is much more limited and will likely be deprecated in the
future.

----

## Page-Level Information

Upon initialization, User-Consent will set two page-level class properties to the document
element on the page (generally the page's `<html>` tag):

* `userconsent-cntry-<cc>`: Where the `<cc>` is the two-letter, lowercase country-code
being used by User-Consent for this page.
* `userconsent-reg-<id>`: Where the `<id>` is the lowercase region identifier being used
for this page.  The region identifiers are set in the configuration for each region, but
by default include:  `ccpa`, `gdpr`, and `global`.

Note that this is only for convenience, to allow CSS controls based on country-code or
privacy region.  These class values are not used by User-Consent itself and can be
removed or replaced without impacting User-Consent's functionality.

----

## OneTrust Error Events

Upon detecting a failure with the initialization of OneTrust, User-Consent will throw the
`oneTrustFailed` event.

The data passed to the event handler as properties of the `detail` object include:
* _region_: A string containing the region config. name, generally `"ccpa"`, `"gdpr"`, or `"global"`.
* _time_: A Javascript Date object containing the time of the consent change.
* _consentConfirmed_: Boolean `true` if the user has confirmed consent in effect.
* _otId_: The OneTrust consent ID of the client, if any.
* _otVers_: The OneTrust version in use, if known.
* _code_: The numeric error code, see list below.
* _msg_: The error message string.

The possible error codes returned are:
1. OneTrust stub code loading failure.
2. OneTrust code initialization failure.
3. OneTrust SDK loading timeout (this may mean OneTrust is being blocked).
4. OneTrust other unknown error.

### OneTrust Blocked Events

Similar to an error with OneTrust, it is possible that OneTrust is being actively blocked from
being loaded or invoked by certain browsers or privacy blocking extensions.  While the detection
of this situation is imperfect, when it is detected a `oneTrustBlocked` event will be thrown.

The data passed to the event handler as properties of the `detail` object include:
* _region_: A string containing the region config. name, generally `"ccpa"`, `"gdpr"`, or `"global"`.
* _time_: A Javascript Date object containing the time of the consent change.
* _consentConfirmed_: Boolean `true` if the user has confirmed consent in effect.
* _otId_: The OneTrust consent ID of the client, if any.
* _otVers_: The OneTrust version in use, if known.

----

## Debugging

Passing a query parameter of `wmuc_debug=1` in the query string of your page URL will
enable "debug" mode.  This will output extra details to the Javascript console which may
assist you in determining exactly what was or wasn't loaded and why.

Debug mode also enables a test function, `WBD.UserConsent.getConsentHistory()`, which will
return an array of objects describing the consent-related actions taken and when.  Each object
consists of:

* `ts` - A Date object timestamp
* `act` - A string describing the action taken.  `SET` for initial consent settings, `CHK` for an `inUserConsentState` check, `ADD` for an `addScript` or `addScriptElement` call, `CHG` for a change in consent, or `NCC` for no consent change after interacting with the OneTrust panel.
* `desc` - The name/id/src of the item being checked or added, or the consent state being set.
* `res` - A boolean reflecting the consent granted state for an ADD/CHK; if this is the default consent for SET; if the CHG will result in a page reload on change.
* `note` - If ADD/CHK refused consent, either a note about being outside the regions list or the required first consent value in the consent list not permitted; for SET and NCC the region being used (ccpa/gdpr/global); for CHG a note that a change function was present and called.

Note that the array is populated in the order functions are called.  Other than `SET` always
being the first entry, the remaining entries should just be in call time order.  Keep that in
mind if using this for automated testing.  _If UserConsent is not in debug mode, the array
returned by `getConsentHistory` will be empty._

You can simulate a specific region by adding the `wmuc_cc=CC` (where "CC" is the two-letter
country code of the country you want to simulate) with the above `wmuc_debug=1` parameter on
the query string.  For example, to simulate GDPR, you might use a query string of:
`?wmuc_debug=1&wmuc_cc=GB`.  If you want to force User-Consent and OneTrust on a page to use
a difference language, you can add the `wmuc_lang=LC` option (where "LC" is the two-letter
language code).  For example, to force a page to use Spanish, you might use:
`?wmuc_debug=1&wmuc_lang=ES`.

----

## Reference

### Functions

- **addScript(options, consents, [parent], [regions])** - Creates and adds a script tag if consent has been granted.  Specify the script tag details (src, async, onload, type, id, class, etc.) in the _options_ object, specify the desired consent string(s) in the _consents_ array, use the optional _parent_ value to specify an HTTP Element to use as the parent for the script tag, and use the optional _regions_ array to limit the script to being invoked in specific regions or geological areas.
- **addScriptElement(element, consents, [parent], [regions])** - Identical to _addScript_ (above), but used to add a script tag which you have already created and not yet added to the DOM.
- **forceReconsent()** - Use to remove the user's current consent choices and force them to reconsent.  Note: Very dangerous, use sparingly and with caution.  Should probably ONLY be used in a "regionChangeAction" function when moving from CCPA or Global to GDPR.
- **getAdChoicesLinkAction()** - Returns a function suitable for calling in response to the user clicking on a link to request Ad Choices.  By default, this function will open the WBD Ad Choices page.
- **getAdChoicesLinkTitle()** - Returns the appropriate text string to use for a link to Ad Choices in the current region.
- **getAffiliatesLinkAction()** - Returns a function suitable for calling in response to the user clicking on a link to request Affiliates.  By default, this function will open the WBD Affiliates page.
- **getAffiliatesLinkTitle()** - Returns the appropriate text string to use for a link to Affiliates in the current region.
- **getCmpString(callback, [region])** - Can be used to get the current region's CMP string.  Pass a callback function that takes four parameters, respectively: region (string), iabVersion (number), cmpString (string), and error (object).  An optional region can be passed to force getting the correct string for that region's CMP function.  The values returned in the callback will be the current - or requested - region (ccpa|gdpr|global), the CMP major version (1|2), the CMP string (base-64 url-encoded), and an Error if there was a problem.
- **getConsentState()** - Returns the current consent state object.
- **getConsentTime()** - Returns a Date value reflecting the date/time the user last made a consent change.
- **getConsentVersion()** - Returns the current OneTrust consent version (probably 5.7.0 or higher).
- **getGeoCountry()** - Returns the current country-code being used for this user.
- **getGeoState()** - Returns the current state-code or geographical region being used for this user.
- **getGppAPIstring()** - Returns the current GPP string, or an empty string ("") if none is present.
- **getGppSection()** - Returns the current GPP section name ["uspnatv1|uspv1|tcfcav1|tcfeuv2"], or "none" if GPP is not in use for the current region.
- **getLinkAction()** - Returns a function suitable for calling in response to the user clicking on a link to request a consent change.  By default, this function will open the OneTrust consent panel.
- **getLinkTitle()** - Returns the appropriate text string to use for a link to request consent changes in the current region.
- **getPrivacyCenterLinkAction()** - Returns a function suitable for calling in response to the user clicking on a link to request the Privacy Center.  By default, this function will open the WBD Privacy Center page.
- **getPrivacyCenterLinkTitle()** - Returns the appropriate text string to use for a link to the Privacy Center in the current region.
- **getRegion()** - Returns the current region, one of "ccpa", "gdpr", or "global".
- **getReloadOnChange()** - Returns a boolean reflecting whether or not the page will reload on consent changes.
- **getReloadOnConsentReduction()** - Returns a boolean reflecting whether or not the page will reload on consent changes that reduce consent.
- **getRightsRequestLinkAction()** - Returns a function suitable for calling in response to the user clicking on a link to request the Individual Rights Request Portal.  By default, this function will open the WBD Individual Rights Request Portal page, except in the US MSPA region which defaults to the WBD US Privacy Rights Page..
- **getRightsRequestLinkTitle()** - Returns the appropriate text string to use for a link to the Individual Rights Request Portal in the current region.
- **getUspAPIstring()** - Returns the current CCPA IAB v1.0 (USPAPI) consent string. Not commonly needed, as most IAB-compliant third-parties will pull this using the `window.__uspapi` interface.
- **getTcfAPIstring()** - Returns the current GDPR IAB v2.0 (TCFAPI) consent string. Not commonly needed, as most IAB-compliant third-parties will pull this using the `window.__tcfapi` interface.
- **getVersion()** - Returns the current version of the User-Consent code.
- **init(config)** - Use to initialize UserConsent if not using `window.WBD.UserConsentConfig` (see "Adding the library" above).
- **inUserConsentState(consents, [options])** - Returns a boolean reflecting whether or not the current consent state permits the consent(s) specified in the _consents_ array.  Extremely useful for determining consent programmatically.  If an `id` property is passed in the optional _options_ object parameter (with a string value), that value will be used in any debug output showing the success or failure of this consent check.
- **isGpcInUse()** - Returns a boolean `true` if the current privacy settings are derived from the user having the GPC flag set, or `false` if not.
- **isGpcSet()** - Returns a boolean `true` if the current user request set the Global Privacy Controls flag, `false` if not.  This does not imply the setting was used to adjust any privacy controls, only that the header was set on the request.
- **isInRegion(region)** - Given _region_ set to "ccpa", "gdpr", or "global", will return a boolean `true` if the current user's region is a match.
- **isOneTrustLoaded()** - Returns a boolean reflecting the current loaded state of OneTrust scripts.
- **isOneTrustFailing()** - Returns a boolean `true` if OneTrust has failed to load or initialize successfully, `false` if it has loaded successfully, or `undefined` if still waiting for it.  Note that this will never return `true` if `isOneTrustLoaded()` is returning `true`.
- **isOneTrustBlocked()** - Returns a boolean `true` if OneTrust is being actively blocked by a content filter, `false` if no such filter is detected, or `undefined` if still testing.
- **isOptanonLoaded()** - _DEPRECATED (replaced with isOneTrustLoaded) - Returns a boolean reflecting the current loaded state of OneTrust scripts.
- **isChild()** - Returns a boolean `true` if this User-Consent instance is initialized and running in a child frame with with the parent frame also running User-Consent.  If `true`, consent state will be inherited from the parent.
- **isReady()** - Returns a boolean `true` once the User-Consent is ready to process consent.  Note that this **does NOT imply** that OneTrust has been loaded!
- **usingExternalConsent()** - Returns a boolean `true` if the consent data is being passed from a calling application, such as for a WebView in a mobile app.
- **usingGPP()** - Returns a boolean `true` if the user is in a region with GPP enabled.
- **usingIAB()** - Returns a boolean `true` if the user is in a region with IAB enabled.

### Events

- **userConsentReady** - Dispatched on the `document` when User-Consent is initialized and ready.  The event is only fired once, so before adding a handler, the current state should be checked with `isReady()`.
- **userConsentChanged** - Dispatched on the `document` when the user has modified their consent state.  NOTE: This event is dispatched _after_ the `consentChangeAction` is invoked, if one is defined.
- **userConsentNotChanged** - Dispatched on the `document` when the user has interacted with the OneTrust panel and accepted or rejected it without modifying their consent state.
- **oneTrustLoaded** - Dispatched on the `document` when OneTrust has loaded and initialized.  The event is only fired once, so before adding a handler, the current state should be checked with `isOptanonLoaded()`.  Note that this event will never fire if `oneTrustFailed` has fired.
- **oneTrustFailed** - Dispatched on the `document` if OneTrust has failed to completely load and initialize.  The event is only fired once, so before adding a handler the current state should be checked by calling `isOneTrustFailing()` (see above) and only adding a handler if it returns `undefined`.  Note that this event will never fire if `oneTrustLoaded` has fired.
- **oneTrustBlocked** - Dispatched on the `document` if OneTrust is being blocked by a content filter, such as EasyList - Cookie.  The event is only fired once, if this state is determined to be true, so before adding a handler the current state should be checked by calling `isOneTrustBlocked()` (see above) and only adding a handler if it returns `undefined`.
- **optanonLoaded** - _DEPRECATED (replaced with oneTrustLoaded)_ - Dispatched on the `document` when OneTrust has loaded and initialized.  The event is only fired once, so before adding a handler, the current state should be checked with `isOptanonLoaded()`.

----

## User-Consent and Categorization

The categories used by User-Consent reflect the current categorization requirements
of GDPR (IAB TCF v2.0) while also accounting for the requirements of the CCPA.

### GDPR (IAB TFC v2.0) Categories

#### Store and/or access information on a device (`data-store`)

Cookies, device identifiers, or other information can be stored or accessed on your
device for the purposes presented to you.

#### Select basic ads (`ads-contextual`)

Ads can be shown to you based on the content you’re viewing, the app you’re using, your approximate location, or your device type.

* **Contextual Ads**

    Ads can be shown to you based on the content you’re viewing, the app you’re using, your approximate location, or your device type.

* **Select basic ads**

    To do basic ad selection vendors can:
    * Use real-time information about the context in which the ad will be shown, to show the ad, including information about the content and the device, such as: device type and capabilities, user agent, URL, IP address
    * Use a user’s non-precise geolocation data
    * Control the frequency of ads shown to a user.
    * Sequence the order in which ads are shown to a user.
    * Prevent an ad from serving in an unsuitable editorial (brand-unsafe) context
    Vendors cannot:
    * Create a personalised ads profile using this information for the selection of future ads.
    * N.B. Non-precise means only an approximate location involving at least a radius of 500 meters is permitted.

#### Personalized Ads (`ads-person-prof` and `ads-person`)

* **Create a personalized ads profile**

    A profile can be built about you and your interests to show you personalised ads that are relevant to you.

    To create a personalised ads profile vendors can:
    * Collect information about a user, including a user's activity, interests, demographic information, or location, to create or edit a user profile for use in personalised advertising.
    * Combine this information with other information previously collected, including from across websites and apps, to create or edit a user profile for use in personalised advertising.

* **Select personalized ads**

    Personalised ads can be shown to you based on a profile about you.
    To select personalised ads vendors can:
    * Select personalised ads based on a user profile or other historical user data, including a user’s prior activity, interests, visits to sites or apps, location, or demographic information.

#### Personalized Content (`content-person-prof` and `content-person`)

* **Create a personalized content profile**

    A profile can be built about you and your interests to show you personalised content that is relevant to you.

    To create a personalised content profile vendors can:
    * Collect information about a user, including a user's activity, interests, demographic information, or location, to create or edit a user profile for use in personalizing content.
    * Combine this information with other information previously collected, including from across websites and apps, to create or edit a user profile for use in personalizing content.

* **Select personalized content**

    Personalised content can be shown to you based on a profile about you.
    To select personalised content vendors can:
    * Select personalised content based on a user profile or other historical user data, including a user’s prior activity, interests, visits to sites or apps, location, or demographic information.

#### Ad and content measurement, audience insights, and product development (`measure-ads`, `measure-content`, `measure-market`, `product-develop`)

* **Ad Performance**

    To measure ad performance vendors can:
    * Measure whether and how ads were delivered to and interacted with by a user
    * Provide reporting about ads including their effectiveness and performance
    * Provide reporting about users who interacted with ads using data observed during the course of the user's interaction with that ad
    * Provide reporting to publishers about the ads displayed on their property
    * Measure whether an ad is serving in a suitable editorial environment (brand-safe) context
    * Determine the percentage of the ad that had the opportunity to be seen and the duration of that opportunity
    * Combine this information with other information previously collected, including from across websites and apps
    Vendors cannot:
    * Apply panel- or similarly-derived audience insights data to ad measurement data without a Legal Basis to apply market research to generate audience insights (Purpose 9).

* **Content performance**

    To measure content performance vendors can:
    * Measure and report on how content was delivered to and interacted with by users.
    * Provide reporting, using directly measurable or known information, about users who interacted with the content
    * Combine this information with other information previously collected, including from across websites and apps.
    Vendors cannot:
    * Measure whether and how ads (including native ads) were delivered to and interacted with by a user.
    * Apply panel- or similarly derived audience insights data to ad measurement data without a Legal Basis to apply market research to generate audience insights (Purpose 9).

* **Audience insights**

    To apply market research to generate audience insights vendors can:
    * Provide aggregate reporting to advertisers or their representatives about the audiences reached by their ads, through panel-based and similarly derived insights.
    * Provide aggregate reporting to publishers about the audiences that were served or interacted with content and/or ads on their property by applying panel-based and similarly derived insights.
    * Associate offline data with an online user for the purposes of market research to generate audience insights if vendors have declared to match and combine offline data sources (Feature 1)
    * Combine this information with other information previously collected including from across websites and apps. 
    Vendors cannot:
    * Measure the performance and effectiveness of ads that a specific user was served or interacted with, without a Legal Basis to measure ad performance.
    * Measure which content a specific user was served and how they interacted with it, without a Legal Basis to measure content performance.

* **Product development**

    Your data can be used to improve existing systems and software, and to develop new products.
    To develop new products and improve products vendors can:
    * Use information to improve their existing products with new features and to develop new products
    * Create new models and algorithms through machine learning
    Vendors cannot:
    * Conduct any other data processing operation allowed under a different purpose under this purpose

#### Ensure security, prevent fraud, and debug (`product-security`)

    Your data can be used to monitor for and prevent fraudulent activity, and ensure systems and processes work properly and securely.
    To ensure security, prevent fraud and debug vendors can:
    * Ensure data are securely transmitted
    * Detect and prevent malicious, fraudulent, invalid, or illegal activity.
    * Ensure correct and efficient operation of systems and processes, including to monitor and enhance the performance of systems and processes engaged in permitted purposes
    Vendors cannot:
    * Conduct any other data processing operation allowed under a different purpose under this purpose.

#### Technically Deliver Ads or Content (`deliver-content`)

    Your device can receive and send information that allows you to see and interact with ads and content.
    To deliver information and respond to technical requests vendors can:
    * Use a user’s IP address to deliver an ad over the internet
    * Respond to a user’s interaction with an ad by sending the user to a landing page
    * Use a user’s IP address to deliver content over the internet
    * Respond to a user’s interaction with content by sending the user to a landing page
    * Use information about the device type and capabilities for delivering ads or content, for example, to deliver the right size ad creative or video file in a format supported by the device
    Vendors cannot:
    * Conduct any other data processing operation allowed under a different purpose under this purpose

#### Save and Communicate Privacy Choices (`privacy-choices`)

    The choices you make regarding the purposes and entities listed in this notice are saved and made
    available to those entities in the form of digital signals (such as a string of characters). This
    is necessary in order to enable both this service and those entities to respect such choices.
    This purpose is intended to enable processing activities such as:
    * Verify information about the transparency, consent, and/or objection status of a Vendor and/or Purpose, the opt-in status of a Special Feature, and Publisher restrictions.
    * Retrieve and/or pass on Signals in the technical formats required by the Specifications and in accordance with Policies, when available.
    * Respect Signals communicated by a CMP or received from a Vendor who forwarded the Signal originating from a CMP in accordance with the Specifications and Policies, and act accordingly.

#### Precise geolocation data (`geolocate`)

    Your precise geolocation data can be used in support of one or more purposes. This means your location can be accurate to within several meters.
    Vendors can:
    * Collect and process precise geolocation data in support of one or more purposes.
    * N.B.: Precise geolocation means that there are no restrictions on the precision of a user’s location; this can be accurate to within several meters.

#### Match and combine offline data sources (`combine-data`)

    Vendors can:
    * Combine data obtained offline with data collected online in support of one or more Purposes or Special Purposes.

#### Link different devices (`link-devices`)

    Different devices can be determined as belonging to you or your household in support of one or more of purposes.
    Vendors can:
    * Deterministically determine that two or more devices belong to the same user or household.
    * Probabilistically determine that two or more devices belong to the same user or household.
    * Actively scan device characteristics for identification for probabilistic identification if users have allowed vendors to actively scan device characteristics for identification (Special Feature 2).

#### Receive and use automatically-sent device characteristics for identification (`id-devices`)

    Vendors can:
    * Create an identifier using data collected automatically from a device for specific characteristics, e.g. IP address, user-agent string.
    * Use such an identifier to attempt to re-identify a device.
    Vendors cannot:
    * Create an identifier using data collected via actively scanning a device for specific characteristics, e.g. installed font or screen resolution without users’ separate opt-in to actively scanning device characteristics for identification.
    * Use such an identifier to re-identify a device.

### GPP US National Requirements - Basic (`vendor`, `targeting`)

The User-Consent "Basic" configuration for GPP US National uses the `vendor` and `targeting`
categories.  If you just wish to use a single toggle within your OneTrust template, make sure
you use a "Category Group" in your template for this control and add both the "Third-Party
Vendor" and "Targeting" categories to it.

### GPP US National Requirements - IAB (`data-share`, `data-sell`, `ads-person-prof`, `ads-person`, etc.)`

The User-Consent "IAB" configuration for GPP US National uses independent values for each
IAB-defined category.  If you want to use a single "Do Not Sell or Share" toggle within your
OneTrust template, make sure you use a "Category Group" in your template for this control and
add: `Sharing of Data`, `Sale of Data`, `Create a personalized ads profile`, and
`Select personalized ads` to the group.

For the full array of IAB categories used, see the [table below](#for-iab-versions-of-user-consent).

### GPP US National Requirements - Legacy Discovery (`ads`)

The User-Consent "Disco" configuration for GPP US National uses the `ads` category.  If you
just wish to use a single toggle within your OneTrust template, make sure you use a "Category Group"
in your template for this control and add the "Ads" category to it.

----

### How Are Categories Used to Determine Consent?

The following table illustrates the consent categories and how they are used
to reflect the user's intentions to grant or withhold consent based on the
GPP and GDPR panel settings used.  Note that a basis with "legimate interest"
implies consent by default (which can be opted-out of).

#### For Basic versions of User-Consent:

| User Consent Category | OneTrust Code | Region | Description | Basis |
| --------------------- | ------------- | ------------------- | ----------- | ----- |
| vendor | tpv | GPP US Nat. | Share and sell data with 3rd party vendors | Legitimate Interest |
| functional | fc | GDPR | Collect data to provide enhanced functionality and personalization of content | Consent |
| performance | pc | GDPR | Collect data to measure and analyze performance | Consent |
| media | mc | GDPR | Collect and share data with social media providers | Consent |
| targeting | tc | GDPR & GPP US Nat. | Collect data to provide targeted ads | GDPR Consent, US Nat. Legitimate Interest |
| 1p-targeting | ftc | Lat. Am. | First-party data collection to provide targeted ads | Consent (Lat. Am. only) |

#### For IAB versions of User-Consent:

| User Consent Category | OneTrust Code | IAB/TCF Designation | Description | Basis |
| --------------------- | ------------- | ------------------- | ----------- | ----- |
| data-store | dsa | GDPR TCF Purpose 1 | Store and/or access information on a device | Consent |
| ads-contextual | cad | GDPR TCF Purpose 2 | Select basic (contextual) ads | Consent |
| ads-person-prof | pap | GDPR TCF Purpose 3 & GPP US Nat. Ads Targeting | Create a personalized ads profile | Consent |
| ads-person | pad | GDPR TCF Purpose 4 & GPP US Nat. Ads Targeting | Select personalized ads | Consent |
| content-person-prof | pcp | GDPR TCF Purpose 5 | Create a personalized content profile | Consent |
| content-person | pcd | GDPR TCF Purpose 6 | Select personalized content | Consent |
| measure-ads | map | GDPR TCF Purpose 7 | Measure ad performance | Consent |
| measure-content | mcp | GDPR TCF Purpose 8 | Measure content performance | Consent |
| measure-market | mra | GDPR TCF Purpose 9 | Apply market research to generate audience insights | Consent |
| product-develop | pdd | GDPR TCF Purpose 10 | Product development | Consent |
| content-contextual | ccd | GDPR TCF Purpose 11 | Select contextual content | Consent |
| product-security | sec | GDPR TCF Special Purpose 1 | Ensure security, prevent fraud, and debug | Strictly Necessary |
| deliver-content | tdc | GDPR TCF Special Purpose 2 | Technically Deliver Ads or Content | Strictly Necessary |
| privacy-choices | scp | GDPR TCF Special Purpose 3 | Save and Communicate Privacy Choices | Strictly Necessary |
| combine-data | cos | GDPR TCF Feature 1 | Match and combine offline data sources | Notice Only |
| link-devices | dlk | GDPR TCF Feature 2 | Link different devices | Notice Only |
| id-devices | did | GDPR TCF Feature 3 | Receive automatically sent device characteristics for identification | Notice Only |
| geolocate | gld | GDPR TCF Special Feature 1 & GPP US Nat. Sensitive Data 8 | Use precise geolocation data | Consent |
| scan-devices | sid | GDPR TCF Special Feature 2 | Scan device characteristics for identification | Consent |
| 1p-targeting | ftc | Lat. Am. | First-party data collection to provide targeted ads | Consent (Lat. Am. only) |
| data-share | dsh | GPP US Nat. Data Sharing | Share data with 3rd party vendors | GPP US Nat. Legitimate Interest |
| data-sell | dsl | GPP US Nat. Data Sales | Sell data to 3rd party vendors | GPP US Nat. Legitimate Interest |
| personal-data | pdu | GPP US Nat. Personal Data | Use personal data for other purposes | Consent |
| known-child-12 | kc12 | GPP US Nat. Known Child 1 | Data from a known child 0-12 years of age | Consent |
| known-child-16 | kc16 | GPP US Nat. Known Child 2 | Data from a known child 13-16 years of age | Consent |
| sensitive-racial | sdre | GPP US Nat. Sensitive Data 1 | Use of sensitive racial or ethnic data | Consent |
| sensitive-belief | sdrb | GPP US Nat. Sensitive Data 2 | Use of sensitive religious or beliefs data | Consent |
| sensitive-health | sdhe | GPP US Nat. Sensitive Data 3 | Use of sensitive medical or health data | Consent |
| sensitive-sexual | sdso | GPP US Nat. Sensitive Data 4 | Use of sensitive sexual orientation or preference data | Consent |
| sensitive-citizen | sdir | GPP US Nat. Sensitive Data 5 | Use of sensitive citizenship or immigration status data | Consent |
| sensitive-gene | sdge | GPP US Nat. Sensitive Data 6 | Use of sensitive genetic data for ID | Consent |
| sensitive-biometric | sdbm | GPP US Nat. Sensitive Data 7 | Use of sensitive biometric data for ID | Consent |
| sensitive-spi | sdsp | GPP US Nat. Sensitive Data 9 | Use of sensitive personal information | Consent |
| sensitive-ssi | sdss | GPP US Nat. Sensitive Data 10 | Use of sensitive security information | Consent |
| sensitive-org | sduo | GPP US Nat. Sensitive Data 11 | Use of sensitive union or organization membership data | Consent |
| sensitive-comm | sdco | GPP US Nat. Sensitive Data 12 | Use of sensitive communication data | Consent |

Note that for CCPA to work correctly, your OneTrust template for CCPA must associate a cookie
category code "ven" and description "Share data with 3rd party vendors" with that purpose in
your configuration.

#### For Legacy Discovery versions of User-Consent:

| User Consent Category | OneTrust Code | Region | Description | Basis |
| --------------------- | ------------- | ------------------- | ----------- | ----- |
| analytics | c0002 | GDPR | Collect data to measure and analyze performance | Consent |
| personalization | c0003 | GDPR | Collect data to provide enhanced functionality and personalization of content | Consent |
| ads | c0004 | GDPR & GPP US Nat. | Collect data to provide advertising and share/sell data with vendors | GDPR Consent, US Nat. Legitimate Interest |
| social | c0005 | GDPR | Collect and share data with social media providers | Consent |

**IMPORTANT: You should NOT follow the OneTrust steps to include their `otCCPAiab.js` script!
User-Consent itself provides this functionality and including this script from OneTrust will
prevent CCPA from working correctly.**

----

#### Consent Examples

So, some examples that may help you with consent...

##### Usabilla

OK, so you use Usabilla on your site to collect feedback and perform surveys.  Normally,
this is done by include a small minified script on your page, like:
```
<script>
window.lightningjs||function(c){function g(b,d){d&&(d+=(/\?/.test(d)?"&":"?")+"lv=1");c[b]||function(){var i=window,h=document,j=b,g=h.location.protocol,l="load",k=0;(function(){function b(){a.P(l);a.w=1;c[j]("_load")}c[j]=function(){function m(){m.id=e;return c[j].apply(m,arguments)}var b,e=++k;b=this&&this!=i?this.id||0:0;(a.s=a.s||[]).push([e,b,arguments]);m.then=function(b,c,h){var d=a.fh[e]=a.fh[e]||[],j=a.eh[e]=a.eh[e]||[],f=a.ph[e]=a.ph[e]||[];b&&d.push(b);c&&j.push(c);h&&f.push(h);return m};return m};var a=c[j]._={};a.fh={};a.eh={};a.ph={};a.l=d?d.replace(/^\/\//,(g=="https:"?g:"http:")+"//"):d;a.p={0:+new Date};a.P=function(b){a.p[b]=new Date-a.p[0]};a.w&&b();i.addEventListener?i.addEventListener(l,b,!1):i.attachEvent("on"+l,b);var q=function(){function b(){return["<head></head><",c,' onload="var d=',n,";d.getElementsByTagName('head')[0].",d,"(d.",g,"('script')).",i,"='",a.l,"'\"></",c,">"].join("")}var c="body",e=h[c];if(!e)return setTimeout(q,100);a.P(1);var d="appendChild",g="createElement",i="src",k=h[g]("div"),l=k[d](h[g]("div")),f=h[g]("iframe"),n="document",p;k.style.display="none";e.insertBefore(k,e.firstChild).id=o+"-"+j;f.frameBorder="0";f.id=o+"-frame-"+j;/MSIE[ ]+6/.test(navigator.userAgent)&&(f[i]="javascript:false");f.allowTransparency="true";l[d](f);try{f.contentWindow[n].open()}catch(s){a.domain=h.domain,p="javascript:var d="+n+".open();d.domain='"+h.domain+"';",f[i]=p+"void(0);"}try{var r=f.contentWindow[n];r.write(b());r.close()}catch(t){f[i]=p+'d.write("'+b().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};a.l&&setTimeout(q,0)})()}();c[b].lv="1";return c[b]}var o="lightningjs",k=window[o]=g(o);k.require=g;k.modules=c}({});if(!navigator.userAgent.match(/Android|BlackBerry|BB10|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){window.usabilla_live = lightningjs.require("usabilla_live", "//w.usabilla.com/c3244e3d16ba.js");}else{window.usabilla_live = lightningjs.require("usabilla_live", "//w.usabilla.com/0649ef72a7be.js");}
</script>
```

But this is done to provide us with a measure of how well the site is operating, which
falls under the GDPR "Apply market research to generate aud. insights" classification
(`measure-market`).  As none of the data involved is used to track the user or their
behavior, and none of the submitted data is used by the third party for anything outside
of their specific business relationship with us, this is not something regulated by the
CCPA (so no `vendor` needed).  Also, Usabilla does not use IAB, we will not use the
special `iab` category.  This means our only consent required to use Usabilla is
`measure-market`.

We can now put the above script blob into an appropriate conditional on our User-Consent
enabled page:
```
<script>
if (window.WBD.UserConsent.inUserConsentState(['measure-market'], {id: 'usabilla'})) {
window.lightningjs||function(c){function g(b,d){d&&(d+=(/\?/.test(d)?"&":"?")+"lv=1");c[b]||function(){var i=window,h=document,j=b,g=h.location.protocol,l="load",k=0;(function(){function b(){a.P(l);a.w=1;c[j]("_load")}c[j]=function(){function m(){m.id=e;return c[j].apply(m,arguments)}var b,e=++k;b=this&&this!=i?this.id||0:0;(a.s=a.s||[]).push([e,b,arguments]);m.then=function(b,c,h){var d=a.fh[e]=a.fh[e]||[],j=a.eh[e]=a.eh[e]||[],f=a.ph[e]=a.ph[e]||[];b&&d.push(b);c&&j.push(c);h&&f.push(h);return m};return m};var a=c[j]._={};a.fh={};a.eh={};a.ph={};a.l=d?d.replace(/^\/\//,(g=="https:"?g:"http:")+"//"):d;a.p={0:+new Date};a.P=function(b){a.p[b]=new Date-a.p[0]};a.w&&b();i.addEventListener?i.addEventListener(l,b,!1):i.attachEvent("on"+l,b);var q=function(){function b(){return["<head></head><",c,' onload="var d=',n,";d.getElementsByTagName('head')[0].",d,"(d.",g,"('script')).",i,"='",a.l,"'\"></",c,">"].join("")}var c="body",e=h[c];if(!e)return setTimeout(q,100);a.P(1);var d="appendChild",g="createElement",i="src",k=h[g]("div"),l=k[d](h[g]("div")),f=h[g]("iframe"),n="document",p;k.style.display="none";e.insertBefore(k,e.firstChild).id=o+"-"+j;f.frameBorder="0";f.id=o+"-frame-"+j;/MSIE[ ]+6/.test(navigator.userAgent)&&(f[i]="javascript:false");f.allowTransparency="true";l[d](f);try{f.contentWindow[n].open()}catch(s){a.domain=h.domain,p="javascript:var d="+n+".open();d.domain='"+h.domain+"';",f[i]=p+"void(0);"}try{var r=f.contentWindow[n];r.write(b());r.close()}catch(t){f[i]=p+'d.write("'+b().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};a.l&&setTimeout(q,0)})()}();c[b].lv="1";return c[b]}var o="lightningjs",k=window[o]=g(o);k.require=g;k.modules=c}({});if(!navigator.userAgent.match(/Android|BlackBerry|BB10|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){window.usabilla_live = lightningjs.require("usabilla_live", "//w.usabilla.com/c3244e3d16ba.js");}else{window.usabilla_live = lightningjs.require("usabilla_live", "//w.usabilla.com/0649ef72a7be.js");}
}
</script>
```

In this case, we will always get Usabilla in a CCPA region, but only get it in a GDPR
region if the user consented to "Ad and content measurements".

That wasn't so bad.  Let's try something with a bit more nuance...


##### BounceX

Let's say you need to place [BounceX](https://www.bouncex.com/products/) on your site to
use their user personalization functionality.  This is normally done with a simple script
tag along the lines of:
```
<script async src="//tag.bounceexchange.com/340/i.js"></script>
```

However, BounceX does six things that touch consent:
1. It can be used to deliver ads targeted to a user based on their behavior.  Since it
directly tracks that behavior, this is clearly something that falls within the GDPR
"Create a personalized ads profile" category.
2. As it then can be used to deliver those personalized ads, it would also then fall
into the "Select personalized ads" category.
3. It can be used to for site personalization based on a user's behavior.  As with ads,
this would place it within the "Create a personalized content profile" category.
4. As it can then be used to delivery that personalized content, it would then also
fall within the "Select personalized content" category.
5. To perform 1 and 3, it tracks users across different websites and analyzes their
behavior.  This is activity that is regulated by the CCPA "Share data with 3rd party
vendors" category.
6. To do all of the above, BounceX uses device identifiers and cookies to help identify a
user across different sites.  This is something that is regulated by the GDPR "Store
and/or access information on a device" category, **and** by the CCPA "Share data with
3rd party vendors" category.

So, since we are only using BounceX to deliver Content and not Ads, we can ignore #1 and
#2.  But we **are** using it to deliver personalized content, so #3 and #4 mean we need
`content-person-prof` and `content-person` consent.  Since #5 is also a thing, we will 
also need consent for `data-store`.  Since #6 is also still a thing, we will also need
`vendor` consent in CCPA.  So, for our use of BounceX, we need four consent values:
`content-person-prof`, `content-person`, `data-store`, and `vendor`.

Now, upon a little [further investigation](https://www.bouncex.com/privacy/), we find that
BounceX is IAB-compliant.  This means, in regions where IAB is in effect, BounceX knows
how to handle consent correctly for itself.  This means we should also add the special
consent category `iab` to the beginning of that list.

So, on our page with User-Consent enabled, we add BounceX support by putting something like
the following on the page:
```
<script>
    window.WBD.UserConsent.addScript({ async: true, src="//tag.bounceexchange.com/340/i.js" },
        ["iab", "vendor", "data-store", "content-person-prof", "content-person"]);
</script>

Now we only get BounceX added to the page if we are in a region that supports IAB, or if the
GDPR user has granted consent for both "Store and access information" _and_
"Personalized content", or if the CCPA user has not opted-in to "Do Not Sell".

In terms of technical difficult, doing this is very easy.  The only difficult part is
evaluting the vendors and how they are used on the page.

