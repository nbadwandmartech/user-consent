/*
 * Example config for self-invoking brand-specific script production
 * for DC (using the basic config).
 *
 * Invoke with: "yarn run build dc.config.mjs"
 *
 * This should build two self-invoking user-consent scripts that are
 * fully-configured for DC and ready to be included on those sites.
 */

import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import pkg from './package.json' assert { type: 'json' };
import fs from 'fs';

const brand = "dc";
const prodConfig = {
    "cookieDomain": ".dc.com",
    "domId": "90a8a3e0-d0c2-4020-a79a-b867cf0f8086",
    "enableTransitionCheck": false
};
const testConfig = {
    "cookieDomain": ".dc.com",
    "domId": "90a8a3e0-d0c2-4020-a79a-b867cf0f8086-test",
    "enableDebug": true,
    "enableTransitionCheck": false
};

/****  Should not need to edit anything below this line  ****/

export default [
    {
        context: 'window',
        input: 'src/wrapper.js',
        output: {
            externalLiveBindings: false,
            file: './' + brand + '-test-userconsent.js',
            format: 'iife',
            generatedCode: 'es2015',
            sourcemap: true
        },
        plugins: [
            replace({
                __ucBrandConfig__: JSON.stringify(testConfig),
                __ucCode__: fs.readFileSync('./dist/userconsent-basic.js'),
                preventAssignment: true
            }),
            commonjs()
        ]
    },
    {
        context: 'window',
        input: 'src/wrapper.js',
        output: {
            externalLiveBindings: false,
            file: './' + brand + '-userconsent.min.js',
            format: 'iife',
            generatedCode: 'es2015',
            sourcemap: true
        },
        plugins: [
            replace({
                __ucBrandConfig__: JSON.stringify(prodConfig),
                __ucCode__: fs.readFileSync('./dist/userconsent-basic.min.js'),
                preventAssignment: true
            }),
            commonjs()
        ]
    }
];

