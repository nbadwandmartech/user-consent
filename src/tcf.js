
    /**
     * Decode from (almost) Base64 into a Buffer
     *
     * @param {string} val - The string to decode
     * @returns {string} decoded value of the string or an empty string
     */
    function decodeBase64(val) {
        if (typeof win.atob === 'function') {
            try {
                return atob(val.replace(/_/g, '/').replace(/-/g, '+'));
            } catch (error) {
                uclog('error', 'Failed to decode TC string');
            }
        }
        return '';
    }

    /**
     * Decode bit string to boolean
     *
     * @param {string} - bit string (char "0" or "1")
     * @returns {boolean} - boolean value of bit
     */
    function decodeBoolean(b) {
        return !!(Number(b));
    }

    /**
     * Decode bits to integer
     *
     * @param {string} - bits string
     * @returns {number} - integer value of binary string
     */
    function decodeInt(b) {
        return parseInt(b, 2) || 0;
    }

    /**
     * Decode bits to date
     *
     * @param {string} - bits string
     * @returns {number} - Epoch date value of binary string
     */
    function decodeDate(b) {
        return decodeInt(b) * 100;
    }

    /**
     * Decode bits to string
     *
     * @param {string} - bits string
     * @returns {string} - Character string of binary string
     */
    function decodeString(b) {
        const cOff = 'A'.charCodeAt(),
            items = b.match(/.{6}/g) || [];
        let res = '';

        for (let i = 0; i < items.length; i++) {
            res += String.fromCharCode(decodeInt(items[i]) + cOff);
        }
        return res;
    }

    /**
     * Decode bits to flags
     *
     * @param {string} - bits string
     * @returns {object} - Object containing boolean flag names and values
     */
    function decodeFlags(b) {
        const items = b.split('');
        let res = {};

        for (let i = 0; i < items.length; i++) {
            if (decodeBoolean(items[i])) {
                res[i + 1] = true;
            }
        }
        return res;
    }

    /**
     * Copy/assign object for TC string schema handling
     *
     * @param {object} - Target object to assign/copy to
     * @param {...object} - Source object(s) to copy from
     * @returns {object} - Target object
     */
    function objectAssign(t) {
        const to = Object(t);

        for (let i = 1, ns; i < arguments.length; i++) {
            ns = arguments[i];
            if (ns !== null) {
                for (let nk in ns) {
                    if (Object.prototype.hasOwnProperty.call(ns, nk)) {
                        to[nk] = ns[nk];
                    }
                }
            }
        }
        return to;
    }

    /**
     * Handle TC string schema
     *
     * @param {object} - Schema object
     * @param {object} - Value object
     * @param {object} - Result object
     * @returns {object} - Result object with value and schema applied
     */
    function applySchemaValue(schema, val, res) {
        if (schema.key && !schema.hidden) {
            if (schema.parent) {
                if (!res[schema.parent]) {
                    res[schema.parent] = {};
                }
                if (typeof res[schema.parent][schema.key] === 'object' && typeof val === 'object') {
                    res[schema.parent][schema.key] = objectAssign(res[schema.parent][schema.key], val);
                } else {
                    res[schema.parent][schema.key] = val;
                }
            } else if (typeof res[schema.key] === 'object' && typeof val === 'object') {
                res[schema.key] = objectAssign(res[schema.key], val);
            } else {
                res[schema.key] = val;
            }
        }
        return res;
    }

    /**
     * Decode TC string segments into bit strings
     *
     * @param {string} - base64 string for segment
     * @returns {array} - Array of segment values
     */
    function getSegments(s) {
        let blocks,
            res = [];

        if (typeof s !== 'string') {
            uclog('error', 'Invalid TC string specified');
            return res;
        }
        blocks = s.split(".");

        for (let i = 0; i < blocks.length; i++) {
            let bin = decodeBase64(blocks[i]),
                r = '';

            for (let j = 0; j < bin.length; j++) {
                let bits = bin.charCodeAt(j).toString(2),
                    p = '00000000'.slice(0, 8 - bits.length);

                r += p + bits;
            }
            res.push(r);
        }

        if (decodeInt(res[0].slice(0, 6)) !== 2) {
            uclog('error', 'Unsupported TC string version');
            return [];
        }
        return res;
    }

    /**
     * Decode TC string segments into bit strings
     *
     * @param {string} - base64 string for segment
     * @returns {array} - Array of segment values
     */
    function getQueue(segs) {
        const queuePurposes = [
                {key: 'purposeConsents', size: 24, decoder: decodeFlags},
                {key: 'purposeLegitimateInterests', size: 24, decoder: decodeFlags}
            ],
            queueVendors = [
                {key: 'maxVendorId', size: 16},
                {key: 'isRangeEncoding', size: 1, decoder: decodeBoolean}
            ],
            queueCore = [
                {key: 'version', size: 6},
                {key: 'created', size: 36, decoder: decodeDate},
                {key: 'lastUpdated', size: 36, decoder: decodeDate},
                {key: 'cmpId', size: 12},
                {key: 'cmpVersion', size: 12},
                {key: 'consentScreen', size: 6},
                {key: 'consentLanguage', size: 12, decoder: decodeString},
                {key: 'vendorListVersion', size: 12},
                {key: 'tcfPolicyVersion', size: 6},
                {key: 'isServiceSpecific', size: 1, decoder: decodeBoolean},
                {key: 'useNonStandardStacks', size: 1, decoder: decodeBoolean},
                {key: 'specialFeatureOptins', size: 12, decoder: decodeFlags}
            ].concat(queuePurposes).concat(
                {key: 'purposeOneTreatment', size: 1, decoder: decodeBoolean},
                {key: 'publisherCountryCode', size: 12, decoder: decodeString},
                {key: 'vendorConsents', queue: [
                    {key: 'maxVendorId', size: 16},
                    {key: 'isRangeEncoding', size: 1, decoder: decodeBoolean}
                ]},
                {key: 'vendorLegitimateInterests', queue: queueVendors},
                {key: 'publisherRestrictions', queue: [{key: 'numPubRestrictions', size: 12}]}
            ),
            queueSegment = [{size: 3}],
            queueDisclosedVendors = [].concat(queueSegment).concat(queueVendors),
            queueAllowedVendors = [].concat(queueSegment).concat(queueVendors),
            queuePublisherTC = [].concat(queueSegment).concat(queuePurposes).concat({
                key: 'numCustomPurposes',
                size: 6
            });
        let res = [{key: 'core', queue: queueCore}];

        for (let i = 1; i < segs.length; i++) {
            let seg = segs[i],
                t = decodeInt(seg.slice(0, 3));

            if (t === 1) {
                res.push({key: 'disclosedVendors', queue: queueDisclosedVendors});
            } else if (t === 2) {
                res.push({key: 'allowedVendors', queue: queueAllowedVendors});
            } else if (t === 3) {
                res.push({key: 'publisherTC', queue: queuePublisherTC});
            }
        }
        return res;
    }

    /**
     * Decode Base64 TC string into component parts
     *
     * @param {string} tcs - Base64 TC string to process
     * @returns {object} - Resulting IAB consent object from TC string
     */
    function processTCString(tcs) {
        let offset = 0,
            iabres = {},
            reduceQueue = function (queue, schema, value, result) {
                let reduceNumPubEntries = () => {
                        if (result.pubRestrictionEntry && result.rangeEntry) {
                            for (let key in result.rangeEntry) {
                                if (Object.prototype.hasOwnProperty.call(result.rangeEntry, key)) {
                                    result.pubRestrictionEntry[key] = (result.pubRestrictionEntry[key] || []).concat(result.rangeEntry[key]);
                                }
                            }
                        }

                        if (result.numPubRestrictions) {
                            result.numPubRestrictions--;
                            queue.push({key: 'purposeId', size: 6}, {key: 'restrictionType', size: 2}, {key: 'numEntries', size: 12});
                        }
                    },
                    reduceNumEntries = () => {
                        if (result.numEntries) {
                            result.numEntries--;
                            queue.push({key: 'isARange', size: 1, decoder: decodeBoolean}, {key: 'startVendorId', size: 16});
                        } else {
                            reduceNumPubEntries();
                        }
                    },
                    getRangeResult = () => {
                        if (result.purposeId) {
                            return [{
                                purpose: result.purposeId,
                                isAllowed: result.restrictionType !== 0,
                                isConsentRequired: result.restrictionType === 1,
                                isLegitimateInterestRequired: result.restrictionType === 2
                            }];
                        }
                        return true;
                    };

                if (schema.key === 'isRangeEncoding') {
                    queue.push(value ? {key: 'numEntries', size: 12} : {key: 'bitField', size: result.maxVendorId, decoder: decodeFlags});
                } else if (schema.key === 'numEntries') {
                    result.rangeEntry = {};
                    reduceNumEntries();
                } else if (schema.key === 'isARange') {
                    if (value) {
                        queue.push({key: 'endVendorId', size: 16});
                    }
                } else if (schema.key === 'startVendorId') {
                    if (!result.isARange) {
                        result.rangeEntry[value] = getRangeResult();
                        reduceNumEntries();
                    }
                } else if (schema.key === 'endVendorId') {
                    for (let vendorId = result.startVendorId; vendorId <= result.endVendorId; vendorId += 1) {
                        result.rangeEntry[vendorId] = getRangeResult();
                    }
                    reduceNumEntries();
                } else if (schema.key === 'numCustomPurposes') {
                    queue.push({
                        key: 'customPurposeConsents',
                        size: result.numCustomPurposes,
                        decoder: decodeFlags
                    }, {
                        key: 'customPurposeLegitimateInterests',
                        size: result.numCustomPurposes,
                        decoder: decodeFlags
                    });
                } else if (schema.key === 'numPubRestrictions') {
                    result.pubRestrictionEntry = {};
                    reduceNumPubEntries();
                }
            },
            reduceResult = (res) => {
                return res.pubRestrictionEntry || res.rangeEntry || res.bitField || res;
            },
            getSchemaResult = (schema, bits) => {
                const val = bits.slice(offset, offset + schema.size);

                offset += schema.size;
                return (schema.decoder || decodeInt)(val);
            },
            getSectionResult = (sectionSchema, bits) => {
                let res = {};

                if (!sectionSchema.queue) {
                    return getSchemaResult(sectionSchema, bits);
                }
                for (let i = 0; i < sectionSchema.queue.length; i += 1) {
                    let schema = sectionSchema.queue[i],
                        val = getSchemaResult(schema, bits);

                    if (schema.key) {
                        res[schema.key] = val;
                    }
                    reduceQueue(sectionSchema.queue, schema, val, res);
                }
                return reduceResult(res);
            },
            getBlockResult = (blockSchema, bits) => {
                let res = {};

                for (let i = 0; i < blockSchema.queue.length; i++) {
                    let schema = blockSchema.queue[i],
                        val = getSectionResult(schema, bits);

                    if (schema.key) {
                        res[schema.key] = val;
                    }
                    reduceQueue(blockSchema.queue, schema, val, res);
                }
                return reduceResult(res);
            };
        const segs = getSegments(tcs),
            queue = getQueue(segs);

        for (let i = 0; i < queue.length; i++) {
            let blkval = getBlockResult(queue[i], segs[i]);
            if (queue[i].key) {
                iabres[queue[i].key] = blkval;
            }
            offset = 0;
        }
        return iabres;
    }

    /**
     * Get consent object via IAB TCF cookie
     *
     * @param {string} tcookie - Name of cookie containing TC String
     * @param {string} acookie - Name of cookie containing Additional Consent String
     * @returns {object} - A consent object
     */
    function getTcfConsent(tcookie, acookie) {
        let res = null;

        if (typeof tcookie === 'string' && tcookie.length !== 0) {
            /* Check for external consent in a webview... */
            let src,
                src2;

            if (config.useExternalConsent === true && win.OTExternalConsent.tcString) {
                /* Get the consent string(s) from the external consent settings */
                tcString = win.OTExternalConsent.tcString;
                src = 'external consent';
                src2 = src;
                acString = (win.OTExternalConsent.addtlConsent) ? win.OTExternalConsent.addtlConsent : '';
            } else {
                /* Get the consent string(s) from the cookie */
                tcString = getCookie(tcookie);
                src = '"' + tcookie + '" cookie';
                src2 = '"' + acookie + '" cookie';
                acString = (typeof acookie === 'string' && acookie.length !== 0) ? getCookie(acookie) : '';
                if (acString === null) {
                    acString = '';
                }
            }
            if (tcString && config.useIabString) {
                const tco = processTCString(tcString);
                if (tco && tco.core && typeof config.tcfOpts.policies[tco.core.tcfPolicyVersion] === 'object' && tco.core.purposeConsents) {
                    let i,
                        j,
                        k,
                        ka,
                        v,
                        tcfOpt = config.tcfOpts.policies[tco.core.tcfPolicyVersion],
                        tcpc = tco.core.purposeConsents,
                        tcpli = tco.core.purposeLegitimateInterests || {},
                        tcsfo = tco.core.specialFeatureOptins || {};

                    if (dbg) {
                        uclog('debug', 'Processed TC string (policy ' + tco.core.tcfPolicyVersion + ') from ' + src + ': ', tco);
                    }
                    tcStringVer = tco.core.tcfPolicyVersion;
                    res = copyConsent(config.consentImpliedDefaults);
                    /* Process the purposes and special features.
                     * Set in this precedence:
                     *   1. Consent, if available
                     *   2. Legitimate interest, if available
                     *   3. The implied default from the config, if set
                     *   4. false
                     */
                    /* Process the Purposes (p#) */
                    for (i = 1; i <= config.tcfOpts.categories.purposes.length; i++) {
                        k = config.tcfOpts.categories.purposes[i - 1];
                        if (k && k !== 'unused') {
                            ka = Array.isArray(k) ? k : [k];
                            for (j = 0; j < ka.length; j++) {
                                k = ka[j];
                                if (k && k !== 'unused' && k !== 'required') {
                                    if (i <= tcfOpt.iabMaxPurposes) {
                                        v = (typeof tcpc[i] === 'boolean') ? tcpc[i] :
                                            (typeof tcpli[i] === 'boolean' ? tcpli[i] :
                                                (typeof config.consentImpliedDefaults[k] === 'boolean' ? config.consentImpliedDefaults[k] : false));
                                    } else {
                                        v = (typeof config.consentImpliedDefaults[k] === 'boolean' ? config.consentImpliedDefaults[k] : false);
                                    }
                                    res[k] = typeof res[k] === 'boolean' ? res[k] && v : v;
                                }
                            }
                        }
                    }
                    /* Process the Special Features (sf#) */
                    for (i = 1; i <= config.tcfOpts.categories.specialFeatures.length; i++) {
                        k = config.tcfOpts.categories.specialFeatures[i - 1];
                        if (k && k !== 'unused') {
                            ka = Array.isArray(k) ? k : [k];
                            for (j = 0; j < ka.length; j++) {
                                k = ka[j];
                                if (k && k !== 'unused' && k !== 'required') {
                                    if (i <= tcfOpt.iabMaxSpecialFeats) {
                                        v = (typeof tcsfo[i] === 'boolean') ? tcsfo[i] :
                                            (typeof config.consentImpliedDefaults[k] === 'boolean' ? config.consentImpliedDefaults[k] : false);
                                    } else {
                                        v = (typeof config.consentImpliedDefaults[k] === 'boolean' ? config.consentImpliedDefaults[k] : false);
                                    }
                                    res[k] = typeof res[k] === 'boolean' ? res[k] && v : v;
                                }
                            }
                        }
                    }
                    if (acString.length !== 0 && acString.search(/^\d+~[\d\.]*$/) === -1) {
                        /* Badly formatted AC String */
                        uclog('error', 'Error: Invalid AC string in ' + src2 + '.');
                        acString = '';
                    }
                } else {
                    uclog('error', 'Error: Invalid TC string in ' + src + '.');
                    tcString = '';
                    acString = '';
                }
            } else if (!config.useIabString) {
                if (dbg) {
                    uclog('debug', 'Bypassed parsing TC string.');
                }
            } else {
                tcString = '';
                acString = '';
            }
        }
        return res;
    }

