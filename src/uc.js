import { CmpApi } from './gpp/CmpApi.js';

window.WBD = window.WBD || {};
window.WM = window.WM || {};

/**
 * Polyfill for CustomEvent constructor for old browsers
 */
(function (win, doc) {
    'use strict';

    if (typeof win.CustomEvent !== 'function') {
        var CustomEvent = function _CustomEvent(event, params) {
                var evt;

                params = params || { bubbles: false, cancelable: false, detail: undefined };
                evt = doc.createEvent('CustomEvent');
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                return evt;
            };

        CustomEvent.prototype = win.Event.prototype;
        win.CustomEvent = CustomEvent;
        if (win.Event !== 'function') win.Event = CustomEvent;
    }
})(window, document);


/**
 * User Consent checking and setup of global object
 *
 * @name WBD.UserConsent
 * @namespace
 * @memberOf WBD
 */
window.WBD.UserConsent = window.WBD.UserConsent || (function UserConsent(win, doc) {
    'use strict';

    var acString = '',
        frameCallId = 0,
        categories,
        cmpCallbacks = {},
        config = {},
        consentConfirmed = false,
        consentHistory = [],
        consentId = '',
        consentInteractions = 0,
        consentSource = '',
        consentState = null,
        consentTime = null,
        consentVersion = 'unknown',
        controls = {},
        dbg = false,
        eventCallbacks = {},
        frame = '',
        geoCountry = '',
        geoState = '',
        geoCS = '',
        gppApisAvail = {
            tcfeuv2: 2,
            tcfcav1: 5,
            uspv1: 6,
            usnat: 7
        },
        gppCmpApi = null,
        gppCmpId = 1,
        gppString = '',
        gppStringVer = '',
        gppStub = null,
        oneTrustLocationSet = false,
        optanonLoaded = false,
        otBlocked,
        otFailed,
        pageLang = 'en',
        parsableTypes = {
            'binary': true,
            'boolean': true,
            'trinary': true,
            'integer': true
        },
        tcString = '',
        tcStringVer = '',
        ucChild = false,
        userConsentVersion = '__ucVersion__',
        usingCmpFrame = null,
        usingCompatMode = false,
        usingGpc = false,
        usingGppFrame = null,
        usingIabCcpa = false,
        usingIabGdpr = false,
        usingIabGpp = false,
        usingOTGpp = false,
        usingUspFrame = null,
        uspString = '',
        defaults = __ucConfig__;


    /**
     * Log to the console for debugging and errors
     *
     * @param {string} level - The log level ('error', 'info', 'debug')
     * @param {...mixed} args - Arguments to output
     */
    function uclog(level /*, args */) {
        const args = Array.prototype.slice.call(arguments);
        args[0] = ('[WMUC]' + (frame.length === 0 ? '' : ' (' + frame + ')') + ':');
        if (level === 'error') {
            console.error.apply(console, args);
        } else {
            console.log.apply(console, args);
        }
    }

    /**
     * Get Consent History (debug only)
     *
     * @returns {array} Array of consent history objects
     */
    function getConsentHistory() {
        return consentHistory;
    }

    /**
     * Returns the value of the given cookie name
     *
     * @param {string} name - the cookie name
     * @returns {mixed} value of the cookie
     */
    function getCookie(name) {
        const matches = doc.cookie.match(new RegExp('(^|;) *' + name + ' *= *([^;]+)'));
        return matches ? matches.pop() : null;
    }

    /**
     * Sets a new cookie with the given values.
     *
     * @param {string} name - the cookie name
     * @param {string} value - the cookie value
     * @param {object} [opts] - the cookie option strings (expires, maxage, domain, path, secure, samesite)
     */
    function setCookie(name, value, opts) {
        if (!name) return;  /* Nothing to do */
        opts = opts || {};
        doc.cookie = name + '=' + (typeof value === 'string' ? value : '') +
            '; Domain=' + (opts.domain || config.cookieDomain) +
            '; Path=' + (opts.path || '/') +
            (opts.maxage ? '; Max-Age=' + opts.maxage : (opts.expires ? '; Expires=' + opts.expires : '')) +
            (opts.secure ? '; Secure' : '') +
            (opts.samesite ? '; SameSite=' + opts.samesite : '');
    }

    __ucTcf__

    /**
     * Returns geoCountry passed in config
     * - if this is unavailable, check the controls cookie for a geolocation parameter
     * - if that is not available, use OneTrust Geolocation service to set the country code cookie.
     *
     * @returns {string} - The current value of geoCountry or an empty string if not yet initialized
     */
    function getGeoCountry() {
        var countryRegEx = new RegExp('^[A-z]{2}($|:[A-z]{2}$)')
        if (geoCountry && countryRegEx.test(geoCountry)) {
            return geoCountry.toUpperCase();
        } else if (processControls().countryCode && countryRegEx.test(processControls().countryCode)) {
            return processControls().countryCode.toUpperCase();
        } else {
            uclog(
                'error',
                'User-Consent unable to determine country, missing or invalid cookies. Dispatching to OneTrust.'
            );
            win.otGeoCheck = function (obj) {
                setCookie('countryCode', obj.country, {
                    domain: config.cookieDomain,
                    maxage: '31536000',
                    path: '/',
                    samesite: config.cookieSameSite,
                    secure: config.cookieSecure
                });
                if (getCookie('countryCode') !== null) {
                    setTimeout(doReload, 100);
                }
            };
            var ns = doc.createElement('script');
            ns.src = 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/otGeoCheck';
            doc.head.appendChild(ns);
            return geoCountry;
        }
    }

    /**
     * Returns geoState
     *
     * @returns {string} - The current value of geoState or an empty string if not yet initialized or not set
     */
    function getGeoState() {
        return geoState;
    }

    /**
     * Checks if the geoCountry is in the specified region
     *
     * @param {Array} codes - An array of uppercase country codes, country code:state codes that
     *                        represent the region, and/or lowercase region names ("ccpa").
     *                        Prefix an entry with "!" to exclude that value.
     * @returns {boolean} - true if in the specified region
     */
    function checkInRegion(codes) {
        let r = false;

        for (let i = 0; i < codes.length; i++) {
            if (codes[i]) {
                if (geoCountry === codes[i] || geoCS === codes[i] || codes[i] === '*' || codes[i] === config.regId) {
                    r = true;
                } else if(codes[i].charAt(0) === '!') {
                    let n = codes[i].substring(1);
                    if (geoCountry === n || geoCS === n || config.regId === n) {
                        r = false;
                        break;
                    }
                }
            }
        }
        return r;
    }

    /**
     * Returns the expiration date/time given the number of years and an optional start time
     *
     * @param {number} years - Number of years before expiration
     * @param {Date} [start] - Start date/time (optional)
     * @returns {Date} - Expiration date/time
     */
    function getExpiration(years, start) {
        let e = start ? new Date(start) : new Date();

        e.setUTCFullYear(e.getUTCFullYear() + years);
        return e;
    }

    /**
     * Returns a localized string given a string/object
     *
     * @param {Object|string} obj - Either a source string or object of localizaed strings
     * @returns {string} - Localized string
     */
    function getLocalizedString(obj) {
        let str;

        if (typeof obj === 'object' && obj !== null) {
            str = obj[pageLang] || obj[config.defaultLanguage] || '';
        } else {
            str = (typeof obj === 'string') ? obj : '';
        }
        return str;
    }

    /**
     * Returns a deep copy of the of the passed-in object
     *
     * @param {Object} obj - The object to copy
     * @returns {Object} - The resulting object copy
     */
    function copyObject(obj) {
        let c;

        if (typeof obj !== 'object' || obj === null) {
            return obj;
        }
        c = Array.isArray(obj) ? [] : {};
        for (let p in obj) {
            c[p] = copyObject(obj[p]);
        }
        return c;
    }

    /**
     * Returns a shallow copy of the passed-in consent object
     *
     * @param {Object} cObj - A consent object
     * @returns {Object} - A shallow copy of the consent object
     */
    function copyConsent(cObj) {
        let nObj = {};

        if (cObj) {
            for (let c = 0; c < categories.length; c++) {
                nObj[categories[c]] = cObj[categories[c]];
            }
        } else {
            uclog('error', 'Critical Error: Attempt to read or copy consent before UserConsent is initialized!');
        }
        return nObj;
    }

    /**
     * Returns a shallow merge of the passed-in consent object to the target consent obj
     *
     * @param {Object} tObj - The target consent object
     * @param {Object} cObj - The consent object to merge into the target
     * @returns {Object} - The resulting target consent object
     */
    function mergeConsent(tObj, cObj) {
        if (cObj) {
            tObj = tObj || {};
            for (let c = 0; c < categories.length; c++) {
                if (typeof cObj[categories[c]] === 'boolean') {
                    tObj[categories[c]] = cObj[categories[c]];
                }
            }
        }
        return tObj;
    }

    /**
     * Push the region information to OneTrust
     */
    function pushGeoLocation() {
        if (!oneTrustLocationSet) {
            if (win.OneTrust && typeof win.OneTrust.getGeolocationData === 'function') {
                let otloc = win.OneTrust.getGeolocationData();
                if (otloc && (otloc.country !== geoCountry || otloc.state !== geoState)) {
                    if (typeof win.OneTrust.setGeoLocation === 'function') {
                        win.OneTrust.setGeoLocation(geoCountry, geoState);
                        oneTrustLocationSet = true;
                    } else if (win.OneTrustStub && typeof win.OneTrustStub.setGeoLocation === 'function') {
                        win.OneTrustStub.setGeoLocation(geoCountry, geoState);
                        oneTrustLocationSet = true;
                    }
                } else {
                    oneTrustLocationSet = true;
                }
            } else if (win.OneTrustStub && typeof win.OneTrustStub.setGeoLocation === 'function') {
                win.OneTrustStub.setGeoLocation(geoCountry, geoState);
                oneTrustLocationSet = true;
            }
            if (dbg) {
                if (oneTrustLocationSet) {
                    uclog('debug', 'Set OneTrust geo-location.');
                } else {
                    uclog('debug', 'Not yet able to set OneTrust geo-location.');
                }
            }
        }
    }

    /**
     * Read the GPP String from the cookie(s)
     *
     * @param {string} gcookie - The base cookie name to read.
     * @returns {string} - The GPP String
     */
    function readGppCookie(gcookie) {
        let gstr;

        if ((gstr = getCookie(gcookie)) === null) {
            /* Large GPP strings are chunked by OT into multiple cookies... */
            gstr = '';
            for (let c = 1, cstr = null; cstr !== '' && c < 10; c++, gstr += cstr) {
                cstr = getCookie(gcookie + c.toString(10)) || '';
            }
        }
        return gstr;
    }

    /**
     * Reads a GPP field and assigns it (dobj.type must be binary, trinary, boolean, or an array of such!)
     *
     * @param {string} section - IAB section name
     * @param {Object} dobj - Field descriptor object
     * @param {Object} state - State to update with field value(s)
     */
    function parseGppField(section, dobj, state) {
        let gval = gppCmpApi.getFieldValue(section, dobj.field),
            rt = dobj.type.toLowerCase(),
            assignToCats = (val, cats, type) => {
                const carr = (typeof cats === 'string') ? [cats] : cats,
                    v = (type === 'boolean' ? val : (type === 'trinary' ? (val === 2) : (val !== 0)));

                for (let c of carr) {
                    if (config.consentNotApplicable.length === 0 || config.consentNotApplicable.indexOf(c) < 0) {
                        if (categories.indexOf(c) >= 0 && config.consentNotApplicable.indexOf(c) < 0) {
                            state[c] = v;
                        } else {
                            uclog('error', 'Invalid consent "' + c + '" specified in GPP Categories!');
                        }
                    }
                }
            };

        if (rt.startsWith('array')) {
            rt = rt.substring(6);
            if (parsableTypes[rt] && dobj.maxCount && Array.isArray(gval)) {
                for (let i = 0; i < dobj.maxCount; i++) {
                    if (dobj[i]) {
                        assignToCats(gval[i], dobj[i], rt);
                    }
                }
            } else {
                uclog('error', 'Error: Unparsable data type "' + rt + '" or missing maxCount in GPP Categories "' + dobj.field + '" value!');
            }
        } else if (dobj.val) {
            if (parsableTypes[rt]) {
                assignToCats(gval, (typeof dobj.val !== 'undefined' ? dobj.val : dobj.default), rt);
            } else {
                uclog('error', 'Error: Unparsable data type "' + rt + '" in GPP Categories!');
            }
        }
    }

    /**
     * Sets a GPP field and from the category data (dobj.type must be binary, trinary, boolean, or an array of such!)
     *
     * @param {string} section - IAB section name
     * @param {Object} dobj - Field descriptor object
     * @param {Object} state - State to update field value(s)
     */
    function writeGppField(section, dobj, state) {
        let gval = gppCmpApi.getFieldValue(section, dobj.field),
            rt = dobj.type.toLowerCase(),
            assignFromCats = (cats, type) => {
                const carr = (typeof cats === 'string') ? [cats] : cats;
                let cnt = 0,
                    v = true;

                for (let c of carr) {
                    if (categories.indexOf(c) >= 0) {
                        if (typeof state[c] !== 'undefined') {
                            v = v && state[c];
                            cnt++;
                        }
                    } else {
                        uclog('error', 'Invalid consent "' + c + '" specified in GPP Categories!');
                    }
                }
                if (cnt > 0) {
                    return (type === 'boolean' ? v : (type === 'trinary' ? (v ? 2 : 1) : (v ? 1 : 0)));
                }
                return (type === 'boolean' ? false : 0);
            };

        try {
            if (rt.startsWith('array')) {
                rt = rt.substring(6);
                if (!parsableTypes[rt]) {
                    throw 'unparse';
                }
                if (!dobj.maxCount || !Array.isArray(gval)) {
                    throw 'badarray';
                }
                for (let i = 0; i < dobj.maxCount; i++) {
                    if (dobj[i]) {
                        gval[i] = assignFromCats(dobj[i], rt);
                    } else if (dobj.default && typeof dobj.default[i] !== 'undefined') {
                        gval[i] = dobj.default[i];
                    }
                }
            } else {
                if (!parsableTypes[rt]) {
                    throw 'unparse';
                }
                if (dobj.val) {
                    gval = assignFromCats(dobj.val, rt);
                } else if (typeof dobj.default !== 'undefined') {
                    gval = dobj.default;
                }
            }
            gppCmpApi.setFieldValue(section, dobj.field, gval);
        } catch (e) {
            if (e === 'unparse') {
                uclog('error', 'Error: Unparsable data type "' + rt + '" in GPP Categories!');
            } else if (e === 'badarray') {
                uclog('error', 'Error: Missing maxCount or bad array in GPP Categories "' + dobj.field + '" value!');
            } else {
                uclog('error', 'Failed to set GPP field "' + dobj.field + '" value!');
            }
        }
    }

    /**
     * Returns a consent object derived from a GPP string
     *
     * @param {string} gstring - The GPP string passed-in with the change, or an empty string if not known.
     * @returns {Object} - Consent state object
     */
    function getGppConsent(gstring) {
        const sect = config.gppSection;
        let gstr = (typeof gstring === 'string' ? gstring : ''),
            src = 'CMP',
            state = null;

        if (gstr.length === 0) {
            /* Check for external consent in a webview... */
            if (config.useExternalConsent) {
                /* Get the consent string(s) from the external consent settings */
                gstr = win.OTExternalConsent.gppString || '';
                src = 'external consent';
            } else {
                /* Get the consent string(s) from the cookie */
                gstr = readGppCookie(config.gppIabCookie) || '';
                src = 'cookie';
            }
        }

        if (gstr === '') {
            /* We have no GPP string */
            if (dbg) {
                uclog('debug', 'No GPP string present.');
            }
            return null;
        }

        /* We have a GPP string, so process it for the consent state */
        try {
            gppCmpApi.setGppString(gstr);
        } catch (err) {
            uclog('error', 'GPP string invalid, ignoring.');
            return null;
        }
        if (!gppCmpApi.hasSection(sect) || !Array.isArray(config.gppCategories[sect])) {
            /* We have a GPP string, but it doesn't have the current section/region */
            if (dbg) {
                uclog('debug', 'GPP string present, but for different region/section.');
            }
            return null;
        } else {
            /* We have a GPP string in the current section, set the API to use it */
            gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
        }

        /* Parse or set the GPP string and associate fields to/from categories */
        const gsect = config.gppCategories[sect];
        state = copyConsent(config.consentDefaults);
        for (let c of gsect) {
            if (typeof c === 'object' && c !== null && c.field) {
                /* Field descriptor, use it */
                parseGppField(sect, c, state);
            }
        }

        if (!config.useExternalConsent) {
            /* Check against GPC */
            if (gppCmpApi.getFieldValue(sect, 'GpcSegmentIncluded')) {
                if (!gppCmpApi.getFieldValue(sect, 'Gpc') === usingGpc) {
                    /* Fix GPP string */
                    gppCmpApi.setFieldValue(sect, 'Gpc', usingGpc);
                    gstr = gppCmpApi.getGppString();
                }
            }
            if (usingGpc) {
                state = mergeConsent(state, config.consentGpcDefaults);
            }
        }
        if (gstr !== gppString) {
            gppString = gstr;
        }

        if (dbg) {
            uclog('debug', 'Processed GPP string from ' + src + ': ' + gppString);
        }

        return state;
    }

    /**
     * Sets the internal GPP string from a new GPP consent string or a consent object
     *
     * @param {string} gstring - The GPP string passed-in with the change, or an empty string if not known.
     * @param {Object} state - The new consent state to set to the GPP string.
     * @returns {boolean} - true on success, false on failure
     */
    function setGppConsent(gstring, state) {
        const oldGpp = gppString,
            sect = config.gppSection;

        if (typeof gstring === 'string' && gstring.length > 0) {
            /* Set the new GPP string from a string we probably got from OT */
            try {
                gppCmpApi.setGppString(gstring);
                gppString = gstring;
                if (gppString && gppCmpApi.hasSection(sect)) {
                    /* We have a GPP string in the current section, set the API to use it */
                    gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                }
                if (dbg) {
                    uclog('debug', 'GPP string set from CMP: ', gstring);
                }
            } catch (e) {
                uclog('error', 'Failed to set GPP string: ', e);
                return false;
            }
        } else if (typeof state === 'object' && state !== null && Array.isArray(config.gppCategories[config.gppSection])) {
            try {
                /* Set the new GPP string from a consent state */
                if (!gppString) {
                    /* prepping new string from a blank slate */
                    gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                }
                if (Array.isArray(config.gppCategories[sect])) {
                    /* Parse or set the GPP string and associate fields to/from categories */
                    const gsect = config.gppCategories[sect];
                    for (let c of gsect) {
                        if (typeof c === 'object' && c !== null && c.field) {
                            /* Field descriptor, use it */
                            writeGppField(sect, c, state);
                        }
                    }
                }
                if (usingGpc) {
                    /* Fix GPP string */
                    gppCmpApi.setFieldValue(sect, 'Gpc', usingGpc);
                }
                gppString = gppCmpApi.getGppString();
                if (gppString && gppCmpApi.hasSection(sect)) {
                    /* We have a GPP string in the current section, set the API to use it */
                    gppCmpApi.setApplicableSections([gppApisAvail[sect]]);
                    /* If this is an update of the internal GPP, send the section change event */
                    if (usingOTGpp === false && oldGpp.length !== 0 && oldGpp !== gppString) {
                        gppCmpApi.fireSectionChange(sect);
                    }
                }
                if (dbg) {
                    uclog('debug', 'GPP string set from consent state: ', gppString);
                }
            } catch (e) {
                uclog('error', 'Failed to set GPP string: ', e);
                return false;
            }
        } else {
            uclog('error', 'Failed to set GPP string, invalid parameters.');
            return false;
        }

        return true;
    }

    /**
     * Compare consent states to determine if consent has been reduced in any way
     *
     * @param {string} oldConsent - Old consent object
     * @param {string} newConsent - New consent object
     * @returns {object} - A consent object
     */
    function hasConsentReduced(oldConsent, newConsent) {
        if (oldConsent && newConsent) {
            for (let c = 0; c < categories.length; c++) {
                if (oldConsent[categories[c]] && !newConsent[categories[c]]) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Processes the OneTrust consent cookie for non-consent data.
     */
    function processOneTrustCookie() {
        let cookieValue = getCookie(config.consentCookie);

        /* If no cookie is set and has a valid groups param then we have something to do */
        if (cookieValue && cookieValue.indexOf('&groups=') >= 0) {
            const fields = cookieValue.split('&');

            for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
                let encodedParameterGroup = fields[fieldIndex];
                if (encodedParameterGroup) {
                    /* fields are encoded paramName=paramVal */
                    let values = encodedParameterGroup.split('='),
                        paramName = values[0],
                        paramValue = values[1];

                    if (paramName === 'version' && paramValue) {
                        consentVersion = paramValue;
                    } else if (paramName === 'consentId' && paramValue) {
                        consentId = paramValue;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Processes the consent state from the OneTrust cookie only.
     *
     * @returns {Object} - An object representing the current consent state of the OT cookie, or null if none.
     */
    function processConsentCookie() {
        let cookieValue = getCookie(config.consentCookie),
            state = null;

        /* If using external consent, override cookie value... */
        if (config.useExternalConsent) {
            cookieValue = 'groups=' + encodeURIComponent(win.OTExternalConsent.groups);
            usingGpc = false;
        }

        /* If no cookie is set and has a valid groups param then we have something to do */
        if (cookieValue && cookieValue.indexOf('&groups=') >= 0) {
            const fields = cookieValue.split('&');

            for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
                let encodedParameterGroup = fields[fieldIndex];
                if (encodedParameterGroup) {
                    /* fields are encoded paramName=paramVal */
                    let values = encodedParameterGroup.split('='),
                        paramName = values[0],
                        paramValue = values[1];

                    /* the 'groups' parameter contains consent data */
                    if (paramName === 'groups' && paramValue) {
                        /* group value is uri encoded */
                        let groups = decodeURIComponent(paramValue).split(','),
                            comCodes = [],
                            comValues = [],
                            count = 0,
                            useTransition = false;

                        state = mergeConsent(copyConsent(config.consentDefaults), config.consentImpliedDefaults);
                        for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
                            let group = groups[groupIndex].split(':'),
                                consentCode = group[0].toLowerCase(),
                                consentValue = (group[1] === '1');

                            if (consentCode) {
                                if (config.categories[consentCode]) { /* && config.consentNotApplicable.indexOf(config.categories[consentCode]) === -1) { */
                                    /* We have a consent value, record it */
                                    state[config.categories[consentCode]] = consentValue;
                                    if (config.categories[consentCode] !== 'required') {
                                        count++;
                                    }
                                } else {
                                    if (config.compatTransition && config.compatTransition.old === consentCode && config.compatTransition.cond === consentValue) {
                                        /* We are in a transitional mixed state */
                                        useTransition = true;
                                    }
                                    if (config.compatCodes[consentCode]) {
                                        /* We have a compatiblity value, store it for maybe later */
                                        comCodes.push(consentCode);
                                        comValues.push(consentValue);
                                    }
                                }
                            }
                        }
                        if (count === 0 && comCodes.length > 0) {
                            /* No current codes in cookie, we are in compatibility mode.
                             * Process the compatibility code lists.
                             */
                            usingCompatMode = true;
                            for (let ccIndex = 0; ccIndex < comCodes.length; ccIndex++) {
                                let comVals = config.compatCodes[comCodes[ccIndex]];
                                if (comVals && !Array.isArray(comVals)) {
                                    comVals = [comVals];
                                }
                                for (let comVal of comVals) {
                                    let comCode = config.categories[comVal];
                                    if (comCode) {
                                        /* Compatibility matches valid consentState so set it */
                                        state[comCode] = comValues[ccIndex];
                                        count++;
                                    }
                                }
                            }
                        } else if (useTransition && config.compatTransition.new) {
                            /* Make sure the new state corresponds to the compatTransition state's condition */
                            if (Array.isArray(config.compatTransition.new)) {
                                for (let comIndex = 0; comIndex < config.compatTransition.new.length; comIndex++) {
                                    state[config.categories[config.compatTransition.new[comIndex]]] = config.compatTransition.cond;
                                    count++;
                                }
                            } else {
                                state[config.categories[config.compatTransition.new]] = config.compatTransition.cond;
                                count++;
                            }
                        }
                        if (count === 0) {
                            state = null;
                        }
                    }
                }
            }
        }

        return state;
    }

    /**
     * Processes the current consent state from the cookie(s), GPP string, and/or the TC string
     *
     * @param {string} [gstr] - An optional new GPP string received from an event notification.
     * @returns {Object} - An object representing the current consent state of the cookie
     */
    function processConsentState(gstr) {
        let state;

        /* If not using external consent and no cookie is set or cookie is invalid then we have no consent */
        if (!config.useExternalConsent && !processOneTrustCookie()) {
            consentConfirmed = false;
            consentTime = null;
        }

        /* Only process if consent is confirmed and not fixed */
        if (!consentConfirmed || config.useFixedConsent) {
            state = copyConsent(config.consentDefaults);
            consentSource = 'defaults';
        } else if (config.useExternalConsent !== true && usingIabGpp && config.gppIabCookie && (state = getGppConsent(gstr))) {
            consentSource = (gstr ? 'gpp string' : config.gppIabCookie + ' cookie');
        } else if (config.useExternalConsent !== true && usingIabGdpr && config.gdprIabCookie && (state = getTcfConsent(config.gdprIabCookie, config.addtlConsentCookie))) {
            consentSource = config.gdprIabCookie + ' cookie';
        } else if (state = processConsentCookie()) {
            consentSource = config.useExternalConsent ? 'external consent' : config.consentCookie + ' cookie';
        } else {
            /* No state set by anything, so use the default. */
            state = copyConsent(config.consentDefaults);
            consentSource = 'defaults';
        }

        if (!config.useExternalConsent) {
            /* Handle special consent cases */
            if (usingGpc) {
                /* If we're using GPC, force relevant states */
                state = mergeConsent(state, config.consentGpcDefaults);
            }
        }

        return state;
    }

    /**
     * Correct badly implemented GPC signal extensions (PrivacyBadger, DuckDuckGo, etc.)
     *
     * We really shouldn't have to do this, but people expect these things to "just work"
     * and when they don't they blame the web site.  Guess what, some of those extensions
     * really do a terrible job at setting a single global boolean value.  So here we are.
     * As a side note, this would be WAY easier if Javascript had access to the request
     * headers that loaded the page.
     *
     * @param {string} cookie - The cookie to use for GPC detection
     */
    function fixGpcWithCookie(cookie) {
        if (typeof win.navigator.globalPrivacyControl === "undefined" && cookie) {
            const val = getCookie(cookie);
            if (val && (val === '1' || val.startsWith('t'))) {
                /* We have GPC cookie, so set GPC in Javascript */
                try {
                    Object.defineProperty(win.Navigator.prototype, 'globalPrivacyControl', {
                        get: function globalPrivacyControl() {
                            return true;
                        },
                        configurable: true,
                        enumerable: true
                    });
                } catch(e) {
                    uclog('error', 'GPC signal error in browser.');
                }
            }
        }
    }

    /**
     * Returns true if user has confirmed consent state, false if using a default state
     *
     * @returns {boolean} - Current user consent confirmation state
     */
    function getConsentConfirmed() {
        return consentConfirmed;
    }

    /**
     * Returns a copy of the current consent state object.
     *
     * @returns {object} - Copy of the current consent object
     */
    function getConsentState() {
        return copyConsent(consentState);
    }

    /**
     * Returns a copy of the current consent state object with specific consents only, no undefined
     *
     * @returns {object} - Copy of the current consent object with undefined values removed
     */
    function getSimpleConsentState() {
        let ncs = {};
        for (let k in consentState) {
            if (typeof consentState[k] === 'boolean') {
                ncs[k] = consentState[k];
            }
        }
        return ncs;
    }

    /**
     * Returns the current consent time as read from the consent cookie.
     *
     * @returns {string} - The current consent time string
     */
    function getConsentTime() {
        return consentTime;
    }

    /**
     * Returns the current consent version as read from the consent cookie.
     *
     * @returns {string} - The current consent version string
     */
    function getConsentVersion() {
        return consentVersion;
    }

    /**
     * Returns the GPP API region being used, if any
     *
     * @returns {string} - Region name or 'none'
     */
    function getGppSection() {
        return (usingIabGpp && config.gppSection !== '') ? config.gppSection : 'none';
    }

    /**
     * Returns the current IAB CMP interface name.
     *
     * @returns {string} - The current IAB interface name or "none".
     */
    function getIABInterface() {
        return usingIabGpp ? '__gpp' : (usingIabCcpa ? '__uspapi' : (usingIabGdpr ? '__tcfapi' : 'none'));
    }

    /**
     * Returns the IAB region being used, if any
     *
     * @returns {string} - Region name or 'none'
     */
    function getIABRegion() {
        return (config.iabRegion !== '' ? config.iabRegion : 'none');
    }

    /**
     * Returns the current IAB CMP interface version.
     *
     * @returns {string} - The current IAB interface version string or "none".
     */
    function getIABVersion() {
        return usingIabGpp ? '1.1' : (usingIabCcpa ? '1.0' : (usingIabGdpr ? '2.2' : 'none'));
    }

    /**
     * Returns the current link action function.
     *
     * @returns {function} - A function to call on "cookie settings" or "do not sell" link clicks
     */
    function getLinkAction() {
        return config.consentLinkAction || (win.OneTrust && win.OneTrust.ToggleInfoDisplay) || function _linkAct() {
            if (win.OneTrust && win.OneTrust.ToggleInfoDisplay) {
                win.Optanon.ToggleInfoDisplay();
            }
        };
    }

    /**
     * Returns the current link action title.
     *
     * @returns {string} - The current link text to use, such as "Manage Cookies+".  Empty if not used in region.
     */
    function getLinkTitle() {
        return config.consentLinkTitle;
    }

    /**
     * Returns the current link action function for the AdChoices, if any is configured.
     *
     * @returns {function} - A function to call on "AdChoices" link clicks or null
     */
    function getAdChoicesLinkAction() {
        if (typeof config.adChoicesLinkAction === 'function') {
            return config.adChoicesLinkAction;
        } else if (typeof config.adChoicesLinkAction === 'string' && config.adChoicesLinkAction.search(/^http/) !== -1) {
            /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
            return function _redirToPC() {
                win.open(config.adChoicesLinkAction, '_blank');
            };
        }
        return null;
    }

    /**
     * Returns the current link action title for the AdChoices, if it exists.
     *
     * @returns {string} - The current link text to use, such as "AdChoices".  Empty if not configured.
     */
    function getAdChoicesLinkTitle() {
        return config.adChoicesLinkTitle;
    }

    /**
     * Returns the current link action function for the Affiliates, if any is configured.
     *
     * @returns {function} - A function to call on "Affiliates" link clicks or null
     */
    function getAffiliatesLinkAction() {
        if (typeof config.affiliatesLinkAction === 'function') {
            return config.affiliatesLinkAction;
        } else if (typeof config.affiliatesLinkAction === 'string' && config.affiliatesLinkAction.search(/^http/) !== -1) {
            /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
            return function _redirToPC() {
                win.open(config.affiliatesLinkAction, '_blank');
            };
        }
        return null;
    }

    /**
     * Returns the current link action title for the Affiliates, if it exists.
     *
     * @returns {string} - The current link text to use, such as "Affiliates".  Empty if not configured.
     */
    function getAffiliatesLinkTitle() {
        return config.affiliatesLinkTitle;
    }

    /**
     * Returns the current link action function for the Privacy Center, if any is configured.
     *
     * @returns {function} - A function to call on "privacy center" link clicks or null
     */
    function getPrivacyCenterLinkAction() {
        if (typeof config.privacyCenterLinkAction === 'function') {
            return config.privacyCenterLinkAction;
        } else if (typeof config.privacyCenterLinkAction === 'string' && config.privacyCenterLinkAction.search(/^http/) !== -1) {
            /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
            return function _redirToPC() {
                win.open(config.privacyCenterLinkAction, '_blank');
            };
        }
        return null;
    }

    /**
     * Returns the current link action title for the Privacy Center, if it exists.
     *
     * @returns {string} - The current link text to use, such as "Privacy Center".  Empty if not configured.
     */
    function getPrivacyCenterLinkTitle() {
        return config.privacyCenterLinkTitle;
    }

    /**
     * Returns the current link action function for the Individual Rights Request Portal, if any is configured.
     *
     * @returns {function} - A function to call on "rights request" link clicks or null
     */
    function getRightsRequestLinkAction() {
        if (typeof config.rightsRequestLinkAction === 'function') {
            return config.rightsRequestLinkAction;
        } else if (typeof config.rightsRequestLinkAction === 'string' && config.rightsRequestLinkAction.search(/^http/) !== -1) {
            /* We have a URL instead of an action function, so return a function that loads it in a new window/tab. */
            return function _redirToPC() {
                win.open(config.rightsRequestLinkAction, '_blank');
            };
        }
        return null;
    }

    /**
     * Returns the current link action title for the Individual Rights Request Portal, if it exists.
     *
     * @returns {string} - The current link text to use, such as "Rights Request".  Empty if not configured.
     */
    function getRightsRequestLinkTitle() {
        return config.rightsRequestLinkTitle;
    }

    /**
     * Returns the current region string
     *
     * @returns {string} - The current region string: ccpa, gdpr, or global.
     */
    function getRegion() {
        return config.regId;
    }

    /**
     * Returns whether or not the page will reload on consent changes
     *
     * @returns {boolean} - true if the page will reload, false if it will not
     */
    function getReloadOnChange() {
        return config.reloadOnConsentChange;
    }

    /**
     * Returns whether or not the page will reload on consent changes
     * resulting in a reduction of consent
     *
     * @returns {boolean} - true if the page will reload, false if it will not
     */
    function getReloadOnConsentReduction() {
        return config.reloadOnConsentReduction;
    }

    /**
     * Returns the current UserConsent version string
     *
     * @returns {string} - The current version of UserConsent
     */
    function getVersion() {
        return userConsentVersion;
    }

    /**
     * Returns a boolean with the child instance state.
     *
     * @returns {boolean} - true if child instance, false otherwise
     */
    function isChild() {
        return ucChild;
    }

    /**
     * Returns a boolean with the top instance state.
     *
     * @returns {boolean} - true if top instance, false otherwise
     */
    function isTop() {
        return !ucChild;
    }

    /**
     * Returns a boolean with the enabled state.  Always true.  DEPRECATED
     *
     * @returns {boolean} - true
     */
    function isEnabled() {
        return true;
    }

    /**
     * Returns a boolean true if GPC is being used for current privacy settings
     *
     * @returns {boolean} - true if GPC is being used, false otherwise
     */
    function isGpcInUse() {
        return usingGpc;
    }

    /**
     * Returns a boolean true if the user has the GPC header set
     *
     * @returns {boolean} - true if GPC is set, false otherwise
     */
    function isGpcSet() {
        return !!navigator.globalPrivacyControl;
    }

    /**
     * Returns a boolean true if currently in the "ccpa" region
     *
     * @returns {boolean} - true if in "ccpa", false otherwise
     */
    function isInCcpaRegion() {
        return checkInRegion(config.ccpaGeos);
    }

    /**
     * Returns a boolean true if currently in the "gdpr" region
     *
     * @returns {boolean} - true if in "gdpr", false otherwise
     */
    function isInGdprRegion() {
        return usingIabGdpr;
    }

    /**
     * Returns a boolean true if currently in a region using GPP
     *
     * @returns {boolean} - true if in a GPP region, false otherwise
     */
    function isInGppRegion() {
        return usingIabGpp;
    }

    /**
     * Returns a boolean true if currently in the specified region
     *
     * @param {string} region - The region to check ("ccpa", "gdpr", or "global").
     * @returns {boolean} - true if in region, false otherwise
     */
    function isInRegion(region) {
        return config.regId === region;
    }

    /**
     * Returns the IAB region being used, if any
     *
     * @param {string} region - The IAB region to check ("gpp", "ccpa", or "gdpr").
     * @returns {boolean} - true if in region, false otherwise
     */
    function isInIabRegion(region) {
        region = (typeof region === 'string') ? region : '';
        return config.iabRegion === region;
    }

    /**
     * Returns a boolean true if OneTrust is being blocked by something like the EasyList - Cookie Notices filter.
     *
     * @returns {mixed} - boolean true if OneTrust is being blocked, false if not, undefined if still testing
     */
    function isOneTrustBlocked() {
        return otBlocked;
    }

    /**
     * Returns a boolean true if OneTrust failed to fully load, which may have happened because
     * things are down or because a browser extension is blocking it.
     *
     * @returns {mixed} - boolean true if OneTrust has not completely loaded, false if things are fine, undefined if still waiting on success/failure
     */
    function isOneTrustFailing() {
        return otFailed;
    }

    /**
     * Returns a boolean true if OneTrust has been loaded.
     *
     * @returns {boolean} - true if OneTrust has been loaded, false if not
     */
    function isOneTrustLoaded() {
        return optanonLoaded;
    }

    /**
     * Returns a boolean true if UserConsent is initialized and ready.
     *
     * @returns {boolean} - true if UserConsent is ready, false if not
     */
    function isReady() {
        return consentState !== null;
    }

    /**
     * Returns a boolean true if UserConsent configuration provides full IAB compliance
     *
     * @returns {boolean} - true if IAB compliant, false if not
     */
    function isSiteIABCompliant() {
        return config.strictIabCompliance;
    }

    /**
     * Returns a boolean true if the requested consent states currently have consent.
     *
     * @param {array} states - An array of strings, where each is a consent state to check.
     * @param {object} [options] - Options, such as "id" which is the idenitifer of the service
     *                             being checked.  (Optional)
     * @returns {boolean} - true if consent is granted, false otherwise
     */
    function inUserConsentState(states, options) {
        const iabReg = 'iab-' + (config.iabRegion || 'N/A'),
            gppSect = 'gpp-' + (config.gppSection || 'N/A');
        let endState = true,
            state = 'not ready';

        options = options || {};
        if (isReady() && states) {
            states = Array.isArray(states) ? states : [states];
            for (let i = 0; i < states.length && endState; i++) {
                state = states[i];
                if (state && state !== 'required') {
                    if (state === 'gpp' || state === 'iab-gpp' || state === gppSect) {
                        if (usingIabGpp && !(options && options.ignoreIAB)) {
                            break;  /* If the "gpp" consent type is added, and usingIabGpp is true, consent is true */
                        }
                    } else if (state === 'iab' || state === iabReg) {
                        if (config.useIAB && config.iabRegion !== '' && !(options && options.ignoreIAB)) {
                            break;  /* If the "iab" consent type is added, and useIAB is true, consent is true */
                        }
                    } else if (typeof consentState[state] === 'boolean' && consentState[state] === false) {
                        endState = false;
                    } else if (config.compatCategories[state]) {
                        /* Check every matching compatCategory value */
                        let ccats = config.compatCategories[state];
                        for (let c = 0; c < ccats.length; c++) {
                            let ccat = ccats[c];
                            if (typeof consentState[ccat] === 'boolean' && consentState[ccat] === false) {
                                endState = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (dbg && !options.internal) {
            options.name = (options.name || options.id || 'unnamed');
            /* Track consent check in history */
            consentHistory.push({
                ts: new Date(),
                act: options.cact || 'CHK',
                desc: options.name,
                res: endState,
                note: (!endState && state) || ''
            });
            if (endState) {
                uclog('debug', 'Check for consent [' + ((states && states.join(',')) || 'empty') + '] ALLOWS "' + options.name + '"' + (options.cact === 'ADD' ? ', script added' : ''));
            } else {
                uclog('debug', 'Check for consent [' + ((states && states.join(',')) || 'empty') + '] REJECTS "' + options.name + '"' + (options.cact === 'ADD' ? ', script NOT added' : ''));
            }
        }
        return endState;
    }

    /**
     * Returns the current IAB GPP string.
     *
     * @returns {string} - Current GPP consent string or "" if not set or not in a GPP region
     */
    function getGppAPIstring() {
        return gppString;
    }

    /**
     * Returns the current GDPR AC string.
     *
     * @returns {string} - Current TCFAPI additional consent string or "" if not set or not in GDPR region
     */
    function getTcfAPIaddtlString() {
        return acString;
    }

    /**
     * Returns the current GDPR TC string.
     *
     * @returns {string} - Current TCFAPI consent string or "" if not in GDPR region
     */
    function getTcfAPIstring() {
        return tcString;
    }

    /**
     * Returns the current CCPA USPAPI consent string.
     *
     * @returns {string} - Current USPAPI consent string or "1---" if not in CCPA region
     */
    function getUspAPIstring() {
        return uspString;
    }

    /**
     * Internal function to set the USP string based on current consent.
     *
     * @returns {string} - Current USPAPI consent string or "1---" if not in CCPA region
     */
    function setUspAPIstring() {
        let uspStr;

        if (usingIabCcpa) {
            uspStr = '1' + (config.uspApiExplicitNotice ? 'Y' : 'N') + (inUserConsentState(['vendor'], {internal: true}) ? 'N' : 'Y') + (config.uspApiIsLspa ? 'Y' : 'N');
        } else {
            uspStr = '1---';
        }
        if (uspStr !== uspString) {
            uspString = uspStr;
            if (isTop()) {
                if (usingUspFrame === null) {
                    /* Set the UspApi session cookie */
                    setCookie(config.uspApiCookieName, uspStr, {
                        domain: config.cookieDomain,
                        path: '/',
                        samesite: config.cookieSameSite,
                        secure: config.cookieSecure
                    });
                }
                if (dbg) {
                    uclog('debug', 'USP string updated: ', uspStr);
                }
            }
        }
        return uspString;
    }

    /**
     * Returns whether or not the cookie put UserConsent into compatibility mode.
     *
     * @returns {boolean} - true if in compatibility mode, false otherwise
     */
    function usingCompatConsent() {
        return usingCompatMode;
    }

    /**
     * Returns whether or not we are using external consent, such as in a WebView
     *
     * @returns {boolean} - true if using external consent, false if not
     */
    function usingExternalConsent() {
        return config.useExternalConsent;
    }

    /**
     * Returns whether or not IAB GPP is available in this region
     *
     * @returns {boolean} - true if GPP is available, false if not
     */
    function usingGPP() {
        return usingIabGpp;
    }

    /**
     * Returns whether or not IAB CMP is available in this region
     *
     * @returns {boolean} - true if IAB is available, false if not
     */
    function usingIAB() {
        return (config.useIAB && (usingIabGpp || usingIabCcpa || usingIabGdpr));
    }

    /**
     * Returns whether or not PSM was detected
     * DEPRECATED - Always returns false
     *
     * @returns {boolean} - true if PSM was detected, false if not
     */
    function usingPSM() {
        return false;
    }

    /* Not long for this world... VERY DEPRECATED */
    function getUserConsentAdvertisingState() {
        return (typeof config.ucFlavor !== 'iab') ? inUserConsentState(['vendor', 'targeting']) : inUserConsentState(['data-share', 'data-sell', 'ads-contextual', 'ads-person-prof', 'ads-person']);
    }

    /**
     * Internal function to detect if something is blocking OneTrust.
     */
    function testOTBlocking() {
        let testElem = function (i, c, e) {
            let chkDiv = doc.createElement(e || 'div'),
                chkStl,
                res;

            if (i) {
                chkDiv.id = i;
            }
            if (c) {
                chkDiv.className = c;
            }
            chkDiv.style.width = '1px';
            chkDiv.style.display = 'block';
            chkDiv = doc.body.appendChild(chkDiv);
            chkStl = win.getComputedStyle(chkDiv);
            res = (chkStl.display === 'none');
            chkDiv.remove();
            return res;
        };

        if (doc.body) {
            otBlocked = (testElem('onetrust-consent-sdk', 'ot-cookie-consent') ||
                testElem('ot-lst-cnt', 'ot-sdk-show-settings') ||
                testElem('onetrust-pc-sdk', 'otPcCenter ot-fade-in') ||
                testElem('ot-pc-header', 'onetrust-pc-dark-filter') ||
                testElem('ot-pc-content', 'ot-pc-scrollbar') ||
                testElem('ot-sdk-btn', 'ot_cookie_settings_btn') || false);

            if (otBlocked) {
                // Browser blocked
                if (dbg) {
                    uclog('debug', 'OneTrust being blocked by filter.');
                }
                doc.dispatchEvent(new CustomEvent('oneTrustBlocked', {
                    bubbles: false,
                    cancelable: false,
                    detail: {
                        region: config.regId,
                        time: new Date(),
                        consentConfirmed: consentConfirmed,
                        otId: consentId,
                        otVers: consentVersion
                    }
                }));
            }
        } else {
            // No body yet, defer a bit
            setTimeout(testOTBlocking.bind(win), 5);
        }
    }

    /**
     * Adds a script element to the DOM if we have consent for it.
     *
     * @param {object} elem - A DOM ScriptElement to be conditionally added.
     * @param {array} consent - An array of strings, where each is a consent state to check.
     * @param {object} [parent] - A DOM ScriptElement to use as the script parent.
     * @param {array} [regions] - An array of strings, where each is either the name of a region
     *                            ("gdpr", "ccpa"), a 2-letter country code ("GB", "DE"), a
     *                            country:state code ("US:CA"), or an "*" for everywhere.  If
     *                            prefixed with "!", that entry will be excluded.  Entries are
     *                            evaluaed in order.  Default is ["*"].
     * @returns {boolean} - true if script was added, false if not.
     */
    function addScriptElement(elem, consent, parent, regions) {
        if (elem) {
            const opts = {
                    cact: 'ADD',
                    name: (elem.name || elem.src || elem.id || 'unnamed inline')
                },
                p = parent || doc.head,
                r = regions || ['*'];

            if (!checkInRegion(r)) {
                if (dbg) {
                    /* Track region check in history */
                    consentHistory.push({
                        ts: new Date(),
                        act: 'ADD',
                        desc: opts.name,
                        res: false,
                        note: 'Not in script region'
                    });
                    uclog('debug', 'Check for region [' + (r.join(',') || 'empty') + '] REJECTS "' + opts.name + '"' + ', script NOT added');
                }
                return false;
            }
            if (inUserConsentState(consent, opts)) {
                p.appendChild(elem);
                return true;
            }
        } else {
            uclog('error', 'Invalid or missing options to addScriptElement.');
        }
        return false;
    }

    /**
     * Adds a script to the DOM if we have consent for it.
     *
     * @param {object} options - The script options, same as if passed to createElement("script").
     * @param {array} consent - An array of strings, where each is a consent state to check.
     * @param {object} [parent] - A DOM ScriptElement to use as the script parent.
     * @param {array} [regions] - An array of strings, where each is either the name of a region
     *                            ("gdpr", "ccpa"), a 2-letter country code ("GB", "DE"), a
     *                            country:state code ("US:CA"), or an "*" for everywhere.  If
     *                            prefixed with "!", that entry will be excluded.  Entries are
     *                            evaluaed in order.  Default is ["*"].
     * @returns {boolean} - true if script was added, false if not.
     */
    function addScript(options, consent, parent, regions) {
        if (options && (options.src || options.text)) {
            const opts = {
                    cact: 'ADD',
                    name: (options.name || options.src || options.id || 'unnamed inline')
                },
                p = parent || doc.head,
                r = regions || ['*'];

            if (!checkInRegion(r)) {
                if (dbg) {
                    /* Track region check in history */
                    consentHistory.push({
                        ts: new Date(),
                        act: 'ADD',
                        desc: opts.name,
                        res: false,
                        note: 'Not in script region'
                    });
                    uclog('debug', 'Check for region [' + (r.join(',') || 'empty') + '] REJECTS "' + opts.name + '"' + ', script NOT added');
                }
                return false;
            }
            if (inUserConsentState(consent, opts)) {
                const ns = doc.createElement('script'),
                    ops = Object.keys(options);

                for (let o = 0; o < ops.length; o++) {
                    ns[ops[o]] = options[ops[o]];
                }
                p.appendChild(ns);
                return true;
            }
        } else {
            uclog('error', 'Invalid or missing options to addScript.');
        }
        return false;
    }

    /**
     * Internal function to reload the current page.
     */
    function doReload() {
        win.location.reload();
    }

    /**
     * Internal function to add a frame for messaging
     *
     * @params {string} locator - locator string to use to name new frame
     * @returns {boolean} - true if successful, false if not
     */
    function addFrame(locator) {
        if (!win.frames[locator]) {
            if (doc.body) {
                const iframe = doc.createElement('iframe');
                iframe.style.cssText = 'display:none';
                iframe.name = locator;
                doc.body.appendChild(iframe);
            } else {
                /* In the case where this stub is located in the head,
                 * this allows us to inject the iframe more quickly than
                 * relying on DOMContentLoaded or other events.
                 */
                setTimeout(addFrame.bind(win, locator), 5);
            }
            return true;
        }
        return false;
    }

    /**
     * Internal function to find a frame, if it exists
     *
     * @param {string} locator - Locator string to use to find frame
     * @returns {object} - Child frame object if found, else null
     */
    function findFrame(locator) {
        let cf = null;

        for (let w = win; w; w = w.parent) {
            try {
                if (w.frames && w.frames[locator]) {
                    cf = w;
                    break;
                }
            } catch (e) {}
            if (w === win.top) {
                break;
            }
        }
        return cf;
    }

    /**
     * Internal function to set a messaging handler
     *
     * @param {function} handler - Function to use as messaging handler
     */
    function setHandler(handler) {
        if (win.addEventListener) {
            win.addEventListener('message', handler, false);
        } else {
            win.attachEvent('onmessage', handler);
        }
    }

    /**
     * Handle detected changes in region (such as CCPA to GDPR).
     *
     * @param {string} prevRegion - The previous region
     * @param {string} newRegion - The new (current) region
     */
    function onRegionChange(prevRegion, newRegion) {
        if (dbg) {
            uclog('debug', 'User-Consent detected region change from "' + prevRegion + '" to "' + newRegion + '".');
        }
        if (typeof config.regionChangeAction === 'function') {
            config.regionChangeAction(prevRegion, newRegion, config.consentLinkAction);
        }
    }

    /**
     * Sets the control cookie
     *
     * @param {Object} [ctrls] - Optional controls object to use to write to cookie (writes current values by default)
     */
    function setControlsCookie(ctrl) {
        /* Update the controls values */
        controls = ctrl || {
            ccpaTCS: uspString,
            consentInteractions: consentInteractions,
            consentTime: consentTime,
            consentVersion: consentVersion,
            countryCode: geoCountry,
            region: config.regId,
            stateCode: geoState,
            userConsentVersion: userConsentVersion
        };
        if (isTop()) {
            /* Set the cookie */
            setCookie(config.controlCookie, 'ccc=' + controls.countryCode + '&csc=' + controls.stateCode +
                '&cic=' + controls.consentInteractions + '&otvers=' + controls.consentVersion +
                '&pctm=' + ((controls.consentTime && encodeURIComponent(controls.consentTime.toISOString())) || '0') +
                '&reg=' + controls.region + '&ustcs=' + encodeURIComponent(controls.ccpaTCS) +
                '&vers=' + controls.userConsentVersion, {
                    domain: config.cookieDomain,
                    expires: getExpiration(config.consentExpireIn).toUTCString(),
                    path: '/',
                    samesite: config.cookieSameSite,
                    secure: config.cookieSecure
                }
            );
        }
    }

    /**
     * Internal function to force reconsent by removing the consent cookies and reloading the page.
     */
    function forceReconsent() {
        if (isTop()) {
            doc.cookie = config.consentCookie + '=; Domain=' + config.cookieDomain + '; Path=/; Expires=Thu, 01 Jan 2000 00:00:01 GMT;';
            doc.cookie = config.confirmCookie + '=; Domain=' + config.cookieDomain + '; Path=/; Expires=Thu, 01 Jan 2000 00:00:01 GMT;';
            setControlsCookie({
                ccpaTCS: '',
                consentInteractions: consentInteractions,
                consentTime: null,
                consentVersion: consentVersion,
                countryCode: geoCountry,
                region: config.regId,
                stateCode: geoState,
                userConsentVersion: userConsentVersion
            });
            setTimeout(doReload, 100);
        }
    }

    /**
     * Process the control cookie
     *
     * @returns {object} - Object containing the values from the current control cookie, if any
     */
    function processControls() {
        const value = getCookie(config.controlCookie),
            result = {
                consentInteractions: consentInteractions,
                consentTime: null,
                consentVersion: '',
                countryCode: '',
                region: '',
                stateCode: '',
                userConsentVersion: ''
            };

        /* Grab control values from control cookie, if set */
        if (typeof value === 'string' && value.length !== 0) {
            /* We have a control cookie */
            const fields = value.split('&');
            for (let f = 0; f < fields.length; f++) {
                let vals = fields[f].split('=');
                if (typeof vals[0] === 'string' && vals[0].length !== 0 && typeof vals[1] === 'string') {
                    switch (vals[0]) {
                    case 'ccc':
                        result.countryCode = vals[1].toLowerCase();
                        break;
                    case 'csc':
                        result.stateCode = vals[1].toLowerCase();
                        break;
                    case 'cic':
                        result.consentInteractions = parseInt(vals[1], 10);
                        if (isNaN(result.consentInteractions) || result.consentInteractions < consentInteractions) {
                            result.consentInteractions = consentInteractions;
                        }
                        break;
                    case 'otvers':
                        result.consentVersion = vals[1].toLowerCase();
                        break;
                    case 'pctm':
                        let ct;
                        try {
                            ct = (vals[1] === '0') ? null : new Date(decodeURIComponent(vals[1]));
                        } catch (e) {
                            ct = null;
                        }
                        result.consentTime = (ct === null || isNaN(ct.valueOf())) ? null : ct;
                        break;
                    case 'reg':
                        result.region = vals[1].toLowerCase();
                        break;
                    case 'ustcs':
                        try {
                            result.ccpaTCS = decodeURIComponent(vals[1]).toUpperCase();
                        } catch (e) {
                            result.ccpaTCS = '';
                        }
                        break;
                    case 'vers':
                        result.userConsentVersion = vals[1].toLowerCase();
                        break;
                    }
                }
            }
        }

        return result;
    }

    /**
     * Process the consent time of the user from the Optanon confirmation cookie (aka OptanonAlertBoxClosed).
     *
     * @returns {object} - A Date object containing the value of the cookie, if any
     */
    function processConsentTime() {
        const val = getCookie(config.confirmCookie);

        /* Grab consent time data from cookie, if set */
        if (typeof val === 'string' && val.length !== 0) {
            /* We have a confirmation cookie */
            let newTime = new Date(val);
            if (!win.isNaN(newTime.valueOf())) {
                return newTime;
            }
        }

        return null;
    }

    /**
     * Process the IAB, OptanonConsent, and Control cookies to determine any changes in the consent value.
     */
    function processConsentChanges(gstr) {
        const abcDate = processConsentTime(),
            oldConfirmed = consentConfirmed,
            oldDate = consentTime,
            otDomData = (win.OneTrust && typeof win.OneTrust.GetDomainData === 'function') ? win.OneTrust.GetDomainData() : null;
        let changed = false,
            reduced = false,
            interact = '',
            newState;

        /* Pull a bit of OT context data */
        if (otDomData) {
            try {
                interact = otDomData.ConsentIntegrationData.consentPayload.dsDataElements.InteractionType;
            } catch (e) {
                interact = '';
            }
        }

        /* Prep the consentTime from the OptanonAlertBoxClosed cookie, if it exists.
         * As this is a potential consent change, this must exist for the change to be valid. */
        if (abcDate && (consentTime === null || abcDate > consentTime)) {
            consentConfirmed = true;
            consentTime = abcDate;
        }

        /* If GPP, try pulling directly */
        if (gstr && !usingIabGpp) {
            gstr = '';
        }

        /* Compare the new state to the previous state */
        newState = processConsentState(gstr);
        for (let c of categories) {
            if (newState[c] !== consentState[c]) {
                changed = true;
                if (newState[c] !== true) {
                    reduced = true;
                    break;
                }
            }
        }

        if (changed || (!oldConfirmed && consentConfirmed)) {
            /* The consent states are different so set the control cookie and do the consent change logic... */
            const newDate = win.WBD.UserConsent_wrapproc > 0 ? new Date(win.WBD.UserConsent_wrapproc) : null;
            let oldState;

            /* Increment the consent change counter */
            consentInteractions++;
            /* Tweak the consent time, if we should */
            if (newDate && (consentTime === null || newDate.getTime() > (consentTime.getTime() + config.consentChangeActionDelay + 1000))) {
                consentTime = newDate;
            }
            /* Update local state so new values are available */
            oldState = consentState;
            consentState = newState;
            /* Update the UspAPI string */
            setUspAPIstring();
            if (usingIabGpp) {
                /* Update the GPP string */
                setGppConsent(gstr, newState);
            }
            /* Update the controls cookie */
            setControlsCookie();
            /* If this is an actual change of consent, not just confirming the defaults... */
            if (changed) {
                if (dbg) {
                    try {
                        /* Track consent change in history */
                        consentHistory.push({
                            ts: new Date(),
                            act: 'CHG',
                            desc: JSON.stringify(newState),
                            res: (config.reloadOnConsentChange || (config.reloadOnConsentReduction && reduced)),
                            note: (typeof config.consentChangeAction === 'function' ? 'change function' : '')
                        });
                    } catch (e) {
                        uclog('error', 'Failed to track consent change: ', e);
                    }
                }
                /* If we have a consentChangeAction, do that now... */
                if (typeof config.consentChangeAction === 'function') {
                    config.consentChangeAction(getConsentState(), config.regId, consentVersion, oldState);
                }
                /* Fire off the consent changed event */
                doc.dispatchEvent(new CustomEvent('userConsentChanged', {
                    bubbles: false,
                    cancelable: false,
                    detail: {
                        region: config.regId,
                        time: consentTime,
                        otId: consentId,
                        otVers: consentVersion,
                        otIact: interact,
                        old: oldState,
                        new: getConsentState(),
                        gpcActive: usingGpc,
                        gpp: gppString,
                        gppCmpId: gppCmpId,
                        gppVers: gppStringVer,
                        usp: uspString,
                        tcf: tcString,
                        tcfVers: tcStringVer,
                        acf: acString
                    }
                }));
                if (config.reloadOnConsentChange || (reduced && config.reloadOnConsentReduction)) {
                    /* Reload the page - new values will be available on re-initialization */
                    setTimeout(doReload, 500);
                } else {
                    if (isTop()) {
                        try {
                            win.sessionStorage.setItem('_ucWBDCons', JSON.stringify({
                                consentState: consentState,
                                consentTime: consentTime,
                                consentVersion: consentVersion,
                                consentConfirmed: consentConfirmed,
                                gppString: gppString,
                                tcString: tcString,
                                acString: acString
                            }));
                            win.postMessage('_ucWBDConsReset', '*');
                        } catch (e) {
                            uclog('error', 'Failed to update session storage and notify children of consent change: ', e);
                        }
                    }
                }
            }
        }
        if (!changed && win.WBD.UserConsent_optLoaded) {
            /* Consent didn't change. */
            try {
                /* Track consent no-change in history */
                consentHistory.push({
                    ts: new Date(),
                    act: 'NCC',
                    desc: JSON.stringify(consentState),
                    res: false,
                    note: config.regId
                });
            } catch (e) {
                uclog('error', 'Failed to track consent no-change: ', e);
            }

            consentTime = oldDate;
            if (!controls.region || (!controls.consentVersion && consentVersion) ||
                !controls.userConsentVersion || controls.userConsentVersion < '3.1.1') {

                /* No controlCookie set, so do that now */
                setControlsCookie();
            }

            /* Fire off the consent changed event */
            doc.dispatchEvent(new CustomEvent('userConsentNotChanged', {
                bubbles: false,
                cancelable: false,
                detail: {
                    region: config.regId,
                    time: consentTime,
                    otId: consentId,
                    otVers: consentVersion,
                    otIact: interact,
                    new: getConsentState(),
                    gpcActive: usingGpc,
                    gpp: gppString,
                    gppCmpId: gppCmpId,
                    gppVers: gppStringVer,
                    usp: uspString,
                    tcf: tcString,
                    tcfVers: tcStringVer,
                    acf: acString
                }
            }));
        }
        win.WBD.UserConsent_optLoaded = true;
        win.WBD.UserConsent_wrapproc = 0;
    }

    /**
     * This creates the GPP, USPAPI, and TCFAPI stub functions used for IAB, as required.
     * The GPP and TCFAPI stub code was based off an example from the IAB Tech Lab.
     */
    function createIabHandlers() {
        let cmpName,
            cmpVer,
            cmpPostHandler = function _cmpPostHandler(cmpName, event) {
                const msgIsString = typeof event.data === 'string',
                    cmpCall = cmpName + 'Return';
                let i = {},
                    json,
                    data;

                try {
                    json = msgIsString ? JSON.parse(event.data) : event.data;
                } catch (err) {
                    json = {};
                }
                data = json[cmpCall];
                if (data && typeof data.callId !== 'undefined' && typeof cmpCallbacks[data.callId] === 'function') {
                    const cid = data.callId,
                        rval = data.returnValue;
                    try {
                        if (rval && typeof rval.listenerId === 'number' && data.success === true) {
                            if (dbg) {
                                uclog('debug', 'Calling post message callback ' + cid + ' (listenerId: ' + rval.listenerId + ')');
                            }
                            eventCallbacks[rval.listenerId] = cid;
                            cmpCallbacks[cid](rval, data.success);
                        } else {
                            if (dbg) {
                                uclog('debug', 'Calling post message callback ', cid);
                            }
                            cmpCallbacks[cid](rval, data.success);
                            delete cmpCallbacks[cid];
                        }
                    } catch (err) {
                        uclog('error', 'Post message callback error (callId ' + cid + '): ', err);
                    }
                } else if (data) {
                    uclog('error', 'Post message bad or missing callback (callId ' + data.callId + ').');
                }
            },
            cmpMsgHandler = function _cmpMsgHandler(cmpName, event) {
                const msgIsString = typeof event.data === 'string',
                    cmpCall = cmpName + 'Call';
                let i = {},
                    json,
                    handler = function (retValue, success) {
                        let returnMsg = {};

                        returnMsg[cmpName + 'Return'] = {
                            returnValue: retValue,
                            success: success,
                            callId: i.callId
                        };
                        try {
                            event.source.postMessage(msgIsString ? JSON.stringify(returnMsg) : returnMsg, '*');
                        } catch (e) {
                            uclog('error', 'Failed to post reply: ', e);
                        }
                    };

                try {
                    json = msgIsString ? JSON.parse(event.data) : event.data;
                } catch (err) {
                    json = {};
                }

                if (typeof json === 'object' && json !== null && json[cmpCall]) {
                    i = json[cmpCall];
                    if (cmpName === '__gpp') {
                        win.__gpp(i.command, handler, i.parameter, i.version);
                    } else {
                        win[cmpName](i.command, i.version, handler, i.parameter);
                    }
                }
            };

        /* CCPA IAB handler must always be defined, even in GDPR regions... */
        usingUspFrame = findFrame('__uspapiLocator');
        if (usingUspFrame === null) {
            addFrame('__uspapiLocator');
            win.__uspapi = function _uspapi(cmd, vers, cb) {
                if (typeof cb === 'function') {
                    vers = (vers === 0 ? 1 : vers);  /* TCF convention is 0 = latest */
                    if (cmd === 'getUSPData' && vers === 1) {
                        cb({
                            version: 1,
                            uspString: win.WBD.UserConsent.getUspAPIstring()
                        }, true);
                        return true;
                    } else if (cmd === 'ping') {
                        cb({
                            version: 1,
                            uspapiLoaded: true
                        }, true);
                        return true;
                    } else {
                        cb(null, false);
                    }
                }
                return false;
            };
            win.__uspapi.msgHandler = cmpMsgHandler.bind(win, '__uspapi');
            setHandler(win.__uspapi.msgHandler);
            if (isChild()) {
                uclog('error', 'Unable to locate USP messaging frame from iframe!  Consent logic may not work correctly!');
            } else if (dbg) {
                uclog('debug', 'IAB for CCPA ready.');
            }
        } else {
            /* USP Messaging to an existing iframe */
            win.__uspapi = function (cmd, vers, cb, param) {
                const callId = frameCallId++,
                    msg = {
                        __uspapiCall: {
                            command: cmd,
                            parameter: param,
                            version: vers || 1,
                            callId: callId
                        }
                    };
                cmpCallbacks[callId] = cb;
                usingUspFrame.postMessage(msg, '*');
            };
            win.__uspapi.postHandler = cmpPostHandler.bind(win, '__uspapi');
            setHandler(win.__uspapi.postHandler);
            if (dbg) {
                uclog('debug', 'IAB for CCPA ready (via frame).');
            }
        }

        /* GDPR IAB handler stub is only needed in GDPR regions... */
        if (usingIabGdpr) {
            cmpName = '__tcfapi';
            cmpVer = '2.0';
            usingCmpFrame = findFrame(cmpName + 'Locator');
            if (usingCmpFrame === null && typeof win[cmpName] !== 'function') {
                addFrame(cmpName + 'Locator');

                win[cmpName] = function stubCMP() {
                    let b = arguments;

                    win[cmpName].a = win[cmpName].a || [];
                    if (b.length > 0) {
                        if (b[0] === 'ping') {
                            b[2]({
                                apiVersion: cmpVer,
                                gdprApplies: true,
                                gdprAppliesGlobally: false,
                                cmpLoaded: false,
                                cmpStatus: 'stub',
                                displayStatus: 'hidden'
                            }, true);
                        } else if (b[0] === 'setGdprApplies' && b.length > 3 && typeof b[3] === 'boolean') {
                            usingIabGdpr = b[3];
                            if (usingIabGdpr && usingIabCcpa) {
                                usingIabCcpa = false;
                            }
                        } else {
                            win[cmpName].a.push([].slice.apply(b));
                        }
                    }
                    return win[cmpName].a;
                };
                win[cmpName].msgHandler = cmpMsgHandler.bind(win, cmpName);
                setHandler(win[cmpName].msgHandler);
                if (isChild()) {
                    uclog('error', 'Unable to locate TCF messaging frame from iframe!  Consent logic may not work correctly!');
                } else if (dbg) {
                    uclog('debug', 'IAB (v' + cmpVer + ') for GDPR ready.');
                }

                /* Add CMP message with callback to catch OT not calling OptanonWrapper in some cases */
                win[cmpName]('getTCData', 0, win.OptanonWrapper);
            } else if (usingCmpFrame) {
                /* CMP Messaging to an existing iframe */
                win.__tcfapi = function (cmd, vers, cb, param) {
                    const callId = frameCallId++,
                        msg = {
                            __tcfapiCall: {
                                command: cmd,
                                parameter: param,
                                version: vers || 2,
                                callId: callId
                            }
                        };
                    cmpCallbacks[callId] = cb;
                    usingCmpFrame.postMessage(msg, '*');
                    if (cmd === 'removeEventListener' && typeof param === 'number' && typeof eventCallbacks[param] !== 'undefined' && cmpCallbacks[eventCallbacks[param]]) {
                        /* Remove the event listener handler */
                        delete cmpCallbacks[eventCallbacks[param]];
                        delete eventCallbacks[param];
                    }
                };
                win.__tcfapi.postHandler = cmpPostHandler.bind(win, '__tcfapi');
                setHandler(win.__tcfapi.postHandler);
                if (dbg) {
                    uclog('debug', 'IAB (v' + cmpVer + ') for GDPR ready (via frame).');
                }
            }
        }

        /* GPP IAB handler stub is only needed when enabled... */
        if (usingIabGpp) {
            cmpName = '__gpp';
            cmpVer = '1.1';
            usingGppFrame = findFrame('__gppLocator');
            if (usingGppFrame === null && !win.__gpp) {
                let apis = [],
                    sects;

                addFrame('__gppLocator');
                /* Init the GPP CMP.  We get __gpp as part of the deal. */
                win.__gpp = function () { return null };
                gppCmpApi = gppCmpApi || new CmpApi(1, 1);
                gppCmpApi.setCmpStatus('loading');
                sects = Object.keys(gppApisAvail);
                for (let s = 0; s < sects.length; s++) {
                    apis.push(gppApisAvail[sects[s]].toString(10) + ':' + sects[s]);
                }
                gppCmpApi.setSupportedAPIs(apis);
                gppStub = win.__gpp;  /* Keep this for later */
                win.__gpp.msgHandler = cmpMsgHandler.bind(win, '__gpp');
                setHandler(win.__gpp.msgHandler);
                if (isChild()) {
                    uclog('error', 'Unable to locate GPP messaging frame from iframe!  Consent logic may not work correctly!');
                } else if (dbg) {
                    uclog('debug', 'IAB for GPP ready.');
                }
            } else if (usingGppFrame !== null) {
                /* GPP Messaging to an existing iframe */
                win.__gpp = function (cmd, cb, param, vers) {
                    const callId = frameCallId++,
                        msg = {
                            __gppCall: {
                                command: cmd,
                                parameter: param,
                                version: vers || cmpVer,
                                callId: callId
                            }
                        };
                    cmpCallbacks[callId] = cb;
                    usingGppFrame.postMessage(msg, '*');
                    if (cmd === 'removeEventListener' && typeof param === 'number' && typeof eventCallbacks[param] !== 'undefined' && cmpCallbacks[eventCallbacks[param]]) {
                        /* Remove the event listener handler */
                        delete cmpCallbacks[eventCallbacks[param]];
                        delete eventCallbacks[param];
                    }
                };
                win.__gpp.postHandler = cmpPostHandler.bind(win, '__gpp');
                setHandler(win.__gpp.postHandler);
                if (dbg) {
                    uclog('debug', 'IAB for GPP ready (via frame).');
                }
            }
        }
    }

    /**
     * Function to be clever and get the correct IAB consent string (and addtl consent string) from the CMP
     *
     * @param {function} callback - The callback to call with the consent string (may be immediate),
     *                              expected to take five parameters, a string, a number, a string, and an Error:
     *                                  function (region, iabVersion, cmpString, addtlString, error)
     *                              If there is an error, the cmpString and addtlString will both be empty.
     * @param {string} [region] - The region to force getting the consent from (optional).
     */
    function getCmpString(callback, region) {
        let cmp,
            reqIab = '',
            reqCmd,
            reqVer;

        if (typeof callback !== 'function') {
            uclog('error', 'getCmpString called without callback');
            return;  // Nothing to do
        }
        region = region || this.getRegion();
        if (!config.useIAB) {
            if (dbg) {
                uclog('debug', 'getCmpString called with IAB disabled');
            }
            callback(region, 0, '', '', new Error('IAB disabled'));
            return;
        }
        if (usingIabGpp) {
            if (typeof gppString === 'string' && gppString.length !== 0) {
                // We have the string, so return it now.
                if (dbg) {
                    uclog('debug', 'getCmpString returning GPP CMP string');
                }
                callback(region, 1, gppString, '', null);
                return;
            }
            cmp = win.__gpp;
            reqCmd = 'ping';
            reqVer = 1;
            reqIab = 'GPP';
        } else if (usingIabGdpr) {
            if (typeof tcString === 'string' && tcString.length !== 0) {
                // We have the string, so return it now.
                if (dbg) {
                    uclog('debug', 'getCmpString returning GDPR v2 CMP string');
                }
                callback(region, 2, tcString, acString, null);
                return;
            }
            cmp = win.__tcfapi;
            reqCmd = 'getTCData';
            reqVer = 2;
            reqIab = 'TCF';
        }
        if (reqCmd) {
            let cmpCb = function _getCmpStringCB(cb, reg, iab, ver, data, success) {
                if (success) {
                    if (dbg) {
                        uclog('debug', 'getCmpString returning ' + iab + ' v' + ver + ' CMP string');
                    }
                    cb(reg, ver, (iab === 'GPP' ? data.pingData.gppString : data.tcString), (data.addtlConsent ? data.addtlConsent : ''), null);
                } else {
                    if (dbg) {
                        uclog('debug', 'getCmpString returning ' + iab + ' v' + ver + ' error');
                    }
                    cb(reg, ver, '', '', new Error(iab + ' CMP request failure'));
                }
            }.bind(this, callback, region, reqIab, reqVer);
            if (reqIab === 'GPP') {
                cmp(reqCmd, cmpCb);
            } else {
                cmp(reqCmd, reqVer, cmpCb);
            }
        } else {
            // We have the USPAPI string, so return it now.
            if (dbg) {
                uclog('debug', 'getCmpString returning CCPA v1 CMP string');
            }
            callback(region, 1, uspString, '', (uspString.length !== 0 ? null : new Error('CMP request failure')));
        }
    }

    /**
     * Function to initialize the UserConsent code
     *
     * @param {object} conf - the configuration object
     */
    function init(conf) {
        let cats,
            forceLang = false,
            prop,
            reg = null,
            ucFrame;

        if (consentState !== null) {
            return;  /* init already called */
        }

        /* Process config */
        if (!conf || !conf.domId || !conf.cookieDomain) {
            throw new Error('Invalid config passed to user-consent!');
        }

        /* Are we running in an iframe with a parent UC? */
        conf.regId = '';
        ucFrame = findFrame('_usrConWBD');
        if (ucFrame !== null) {
            if (win === win.top) {
                /* We are NOT an iframe, but another instance is acting like it. */
                uclog('error', 'Detected an instance of UserConsent in an iframe acting as the primary instance.  This was likely caused by a delay in this instance initializing, which must be corrected.  Consent is not working correctly!');
            } else {
                /* We are an iframe in a window running UserConsent */
                let sconf,
                    scons;

                frame = win.name || 'child';
                try {
                    sconf = JSON.parse(win.sessionStorage.getItem('_ucWBDConf'));
                } catch (err) {
                    sconf = null;
                    uclog('error', 'Failed to parse parent frame consent settings.');
                }
                if (typeof sconf === 'object' && sconf !== null) {
                    /* We have a remote config object from a parent UserConsent, use it */
                    conf.countryCode = sconf.countryCode;
                    conf.cookieDomain = sconf.cookieDomain;
                    conf.cookieSameSite = sconf.cookieSameSite;
                    conf.cookieSecure = sconf.cookieSecure;
                    conf.domId = sconf.domId;
                    conf.languageFromBrowser = !!sconf.langFromBrowser;
                    conf.enableDebug = !!sconf.enableDebug;
                    conf.enableGPC = !!sconf.enableGPC;
                    conf.regId = sconf.regId;
                    conf.stateCode = sconf.stateCode;
                    conf.src = sconf.src;
                    try {
                        scons = JSON.parse(win.sessionStorage.getItem('_ucWBDCons'));
                    } catch (err) {
                        scons = null;
                        uclog('error', 'Failed to parse parent frame consent state.');
                    }
                    if (typeof scons === 'object' && scons !== null) {
                        consentState = scons.consentState;
                        consentTime = scons.consentTime;
                        consentVersion = scons.consentVersion;
                        consentConfirmed = scons.consentConfirmed;
                        gppString = scons.gppString;
                        tcString = scons.tcString;
                        acString = scons.acString;
                        ucChild = true;
                    }
                    if (!sconf.parentReload) {
                        /* If the parent window won't reload on consent changes... */
                        setHandler(function _handleChangesInFrame(e) {
                            var reduced,
                                val;

                            if (e.data === '_ucWBDConsReset') {
                                try {
                                    val = JSON.parse(win.sessionStorage.getItem('_ucWBDCons'));
                                } catch (err) {
                                    val = null;
                                }
                                if (typeof val === 'object' && val !== null) {
                                    reduced = hasConsentReduced(consentState, val.consentState);
                                    /* If the iframe is set to reload on changes, do that... */
                                    if (config.reloadOnConsentChange || (reduced && config.reloadOnConsentReduction)) {
                                        /* Reload the frame - new values will be available on re-initialization */
                                        setTimeout(doReload, 600 + config.consentChangeActionDelay);
                                    } else {
                                        /* Update local state so new values are available */
                                        consentState = val.consentState;
                                        consentTime = val.consentTime;
                                        consentVersion = val.consentVersion;
                                        consentConfirmed = val.consentConfirmed;
                                        gppString = val.gppString || '';
                                        tcString = val.tcString || '';
                                        acString = val.acString || '';
                                        setUspAPIstring();
                                    }
                                }
                            }
                        });
                    }
                }
            }
        }

        /* Override with any explicit settings */
        if (typeof conf.gppCategories === 'undefined') {
            conf.gppCategories = defaults.gppCategories;
        } else {
            for (prop in conf.gppCategories) {
                if (prop === 'usnatv1') {
                    conf.gppCategories.usnat = conf.gppCategories.usnatv1;
                    delete conf.gppCategories.usnatv1;
                } else if (prop === 'uspnatv1') {
                    conf.gppCategories.uspv1 = conf.gppCategories.uspnatv1;
                    delete conf.gppCategories.uspnatv1;
                } else if (!gppApisAvail[prop]) {
                    uclog('error', 'Error: Unsupported GPP section "' + prop + '" ignored.');
                    delete conf.gppCategories[prop];
                }
            }
            for (prop in defaults.gppCategories) {
                conf.gppCategories[prop] = conf.gppCategories[prop] || defaults.gppCategories[prop];
            }
        }
        for (prop in defaults) {
            config[prop] = (typeof conf[prop] !== 'undefined') ? conf[prop] : defaults[prop];
        }
        defaults = null;  /* Done with defaults */

        /* Top-level configs with no defaults */
        config.cookieDomain = conf.cookieDomain;
        config.domId = conf.domId;
        config.changeRegions = conf.changeRegions;
        dbg = !!(console && (conf.enableDebug || (win.location.search.search(/[?&]wmuc_debug=[1t]/) !== -1)));
        if (dbg) {
            uclog('debug', 'Initializing UserConsent v' + userConsentVersion);
        }
        config.strictIabCompliance = !!config.strictIabCompliance;
        if (typeof conf.countryCode === 'string' && conf.countryCode.length === 2) {
            config.countryCode = conf.countryCode;
        }
        if (typeof conf.stateCode === 'string' && conf.stateCode.length === 2) {
            config.stateCode = conf.stateCode;
        }

        if (config.gpcFixCookie !== '') {
            /* Some GPC extensions are... not great.  Let's try to fix them up a bit with a cookie-based thing in Fastly. */
            fixGpcWithCookie(config.gpcFixCookie);
        }

        /* Are we using external consent data in a webview? */
        if (typeof win.GetExternalConsent === 'object' && win.GetExternalConsent !== null && typeof win.GetExternalConsent.oneTrustCookie === 'function') {
            /* Initialize external consent values */
            let extcon;
            try {
                extcon = JSON.parse(win.GetExternalConsent.oneTrustCookie());
            } catch (e) {
                extcon = null;
            }
            if (typeof extcon === 'object' && extcon !== null && extcon.consentedDate && (extcon.gppString || extcon.tcString || extcon.groups)) {
                /* Valid ExternalConsent, use it */
                extcon.gppString = extcon.gppString || '';
                win.OTExternalConsent = extcon;
            }

            if (typeof win.GetExternalConsent.countryCode === 'function' && win.GetExternalConsent.countryCode()) {
                win.ExternalConsentGeo = {
                    countryCode: win.GetExternalConsent.countryCode(),
                    stateCode: win.GetExternalConsent.stateCode() || ''
                };
            }
        }
        if (config.enableWebViewCheck && typeof win.OTExternalConsent === 'object' && win.OTExternalConsent !== null && win.OTExternalConsent.consentedDate) {
            config.useExternalConsent = true;
            if (dbg) {
                uclog('debug', 'Reading consent from external consent data: ', win.OTExternalConsent);
            }
        } else {
            config.useExternalConsent = false;
        }

        /* Process country/state */
        const geoRet = (typeof config.geoCheckFunction === 'function') ? config.geoCheckFunction() : null;
        if (isTop() && dbg && win.location.search.search(/[?&]wmuc_cc=[A-Za-z]{2}/) !== -1) {
            /* Pulled from the query string */
            geoCountry = win.location.search.match(/[?&]wmuc_cc=([A-Za-z]{2})/)[1].toUpperCase();
            uclog('debug', 'Set debug CC to: ', geoCountry);
        } else if (config.useExternalConsent && typeof win.ExternalConsentGeo === 'object' && typeof win.ExternalConsentGeo.countryCode === 'string' && win.ExternalConsentGeo.countryCode.length === 2) {
            /* Pulled from the external consent object */
            geoCountry = win.ExternalConsentGeo.countryCode.toUpperCase();
        } else if (typeof config.countryCode === 'string' && config.countryCode.length === 2) {
            /* Pulled from the config options */
            geoCountry = config.countryCode.toUpperCase();
        } else if (geoRet && geoRet.countryCode && geoRet.countryCode.length === 2) {
            /* Pulled from the configured geoCheckFunction */
            geoCountry = geoRet.countryCode.toUpperCase();
        } else {
            /* Pulled from the cookie */
            const ccValue = getCookie(config.ccCookie || 'countryCode');
            if (ccValue && ccValue.length === 2) {
                geoCountry = ccValue.toUpperCase();
            } else {
                /* No country value found.  Go get the country code */
                uclog('error', 'User-Consent unable to determine country, missing or invalid cookies!');
                getGeoCountry();
            }
        }
        if (!geoCountry || geoCountry.length !== 2) {
            /* No country value found.  Default to default setting or "US", since that is what Prism does. */
            geoCountry = (config.defaultCountry && config.defaultCountry.length == 2) ? config.defaultCountry.toUpperCase() : 'US';
            uclog('error', 'User-Consent unable to determine country, missing or invalid cookies!  Using default (' + geoCountry + ').');
        }
        if (dbg && win.location.search.search(/[?&]wmuc_sc=[A-Za-z]{2}/) !== -1) {
            geoState = win.location.search.match(/[?&]wmuc_sc=([A-Za-z]{2})/)[1].toUpperCase();
            uclog('debug', 'Set debug SC to: ', geoState);
        } else if (config.useExternalConsent && typeof win.ExternalConsentGeo === 'object' && typeof win.ExternalConsentGeo.stateCode === 'string' && win.ExternalConsentGeo.stateCode.length === 2) {
            geoState = win.ExternalConsentGeo.stateCode.toUpperCase();
        } else if (typeof config.stateCode === 'string' && config.stateCode.length === 2) {
            geoState = config.stateCode.toUpperCase();
        } else if (geoRet && geoRet.countryCode && geoRet.countryCode.length === 2) {
            geoState = (typeof geoRet.stateCode === 'string') ? geoRet.stateCode.toUpperCase() : '';
        } else {
            const scValue = getCookie(config.scCookie || 'stateCode');
            if (scValue && scValue.length === 2) {
                geoState = scValue.toUpperCase();
            }
        }
        if (!geoState || geoState.length === 0) {
            geoState = (config.defaultState && config.defaultState.length > 0) ? config.defaultState.toUpperCase() : '';
            if (dbg) {
                uclog('debug', 'User-Consent unable to determine state.  Using default (' + geoState + ').');
            }
        }
        geoCS = geoCountry + ':' + geoState;

        if (isTop()) {
            if (!oneTrustLocationSet && config.geoPassedToOneTrust) {
                /* Prep OneTrust to use our geoIP data (needed for older versions) */
                win.OneTrust = win.OneTrust || {};
                win.OneTrust.geolocationResponse = {
                    countryCode: geoCountry,
                    stateCode: geoState
                };
            }
            /* Set-up handler to catch OneTrust consent change (and other) events */
            let handler = function _OTchanges(e) {
                /* Make sure we've correctly set the OT region */
                if (config.geoPassedToOneTrust) {
                    pushGeoLocation();
                }
                /* Handle change events after loading */
                if (optanonLoaded && !config.useExternalConsent && consentState !== null && win.WBD.UserConsent_wrapproc === 0) {
                    /* Handle consent changes */
                    win.WBD.UserConsent_wrapproc = new Date().getTime();
                    if (dbg) {
                        uclog('debug', 'Consent changed event handler determining consent changes.');
                    }
                    /* If we're using GPP, get the new consent data asynchronously */
                    if (usingIabGpp && usingOTGpp) {
                        win.__gpp('ping', function (pd) {
                            if (pd && pd.gppString) {
                                processConsentChanges(pd.gppString);
                            }
                        });
                    } else {
                        /* We need to wait on cookie updates */
                        setTimeout(processConsentChanges, config.consentChangeActionDelay);
                    }
                }
            };
            if (win.addEventListener) {
                win.addEventListener('consent.onetrust', handler, false);
            } else {
                win.attachEvent('consent.onetrust', handler);
            }
        }

        cats = Object.keys(config.categories);
        categories = [];
        for (let c = 0; c < cats.length; c++) {
            categories.push(config.categories[cats[c]]);
        }
        if (config.changeRegions) {
            for (let act of ['remove', 'replace', 'insert']) {
                if (config.changeRegions[act] && Array.isArray(config.changeRegions[act]) && config.changeRegions[act].length !== 0) {
                    prop = config.changeRegions[act];
                    for (let i = 0; i < prop.length; i++) {
                        if (typeof prop[i] === 'object' && prop[i] !== null && prop[i].id) {
                            let crl = config.regions.length,
                                cReg = prop[i],
                                rid = (act === 'insert' && cReg.insertAfter) ? cReg.insertAfter : cReg.id,
                                r = 0;

                            RLOOP: for (; r < crl; r++) {
                                if (config.regions[r] && config.regions[r].id && config.regions[r].id === rid) {
                                    break RLOOP;
                                }
                            }
                            if (act === 'remove') {
                                if (r < crl) {
                                    delete config.regions[r];
                                }
                            } else if (act === 'replace') {
                                if (r < crl) {
                                    config.regions[r] = cReg;
                                }
                            } else if (act === 'insert') {
                                if (r < crl) {
                                    if (cReg.insertAfter) {
                                        delete cReg.insertAfter;
                                        config.regions.splice(r + 1, 0, cReg);
                                    } else {
                                        /* Replace the matching region */
                                        config.regions[r] = cReg;
                                    }
                                } else {
                                    /* Insert at top of list */
                                    delete cReg.insertAfter;
                                    config.regions.splice(r + 1, 0, cReg);
                                }
                            }
                        }
                    }
                }
            }
            delete config.changeRegions;
        }
        for (let r = 0; r < config.regions.length; r++) {
            if (!config.regions[r] || !config.regions[r].id || !config.regions[r].geoMatch) {
                uclog('error', 'Invalid region, missing id or geoMatch!');
            } else {
                if ((isTop() && checkInRegion(config.regions[r].geoMatch)) ||
                    (isChild() && config.regions[r].id === conf.regId)) {

                    reg = config.regions[r];
                    break;
                }
            }
        }
        if (!reg) {
            if (isChild()) {
                throw new Error('No matching user-consent region, parent and iframe configs do not match!');
            }
            throw new Error('No matching user-consent region!');
        }
        config.regId = reg.id;
        config.defaultLanguage = (reg.defaultLanguage || config.defaultLanguage).toLowerCase();
        /* Get the page language we are using */
        try {
            let lang = '';
            if (!config.languageFromBrowser) {
                /* We want the language from the page, so try that first... */
                /* Apparently xml:lang gets precedence over lang.  Thanks W3! */
                lang = doc.getElementsByTagName('html')[0].getAttribute('xml:lang') || doc.documentElement.lang || config.defaultLanguage;
            }
            if (!lang) {
                /* Pull the language from the browser */
                lang = win.navigator.language || config.defaultLanguage;
            }
            pageLang = lang ? lang.substr(0, 2).toLowerCase() : 'en';
        } catch(err) {
            pageLang = 'en';
        }
        if (dbg && win.location.search.search(/[?&]wmuc_lang=[A-Za-z]{2}/) !== -1) {
            /* Try to force the page language to the debug value passed-in */
            let fl = win.location.search.match(/[?&]wmuc_lang=([A-Za-z]{2})/)[1].toLowerCase();
            forceLang = (pageLang !== fl);
            pageLang = fl;
            uclog('debug', 'Set debug Language to: ', pageLang);
        }
        config.adChoicesLinkAction = reg.adChoicesLinkAction || config.adChoicesLinkAction || null;
        config.adChoicesLinkTitle = getLocalizedString(reg.adChoicesLinkTitle || config.adChoicesLinkTitle);
        config.affiliatesLinkAction = reg.affiliatesLinkAction || config.affiliatesLinkAction || null;
        config.affiliatesLinkTitle = getLocalizedString(reg.affiliatesLinkTitle || config.affiliatesLinkTitle);
        config.compatTransition = config.enableTransitionCheck && reg.compatTransition ? reg.compatTransition : null;
        config.compatCategories = reg.compatCategories || config.compatCategories || {};
        config.compatCodes = reg.compatCodes || config.compatCodes || {};
        config.consentExpireIn = reg.consentExpireIn || config.consentExpireIn || 1;
        config.consentLinkAction = reg.consentLinkAction || config.consentLinkAction || null;
        config.consentLinkTitle = getLocalizedString(reg.consentLinkTitle || config.consentLinkTitle);
        config.confirmCookie = reg.confirmCookie || config.confirmCookie;
        config.consentCookie = reg.consentCookie || config.consentCookie;
        config.addtlConsentCookie = reg.addtlConsentCookie || config.addtlConsentCookie;
        if (reg.consentDefaults) {
            config.consentDefaults = mergeConsent(config.consentDefaults, reg.consentDefaults);
        }
        config.consentGpcDefaults = reg.consentGpcDefaults || config.consentGpcDefaults || null;
        config.consentImpliedDefaults = reg.consentImpliedDefaults || config.consentImpliedDefaults || {};
        if (!config.consentImpliedDefaults.required) {
            config.consentImpliedDefaults.required = true;
        }
        config.consentNotApplicable = reg.consentNotApplicable || config.consentNotApplicable || [];
        if (config.consentNotApplicable && Array.isArray(config.consentNotApplicable) && config.consentNotApplicable.length !== 0) {
            /* Remove N/A consents from defaults and implied defaults */
            for (let na of config.consentNotApplicable) {
                if (typeof config.consentDefaults[na] !== 'undefined') {
                    delete config.consentDefaults[na];
                }
                if (typeof config.consentImpliedDefaults[na] !== 'undefined') {
                    delete config.consentImpliedDefaults[na];
                }
            }
        }
        config.useFixedConsent = (typeof reg.useFixedConsent === 'boolean' ? reg.useFixedConsent : config.useFixedConsent);
        config.domId = reg.domId || config.domId;
        config.src = reg.src || config.src;
        config.gdprIabCookie = reg.gdprIabCookie || config.gdprIabCookie;
        config.tcfOpts = reg.tcfOpts || config.tcfOpts || null;
        config.privacyCenterLinkAction = reg.privacyCenterLinkAction || config.privacyCenterLinkAction || null;
        config.privacyCenterLinkTitle = getLocalizedString(reg.privacyCenterLinkTitle || config.privacyCenterLinkTitle);
        config.rightsRequestLinkAction = reg.rightsRequestLinkAction || config.rightsRequestLinkAction || null;
        config.rightsRequestLinkTitle = getLocalizedString(reg.rightsRequestLinkTitle || config.rightsRequestLinkTitle);
        /* Prep IAB options for region */
        if (config.useIAB) {
            config.iabRegion = ((typeof reg.iabRegion === 'string' && reg.iabRegion) || config.iabRegion).toLowerCase();
            if (config.iabRegion === 'ccpa') {
                usingIabCcpa = true;
            } else if (config.iabRegion === 'gdpr' && config.tcfOpts) {
                usingIabGdpr = true;
            } else if (config.iabRegion && config.iabRegion !== 'gpp') {
                uclog('error', 'Error: Invalid IAB region "' + config.iabRegion + '" specified for region "' + config.regId + '", IAB not enabled for region!');
            }
        }
        /* Prep GPP options for region */
        if (config.useGPP) {
            config.gppSection = ((typeof reg.gppSection === 'string' && reg.gppSection) || config.gppSection).toLowerCase();
            config.gppSection = (config.gppSection === 'usnat' || config.gppSection === 'usnatv1') ? 'usnat' : ((config.gppSection === 'uspv1' || config.gppSection === 'uspnatv1') ? 'uspv1' : config.gppSection);
            if (config.useGPP && config.gppSection) {
                if (gppApisAvail[config.gppSection] && config.gppCategories[config.gppSection]) {
                    usingIabGpp = true;
                    if (usingIabCcpa && config.ccpaGeos && !checkInRegion(config.ccpaGeos)) {
                        usingIabCcpa = false;
                    }
                } else {
                    uclog('error', 'Error: Invalid GPP section "' + config.gppSection + '" specified for region "' + config.regId + '", IAB/GPP not enabled for region!');
                }
            }
        }
        if (!usingIabGpp && !usingIabCcpa && !usingIabGdpr) {
            /* Ignore IAB/GPP region if not supported */
            config.iabRegion = '';
            config.gppSection = '';
            config.useIAB = false;
            config.useGPP = false;
        }

        /* Detect GPC existance and use */
        if (config.enableGPC && config.consentGpcDefaults && navigator.globalPrivacyControl) {
            usingGpc = true;
        }

        /* Expose the countryCode, stateCode, region, and GPC use by adding "userconsent-cntry-<cc> userconsent-state-<sc> userconsent-reg-<reg> userconsent-gpc" classes to the HTML tag... */
        if (config.setPageClass && doc.documentElement) {
            doc.documentElement.className = ((doc.documentElement.className && doc.documentElement.className !== ' ') ? doc.documentElement.className + ' userconsent-cntry-' : 'userconsent-cntry-') + geoCountry.toLowerCase() + ' userconsent-state-' + geoState.toLowerCase() + ' userconsent-reg-' + config.regId.toLowerCase() + (usingGpc ? ' userconsent-gpc' : '');
        }

        if (dbg) {
            uclog('debug', 'GeoIP Country Code: ' + geoCountry + ', using consent region: ' + config.regId);
            uclog('debug', 'IAB ' + (config.useIAB ? 'enabled' : 'disabled'));
        }

        if (usingIabGpp || usingIabCcpa || usingIabGdpr || config.ccpaGeos) {
            /* We are using IAB, so prep the __uspapi function and the __gpp/__tcfapi stubs (later replaced by OneTrust) */
            createIabHandlers();
        }

        /* If we are the top-level instance... */
        if (isTop()) {
            let gpclog;

            /* Grab control values from control cookie, if set */
            controls = processControls();
            consentInteractions = controls.consentInteractions;

            if (config.useExternalConsent) {
                /* Grab consent time data from external consent object */
                try {
                    consentTime = new Date(win.OTExternalConsent.consentedDate);
                    usingGpc = false;
                    if (dbg) {
                        uclog('debug', 'Consent time read from external consent data: ', consentTime);
                    }
                } catch (e) {
                    uclog('error', 'Consent Date from external consent data is invalid.');
                    consentTime = null;  /* Force using defaults */
                }
            } else {
                /* Grab consent time data from cookie, if set */
                consentTime = processConsentTime();

                /* Use the most relevant consent time */
                if (controls.consentTime !== null && (consentTime === null || controls.consentTime > consentTime)) {
                    consentTime = controls.consentTime;
                    if (dbg) {
                        uclog('debug', 'Consent time read from "' + config.controlCookie + '": ', consentTime);
                    }
                } else if (consentTime !== null && dbg) {
                    uclog('debug', 'Consent time read from "' + config.confirmCookie + '": ', consentTime);
                }
            }

            gpclog = (dbg && usingGpc) ? ' [GPC override]' : '';
            if (consentTime !== null) {
                /* We have a confirmation cookie */
                consentConfirmed = true;
                consentState = processConsentState();
                if (consentConfirmed) {
                    if (controls.consentTime !== null && controls.consentTime < consentTime) {
                        /* Need to reset controls cookie */
                        controls.region = '';
                    }
                    if (dbg) {
                        uclog('debug', 'Consent state read from ' + consentSource + ' (' + consentVersion + ')' + gpclog + ': ', consentState);
                        if (usingCompatMode) {
                            uclog('debug', 'Consent state using compatibility config.');
                        }
                    }
                } else {
                    /* This means the OptanonConsent cookie is expired or deleted, so we are using defaults again */
                    consentTime = null;
                    controls.region = '';  /* This will force the cookie to be reset */
                    if (dbg) {
                        uclog('debug', 'Consent state expired or removed, reset from defaults' + gpclog + ': ', consentState);
                    }
                }
            } else {
                /* We are using the defaults */
                consentState = copyConsent(config.consentDefaults);
                /* Check against GPC */
                if (usingGpc) {
                    consentState = mergeConsent(consentState, config.consentGpcDefaults);
                }
                if (dbg) {
                    uclog('debug', 'Consent state from defaults' + gpclog + ': ', consentState);
                }
            }

            /* If we're using GPP, but don't have a GPP string, set it */
            if (usingIabGpp && !gppString) {
                setGppConsent('', consentState);
            }

            if (dbg) {
                try {
                    /* Track consent change in history */
                    consentHistory.push({
                        ts: new Date(),
                        act: 'SET',
                        desc: JSON.stringify(consentState),
                        res: (consentTime !== null),
                        note: config.regId
                    });
                } catch (e) {
                    uclog('error', 'Failed to track setting initial consent: ', e);
                }
            }
        }

        /* Make sure this is not a second instance */
        if (win.WBD.UserConsent_initted) {
            uclog('error', 'ERROR:  Second instance of UserConsent initialized!');
            return;
        }
        win.WBD.UserConsent_initted = true;

        setUspAPIstring();

        /* If we are the top-level instance... */
        if (isTop()) {
            if (config.useExternalConsent) {
                /* If using external consent, write the controls cookie */
                setControlsCookie();
            } else if (controls.region && controls.region !== config.regId) {
                /* If we have switched regions, maybe do something about that... */
                onRegionChange(controls.region, config.regId);
            }

            /* Tag __gpp as "ready" if we have a GPP string */
            /* if (usingIabGpp && gppString.length > 0) {
                gppCmpApi.setEventStatus('gpploaded');
            } */

            /* Setup the frame data */
            if (ucFrame === null) {
                /* Prep the session data for frames */
                if (addFrame('_usrConWBD')) {
                    if (dbg) {
                        uclog('debug', 'Setup UserConsent IPC frame.');
                    }

                    try {
                        win.sessionStorage.setItem('_ucWBDConf', JSON.stringify({
                            cookieDomain: config.cookieDomain,
                            cookieSameSite: config.cookieSameSite,
                            cookieSecure: config.cookieSecure,
                            countryCode: geoCountry,
                            domId: config.domId,
                            enableDebug: dbg,
                            langFromBrowser: config.languageFromBrowser,
                            parentReload: config.reloadOnConsentChange,
                            regId: config.regId,
                            src: config.src,
                            stateCode: geoState
                        }));
                        win.sessionStorage.setItem('_ucWBDCons', JSON.stringify({
                            consentState: consentState,
                            consentTime: consentTime,
                            consentVersion: consentVersion,
                            consentConfirmed: consentConfirmed,
                            gppString: gppString,
                            tcString: tcString,
                            acString: acString
                        }));
                    } catch (err) {
                        funcs.uclog('error', 'Failed to set UserConsent frame data!');
                    }
                } else {
                    uclog('error', 'Failed to setup UserConsent IPC frame!');
                }
            }
        }

        const funcs = {
                isTop,
                uclog
            },
            vals = {
                acString,
                config,
                consentState,
                consentTime,
                consentVersion,
                consentConfirmed,
                dbg,
                forceLang,
                geoCountry,
                geoState,
                gppString,
                pageLang,
                tcString,
                ucFrame,
                usingGpc
            };

        /* Create a function to modify the DOM once it's ready. */
        const moddom = function _moddom(win, doc, funcs, vals, evt) {
                if (!evt || (evt && doc.readyState === 'interactive')) {
                    /* OK to modify the DOM, so get to it... */
                    /* Expose the countryCode, stateCode, region, and GPC use by adding "userconsent-cntry-<cc> userconsent-state-<sc> userconsent-reg-<reg> userconsent-gpc" classes to the HTML tag... */
                    if (vals.config.setPageClass && !doc.documentElement.className.toString().includes('userconsent-cntry-')) {
                        doc.documentElement.className = ((doc.documentElement.className && doc.documentElement.className !== ' ') ? doc.documentElement.className + ' userconsent-cntry-' : 'userconsent-cntry-') + vals.geoCountry.toLowerCase() + ' userconsent-state-' + vals.geoState.toLowerCase() + ' userconsent-reg-' + vals.config.regId.toLowerCase() + (vals.usingGpc ? ' userconsent-gpc' : '');
                    }

                    if (funcs.isTop()) {
                        /* Load the OneTrust script */
                        const ots = doc.createElement('script');

                        win.WBD.UserConsent_wait = setTimeout(win.OptanonWrapper.bind(win, true), vals.config.oneTrustLoadTimeout);
                        if (vals.consentConfirmed) {
                            ots.async = true;
                        }
                        ots.charset = 'utf-8';
                        if (!vals.config.languageFromBrowser || vals.forceLang) {
                            ots.dataset.documentLanguage = 'true';
                            if (vals.forceLang) {
                                ots.dataset.language = vals.pageLang;
                            }
                        }
                        ots.dataset.domainScript = vals.config.domId;
                        ots.type = 'text/javascript';
                        ots.src = vals.config.src;
                        if (doc.head) {
                            doc.head.appendChild(ots);
                        } else {
                            doc.body.appendChild(ots);
                        }
                        if (vals.dbg) {
                            funcs.uclog('debug', 'Loading OneTrust.');
                        }
                    }
                }
            };

        if (doc.readyState === 'loading') {
            /* Wait on the DOM to finish being read */
            doc.addEventListener('readystatechange', moddom.bind(this, win, doc, funcs, vals));
        } else {
            /* Ready to go now, so just do it */
            moddom(win, doc, funcs, vals);
        }

        /* Debug GPP events */
        if (dbg && usingIabGpp && win.__gpp) {
            win.__gpp('addEventListener', function _handleGppChanges(obj, _suc) {
                uclog('debug', 'GPP event: ', obj);
            });
        }

        /* Detect EasyList Cookie blocking of OneTrust */
        testOTBlocking();

        /***
            We are ready to handle consent at this point!
        ***/

        if (dbg) {
            uclog('debug', 'Dispatching UserConsentReady event.');
        }
        doc.dispatchEvent(new CustomEvent('userConsentReady', {
            bubbles: false,
            cancelable: false,
            detail: {
                region: config.regId,
                time: new Date(),
                consentConfirmed: consentConfirmed
            }
        }));
    }

    /* The wrapper called on Optanon load and cookie changes */
    if (win.WBD.UserConsent_loaded) {
        uclog('error', 'ERROR:  Second instance of UserConsent loaded!');
    } else {
        win.WBD.UserConsent_loaded = true;
        win.WBD.UserConsent_optLoaded = false;
        win.WBD.UserConsent_wrapproc = 0;
        win.WBD.UserConsent_wait = -1;
        win.OptanonWrapper = function OptanonWrapper(tos) {
            if (!optanonLoaded && !otFailed) {
                /* Set this up for later... */
                let doLoaded = function () {
                    if (optanonLoaded) {
                        consentId = (typeof win.OneTrust.getDataSubjectId === 'function' && win.OneTrust.getDataSubjectId()) || consentId;
                        /* Send OT loaded events */
                        if (dbg) {
                            uclog('debug', 'Dispatching oneTrustLoaded event.');
                        }
                        doc.dispatchEvent(new CustomEvent('oneTrustLoaded', {
                            bubbles: false,
                            cancelable: false,
                            detail: {
                                region: config.regId,
                                time: new Date(),
                                consentConfirmed: consentConfirmed,
                                otId: consentId,
                                otVers: consentVersion
                            }
                        }));
                        doc.dispatchEvent(new CustomEvent('optanonLoaded', {
                            bubbles: false,
                            cancelable: false,
                            detail: {
                                region: config.regId,
                                time: new Date(),
                                consentConfirmed: consentConfirmed,
                                otId: consentId,
                                otVers: consentVersion
                            }
                        }));

                        /* If WebView with external consent, drop any banner pop-ups. */
                        if (config.useExternalConsent && !win.OneTrust.IsAlertBoxClosed()) {
                            win.Optanon.Close();
                        }
                    }

                    /* No need to keep calling this */
                    win.OptanonWrapper = function () {};
                };

                /* Remove the timeout */
                if (win.WBD.UserConsent_wait >= 0) {
                    clearTimeout(win.WBD.UserConsent_wait);
                    win.WBD.UserConsent_wait = -1;
                }

                /* Did OneTrust actually load, or did we timeout/incompletely load? */
                if (!win.OneTrust || typeof win.OneTrust.GetDomainData !== 'function') {
                    /* 1 = Stub load failure, 2 = Initialization failure, 3 = SDK load timeout/fail, 4 = other fail */
                    const code = (!win.OneTrustStub ? 1 : (!win.OneTrustStub.otSdkStub ? 2 : (tos ? 3 : 4))),
                        msg = (code === 1 ? 'Stub load failure' : (code === 2 ? 'Initialization failure' : (code === 3 ? 'SDK load timeout' : 'Unknown error')));
                    /* OneTrust failed to fully load, so reflect that. */
                    optanonLoaded = false;
                    otFailed = true;
                    uclog('error', 'OneTrust Error (', code, '): ', msg);
                    if (dbg) {
                        uclog('debug', 'Dispatching oneTrustFailed event.');
                    }
                    doc.dispatchEvent(new CustomEvent('oneTrustFailed', {
                        bubbles: false,
                        cancelable: false,
                        detail: {
                            region: config.regId,
                            time: new Date(),
                            consentConfirmed: consentConfirmed,
                            otId: consentId,
                            otVers: consentVersion,
                            code: code,
                            msg: msg
                        }
                    }));
                    /* No need to keep calling this */
                    win.OptanonWrapper = function () {};
                    return;
                }

                /* This is the first call, on page load once OneTrust has been loaded */
                optanonLoaded = true;

                /* Make sure we've correctly set the geo-location with OT */
                if (config.geoPassedToOneTrust) {
                    pushGeoLocation();
                }
                /* If using GPP, we need to verify GPP was set, otherwise keep usingOTGpp set to false */
                if (usingIabGpp) {
                    let obj;
                    if (typeof win.__gpp !== 'function') {
                        /* OneTrust loaded, but reset __gpp incorrectly and is broken.  Reset it to our own. */
                        win.__gpp = gppStub;
                    }
                    win.__gpp('ping', function (pd) {
                        if (!pd || pd.cmpId <= 0) {
                            /* OneTrust loaded, but __gpp is not working, so GPP is broken in this OT template */
                            usingIabGpp = false;
                            if (gppCmpApi) {
                                gppCmpApi.setCmpStatus('error');
                                gppCmpApi.fireErrorEvent('CMP did not initialize GPP for this region.');
                            }
                            if (dbg) {
                                uclog('debug', 'OneTrust GPP for this region is broken.  Disabling use of GPP.');
                            }
                        } else if (pd.cmpId === 1) {
                            /* OneTrust loaded, but didn't try to reset __gpp, so GPP is not supported by this OT template. */
                            usingOTGpp = false;
                            if (gppCmpApi) {
                                if (dbg) {
                                    uclog('debug', 'OneTrust did NOT initialize GPP for this region.  Using GPP from UserConsent.');
                                }
                                gppCmpApi.setCmpStatus('loaded');
                                gppCmpApi.fireEvent('cmpStatus', 'loaded');
                                gppCmpApi.setSignalStatus('ready');
                                gppCmpApi.fireEvent('signalStatus', 'ready');
                            } else {
                                usingIabGpp = false;
                                if (dbg) {
                                    uclog('debug', 'OneTrust did NOT initialize GPP for this region.  UserConsent GPP failed to initialize.  Disabling use of GPP.');
                                }
                            }
                        } else {
                            /* OneTrust has loaded, we need to grab the new GPP string, if any, and use that */
                            const cmpStat = pd.cmpStatus;
                            gppCmpId = pd.cmpId;
                            usingOTGpp = true;
                            if (pd.gppVersion && pd.gppVersion === '1.0') {
                                pd = win.__gpp('getGPPData');
                            }
                            if (pd && pd.gppString) {
                                gppStringVer = pd.gppVersion;
                                setGppConsent(pd.gppString, null);
                            }
                            if (dbg) {
                                uclog('debug', 'OneTrust GPP initialized (status "' + cmpStat + '").');
                            }
                        }
                        doLoaded();
                    });
                } else {
                    doLoaded();
                }
            }
        };

        /* Auto-init if we can */
        if (typeof win.WBD.UserConsentConfig === 'object' && win.WBD.UserConsentConfig !== null) {
            init(win.WBD.UserConsentConfig);
        } else if (typeof win.WM.UserConsentConfig === 'object' && win.WM.UserConsentConfig !== null) {
            init(win.WM.UserConsentConfig);
        }
    }

    return {
        addScript: addScript,
        addScriptElement: addScriptElement,
        forceReconsent: forceReconsent,
        getAdChoicesLinkAction: getAdChoicesLinkAction,
        getAdChoicesLinkTitle: getAdChoicesLinkTitle,
        getAffiliatesLinkAction: getAffiliatesLinkAction,
        getAffiliatesLinkTitle: getAffiliatesLinkTitle,
        getCmpString: getCmpString,
        getConsentConfirmed: getConsentConfirmed,
        getConsentHistory: getConsentHistory,
        getConsentState: getConsentState,
        getConsentTime: getConsentTime,
        getConsentVersion: getConsentVersion,
        getGeoCountry: getGeoCountry,
        getGeoState: getGeoState,
        getGppAPIstring: getGppAPIstring,
        getGppSection: getGppSection,
        getIABInterface: getIABInterface,
        getIABRegion: getIABRegion,
        getIABVersion: getIABVersion,
        getLinkAction: getLinkAction,
        getLinkTitle: getLinkTitle,
        getPrivacyCenterLinkAction: getPrivacyCenterLinkAction,
        getPrivacyCenterLinkTitle: getPrivacyCenterLinkTitle,
        getRegion: getRegion,
        getReloadOnChange: getReloadOnChange,
        getReloadOnConsentReduction: getReloadOnConsentReduction,
        getRightsRequestLinkAction: getRightsRequestLinkAction,
        getRightsRequestLinkTitle: getRightsRequestLinkTitle,
        getSimpleConsentState: getSimpleConsentState,
        getTcfAPIaddtlString: getTcfAPIaddtlString,
        getTcfAPIstring: getTcfAPIstring,
        getUserConsentAdvertisingState: getUserConsentAdvertisingState,
        getUspAPIstring: getUspAPIstring,
        getVersion: getVersion,
        init: init,
        inUserConsentState: inUserConsentState,
        isChild: isChild,
        isTop: isTop,
        isEnabled: isEnabled,
        isGpcInUse: isGpcInUse,
        isGpcSet: isGpcSet,
        isInCcpaRegion: isInCcpaRegion,
        isInGdprRegion: isInGdprRegion,
        isInGppRegion: isInGppRegion,
        isInIabRegion: isInIabRegion,
        isInRegion: isInRegion,
        isOneTrustBlocked: isOneTrustBlocked,
        isOneTrustFailing: isOneTrustFailing,
        isOneTrustLoaded: isOneTrustLoaded,
        isOptanonLoaded: isOneTrustLoaded,
        isReady: isReady,
        isSiteIABCompliant: isSiteIABCompliant,
        usingCompatConsent: usingCompatConsent,
        usingExternalConsent: usingExternalConsent,
        usingGPP: usingGPP,
        usingIAB: usingIAB,
        usingPSM: usingPSM
    };
})(window, document);

/* For compatibility... */
window.WM = window.WBD;

