/* UserConsent Wrapper to produce self-configured, self-invoking script */

import window from 'window';
import document from 'document';

__ucBrandConfig__

/* Set-up event-handling with Zion, tweak config when necessary */
function handleConsentChanges(evt) {
    /* Log consent changes with Zion */
    if (evt && evt.detail && window.zion_analytics && window.zion_analytics.isReady && window.ZION_SDK && window.ZION_SDK.V2) {
        window.zion_analytics.track(new ZION_SDK.V2.DeviceChangedConsentPreferences({
            consent_id: evt.detail.otId,
            consent_interaction: evt.detail.otIact,
            consent_gpc_active: evt.detail.gpcActive,
            consent_region: evt.detail.region,
            consent_state: window.WBD.UserConsent.getSimpleConsentState(),
            consent_version: evt.detail.otVers + '|' + window.WBD.UserConsent.getVersion() + '|GPP' + evt.detail.gppVers + '|TCF' + evt.detail.tcfVers
        }));
    }
}

function handleConsentErrors(evt) {
    /* Log consent-related errors with Zion */
    if (evt && evt.detail && window.zion_analytics && window.zion_analytics.isReady && window.ZION_SDK && window.ZION_SDK.V2) {
        let code = 9,
            msg = 'Blocked';

        if (evt.detail.code) {
            code = evt.detail.code;
            msg = evt.detail.msg || 'Unknown error';
        }
        window.zion_analytics.track(new ZION_SDK.V2.ConsentError({
            consent_error_code: code,
            consent_error_message: msg,
            consent_region: evt.detail.region,
            consent_version: evt.detail.otVers + '|' + window.WBD.UserConsent.getVersion() + '||'
        }));
    }
}

document.addEventListener('userConsentChanged', handleConsentChanges, false);
document.addEventListener('oneTrustFailed', handleConsentErrors, false);
document.addEventListener('oneTrustBlocked', handleConsentErrors, {once: true});

/* Tweak the UserConsent config to get the local src right for different hosts */
if (window.location && window.location.hostname && window.WBD.UserConsentConfig && window.WBD.UserConsentConfig.domId) {
    if (window.location.hostname.search(/^(?:www|us|edition|cnnespanol|cnne-stage|lite|dev-lite|(?:develop\.)?arabic)\.cnn\.com$/i) === 0 || window.location.hostname.search(/stage\d?\.cnn\.com$/i) >= 0) {
        window.WBD.UserConsentConfig.src = '/wbdot';
    } else {
        window.WBD.UserConsentConfig.src = 'https://us.cnn.com/wbdot';
    }
    window.WBD.UserConsentConfig.src += (window.WBD.UserConsentConfig.domId.startsWith('0c1') ? 's' : 'p') + '/scripttemplates/otSDKStub.js';
}

__ucCode__
