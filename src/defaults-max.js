
/* Default UserConsent configuration for MAX configs. */
/* See https://github.com/turnercode/user-consent#readme for details. */
{
    addtlConsentCookie: 'OTAdditionalConsentString',
    adChoicesLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    adChoicesLinkTitle: {
        ar: 'اختيارات الإعلان',
        en: 'Ad Choices',
        de: 'Anzeigenauswahl',
        es: 'Elecciones de anuncios',
        fr: 'Choix d’annonces'
    },
    affiliatesLinkAction: 'https://www.wbdprivacy.com/policycenter/affiliates/',
    affiliatesLinkTitle: {
        ar: 'الشركات التابعة',
        en: 'Affiliates',
        de: 'Mitgliedsorganisationen',
        es: 'Afiliadas',
        fr: 'Affiliées'
    },
    categories: {
        sc: 'required',            /* Built-in */
        tpv: 'vendor',
        fc: 'functional',
        mc: 'media',
        pc: 'performance',
        tc: '3p-targeting',
        ftc: '1p-targeting',
        pfc: 'vendor-perf-func'
    },
    ccCookie: 'countryCode',
    ccpaGeos: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
    compatCategories: {
        'targeting': ['3p-targeting', '1p-targeting']
    },
    confirmCookie: 'OptanonAlertBoxClosed',
    consentChangeAction: null,
    consentChangeActionDelay: 500,
    consentCookie: 'OptanonConsent',
    consentDefaults: {
        'required': true,
        'vendor': true,
        'functional': true,
        'media': true,
        'performance': true,
        '3p-targeting': true,
        '1p-targeting': true,
        'vendor-perf-func': true
    },
    consentExpireIn: 1,
    consentLinkTitle: {
        ar: 'ملفات تعريف الارتباط',
        de: 'Cookie-Einstellungen',
        en: 'Cookie Settings',
        es: 'Configuración de Cookies',
        fr: 'Paramètres des Cookies'
    },
    consentNotApplicable: [],
    controlCookie: 'OptanonControl',
    cookieSameSite: 'Lax',
    cookieSecure: false,
    enableDebug: false,
    enableGPC: true,
    enableTransitionCheck: true,
    enableWebViewCheck: true,
    defaultCountry: 'US',
    defaultLanguage: 'en',
    defaultState: '',
    gdprIabCookie: 'eupubconsent-v2',
    geoCheckFunction: null,
    geoPassedToOneTrust: true,
    gpcFixCookie: '',
    gppCategories: {
        usnat: [
            {
                field: 'SharingNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SaleOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'TargetedAdvertisingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOut',
                type: 'trinary',
                val: 'vendor'
            },
            {
                field: 'SaleOptOut',
                type: 'trinary',
                val: 'vendor'
            },
            {
                field: 'TargetedAdvertisingOptOut',
                type: 'trinary',
                val: '3p-targeting'
            }
        ],
        uspv1: [
            {
                field: 'OptOutSale',
                type: 'binary',
                val: ['vendor', '3p-targeting']
            }
        ]
    },
    gppIabCookie: 'OTGPPConsent',
    gppSection: '',
    iabRegion: '',
    languageFromBrowser: true,
    oneTrustLoadTimeout: 10000,
    privacyCenterLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    privacyCenterLinkTitle: {
        ar: 'سياسة خصوصية المستهلك',
        de: 'Datenschutzhinweise',
        en: 'Privacy Policy',
        es: 'Política de Privacidad',
        fr: 'Politique de Confidentialité'
    },
    regionChangeAction: null,
    regions: [
        {
            id: 'us',
            compatCodes: {
                req: ['sc'],
                ven: ['tpv']
            },
            consentExpireIn: 3,
            consentGpcDefaults: {
                'vendor': false,
                '3p-targeting': false
            },
            consentImpliedDefaults: {
                'required': true,
                'functional': true,
                'media': true,
                'performance': true,
                '1p-targeting': true,
                'vendor-perf-func': true
            },
            consentLinkTitle: {
                ar: 'لا تبيع أو تشارك معلوماتي الشخصية',
                de: 'Nicht Verkauf oder Nicht Weitergabe Ihrer personenbezogenen Daten zu stellen',
                en: 'Do Not Sell Or Share My Personal Information',
                es: 'No Venda Vi Comparta Mi Información Personal',
                fr: 'Ne pas vendre ni partager mes informations personnelles'
            },
            rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/usstatesupplement/en-us/',
            geoMatch: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
            gppSection: 'usnat',  /* usnat */
            iabRegion: 'ccpa'
        },
        {
            id: 'gdpr',
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                'functional': false,
                'media': false,
                'performance': false,
                '3p-targeting': false,
                '1p-targeting': false,
                'vendor-perf-func': false
            },
            consentImpliedDefaults: {
                'vendor': true
            },
            consentLinkTitle: {
                ar: 'إدارة ملفات تعريف الارتباط+',
                de: 'Cookies Verwalten+',
                en: 'Manage Cookies+',
                es: 'Administrar cookies+',
                fr: 'Gérer les Cookies+'
            },
            geoMatch: [
                'GB', 'DE', 'FR', 'IT', 'ES', 'PL', 'RO', 'NL', 'BE', 'GR', 'CZ', 'PT', 'SE', 'HU', 'AT', 'BG', 'DK', 'FI', 'SK', 'IE', 'HR', 'LT', 'SI', 'LV', 'EE', 'CY', 'LU', 'MT', 'NO', 'IS', 'LI', /* EU - GDPR */
                'CH', 'SJ', 'MQ', 'PM', 'XK', 'GL', 'FO', 'MF', 'MD', 'MK', 'YT', 'WF', 'BL', 'PF', 'NC', 'GP', 'GF', 'ME', 'RS', 'BA', 'AD', 'IC', 'AX', 'MC', 'TR' /* EU - Territories */
            ],
            iabRegion: 'gdpr'
        },
        {
            id: 'other-optin',
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                '3p-targeting': false,
                'vendor-perf-func': false
            },
            consentImpliedDefaults: {
                'vendor': true,
                'functional': true,
                'media': true,
                'performance': true,
                '1p-targeting': true
            },
            geoMatch: ['AR', 'BR', 'CL', 'CO', 'CR', 'PA', 'PE', 'UY']  /* LatAm Opt-In */
        },
        {
            id: 'other-optout',
            consentImpliedDefaults: {
                'vendor': true,
                'functional': true,
                'media': true,
                'performance': true,
                '1p-targeting': true
            },
            geoMatch: ['AG', 'AI', 'AW', 'BB', 'BS', 'BZ', 'DM', 'DO', 'MX', 'NI', 'PY', 'SR', 'VE', 'VG',  /* LatAm Opt-Out */
                       'HK', 'SG']  /* APAC Opt-Out */
        },
        {
            id: 'global',
            geoMatch: ['*'],  /* Matches everything not yet matched */
            useFixedConsent: true
        }
    ],
    reloadOnConsentChange: false,
    reloadOnConsentReduction: true,
    rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    rightsRequestLinkTitle: {
        ar: 'بوابة طلبات الحقوق الفردية',
        de: 'Anfrageportal für Individualrechte',
        en: 'Individual Rights Request Portal',
        es: 'Portal de solicitud de derechos individuales',
        fr: 'Portail de demande de droits des individus'
    },
    scCookie: 'stateCode',
    setPageClass: false,
    src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
    strictIabCompliance: false,  /* False unless using all the IAB categories */
    tcfOpts: {
        categories: {
            purposes: [
                ['functional', 'performance', '3p-targeting', '1p-targeting', 'vendor-perf-func'],  /* GDPR Purpose 1 */
                '1p-targeting',         /* GDPR Purpose 2 */
                '3p-targeting',         /* GDPR Purpose 3 */
                '3p-targeting',         /* GDPR Purpose 4 */
                'unused',               /* GDPR Purpose 5 */
                'unused',               /* GDPR Purpose 6 */
                ['functional', 'performance', 'vendor-perf-func'],  /* GDPR Purpose 7 */
                'unused',               /* GDPR Purpose 8 */
                'unused',               /* GDPR Purpose 9 */
                'unused',               /* GDPR Purpose 10 */
                'unused'                /* GDPR Purpose 11 */
            ],
            specialPurposes: [
                'required',             /* GDPR Special Purpose 1 */
                'required',             /* GDPR Special Purpose 2 */
                'required'              /* GDPR Special Purpose 3 */
            ],
            features: [
                'required',             /* GDPR Feature 1 */
                'required',             /* GDPR Feature 2 */
                'required'              /* GDPR Feature 3 */
            ],
            specialFeatures: [
                'unused',               /* GDPR Special Feature 1 */
                'unused'                /* GDPR Special Feature 2 */
            ]
        },
        policies: {
            2: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            3: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            4: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            },
            5: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            }
        }
    },
    ucFlavor: 'max',
    useFixedConsent: false,
    useGPP: true,
    useIAB: true,
    useIabString: true,
    uspApiCookieName: 'usprivacy',
    uspApiExplicitNotice: true,
    uspApiIsLspa: false
}

