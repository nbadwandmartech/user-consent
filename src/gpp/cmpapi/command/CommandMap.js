var _a, _b, _c, _d, _e, _f;
import { PingCommand } from "./PingCommand.js";
import { GetFieldCommand } from "./GetFieldCommand.js";
import { GetSectionCommand } from "./GetSectionCommand.js";
import { HasSectionCommand } from "./HasSectionCommand.js";
import { GppCommand } from "./GppCommand.js";
import { AddEventListenerCommand } from "./AddEventListenerCommand.js";
import { RemoveEventListenerCommand } from "./RemoveEventListenerCommand.js";
export class CommandMap {
}
_a = GppCommand.ADD_EVENT_LISTENER, _b = GppCommand.GET_FIELD, _c = GppCommand.GET_SECTION, _d = GppCommand.HAS_SECTION, _e = GppCommand.PING, _f = GppCommand.REMOVE_EVENT_LISTENER;
CommandMap[_a] = AddEventListenerCommand;
CommandMap[_b] = GetFieldCommand;
CommandMap[_c] = GetSectionCommand;
CommandMap[_d] = HasSectionCommand;
CommandMap[_e] = PingCommand;
CommandMap[_f] = RemoveEventListenerCommand;
