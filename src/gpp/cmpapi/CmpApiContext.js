import { CmpStatus } from "./status/CmpStatus.js";
import { CmpDisplayStatus } from "./status/CmpDisplayStatus.js";
import { EventListenerQueue } from "./EventListenerQueue.js";
import { GppModel } from "../encoder/GppModel.js";
/*
import { UsCa } from "../encoder/section/UsCa.js";
import { UsVa } from "../encoder/section/UsVa.js";
import { UsCo } from "../encoder/section/UsCo.js";
import { UsUt } from "../encoder/section/UsUt.js";
import { UsCt } from "../encoder/section/UsCt.js";
*/
import { SignalStatus } from "./status/SignalStatus.js";
/**
 * Class holds shareable data across cmp api and provides change event listener for GppModel.
 * Within the context of the CmpApi, this class acts much like a global state or database,
 * where CmpApi sets data and Commands read the data.
 */
export class CmpApiContext {
    constructor() {
        this.gppVersion = "1.1";
        this.supportedAPIs = [];
        this.eventQueue = new EventListenerQueue(this);
        this.cmpStatus = CmpStatus.LOADING;
        this.cmpDisplayStatus = CmpDisplayStatus.HIDDEN;
        this.signalStatus = SignalStatus.NOT_READY;
        this.applicableSections = [];
        this.gppModel = new GppModel();
    }
    reset() {
        this.eventQueue.clear();
        this.cmpStatus = CmpStatus.LOADING;
        this.cmpDisplayStatus = CmpDisplayStatus.HIDDEN;
        this.signalStatus = SignalStatus.NOT_READY;
        this.applicableSections = [];
        this.supportedAPIs = [];
        this.gppModel = new GppModel();
        delete this.cmpId;
        delete this.cmpVersion;
        delete this.eventStatus;
    }
}
