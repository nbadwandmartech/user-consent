
/* Default UserConsent configuration for fully IAB-compliant configs. */
/* See https://github.com/turnercode/user-consent#readme for details. */
{
    addtlConsentCookie: 'OTAdditionalConsentString',
    adChoicesLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    adChoicesLinkTitle: {
        ar: 'اختيارات الإعلان',
        de: 'Anzeigenauswahl',
        en: 'Ad Choices',
        es: 'Elecciones de anuncios',
        fr: 'Choix d’annonces'
    },
    affiliatesLinkAction: 'https://www.wbdprivacy.com/policycenter/affiliates/',
    affiliatesLinkTitle: {
        ar: 'الشركات التابعة',
        en: 'Affiliates',
        de: 'Mitgliedsorganisationen',
        es: 'Afiliadas',
        fr: 'Affiliées'
    },
    categories: {
        req: 'required',            /* Built-in */
        dsa: 'data-store',          /* GDPR Purpose 1 */
        cad: 'ads-contextual',      /* GDPR Purpose 2 */
        pap: 'ads-person-prof',     /* GDPR Purpose 3 / US GPP Targeted Ads */
        pad: 'ads-person',          /* GDPR Purpose 4 / US GPP Targeted Ads */
        pcp: 'content-person-prof', /* GDPR Purpose 5 */
        pcd: 'content-person',      /* GDPR Purpose 6 */
        map: 'measure-ads',         /* GDPR Purpose 7 */
        mcp: 'measure-content',     /* GDPR Purpose 8 */
        mra: 'measure-market',      /* GDPR Purpose 9 */
        pdd: 'product-develop',     /* GDPR Purpose 10 */
        ccd: 'content-contextual',  /* GDPR Purpose 11 */
        sec: 'product-security',    /* GDPR Special Purpose 1 */
        tdc: 'deliver-content',     /* GDPR Special Purpose 2 */
        scp: 'privacy-choices',     /* GDPR Special Purpose 3 */
        cos: 'combine-data',        /* GDPR Feature 1 */
        dlk: 'link-devices',        /* GDPR Feature 2 */
        did: 'id-devices',          /* GDPR Feature 3 */
        gld: 'geolocate',           /* GDPR Special Feature 1 / US GPP Sensitive Data 8 - Geolocate */
        sid: 'scan-devices',        /* GDPR Special Feature 2 */
        ftc: '1p-targeting',        /* EMEA First-Party Targeting */
        dsh: 'data-share',          /* US GPP Data Sharing */
        dsl: 'data-sell',           /* US GPP Data Selling */
        pdu: 'personal-data',       /* US GPP Personal Data Usage */
        kc12: 'known-child-12',     /* US GPP Known Child 1 - 0-12 */
        kc16: 'known-child-16',     /* US GPP Known Child 2 - 13-16 */
        sdre: 'sensitive-racial',   /* US GPP Sensitive Data 1 - Racial, Ethnic */
        sdrb: 'sensitive-belief',   /* US GPP Sensitive Data 2 - Religion, Beliefs */
        sdhe: 'sensitive-health',   /* US GPP Sensitive Data 3 - Personal Health Data */
        sdso: 'sensitive-sexual',   /* US GPP Sensitive Data 4 - Sexual Orientation, Preference */
        sdir: 'sensitive-citizen',  /* US GPP Sensitive Data 5 - Citizenship, Immigration, Residency */
        sdge: 'sensitive-gene',     /* US GPP Sensitive Data 6 - Genetic */
        sdbm: 'sensitive-biometric',/* US GPP Sensitive Data 7 - Biometric */
        sdsp: 'sensitive-spi',      /* US GPP Sensitive Data 9 - Sensitive Personal Info */
        sdss: 'sensitive-ssi',      /* US GPP Sensitive Data 10 - Sensitive Security Info */
        sduo: 'sensitive-org',      /* US GPP Sensitive Data 11 - Union and organization membership */
        sdco: 'sensitive-comm',     /* US GPP Sensitive Data 12 - Communication */
        sdch: 'sensitive-conhealth', /* US GPP Sensitive Data 13 - Consumer Health Data */
        sdcv: 'sensitive-victim',   /* US GPP Sensitive Data 14 - Crime Victim status */
        sdno: 'sensitive-origin',   /* US GPP Sensitive Data 15 - National Origin */
        sdsx: 'sensitive-extsex'     /* US GPP Sensitive Data 16 - Extended Sexual Identity, Trans, Non-binary */
    },
    ccCookie: 'countryCode',
    ccpaGeos: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],  /* During transition to GPP */
    compatCategories: {
        'vendor': ['data-share', 'data-sell', 'ads-person-prof', 'ads-person'],
        'targeted-ads': ['ads-person-prof', 'ads-person'],
        'sensitive-geo': ['geolocate']
    },
    confirmCookie: 'OptanonAlertBoxClosed',
    consentChangeAction: null,
    consentChangeActionDelay: 500,
    consentCookie: 'OptanonConsent',
    consentDefaults: {
        'required': true,
        'data-store': true,
        'ads-contextual': true,
        'ads-person': true,
        'ads-person-prof': true,
        'content-person': true,
        'content-person-prof': true,
        'measure-ads': true,
        'measure-content': true,
        'measure-market': true,
        'product-develop': true,
        'content-contextual': true,
        'product-security': true,
        'deliver-content': true,
        'privacy-choices': true,
        'combine-data': true,
        'link-devices': true,
        'id-devices': true,
        'geolocate': false,
        'scan-devices': false,
        '1p-targeting': true,
        'data-share': true,
        'data-sell': true,
        'personal-data': false,
        'known-child-12': false,
        'known-child-16': false,
        'sensitive-racial': false,
        'sensitive-belief': false,
        'sensitive-health': false,
        'sensitive-sexual': false,
        'sensitive-citizen': false,
        'sensitive-gene': false,
        'sensitive-biometric': false,
        'sensitive-spi': false,
        'sensitive-ssi': false,
        'sensitive-org': false,
        'sensitive-comm': false,
        'sensitive-conhealth': false,
        'sensitive-victim': false,
        'sensitive-origin': false,
        'sensitive-extsex': false
    },
    consentExpireIn: 1,
    consentNotApplicable: [
        'personal-data',
        'known-child-12',
        'known-child-16',
        'sensitive-racial',
        'sensitive-belief',
        'sensitive-health',
        'sensitive-sexual',
        'sensitive-citizen',
        'sensitive-gene',
        'sensitive-biometric',
        'geolocate',
        'sensitive-spi',
        'sensitive-ssi',
        'sensitive-org',
        'sensitive-comm',
        'sensitive-conhealth',
        'sensitive-victim',
        'sensitive-origin',
        'sensitive-extsex'
    ],
    consentLinkTitle: {
        ar: 'ملفات تعريف الارتباط',
        de: 'Cookie-Einstellungen',
        en: 'Cookie Settings',
        es: 'Configuración de Cookies',
        fr: 'Paramètres des Cookies'
    },
    controlCookie: 'OptanonControl',
    cookieSameSite: 'Lax',
    cookieSecure: false,
    defaultCountry: 'US',
    defaultLanguage: 'en',
    defaultState: '',
    enableDebug: false,
    enableGPC: true,
    enableTransitionCheck: true,
    enableWebViewCheck: true,
    gdprIabCookie: 'eupubconsent-v2',
    geoCheckFunction: null,
    geoPassedToOneTrust: true,
    gpcFixCookie: '',
    gppCategories: {
        /* To be added when OneTrust supports it...
        tcfeuv2: [
            { 
                field: 'PurposeConsents',
                type: 'array-binary',
                maxCount: 10,
                0: 'data-store',
                1: 'ads-contextual',
                2: 'ads-person-prof',
                3: 'ads-person',
                4: 'content-person-prof',
                5: 'content-person',
                6: 'measure-ads',
                7: 'measure-content',
                8: 'measure-market',
                9: 'product-develop',
                10: 'content-contextual'
            },
            {
                field: 'PurposesLITransparency',
                type: 'array-binary',
                maxCount: 10
            },
            {
                field: 'SpecialFeatureOptIns',
                type: 'array-binary',
                maxCount: 2,
                0: 'geolocate',
                1: 'scan-devices'
            }
        ], */
        /* To be added when OneTrust supports it...
        tcfcav1: [
            {
                field: 'PurposesExpressConsent',
                type: 'array-binary',
                maxCount: 10,
                0: 'data-store',
                1: 'ads-contextual',
                2: 'ads-person-prof',
                3: 'ads-person',
                4: 'content-person-prof',
                5: 'content-person',
                6: 'measure-ads',
                7: 'measure-content',
                8: 'measure-market',
                9: 'product-develop',
                10: 'content-contextual'
            },
            {
                field: 'PurposesImpliedConsent',
                type: 'array-binary',
                maxCount: 10
            },
            {
                field: 'SpecialFeatureExpressConsent',
                type: 'array-binary',
                maxCount: 2,
                0: 'geolocate',
                1: 'scan-devices'
            }
        ], */
        usnat: [
            {
                field: 'SharingNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SaleOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'TargetedAdvertisingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOut',
                type: 'trinary',
                val: 'data-share'
            },
            {
                field: 'SaleOptOut',
                type: 'trinary',
                val: 'data-sell'
            },
            {
                field: 'TargetedAdvertisingOptOut',
                type: 'trinary',
                val: ['ads-person-prof', 'ads-person']
            },
            {
                field: 'PersonalDataConsents',
                type: 'trinary',
                default: 0,
                val: 'personal-data'
            },
            {
                field: 'KnownChildSensitiveDataConsents',
                type: 'array-trinary',
                default: [0, 0],
                maxCount: 2,
                0: 'known-child-12',
                1: 'known-child-16'
            },
            {
                field: 'SensitiveDataProcessing',
                type: 'array-trinary',
                default: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                maxCount: 12,
                0: 'sensitive-racial',
                1: 'sensitive-belief',
                2: 'sensitive-health',
                3: 'sensitive-sexual',
                4: 'sensitive-citizen',
                5: 'sensitive-gene',
                6: 'sensitive-biometric',
                7: 'geolocate',
                8: 'sensitive-spi',
                9: 'sensitive-ssi',
                10: 'sensitive-org',
                11: 'sensitive-comm',
                12: 'sensitive-conhealth',
                13: 'sensitive-victim',
                14: 'sensitive-origin',
                15: 'sensitive-extsex'
            }
        ],
        uspv1: [
            {
                field: 'OptOutSale',
                type: 'binary',
                val: ['data-share', 'data-sell', 'ads-person-prof', 'ads-person']
            }
        ]
    },
    gppIabCookie: 'OTGPPConsent',
    gppSection: '',
    iabRegion: '',
    languageFromBrowser: true,
    oneTrustLoadTimeout: 10000,
    privacyCenterLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    privacyCenterLinkTitle: {
        ar: 'سياسة خصوصية المستهلك',
        de: 'Datenschutzhinweise',
        en: 'Privacy Policy',
        es: 'Política de Privacidad',
        fr: 'Politique de Confidentialité'
    },
    regionChangeAction: null,
    regions: [
        {
            id: 'us',
            compatCodes: {
                ven: ['dsh', 'dsl', 'pap', 'pad'],
                tpv: ['dsh', 'dsl', 'pap', 'pad']
            },
            compatTransition: {
                'cond': false,
                'new': ['dsh', 'dsl', 'pap', 'pad'],
                'old': 'ven'
            },
            consentExpireIn: 3,
            consentGpcDefaults: {
                'data-share': false,
                'data-sell': false,
                'ads-person-prof': false,
                'ads-person': false
            },
            consentImpliedDefaults: {
                'data-store': true,
                'ads-contextual': true,
                'content-person': true,
                'content-person-prof': true,
                'measure-ads': true,
                'measure-content': true,
                'measure-market': true,
                'product-develop': true,
                'content-contextual': true,
                'product-security': true,
                'deliver-content': true,
                'privacy-choices': true,
                'combine-data': true,
                'link-devices': true,
                'id-devices': true,
                '1p-targeting': true,
                'geolocate': false,
                'scan-devices': false
            },
            consentLinkTitle: {
                ar: 'لا تبيع أو تشارك معلوماتي الشخصية',
                de: 'Nicht Verkauf oder Nicht Weitergabe Ihrer personenbezogenen Daten zu stellen',
                en: 'Do Not Sell Or Share My Personal Information',
                es: 'No Venda Vi Comparta Mi Información Personal',
                fr: 'Ne pas vendre ni partager mes informations personnelles'
            },
            rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/usstatesupplement/en-us/',
            geoMatch: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
            gppSection: 'usnat',  /* usnat */
            iabRegion: 'ccpa'
        },
        {
            id: 'gdpr',
            consentDefaults: {  /* Setting this here will override the specified top-level defaults when in this region */
                'data-store': false,          /* Consent */
                'ads-contextual': false,      /* Consent */
                'ads-person-prof': false,     /* Consent */
                'ads-person': false,          /* Consent */
                'content-person-prof': false, /* Consent */
                'content-person': false,      /* Consent */
                'measure-ads': false,         /* Consent */
                'measure-content': false,     /* Consent */
                'measure-market': false,      /* Consent */
                'product-develop': false,     /* Consent */
                'content-contextual': false,  /* Consent */
                'combine-data': false,        /* Notice Only */
                'link-devices': false,        /* Notice Only */
                'id-devices': false           /* Notice Only */
            },
            consentImpliedDefaults: {
                'product-security': true,     /* Notice Only */
                'deliver-content': true,      /* Notice Only */
                'privacy-choices': true,      /* Notice Only */
                'combine-data': true,         /* Notice Only */
                'link-devices': true,         /* Notice Only */
                'id-devices': true,           /* Notice Only */
                '1p-targeting': true,         /* Notice Only */
                'geolocate': false,           /* Not Used by WBD */
                'scan-devices': false,        /* Not Used by WBD */
                'data-share': true,           /* Required (US GPP) */
                'data-sell': true             /* Required (US GPP) */
            },
            consentLinkTitle: {
                ar: 'إدارة ملفات تعريف الارتباط+',
                de: 'Cookies Verwalten+',
                en: 'Manage Cookies+',
                es: 'Administrar cookies+',
                fr: 'Gérer les Cookies+'
            },
            geoMatch: ['*'], // geoMatch: ['GB', 'DE', 'FR', 'IT', 'ES', 'PL', 'RO', 'NL', 'BE', 'GR', 'CZ', 'PT', 'SE', 'HU', 'AT', 'BG', 'DK', 'FI', 'SK', 'IE', 'HR', 'LT', 'SI', 'LV', 'EE', 'CY', 'LU', 'MT', 'NO', 'IS', 'LI', 'CH'] /* GDPR */
            /* Waiting on GPP support from OneTrust...
            gppSection: 'tcfeuv2', */
            iabRegion: 'gdpr'
        // },
        // {
        //     id: 'other-optin',
        //     consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
        //         'data-store': false,          /* Consent */
        //         'ads-contextual': false,      /* Consent */
        //         'ads-person-prof': false,     /* Consent */
        //         'ads-person': false,          /* Consent */
        //         'content-person-prof': false, /* Consent */
        //         'content-person': false,      /* Consent */
        //         'measure-ads': false,         /* Consent */
        //         'measure-content': false,     /* Consent */
        //         'measure-market': false,      /* Consent */
        //         'product-develop': false,     /* Consent */
        //         'content-contextual': false,  /* Consent */
        //         'combine-data': false,        /* Notice Only */
        //         'link-devices': false,        /* Notice Only */
        //         'id-devices': false           /* Notice Only */
        //     },
        //     consentImpliedDefaults: {
        //         'product-security': true,     /* Notice Only */
        //         'deliver-content': true,      /* Notice Only */
        //         'privacy-choices': true,      /* Notice Only */
        //         'combine-data': true,         /* Notice Only */
        //         'link-devices': true,         /* Notice Only */
        //         'id-devices': true,           /* Notice Only */
        //         '1p-targeting': true,         /* Notice Only */
        //         'geolocate': false,           /* Not Used by WBD */
        //         'scan-devices': false,        /* Not Used by WBD */
        //         'data-share': true,           /* Required (US GPP) */
        //         'data-sell': true             /* Required (US GPP) */
        //     },
        //     geoMatch: ['CO', 'UY', 'PE', 'AR', 'CR', 'CL']
        // },
        // {
        //     id: 'other-optout',
        //     consentImpliedDefaults: {
        //         'product-security': true,     /* Notice Only */
        //         'deliver-content': true,      /* Notice Only */
        //         'privacy-choices': true,      /* Notice Only */
        //         'combine-data': true,         /* Notice Only */
        //         'link-devices': true,         /* Notice Only */
        //         'id-devices': true,           /* Notice Only */
        //         '1p-targeting': true,         /* Notice Only */
        //         'geolocate': false,           /* Not Used by WBD */
        //         'scan-devices': false,        /* Not Used by WBD */
        //         'data-share': true,           /* Required (US GPP) */
        //         'data-sell': true             /* Required (US GPP) */
        //     },
        //     geoMatch: ['MX', 'PY', 'BR', 'VE', 'NI']
        // },
        // {
        //     id: 'global',
        //     geoMatch: ['*'],  /* Matches everything not yet matched */
        //     useFixedConsent: true
        }
    ],
    reloadOnConsentChange: true,
    reloadOnConsentReduction: false,
    rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    rightsRequestLinkTitle: {
        ar: 'بوابة طلبات الحقوق الفردية',
        de: 'Anfrageportal für Individualrechte',
        en: 'Individual Rights Request Portal',
        es: 'Portal de solicitud de derechos individuales',
        fr: 'Portail de demande de droits des individus'
    },
    scCookie: 'stateCode',
    setPageClass: true,
    src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
    strictIabCompliance: true,  /* True because we use all the IAB categories */
    tcfOpts: {
        categories: {
            purposes: [
                'data-store',           /* GDPR Purpose 1 */
                'ads-contextual',       /* GDPR Purpose 2 */
                'ads-person-prof',      /* GDPR Purpose 3 */
                'ads-person',           /* GDPR Purpose 4 */
                'content-person-prof',  /* GDPR Purpose 5 */
                'content-person',       /* GDPR Purpose 6 */
                'measure-ads',          /* GDPR Purpose 7 */
                'measure-content',      /* GDPR Purpose 8 */
                'measure-market',       /* GDPR Purpose 9 */
                'product-develop',      /* GDPR Purpose 10 */
                'content-contextual'    /* GDPR Purpose 11 */
            ],
            specialPurposes: [
                'product-security',     /* GDPR Special Purpose 1 */
                'deliver-content',      /* GDPR Special Purpose 2 */
                'privacy-choices'       /* GDPR Special Purpose 3 */
            ],
            features: [
                'combine-data',         /* GDPR Feature 1 */
                'link-devices',         /* GDPR Feature 2 */
                'id-devices'            /* GDPR Feature 3 */
            ],
            specialFeatures: [
                'geolocate',            /* GDPR Special Feature 1 */
                'scan-devices'          /* GDPR Special Feature 2 */
            ]
        },
        policies: {
            2: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            3: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            4: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            },
            5: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            }
        }
    },
    ucFlavor: 'iab',
    useFixedConsent: false,
    useGPP: true,
    useIAB: true,
    useIabString: true,
    uspApiCookieName: 'usprivacy',
    uspApiExplicitNotice: true,
    uspApiIsLspa: false
}

