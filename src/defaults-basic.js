
/* Default UserConsent configuration for basic WBD configs. */
/* See https://github.com/turnercode/user-consent#readme for details. */
{
    addtlConsentCookie: 'OTAdditionalConsentString',
    adChoicesLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    adChoicesLinkTitle: {
        ar: 'اختيارات الإعلان',
        en: 'Ad Choices',
        de: 'Anzeigenauswahl',
        es: 'Elecciones de anuncios',
        fr: 'Choix d’annonces'
    },
    affiliatesLinkAction: 'https://www.wbdprivacy.com/policycenter/affiliates/',
    affiliatesLinkTitle: {
        ar: 'الشركات التابعة',
        en: 'Affiliates',
        de: 'Mitgliedsorganisationen',
        es: 'Afiliadas',
        fr: 'Affiliées'
    },
    categories: {
        sc: 'required',            /* Built-in */
        tpv: 'vendor',
        fc: 'functional',
        mc: 'media',
        pc: 'performance',
        tc: 'targeting',
        ftc: '1p-targeting'
    },
    ccCookie: 'countryCode',
    ccpaGeos: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
    confirmCookie: 'OptanonAlertBoxClosed',
    consentChangeAction: null,
    consentChangeActionDelay: 500,
    consentCookie: 'OptanonConsent',
    consentDefaults: {
        'required': true,
        'vendor': true,
        'functional': true,
        'media': true,
        'performance': true,
        'targeting': true,
        '1p-targeting': true
    },
    consentExpireIn: 1,
    consentLinkTitle: {
        ar: 'ملفات تعريف الارتباط',
        de: 'Cookie-Einstellungen',
        en: 'Cookie Settings',
        es: 'Configuración de Cookies',
        fr: 'Paramètres des Cookies'
    },
    consentNotApplicable: [],
    controlCookie: 'OptanonControl',
    cookieSameSite: 'Lax',
    cookieSecure: false,
    enableDebug: false,
    enableGPC: true,
    enableTransitionCheck: true,
    enableWebViewCheck: true,
    defaultCountry: 'US',
    defaultLanguage: 'en',
    defaultState: '',
    gdprIabCookie: 'eupubconsent-v2',
    geoCheckFunction: null,
    geoPassedToOneTrust: true,
    gpcFixCookie: '',
    gppCategories: {
        usnat: [
            {
                field: 'SharingNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SaleOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'TargetedAdvertisingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOut',
                type: 'trinary',
                val: 'vendor'
            },
            {
                field: 'SaleOptOut',
                type: 'trinary',
                val: 'vendor'
            },
            {
                field: 'TargetedAdvertisingOptOut',
                type: 'trinary',
                val: 'targeting'
            }
        ],
        uspv1: [
            {
                field: 'OptOutSale',
                type: 'binary',
                val: ['vendor', 'targeting']
            }
        ]
    },
    gppIabCookie: 'OTGPPConsent',
    gppSection: '',
    iabRegion: '',
    languageFromBrowser: true,
    oneTrustLoadTimeout: 10000,
    privacyCenterLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    privacyCenterLinkTitle: {
        ar: 'سياسة خصوصية المستهلك',
        de: 'Datenschutzhinweise',
        en: 'Privacy Policy',
        es: 'Política de Privacidad',
        fr: 'Politique de Confidentialité'
    },
    regionChangeAction: null,
    regions: [
        {
            id: 'us',
            compatCodes: {
                req: ['sc'],
                ven: ['tpv']
            },
            consentExpireIn: 3,
            consentGpcDefaults: {
                'vendor': false,
                'targeting': false
            },
            consentImpliedDefaults: {
                'required': true,
                'functional': true,
                'media': true,
                'performance': true,
                '1p-targeting': true
            },
            consentLinkTitle: {
                ar: 'لا تبيع أو تشارك معلوماتي الشخصية',
                de: 'Nicht Verkauf oder Nicht Weitergabe Ihrer personenbezogenen Daten zu stellen',
                en: 'Do Not Sell Or Share My Personal Information',
                es: 'No Venda Vi Comparta Mi Información Personal',
                fr: 'Ne pas vendre ni partager mes informations personnelles'
            },
            rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/usstatesupplement/en-us/',
            geoMatch: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
            gppSection: 'usnat',  /* usnat */
            iabRegion: 'ccpa'
        },
        {
            id: 'gdpr',
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                'functional': false,
                'media': false,
                'performance': false,
                'targeting': false
            },
            consentImpliedDefaults: {
                'vendor': true
            },
            consentLinkTitle: {
                ar: 'إدارة ملفات تعريف الارتباط+',
                de: 'Cookies Verwalten+',
                en: 'Manage Cookies+',
                es: 'Administrar cookies+',
                fr: 'Gérer les Cookies+'
            },
            geoMatch: ['GB', 'DE', 'FR', 'IT', 'ES', 'PL', 'RO', 'NL', 'BE', 'GR', 'CZ', 'PT', 'SE', 'HU', 'AT', 'BG', 'DK', 'FI', 'SK', 'IE', 'HR', 'LT', 'SI', 'LV', 'EE', 'CY', 'LU', 'MT', 'NO', 'IS', 'LI', /* GDPR */
                'CH']  /* Other Europe */
        },
        {
            id: 'other-optin',
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                'functional': false,
                'media': false,
                'performance': false,
                'targeting': false
            },
            consentImpliedDefaults: {
                'vendor': true,
                '1p-targeting': true
            },
            geoMatch: ['CO', 'UY', 'PE', 'AR', 'CR', 'CL']
        },
        {
            id: 'other-optout',
            consentImpliedDefaults: {
                'vendor': true,
                '1p-targeting': true
            },
            geoMatch: ['MX', 'PY', 'BR', 'VE', 'NI']
        },
        {
            id: 'global',
            geoMatch: ['*'],  /* Matches everything not yet matched */
            useFixedConsent: true
        }
    ],
    reloadOnConsentChange: true,
    reloadOnConsentReduction: false,
    rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    rightsRequestLinkTitle: {
        ar: 'بوابة طلبات الحقوق الفردية',
        de: 'Anfrageportal für Individualrechte',
        en: 'Individual Rights Request Portal',
        es: 'Portal de solicitud de derechos individuales',
        fr: 'Portail de demande de droits des individus'
    },
    scCookie: 'stateCode',
    setPageClass: false,
    src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
    strictIabCompliance: false,  /* False unless using all the IAB categories */
    ucFlavor: 'basic',
    useFixedConsent: false,
    useGPP: true,
    useIAB: true,
    useIabString: true,
    uspApiCookieName: 'usprivacy',
    uspApiExplicitNotice: true,
    uspApiIsLspa: false
}

