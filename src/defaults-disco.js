
/* Default UserConsent configuration for legacy Discovery configs. */
/* See https://github.com/turnercode/user-consent#readme for details. */
{
    addtlConsentCookie: 'OTAdditionalConsentString',
    adChoicesLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    adChoicesLinkTitle: {
      ar: 'اختيارات الإعلان',
      de: 'Anzeigenauswahl',
      en: 'Ad Choices',
      es: 'Elecciones de anuncios',
      fr: 'Choix d’annonces'
    },
    affiliatesLinkAction: 'https://www.wbdprivacy.com/policycenter/affiliates/',
    affiliatesLinkTitle: {
        ar: 'الشركات التابعة',
        en: 'Affiliates',
        de: 'Mitgliedsorganisationen',
        es: 'Afiliadas',
        fr: 'Affiliées'
    },
    categories: {  /* Normalized to lower-case */
        c0001: 'required',            /* Built-in */
        c0002: 'analytics',
        c0003: 'personalization',
        c0004: 'ads',
        c0005: 'social'
    },
    ccCookie: 'countryCode',
    ccpaGeos: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
    compatCategories: {
        'vendor': ['ads'],
        'targeting': ['ads']
    },
    confirmCookie: 'OptanonAlertBoxClosed',
    consentChangeAction: null,
    consentChangeActionDelay: 500,
    consentCookie: 'OptanonConsent',
    consentDefaults: {
        required: true,
        analytics: true,
        personalization: true,
        ads: true,
        social: true
    },
    consentExpireIn: 1,
    consentLinkTitle: {
        ar: 'ملفات تعريف الارتباط',
        de: 'Cookie-Einstellungen',
        en: 'Cookie Settings',
        es: 'Configuración de Cookies',
        fr: 'Paramètres des Cookies'
    },
    consentNotApplicable: [],
    controlCookie: 'OptanonControl',
    cookieSameSite: 'Lax',
    cookieSecure: false,
    enableDebug: false,
    enableGPC: true,
    enableTransitionCheck: true,
    enableWebViewCheck: true,
    defaultCountry: 'US',
    defaultLanguage: 'en',
    defaultState: '',
    gdprIabCookie: 'eupubconsent-v2',
    geoCheckFunction: null,
    geoPassedToOneTrust: true,
    gpcFixCookie: '',
    gppCategories: {
        usnat: [
            {
                field: 'SharingNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SaleOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'TargetedAdvertisingOptOutNotice',
                type: 'trinary',
                default: 1
            },
            {
                field: 'SharingOptOut',
                type: 'trinary',
                val: 'ads'
            },
            {
                field: 'SaleOptOut',
                type: 'trinary',
                val: 'ads'
            },
            {
                field: 'TargetedAdvertisingOptOut',
                type: 'trinary',
                val: 'ads'
            }
        ],
        uspv1: [
            {
                field: 'OptOutSale',
                type: 'binary',
                val: 'ads'
            }
        ]
    },
    gppIabCookie: 'OTGPPConsent',
    gppSection: '',
    iabRegion: '',
    languageFromBrowser: true,
    oneTrustLoadTimeout: 10000,
    privacyCenterLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    privacyCenterLinkTitle: {
      ar: 'سياسة خصوصية المستهلك',
      de: 'Datenschutzhinweise',
      en: 'Privacy Policy',
      es: 'Política de Privacidad',
      fr: 'Politique de Confidentialité'
    },
    regionChangeAction: null,
    regions: [
        {
            id: 'us',
            consentExpireIn: 3,
            consentGpcDefaults: {
                'ads': false
            },
            consentImpliedDefaults: {
                'required': true,
                'analytics': true,
                'personalization': true,
                'social': true
            },
            consentLinkTitle: {
                ar: 'لا تبيع أو تشارك معلوماتي الشخصية',
                de: 'Nicht Verkauf oder Nicht Weitergabe Ihrer personenbezogenen Daten zu stellen',
                en: 'Do Not Sell Or Share My Personal Information',
                es: 'No Venda Vi Comparta Mi Información Personal',
                fr: 'Ne pas vendre ni partager mes informations personnelles'
            },
            rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/usstatesupplement/en-us/',
            geoMatch: ['US:CA', 'US:CO', 'US:CT', 'US:DE', 'US:IA', 'US:MT', 'US:NE', 'US:NH', 'US:NJ', 'US:OR', 'US:TX', 'US:UT', 'US:VA'],
            gppSection: 'usnat',  /* usnat */
            iabRegion: 'ccpa'
        },
        {
            id: 'gdpr',
            consentDefaults: {  /* Setting this here will override the top-level defaults when in this region */
                'analytics': false,
                'personalization': false,
                'ads': false,
                'social': false
            },
            consentImpliedDefaults: {
                'required': true
            },
            consentLinkTitle: {
                ar: 'إدارة ملفات تعريف الارتباط+',
                de: 'Cookies Verwalten+',
                en: 'Manage Cookies+',
                es: 'Administrar cookies+',
                fr: 'Gérer les Cookies+'
            },
            geoMatch: ['GB', 'DE', 'FR', 'IT', 'ES', 'PL', 'RO', 'NL', 'BE', 'GR', 'CZ', 'PT', 'SE', 'HU', 'AT', 'BG', 'DK', 'FI', 'SK', 'IE', 'HR', 'LT', 'SI', 'LV', 'EE', 'CY', 'LU', 'MT', 'NO', 'IS', 'LI', /* GDPR */
                'CH'], /* Other Europe */
            iabRegion: 'gdpr',
            useIabString: false
        },
        {
            id: 'global',
            geoMatch: ['*'],  /* Matches everything not yet matched */
            useFixedConsent: true
        }
    ],
    reloadOnConsentChange: true,
    reloadOnConsentReduction: false,
    rightsRequestLinkAction: 'https://www.wbdprivacy.com/policycenter/b2c/',
    rightsRequestLinkTitle: {
        ar: 'بوابة طلبات الحقوق الفردية',
        de: 'Anfrageportal für Individualrechte',
        en: 'Individual Rights Request Portal',
        es: 'Portal de solicitud de derechos individuales',
        fr: 'Portail de demande de droits des individus'
    },
    scCookie: 'stateCode',
    setPageClass: false,
    src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
    strictIabCompliance: false,  /* False unless using all the IAB categories */
    tcfOpts: {
        categories: {
            purposes: [
                'ads',                  /* GDPR Purpose 1 */
                'ads',                  /* GDPR Purpose 2 */
                'ads',                  /* GDPR Purpose 3 */
                'ads',                  /* GDPR Purpose 4 */
                'personalization',      /* GDPR Purpose 5 */
                'personalization',      /* GDPR Purpose 6 */
                'ads',                  /* GDPR Purpose 7 */
                'personalization',      /* GDPR Purpose 8 */
                'ads',                  /* GDPR Purpose 9 */
                'ads',                  /* GDPR Purpose 10 */
                'unused'                /* GDPR Purpose 11 - Not used */
            ],
            specialPurposes: [
                'required',             /* GDPR Special Purpose 1 */
                'required',             /* GDPR Special Purpose 2 */
                'required'              /* GDPR Special Purpose 3 */
            ],
            features: [
                'ads',                  /* GDPR Feature 1 */
                'ads',                  /* GDPR Feature 2 */
                'ads'                   /* GDPR Feature 3 */
            ],
            specialFeatures: [
                'unused',               /* GDPR Special Feature 1 - Not used */
                'unused'                /* GDPR Special Feature 2 - Not used */
            ]
        },
        policies: {
            2: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            3: {
                iabMaxPurposes: 10,
                iabMaxSpecialFeats: 2
            },
            4: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            },
            5: {
                iabMaxPurposes: 11,
                iabMaxSpecialFeats: 2
            }
        }
    },
    ucFlavor: 'disco',
    useFixedConsent: false,
    useGPP: true,
    useIAB: true,
    useIabString: true,
    uspApiCookieName: 'usprivacy',
    uspApiExplicitNotice: true,
    uspApiIsLspa: false
}

