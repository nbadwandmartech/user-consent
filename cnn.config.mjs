/*
 * Example config for self-invoking brand-specific script production
 * for CNN (using the IAB config).
 *
 * Invoke with: "yarn run build cnn.config.mjs"
 *
 * This should build two self-invoking user-consent scripts that are
 * fully-configured for CNN and ready to be included on those sites.
 */

import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import terser from '@rollup/plugin-terser';
import pkg from './package.json' assert { type: 'json' };
import fs from 'fs';

const brand = 'cnn';

const configSet = 'window.WBD = window.WBD || {};\nwindow.WBD.UserConsentConfig = ';

const prodConfig = {
  "cookieDomain": ".cnn.com",
  "cookieSameSite": "None",
  "cookieSecure": true,
  "domId": "3d9a6f21-8e47-43f8-8d58-d86150f3e92b",
  "gpcFixCookie": "SecGpc"
};

const testConfig = {
  "cookieDomain": ".cnn.com",
  "cookieSameSite": "None",
  "cookieSecure": true,
  "domId": "0c1a116f-9331-4d13-8d20-7f05b792c5ad",
  "gpcFixCookie": "SecGpc"
};

/****  Should not need to edit anything below this line  ****/

export default [
    {
        context: 'window',
        input: 'src/cnn_wrapper.js',
        external: [
            'window',
            'document'
        ],
        output: {
            externalLiveBindings: false,
            file: './' + brand + '_stage_user_consent.js',
            format: 'iife',
            generatedCode: 'es2015',
            globals: {
                'window': 'window',
                'document': 'document'
            }
        },
        plugins: [
            replace({
                __ucBrandConfig__: configSet + JSON.stringify(testConfig) + ';',
                __ucCode__: fs.readFileSync('./dist/userconsent-iab.js'),
                delimiters: ['', ''],
                preventAssignment: true
            }),
            commonjs()
        ]
    },
    {
        context: 'window',
        input: 'src/cnn_wrapper.js',
        external: [
            'window',
            'document'
        ],
        output: {
            externalLiveBindings: false,
            file: './' + brand + '_stage_user_consent.min.js',
            format: 'iife',
            generatedCode: 'es2015',
            globals: {
                'window': 'window',
                'document': 'document'
            },
            plugins: [terser({ecma: 2015})]
        },
        plugins: [
            replace({
                __ucBrandConfig__: configSet + JSON.stringify(testConfig) + ';',
                __ucCode__: fs.readFileSync('./dist/userconsent-iab.min.js'),
                delimiters: ['', ''],
                preventAssignment: true
            }),
            commonjs()
        ]
    },
    {
        context: 'window',
        input: 'src/cnn_wrapper.js',
        external: [
            'window',
            'document'
        ],
        output: {
            externalLiveBindings: false,
            file: './' + brand + '_user_consent.min.js',
            format: 'iife',
            generatedCode: 'es2015',
            globals: {
                'window': 'window',
                'document': 'document'
            },
            plugins: [terser({ecma: 2015})]
        },
        plugins: [
            replace({
                __ucBrandConfig__: configSet + JSON.stringify(prodConfig) + ';',
                __ucCode__: fs.readFileSync('./dist/userconsent-iab.min.js'),
                delimiters: ['', ''],
                preventAssignment: true
            }),
            commonjs()
        ]
    },
    {
        context: 'window',
        input: 'src/cnn_wrapper.js',
        external: [
            'window',
            'document'
        ],
        output: {
            externalLiveBindings: false,
            file: './' + brand + '_embed_user_consent.min.js',
            format: 'iife',
            generatedCode: 'es2015',
            globals: {
                'window': 'window',
                'document': 'document'
            },
            plugins: [terser({ecma: 2015})]
        },
        plugins: [
            replace({
                __ucBrandConfig__: '',
                __ucCode__: fs.readFileSync('./dist/userconsent-iab.min.js'),
                delimiters: ['', ''],
                preventAssignment: true
            }),
            commonjs()
        ]
    }
];

