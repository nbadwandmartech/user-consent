/*
 * Example config for self-invoking brand-specific script production
 * for brands using the basic config.
 *
 * Use by doing the following:
 * 1. Make sure you are using Node v18 or later, have "yarn" installed,
 *    and have run "yarn install" to install this packages dependencies.
 * 2. Invoke: "yarn run build" to build the base libraries.
 * 3. Copy this file to "<brand>.config.mjs" (e.g. "hbo.config.mjs") and
 *    edit this new file.
 * 4. Change the value set to "const brand" below to your brand name
 *    with no spaces (dashes are fine).  E.g.:  "const brand = 'hbo';"
 * 5. Change the value set to "prodConfig" below to match your brand's
 *    production UserConsent config.
 * 6. Change the value set to "testConfig" below to match your brand's
 *    test/stage UserConsent config.  Note that this is often the same as
 *    your production config, but with "-test" appended to your "domId".
 * 7. Invoke: "yarn run build <brand>.config.mjs" to build
 *    the script(s).
 *    E.g.: "yarn run build hbo.config.mjs"
 *
 * This should build two self-invoking user-consent scripts that are
 * fully-configured for your brand and ready to be included on your site.
 */

import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import pkg from './package.json' assert { type: 'json' };
import fs from 'fs';

const brand = 'brand';

const prodConfig = {
    "cookieDomain": ".brand.com",
    "domId": "1x1x1x1x-1x1x-1x1x-1x1x-1x1x1x1x1x1x",
    "enableTransitionCheck": false
};

const testConfig = {
    "cookieDomain": ".brand.com",
    "domId": "1x1x1x1x-1x1x-1x1x-1x1x-1x1x1x1x1x1x-test",
    "enableDebug": true,
    "enableTransitionCheck": false
};

/****  Should not need to edit anything below this line  ****/

export default [
    {
        context: 'window',
        input: 'src/wrapper.js',
        output: {
            externalLiveBindings: false,
            file: './' + brand + '-test-userconsent.js',
            format: 'iife',
            generatedCode: 'es2015',
            sourcemap: true
        },
        plugins: [
            replace({
                __ucBrandConfig__: JSON.stringify(testConfig),
                __ucCode__: fs.readFileSync('./dist/userconsent-basic.js'),
                preventAssignment: true
            }),
            commonjs()
        ]
    },
    {
        context: 'window',
        input: 'src/wrapper.js',
        output: {
            externalLiveBindings: false,
            file: './' + brand + '-userconsent.min.js',
            format: 'iife',
            generatedCode: 'es2015',
            sourcemap: true
        },
        plugins: [
            replace({
                __ucBrandConfig__: JSON.stringify(prodConfig),
                __ucCode__: fs.readFileSync('./dist/userconsent-basic.min.js'),
                preventAssignment: true
            }),
            commonjs()
        ]
    }
];

